﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TutorialAnimation : MonoBehaviour
{
    [SerializeField]
    private Image _image;
    private Tween _blink, _move;

    public bool blink;

    void Awake()
    {
        if (_image == null) { _image = GetComponent<Image>(); }
        _blink = _image.DOFade(0f, 0.6f).SetAutoKill(false).SetLoops(-1, LoopType.Yoyo).Pause();
    }

    void Start()
    {
        if (blink) { _blink.Play(); }
    }

    public void DragAnimation(int cardIdx, int playerNumber)
    {
        transform.position = GameControlUnit.Instance.Hand.GetCard(cardIdx).transform.position;
        _move = transform.DOMove(CardPositionManager.Instance.GetPlayerPosition(playerNumber), 1.5f).SetLoops(-1, LoopType.Restart);
    }

    public void StopDragAnimation()
    {
        if (_move != null) { _move.Kill(); }
    }
}