﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public struct ScriptCommand
{
    public string Name;
    public string Parameters;
}

public class TutorialManager : Singleton<TutorialManager>
{
    private int _lastIdx;
    private Dictionary<int, ScriptCommand> _dict = new Dictionary<int, ScriptCommand>();
    private const string _FILE = @"Data/Tutorial";
    private const int DECODE_PARSE_COUNT = 2;

    void Awake()
    {
        LoadFromFile();
    }

    void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        char[] charsToTrim = { ' ', '\t' };

        int i = 0;

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);
            if (ln[0] == ';') { continue; }
            else
            {
                if (ln != string.Empty)
                {
                    string[] parsings = ln.Split(',');
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        ScriptCommand tempCommand;
                        tempCommand.Name = parsings[0];
                        tempCommand.Parameters = parsings[1];
                        _dict[i] = tempCommand;
                        i++;
                    }
                }
            }
        }
    }

    public IEnumerator LoadSequence()
    {
        while (true)
        {
            yield return _dict[_lastIdx];
            _lastIdx++;
            if (_dict[_lastIdx].Name == "Pause") { _lastIdx++; yield break; }
        }
    }

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                transform.position += new Vector3(0f, 0.37f, 0f);
                break;
            case RatioType.aPhone:
                transform.position += new Vector3(0f, 0.68f, 0f);
                break;
            case RatioType.aPad:
                transform.position += new Vector3(0f, 1.17f, 0f);
                break;
        }
    }
}