﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TutorialDropArea : DropArea
{
    #region IDropHandler implementation

    public override void OnDrop(PointerEventData eventData)
    {
        //int cardIdx = (int)eventData.pointerDrag.GetComponent<CardUnit>().cardPosition;
        //GameControlUnit.Instance.Hand.LastCardIndex = cardIdx;
        //PacketManager.Instance.SendCSReqUseCard(cardIdx + 1, 0);
        TutorialUnit.Instance.NextSequence();
        //eventData.pointerDrag.GetComponent<DragItem>().SuccessDrop = true;
        SoundControlUnit.Instance.PlaySceneClip(PlayClip.CardDrop);
    }

    #endregion

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                transform.position += new Vector3(0f, 0.3f, 0f);
                break;
            case RatioType.aPhone:
                transform.position += new Vector3(0f, 1f, 0f);
                break;
            case RatioType.aPad:
                transform.position += new Vector3(0f, 1f, 0f);
                break;
        }
    }
}