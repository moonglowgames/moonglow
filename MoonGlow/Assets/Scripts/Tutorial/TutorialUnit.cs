﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class TutorialUnit : Singleton<TutorialUnit>
{
    [SerializeField]
    private CanvasGroup _petTeacherCanvas;
    [SerializeField]
    private RectTransform _canvasParent, _petTeacher, _ballon, _buttonsPanel, _inputMask;
    [SerializeField]
    private Animator _petTeacherAnimator;
    [SerializeField]
    private Image _Bg, _ballonImage, _boardImage, _arrow;
    [SerializeField]
    private Text _ballonText, _boardText, _notiText;
    [SerializeField]
    private TutorialAnimation _handIcon;
    [SerializeField]
    private ParticleSystem[] _particleFrames;

    private IEnumerator _sequence;

    void Awake()
    {
        RoomManagerUnit.Instance.Map = new TargetMap(PlayMode.Single2);
        RoomManagerUnit.Instance.Map.Shift = 0;
        GameControlUnit.Instance.RoomInfo.gameType = GameType.Colmaret;
        GameControlUnit.Instance.RoomInfo.goalHP = 50;
        GameControlUnit.Instance.RoomInfo.goalRP = 150;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in _buttonsPanel.GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }

        _sequence = TutorialManager.Instance.LoadSequence();
        while (_sequence.MoveNext())
        {
            ScriptCommand command = (ScriptCommand)_sequence.Current;
            SendMessage(command.Name, command.Parameters);
        }
    }

    void InitTutorial(string arg)
    {
        if (GameControlUnit.Instance.PlayerInfo != null)
        {
            GameControlUnit.Instance.PlayerList.GetPlayer(1).playerName = GameControlUnit.Instance.PlayerInfo.userName;
            GameControlUnit.Instance.PlayerList.GetPlayer(1).level = GameControlUnit.Instance.PlayerInfo.userLevel;
            GameControlUnit.Instance.PlayerList.GetPlayer(1).avatarIdx = GameControlUnit.Instance.PlayerInfo.avatarIdx;
            GameControlUnit.Instance.PlayerList.GetPlayer(1).badgeIdx = GameControlUnit.Instance.PlayerInfo.badgeIdx;
            GameControlUnit.Instance.PlayerList.GetPlayer(1).petList = new List<int>();
            GameControlUnit.Instance.PlayerList.GetPlayer(1).itemSetup = true;
        }
        GameControlUnit.Instance.PlayerList.GetPlayer(2).playerName = "꾸꾸";
        GameControlUnit.Instance.PlayerList.GetPlayer(2).level = 30;
        GameControlUnit.Instance.Hand.Slide(true);
    }

    void Clear(string arg)
    {
        _Bg.gameObject.SetActive(false);
        _petTeacherCanvas.alpha = 0f;
        _ballon.gameObject.SetActive(false);
        _boardImage.gameObject.SetActive(false);
        _arrow.gameObject.SetActive(false);
        _notiText.gameObject.SetActive(false);
        _handIcon.gameObject.SetActive(false);
        _buttonsPanel.gameObject.SetActive(false);
    }

    void ToggleBackground(string arg)
    {
        _Bg.gameObject.SetActive(Convert.ToBoolean(arg));
    }

    void EnableInputListener(string arg)
    {
        _inputMask.gameObject.SetActive(true);
    }

    void TogglePet(string arg)
    {
        string[] args = arg.Split('*');
        if (Convert.ToBoolean(args[0]))
        {
            _petTeacherCanvas.alpha = 1f;
            _petTeacher.anchoredPosition = new Vector2(Convert.ToInt16(args[1]), Convert.ToInt16(args[2]));
            _petTeacher.localScale = new Vector3(Convert.ToInt16(args[3]), 1f, 1f);
        }
        else { _petTeacherCanvas.alpha = 0f; }
    }

    void AnimatePet(string arg)
    {
        string[] args = arg.Split('*');
        _petTeacherAnimator.SetTrigger(args[0]);
    }

    void UpdateHand(string arg)
    {
        string[] args = arg.Split('*');

        for (int i = 0; i < 6; i++)
        {
            GameControlUnit.Instance.Hand.GetCard(i).cardIdx = Convert.ToInt16(args[i]);
        }
    }

    void UpdatePlayer(string arg)
    {
        string[] args = arg.Split('*');
        PlayerUnit playerUnit = GameControlUnit.Instance.PlayerList.GetPlayer(Convert.ToInt16(args[0]));
        playerUnit.hitPoints = Convert.ToInt16(args[1]);
        playerUnit.shieldPoints = Convert.ToInt16(args[2]);
        playerUnit.moonStonePower = Convert.ToInt16(args[3]);
        playerUnit.moonDust = Convert.ToInt16(args[4]);
        playerUnit.zenStonePower = Convert.ToInt16(args[5]);
        playerUnit.mana = Convert.ToInt16(args[6]);
        playerUnit.moonCrystalPower = Convert.ToInt16(args[7]);
        playerUnit.rodEnergy = Convert.ToInt16(args[8]);
        playerUnit.needUpdate = true;
    }

    void ToggleParticleFrame(string arg)
    {
        string[] args = arg.Split('*');
        int i = Convert.ToInt16(args[0]);
        if (i > 3) { i += EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0); }
        if (Convert.ToBoolean(args[1]))
        {
            _particleFrames[i].Play();
        }
        else { _particleFrames[i].Stop(); }
    }

    void ToggleDiscardState(string arg)
    {
        if (Convert.ToBoolean(arg)) { GameControlUnit.Instance.Hand.SetDiscardState(); }
        else { GameControlUnit.Instance.Hand.SetNormalState(); }
    }

    void SetNormalCard(string arg)
    {
        GameControlUnit.Instance.Hand.GetCard(Convert.ToInt16(arg) - 1).NormalState();
    }

    void ToggleBallon(string arg)
    {
        string[] args = arg.Split('*');
        if (Convert.ToBoolean(args[0]))
        {
            _ballon.gameObject.SetActive(true);
            _ballon.anchoredPosition = new Vector2(Convert.ToInt16(args[1]), Convert.ToInt16(args[2]));
            _ballonImage.rectTransform.localScale = new Vector3(Convert.ToInt16(args[3]), 1f, 1f);
            string msg = TutorialLocalizationManager.Instance.GetLine(Convert.ToInt16(args[4]));
            msg = msg.Replace('^', ',');
            _ballonText.text = msg.Replace("@", System.Environment.NewLine);
        }
        else { _ballon.gameObject.SetActive(false); }
    }

    void ToggleBoard(string arg)
    {
        string[] args = arg.Split('*');
        if (Convert.ToBoolean(args[0]))
        {
            _boardImage.gameObject.SetActive(true);
            _boardImage.rectTransform.anchoredPosition = new Vector2(0f, Convert.ToInt16(args[2]));
            //_boardImage.rectTransform.localScale = new Vector3(Convert.ToInt16(args[3]), 1f, 1f);
            if (args[3] == "1")
            {
                _boardText.rectTransform.anchoredPosition = new Vector2(210f, 0f);
            }
            else
            {
                _boardText.rectTransform.anchoredPosition = new Vector2(-210f, 0f);
            }
            string msg = TutorialLocalizationManager.Instance.GetLine(Convert.ToInt16(args[4]));
            msg = msg.Replace('^', ',');
            _boardText.text = msg.Replace("@", System.Environment.NewLine);
        }
        else { _boardImage.gameObject.SetActive(false); }
    }

    void ToggleArrow(string arg)
    {
        string[] args = arg.Split('*');
        if (Convert.ToBoolean(args[0]))
        {
            _arrow.gameObject.SetActive(true);
            _arrow.rectTransform.anchoredPosition = new Vector2(Convert.ToInt16(args[1]), Convert.ToInt16(args[2]));
            _arrow.rectTransform.localRotation = Quaternion.AngleAxis(Convert.ToSingle(args[3]), Vector3.forward);
        }
        else { _arrow.gameObject.SetActive(false); }
    }

    void ToggleNotice(string arg)
    {
        string[] args = arg.Split('*');
        if (Convert.ToBoolean(args[0]))
        {
            _notiText.gameObject.SetActive(true);
            _notiText.rectTransform.anchoredPosition = new Vector2(Convert.ToInt16(args[1]), Convert.ToInt16(args[2]));
            _notiText.text = TutorialLocalizationManager.Instance.GetLine(16);
        }
        else { _notiText.gameObject.SetActive(false); }
    }

    void DragAnimation(string arg)
    {
        string[] args = arg.Split('*');
        if (Convert.ToBoolean(args[0]))
        {
            _handIcon.gameObject.SetActive(true);
            _handIcon.DragAnimation(Convert.ToInt16(args[1]), Convert.ToInt16(args[2]));
            GameControlUnit.Instance.Hand.ToggleDragOption(false);
            GameControlUnit.Instance.Hand.GetCard(Convert.ToInt16(args[1])).GetComponent<DragItem>().enabled = true;
            RoomManagerUnit.Instance.userPlayer.enabled = false;
            RoomManagerUnit.Instance.enemies[0].enabled = false;
            RoomManagerUnit.Instance.deck.enabled = false;
            switch (Convert.ToInt16(args[2]))
            {
                case 0:
                    RoomManagerUnit.Instance.userPlayer.enabled = true;
                    break;
                case 1:
                    RoomManagerUnit.Instance.enemies[0].enabled = true;
                    break;
                case 6:
                    RoomManagerUnit.Instance.deck.enabled = true;
                    break;
            }
        }
        else
        {
            _handIcon.StopDragAnimation();
            _handIcon.gameObject.SetActive(false);
        }
    }

    void AddPrefab(string arg)
    {
        string[] args = arg.Split('*');
        var prefab = (GameObject)Instantiate(Resources.Load(args[0]));
        var prefab_transform = prefab.GetComponent<RectTransform>();
        prefab_transform.SetParent(_canvasParent, false);
        //prefab_transform.localScale = Vector3.one;
        prefab_transform.anchoredPosition = new Vector2(Convert.ToInt16(args[1]), Convert.ToInt16(args[2]));
    }

    void AddEffect(string arg)
    {
        string[] args = arg.Split('*');
        EffectManager.Instance.InstantiateEffect(Convert.ToInt16(args[0]), Convert.ToInt16(args[1]));
    }

    void ToggleButton(string arg)
    {
        string[] args = arg.Split('*');
        if (Convert.ToBoolean(args[0]))
        {
            _buttonsPanel.gameObject.SetActive(true);
            _buttonsPanel.anchoredPosition = new Vector2(Convert.ToInt16(args[1]), Convert.ToInt16(args[2]));
        }
        else { _buttonsPanel.gameObject.SetActive(false); }
    }

    public void NextSequence()
    {
        _inputMask.gameObject.SetActive(false);
        _sequence = TutorialManager.Instance.LoadSequence();
        while (_sequence.MoveNext())
        {
            ScriptCommand command = (ScriptCommand)_sequence.Current;
            SendMessage(command.Name, command.Parameters);
        }
    }

    public void SkipTutorial()
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.tutorial, 1);
        SceneManager.LoadScene(1);
        PacketManager.Instance.SendCSNotiSceneEnter(SceneType.Home);
    }

    public void FinishTutorial()
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.tutorial, 1);
        PacketManager.Instance.SendCSReqReadTutorial();
        SceneManager.LoadScene(1);
        PacketManager.Instance.SendCSNotiSceneEnter(SceneType.Home);
    }
}