﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingManager : Singleton<LoadingManager>
{
    [SerializeField]
    private Text _loadingString, _progress;
    private int _currentProgress = 0;

    public string clientVersion;

    void Awake()
    {
        if (Application.genuineCheckAvailable)
        {
            if (!Application.genuine) { Application.Quit(); }
        }

        if (!EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.language))
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Korean:
                    EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.language, 1);
                    break;
                default:
                    EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.language, 0);
                    break;
            }
        }

        //if (!EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.intro))
        //{
        //    GameControlUnit.Instance.LastScene = 0;
        //    SceneManager.LoadScene(4);
        //}
    }

    void Start()
    {
        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.clientVersion, clientVersion);
        StateManagerUnit.Instance.SetState(ClientState.Unknown);
    }

    void LoadingProgress()
    {
        _currentProgress++;
        _progress.text = _currentProgress.ToString() + " %";
        if (_currentProgress == 100)
        {
            CancelInvoke();
            StateManagerUnit.Instance.SetState(ClientState.LoadingReadyToHome);
        }
    }

    public void AssetLoadingStart()
    {
        if (_loadingString != null)
        {
            _loadingString.text = LocalizationManager.Instance.Get("CONNECT");// + ConfigManager.Instance.Get(configKey.LoginServerIP);
            _loadingString.rectTransform.parent.gameObject.SetActive(true);
            InvokeRepeating("LoadingProgress", 0.25f, 0.005f);
        }
        else
        {
            StateManagerUnit.Instance.SetState(ClientState.LoadingReadyToHome);
        }
    }

    public void LoadHomeScene(string session)
    {
        SceneManager.LoadScene(1);
        PacketManager.Instance.SendCSNotiSceneEnter(SceneType.Home);
    }

    public void OpenStore(string url)
    {
        Application.OpenURL(url);
    }
}