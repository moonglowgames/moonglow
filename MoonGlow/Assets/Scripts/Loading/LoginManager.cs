﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class LoginManager : Singleton<LoginManager>
{
    [SerializeField]
    private CanvasGroup _canvas, _canvasPopUp;
    [SerializeField]
    private Text _popUp;
    [SerializeField]
    private RectTransform _baseSize;
    [SerializeField]
    private Toggle _mainToggle, _termsToggle;
    [SerializeField]
    private GameObject mainTab;
    [SerializeField]
    private LoginUnit logInTab;
    [SerializeField]
    private SignUpUnit guestLogInTab, signUpTab;
    [SerializeField]
    private TermsUnit termsTab;
    [SerializeField]
    private PasswordUnit pwdTab;

    private Sequence _sequence;

    public float padding = 15f;
    public float minwidth = 120f;

    void OnLevelWasLoaded(int level)
    {
        switch (level)
        {
            case 0:
                if (GameControlUnit.Instance.ForceLogin)
                {
                    PacketManager.Instance.DisConnect();
                    StateManagerUnit.Instance.SetState(ClientState.Loading);
                }
                break;
        }
    }

    void Start()
    {
        _sequence = DOTween.Sequence();
        _sequence.Append(_canvasPopUp.DOFade(1f, 0f));
        _sequence.AppendInterval(3f);
        _sequence.Append(_canvasPopUp.DOFade(0f, 1f));
        _sequence.SetAutoKill(false).Pause();

        if (EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.license, 0) == 0)
        {
            _termsToggle.isOn = true;
        }

        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    private IEnumerator BalloonText(string msg)
    {
        _popUp.text = msg;
        _sequence.Restart();
        yield return new WaitForEndOfFrame();
        float width = _popUp.rectTransform.rect.width + padding * 2;
        if (width < minwidth) { width = minwidth; }

        _baseSize.sizeDelta = new Vector2(width, _popUp.rectTransform.rect.height + padding * 2);
    }

    public void ShowBallon(string msgKey)
    {
        StartCoroutine(BalloonText(MsgBoxLocalizationManager.Instance.GetMessage(msgKey)));
    }

    public void OpenMainTab()
    {
        _mainToggle.isOn = true;
    }

    public void MainTab(bool isOn)
    {
        mainTab.SetActive(isOn);
    }

    public void LoginTab(bool isOn)
    {
        logInTab.gameObject.SetActive(isOn);
    }

    public void GuestLoginTab(bool isOn)
    {
        if (isOn)
        {
            if (EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.guestLogin, 0) == 1)
            {
                string deviceID = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.deviceID);

                PacketManager.Instance.SendCSReqLogin(AccountType.Guest, deviceID, string.Empty, deviceID);
            }
            else { guestLogInTab.gameObject.SetActive(true); }
        }
        else { guestLogInTab.gameObject.SetActive(false); }
    }

    public void SignTab(bool isOn)
    {
        signUpTab.gameObject.SetActive(isOn);
    }

    public void TermsTab(bool isOn)
    {
        termsTab.gameObject.SetActive(isOn);
        termsTab.Reset();
    }

    public void PasswordTab(bool isOn)
    {
        pwdTab.gameObject.SetActive(isOn);
    }

    public void ToggleWindow(bool state)
    {
        _canvas.alpha = state ? 1f : 0f;
        _canvas.blocksRaycasts = state;
    }
}