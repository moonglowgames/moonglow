﻿#pragma warning disable 649

using UnityEngine;
using System.Text;
using System.Text.RegularExpressions;


public class SignUpUnit : MonoBehaviour
{
    private string _account = string.Empty;
    private string _nick = string.Empty;
    private string _password = string.Empty;
    private string _password2 = string.Empty;
    private int _avatarIdx = 501;
    private bool _pwd2 = false;

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void IdEndType(string tmp)
    {
        _account = tmp;
        if (!_account.IsEmail()) { LoginManager.Instance.ShowBallon("NO_EMAIL"); }
    }

    public void NickEndType(string tmp)
    {
        _nick = tmp;
        if (!_nick.IsNickName()) { LoginManager.Instance.ShowBallon("NO_NICK"); }
    }

    public void PwdEndType(string tmp)
    {
        if (tmp.Length < 5) { LoginManager.Instance.ShowBallon("NO_PWD"); return; }

        _password = tmp.EncryptDES();
        if (_pwd2)
        {
            if (_password2 != _password) { LoginManager.Instance.ShowBallon("NO_PWD2"); }//
        }
    }

    public void PwdConfirmEndType(string tmp)
    {
        _pwd2 = true;
        if (tmp.Length < 5) { return; }
        _password2 = tmp.EncryptDES();
        if (_password2 != _password) { LoginManager.Instance.ShowBallon("NO_PWD2"); }
    }

    public void AvatarEndChoice(int iDx)
    {
        _avatarIdx = iDx;
    }

    public void SignUp()
    {
        if (!_account.IsEmail()) { LoginManager.Instance.ShowBallon("NO_EMAIL"); return; }
        if (_password == string.Empty) { LoginManager.Instance.ShowBallon("NO_PWD"); return; }//Enter Password
        if (_password.Length < 5) { LoginManager.Instance.ShowBallon("NO_PWD"); return; }
        if (_password2 != _password) { LoginManager.Instance.ShowBallon("NO_PWD2"); return; }

        if (!_nick.IsNickName())
        {
            LoginManager.Instance.ShowBallon("NO_NICK");
            //signUpTab.transform.FindChild("LogInPanel/NickName/Text").GetComponent<RectTransform>().DOShakeAnchorPos(1f, 15f);
            return;
        }

        if (Encoding.UTF8.GetByteCount(_nick) > 24)
        {
            LoginManager.Instance.ShowBallon("LONG_NICK");
            return;
        }
        //if (IsRegexMatch(6, _nick)) { return; }
        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.account, _account);
        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.password, _password);

        string deviceID;
        if (EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.deviceID))
        {
            deviceID = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.deviceID);
        }
        else
        {
            deviceID = "CLI" + Random.Range(1000, 100000000).ToString("D9");
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.deviceID, deviceID);
        }
        
        Debug.Log("DeviceID set......................" + deviceID);
        PacketManager.Instance.SendCSReqRegister(AccountType.Email, _account, _password, _nick, _avatarIdx, deviceID);
    }

    public void GuestLogIn()
    {
        if (!_nick.IsNickName())
        {
            LoginManager.Instance.ShowBallon("NO_NICK");
            return;
        }

        if (Encoding.UTF8.GetByteCount(_nick) > 24)
        {
            LoginManager.Instance.ShowBallon("LONG_NICK");
            return;
        }

        string deviceID;
        if (EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.deviceID))
        {
            deviceID = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.deviceID);
        }
        else
        {
            deviceID = "CLI" + Random.Range(1000, 100000000).ToString("D9");
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.deviceID, deviceID);
        }
        PacketManager.Instance.SendCSReqRegister(AccountType.Guest, deviceID, string.Empty, _nick, _avatarIdx, deviceID);
    }

    //1 : 숫자, 2 : 영문자, 3 : 한글, 4 : 숫자+영문자,  5 : 숫자+영문자+한자, 6 : 숫자+영문자+한자+한글
    public static bool IsRegexMatch(int type, string plainText)
    {
        Regex rx;
        switch (type)
        {
            case 1:
                rx = new Regex(@"^[0-9]*$", RegexOptions.None);
                break;
            case 2:
                rx = new Regex(@"^[a-zA-Z]*$", RegexOptions.None);
                break;
            case 3:
                rx = new Regex(@"^[가-힣]*$", RegexOptions.None);
                break;
            case 4:
                rx = new Regex(@"^[a-zA-Z0-9]*$", RegexOptions.None);
                break;
            case 5:
                rx = new Regex(@"^[a-zA-Z0-9一-龥]*$", RegexOptions.None);
                break;
            case 6:
                rx = new Regex(@"^[a-zA-Z0-9가-힣一-龥]*$", RegexOptions.None);
                break;
            default:
                return false;
        }
        return (string.IsNullOrEmpty(plainText)) ? false : rx.IsMatch(plainText);
    }
}