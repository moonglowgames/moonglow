﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class TermsUnit : MonoBehaviour
{
    [SerializeField]
    private Text _page1, _page2;
    [SerializeField]
    private Toggle _page1Toggle, _page2Toggle;
    [SerializeField]
    private ScrollRect _gameTerms, _gameAgreement;
    [SerializeField]
    private Button _closeButton;
    [SerializeField]
    private Button[] _navigationButtons;

    public TextAsset[] licenseEng, licenseKor;

    void Awake()
    {
        if (EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.license, 0) == 1)
        {
            _closeButton.gameObject.SetActive(true);
        }
    }

    void Start()
    {
        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                _page1.text = licenseEng[0].text;
                _page2.text = licenseEng[2].text;
                _navigationButtons[1].gameObject.SetActive(true);
                _navigationButtons[3].gameObject.SetActive(true);
                break;
            case Language.Korean:
                _page1.text = licenseKor[0].text;
                _page2.text = licenseKor[1].text;
                break;
        }

        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    void FinishReading()
    {
        if(_page1Toggle.isOn && _page2Toggle.isOn )
        {
            EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.license, 1);
            LoginManager.Instance.OpenMainTab();
        }
    }

    public void Reset()
    {
        _page1Toggle.isOn = false;
        _page2Toggle.isOn = false;
        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                SetTermsPage(0);
                SetAgreementPage(2);
                break;
            case Language.Korean:
                break;
        }
    }

    public void SetTermsPage(int pageIdx)
    {
        if (pageIdx == 1)
        {
            _navigationButtons[0].gameObject.SetActive(true);
            _navigationButtons[1].gameObject.SetActive(false);
        }
        else
        {
            _navigationButtons[0].gameObject.SetActive(false);
            _navigationButtons[1].gameObject.SetActive(true);
        }
        _page1.text = licenseEng[pageIdx].text;
        _gameTerms.verticalNormalizedPosition = 1f;
    }

    public void SetAgreementPage(int pageIdx)
    {
        if(pageIdx == 3)
        {
            _navigationButtons[2].gameObject.SetActive(true);
            _navigationButtons[3].gameObject.SetActive(false);
        }
        else
        {
            _navigationButtons[2].gameObject.SetActive(false);
            _navigationButtons[3].gameObject.SetActive(true);
            
        }
        
        _page2.text = licenseEng[pageIdx].text;
        _gameAgreement.verticalNormalizedPosition = 1f;
    }

    public void Agree(bool isOn)
    {
        if (isOn) { FinishReading(); }
    }

    public void CloseButton()
    {
        LoginManager.Instance.OpenMainTab();
    }
}