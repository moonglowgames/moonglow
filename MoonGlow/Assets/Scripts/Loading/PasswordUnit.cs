﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class PasswordUnit : MonoBehaviour
{
    [SerializeField]
    private Button _emailSendButton;
    private string _account = string.Empty;
    private string _tempPassword = string.Empty;
    private string _password = string.Empty;
    private string _password2 = string.Empty;
    private bool _pwd2 = false;

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void IdEndType(string tmp)
    {
        _account = tmp;
        if (!_account.IsEmail()) { LoginManager.Instance.ShowBallon("NO_EMAIL"); }
    }

    public void TempPwdEndType(string tmp)
    {
        _tempPassword = tmp.EncryptDES();
    }

    public void PwdEndType(string tmp)
    {
        if (tmp.Length < 5) { LoginManager.Instance.ShowBallon("NO_PWD"); return; }

        _password = tmp.EncryptDES();
        if (_pwd2)
        {
            if (_password2 != _password) { LoginManager.Instance.ShowBallon("NO_PWD2"); }//
        }
    }

    public void PwdConfirmEndType(string tmp)
    {
        _pwd2 = true;
        if (tmp.Length < 5) { return; }
        _password2 = tmp.EncryptDES();
        if (_password2 != _password) { LoginManager.Instance.ShowBallon("NO_PWD2"); }
    }

    void ResetDelay()
    {
        _emailSendButton.interactable = true;
    }

    public void SendEmail()
    {
        if (!_account.IsEmail()) { LoginManager.Instance.ShowBallon("NO_EMAIL"); return; }
        _emailSendButton.interactable = false;
        Invoke("ResetDelay", 5f);
        PacketManager.Instance.SendCSReqFindPassword(_account);
    }

    public void ResetPassword()
    {
        if (!_account.IsEmail()) { LoginManager.Instance.ShowBallon("NO_EMAIL"); return; }
        if (_password == string.Empty) { LoginManager.Instance.ShowBallon("NO_PWD"); return; }
        if (_password.Length < 5) { LoginManager.Instance.ShowBallon("NO_PWD"); return; }
        if (_password2 != _password) { LoginManager.Instance.ShowBallon("NO_PWD2"); return; }
        PacketManager.Instance.SendCSReqChangePassword(_account, _tempPassword, _password);
    }
}