﻿#pragma warning disable 649

using UnityEngine;

public class LoginUnit : MonoBehaviour
{
    private string _account = string.Empty;
    private string _password = string.Empty;
    private bool _autoLogin = false;

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void IdEndType(string tmp)
    {
        _account = tmp;
    }

    public void PwdEndType(string tmp)
    {
        _password = tmp.EncryptDES();
    }

    public void AutoLogin(bool isOn)
    {
        if (isOn) {  }
        else
        {
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.account, string.Empty);
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.password, string.Empty);
        }
        _autoLogin = isOn;
    }

    public void LogIn()
    {
        if (!_account.IsEmail()) { GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "NO_ID"); return; }
        if (_password == string.Empty) { return; }

        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.account, _account);
        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.password, _password);

        if (_autoLogin) { EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.autoLogin, 1); }
        else { EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.autoLogin, 0); }

        string deviceID;
        if (EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.deviceID))
        {
            deviceID = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.deviceID);
        }
        else
        {
            deviceID = "CLI" + Random.Range(1000, 100000000).ToString("D9");
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.deviceID, deviceID);
        }
        PacketManager.Instance.SendCSReqLogin(AccountType.Email, _account, _password, deviceID);
    }
}