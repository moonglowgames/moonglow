public class PlayerPrefsKey
{
    static public string license = "License";
    static public string clientVersion = "ClientVersion";
    static public string session = "session";
    static public string account = "Account";
    static public string password = "Password";
    static public string autoLogin = "AutoLogin";
    static public string guestLogin = "GuestLogin";
    static public string deviceID = "deviceID";
    static public string userID = "userID";
    static public string loginState = "loginState";
    static public string connectUrl = "connectURL";

    static public string connectIP = "connectIP";
    static public string connectPort = "connectPort";

    static public string newsAlarm = "NewsAlarm";
    static public string shopAlarm = "ShopAlarm";

    static public string intro = "Intro";
    static public string sound = "Sound";
    static public string music = "Music";
    static public string pushNoti = "PushNotification";
    static public string vibration = "Vibration";
    static public string language = "Language";
    static public string country = "Country";
    static public string tutorial = "Tutorial";

    static public string iapInfoBuy = "IapInfoBuy";
    static public string iapInfoResult = "IapInfoResult";

    static public string gameType = "GameType";
    static public string playTime = "PlayTime";
    static public string stake = "Stake";
}