﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.IO;

public static class Extention
{
    //public static string Encription(this string normalstr)
    //{
    //    return normalstr;
    //}
    public static string Encription(this string normalstr)
    {
        string retstr = string.Empty;

        // change to byte ....
        byte[] arr = Encoding.UTF8.GetBytes(normalstr);

        // encryption 1th
        //....
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] ^= (byte)(0x8a + (byte)(i));
        }

        retstr = Convert.ToBase64String(arr);

        return retstr;
    }
    //public static string Decription(this string encodedstr)
    //{
    //    return encodedstr;
    //}
    public static string Decription(this string encodedstr)
    {
        string retstr = string.Empty;

        // change to byte...
        byte[] arr = Convert.FromBase64String(encodedstr);

        // decryption 1th
        // ...
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] ^= (byte)(0x8a + (byte)(i));
        }

        retstr = Encoding.UTF8.GetString(arr);
        return retstr;
    }

    //Test
    /*
    public static TestEncription( this string normalstr )
    {
        string normal = "{acdefg:12345}";

        Debug.Log( normal );

        string encryption = normal.Encription();

        Debug.Log( encryption );

        string decryption = encryption.Decription();

        Debug.Log( decryption );
    }
    */
    static byte[] Skey = ASCIIEncoding.ASCII.GetBytes("XTYZ-!%8");

    public static string EncryptDES(this string p_data)
    {
        // 암호화 알고리즘중 RC2 암호화를 하려면 RC를
        // DES알고리즘을 사용하려면 DESCryptoServiceProvider 객체를 선언한다.
        //RC2 rc2 = new RC2CryptoServiceProvider();
        DESCryptoServiceProvider rc2 = new DESCryptoServiceProvider();

        // 대칭키 배치
        rc2.Key = Skey;
        rc2.IV = Skey;

        // 암호화는 스트림(바이트 배열)을
        // 대칭키에 의존하여 암호화 하기때문에 먼저 메모리 스트림을 생성한다.
        MemoryStream ms = new MemoryStream();

        //만들어진 메모리 스트림을 이용해서 암호화 스트림 생성 
        CryptoStream cryStream = new CryptoStream(ms, rc2.CreateEncryptor(), CryptoStreamMode.Write);

        // 데이터를 바이트 배열로 변경
        byte[] data = Encoding.UTF8.GetBytes(p_data.ToCharArray());

        // 암호화 스트림에 데이터 씀
        cryStream.Write(data, 0, data.Length);
        cryStream.FlushFinalBlock();

        // 암호화 완료 (string으로 컨버팅해서 반환)
        return Convert.ToBase64String(ms.ToArray());
    }

    public static string DecryptDES(this string p_data)
    {

        // 암호화 알고리즘중 RC2 암호화를 하려면 RC를
        // DES알고리즘을 사용하려면 DESCryptoServiceProvider 객체를 선언한다.
        //RC2 rc2 = new RC2CryptoServiceProvider();
        DESCryptoServiceProvider rc2 = new DESCryptoServiceProvider();

        // 대칭키 배치
        rc2.Key = Skey;
        rc2.IV = Skey;

        // 암호화는 스트림(바이트 배열)을
        // 대칭키에 의존하여 암호화 하기때문에 먼저 메모리 스트림을 생성한다.
        MemoryStream ms = new MemoryStream();

        //만들어진 메모리 스트림을 이용해서 암호화 스트림 생성 
        CryptoStream cryStream = new CryptoStream(ms, rc2.CreateDecryptor(), CryptoStreamMode.Write);

        //데이터를 바이트배열로 변경한다.
        byte[] data = Convert.FromBase64String(p_data);

        //변경된 바이트배열을 암호화 한다.
        cryStream.Write(data, 0, data.Length);
        cryStream.FlushFinalBlock();

        //암호화 한 데이터를 스트링으로 변환해서 리턴
        return Encoding.UTF8.GetString(ms.GetBuffer());
    }

    public static bool IsEmail(this string email)
    {
        if (string.IsNullOrEmpty(email))
        { return false; }
        try
        {
            Regex _regex = new Regex("^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])" +
                "+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)" +
                "((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|" +
                "[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\u" +
                "FDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|" +
                "(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|" +
                "[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900" +
                "-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFF" +
                "EF])))\\.?$", RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
            return _regex.IsMatch(email);
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static bool IsNickName(this string strNick)
    {
        if (strNick == string.Empty) { return false; }
        foreach (var ch in strNick)
        {
            if (!char.IsLetterOrDigit(ch)) { return false; }
        }
        return true;
    }
}