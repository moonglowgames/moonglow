﻿public class StateManagerUnit : Singleton<StateManagerUnit>
{
    private StateManager<ClientState, IStateItem<ClientState>> stateManager = null;

    void Init()
    {
        stateManager = new StateManager<ClientState, IStateItem<ClientState>>();
        stateManager[ClientState.Loading] = new SILoading();
        stateManager[ClientState.LoginServerConnect] = new SILoginServerConnect();
        stateManager[ClientState.UserLogin] = new SIUserLogin();
        stateManager[ClientState.SessionLogin] = new SISessionLogin();
        stateManager[ClientState.AssetLoading] = new SIAssetLoading();
        stateManager[ClientState.LoadingReadyToHome] = new SILoadingReadyToHome();
        stateManager[ClientState.Home] = new SIHome();
        stateManager[ClientState.ReadyToRoom] = new SIReadyToRoom();
        stateManager[ClientState.Room] = new SIRoom();
        stateManager[ClientState.Play] = new SIPlay();
        stateManager[ClientState.PlayResult] = new SIPlayResult();
    }

    void Awake()
    {
        Init();
    }

    void Start()
    {
        stateManager.SetState(ClientState.Loading);
    }

    void OnApplicationFocus(bool focusStatus)
    {
        // reconnect...
        if (PacketManager.Instance.isConnect == false)
        {
            ReConnect();
        }
        else
        {
            if (focusStatus)
            {
                PacketManager.Instance.SendCSReqClientFocus();
                Invoke("ReConnect", 3f);
            }
            else
            {
                PacketManager.Instance.SendCSReqClientPause();
            }
        }
    }

    public void SetState(ClientState nextState)
    {
        if (stateManager == null)
        {
            Init();
        }

        stateManager.SetState(nextState);
    }

    public void ReConnect()
    {
        PacketManager.Instance.Reconnect();
    }

    public void RecvSCNotiClientFocus()
    {
        if (IsInvoking("ReConnect"))
        {
            CancelInvoke("ReConnect");
        }
    }
}