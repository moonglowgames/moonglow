﻿using UnityEngine;

public class CanvasCameraInit : MonoBehaviour
{
    void Start()
    {
        gameObject.GetComponentInChildren<Canvas>().worldCamera = Camera.main;
        gameObject.GetComponentInChildren<Canvas>().planeDistance = 10f;
    }
}