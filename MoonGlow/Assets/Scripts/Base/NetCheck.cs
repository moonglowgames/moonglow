﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class NetCheck : MonoBehaviour
{
    [SerializeField]
    private Image _imgChecker;

    public bool state = true;

    void Update()
    {
        if (state) { _imgChecker.color = PacketManager.Instance.isConnect ? Color.green : Color.red; }
    }
}