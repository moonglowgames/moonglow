﻿using UnityEngine;
using System.Collections;

public interface IStateItem<T> where T : struct
{
    void OnEnter(T beforeState);
    void OnExit();
    void OnUpdate();
}