using UnityEngine;
using UnityEngine.SceneManagement;

public enum ClientState {Unknown, Loading, LoginServerConnect, UserLogin, SessionLogin, AssetLoading, LoadingReadyToHome, Home, ReadyToRoom, Room, Play, PlayResult}

/*
 *   ReadyToRoom 
 * 			- OnEnter: change state
 * 
 *   Room  ( SCNotiEnterRoom )
 * 			- OnEnter : 1.Initialize, 2.SendCSNotiRoomEnter ( Server Send PlayerInfo... )
 * 	 [ Room -> Home : CSReqWantGame want = 1 ] 
 * 	 Play  ( SCNotiStartGame ) 
 * 			- OnEnter : 
 *   [ Play -> Home : CSReqWantGame want = 1 ]
 *   PlayResult ( SCNotiGameResult )
 * 			- OnEnter : 1. Show Result Window 
 * 			- OnExit : 2. Close Result Window
 *   [ PlayerResult -> Room : ( RestartGame : CSReqWantGame  want = 3 ) ]
 *   [ PlayerReault -> Home : ( ExitGame : CSReqWantGame want = 2 ) ]
 */

//Loading
public class SILoading : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]Loading Enter from " + beforeState.ToString());
        StateManagerUnit.Instance.SetState(ClientState.LoginServerConnect);
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]Loading Exit");
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]Loading Update");
    }

    #endregion
}

//LoginServerConnect
public class SILoginServerConnect : IStateItem<ClientState>
{
    #region IStateItem implementation
    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]LoginServerConnect Enter from " + beforeState.ToString());

        string connectIP = ConfigManager.Instance.Get(configKey.LoginServerIP);
        int connectPort = int.Parse(ConfigManager.Instance.Get(configKey.LoginServerPort));

        PacketManager.Instance.Connect(connectIP, connectPort);
    }

    public void OnExit()
    {
        //if Get SCNotiHello 
        //then set state Login
        Debug.Log("[CLIENT_STATE]LoginServerConnect Exit");
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]LoginServerConnect Update");
    }

    #endregion
}

//UserLogin
public class SIUserLogin : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]UserLogin Enter from " + beforeState.ToString());
        // send CSReqLogin 
        Login();
    }

    private void Login()
    {
        if (EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.account) == true)
        {
            if (EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.autoLogin, 0) == 1)
            {
                string account = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.account);
                string password = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.password);
                string id = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.deviceID);

                PacketManager.Instance.SendCSReqLogin(AccountType.Email, account, password, id);
            }
            else
            {
                LoginManager.Instance.ToggleWindow(true);
            }
        }
        else
        {
            LoginManager.Instance.ToggleWindow(true);
        }
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE] UserLogin Exit");
        //if get SCRspLogin
        //then set state AssetLoading
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE] UserLogin Update");
        //from login server to gameserver
    }

    #endregion
}

//SessionLogin
public class SISessionLogin : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]SessionLogin Enter from " + beforeState.ToString());

        GameControlUnit.Instance.mailAlarm = null;
        GameControlUnit.Instance.friendAlarm = null;
        GameControlUnit.Instance.shopAlarm = null;
        GameControlUnit.Instance.newsAlarm = null;
        // send CSReqLogin 
        SessionLogin();
    }

    private void SessionLogin()
    {
        //from login server to gameserver
        PacketManager.Instance.SendCSReqSessionLogin(EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session), EncryptedPlayerPrefs.GetString(PlayerPrefsKey.userID), (Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 1));
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE] SessionLogin Exit");
        //if get SCRspLogin
        //then set state AssetLoading
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE] SessionLogin Update");
        //from login server to gameserver
        //PacketManager.Instance.SendCSReqSessionLogin(PlayerPrefs.GetString(PlayerPrefsKey.session));
    }

    #endregion
}

//AssetLoading
public class SIAssetLoading : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]AssetLoading Enter from " + beforeState.ToString());
        // invoke (0 ~ 100%)
        LoadingManager.Instance.AssetLoadingStart();
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]AssetLoading Exit");

        //if upto 100%
        //then set state LoadingReayToHome
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]AssetLoading Update");
    }
    #endregion
}

//LoadingReadyToHome
public class SILoadingReadyToHome : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]LoadingReadyToHome Enter from " + beforeState.ToString());
        // send CSReqGotoScene 
        PacketManager.Instance.SendCSReqGotoScene(SceneType.Home);
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]LoadingReadyToHome Exit");

        //if get SCNotiGotoScene 
        //then set state Home
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]LoadingReadyToHome Update");
    }
    #endregion
}

//Home
public class SIHome : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void LoadHomeScene()
    {
        SceneManager.LoadScene(1);
        PacketManager.Instance.SendCSNotiSceneEnter(SceneType.Home);
    }

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]Home Enter from " + beforeState.ToString());
        //send CSNotiSceneEnter 
        LoadHomeScene();

        //when sent CSReqRoomCreate or CSReqQuickStart  or CSReqJoinRoom 
        //then set state ReadyToRoom
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]Home Exit");
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]Home Update");
    }

    #endregion
}

//ReadyToRoom
public class SIReadyToRoom : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]ReadyToRoom Enter from " + beforeState.ToString());
        //if get SCNotiEnterRoom
        //change state ->Room 
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]ReadyToRoom Exit");
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]ReadyToRoom Update");
    }

    #endregion
}

//Room
public class SIRoom : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]Room Enter from " + beforeState.ToString());
        //SendCSNotiRoomEnter 
        PacketManager.Instance.SendCSNotiEnterRoom();

        //if send CSReqWantGame (want =1)
        //change state Home

        //if get SCNotiStartGame
        //change state Play
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]Room Exit");
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]Room Update");
    }
    #endregion
}

//Play
public class SIPlay : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]Play Enter from " + beforeState.ToString());
        //if send CSReqWantGame (want = 1)
        //change state Home

        //if get SCNotiGameResult
        //change state PlayResult
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]Play Exit");
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]Play Update");
    }
    #endregion
}

//PlayResult
public class SIPlayResult : IStateItem<ClientState>
{
    #region IStateItem implementation

    public void OnEnter(ClientState beforeState)
    {
        Debug.Log("[CLIENT_STATE]PlayResult Enter from " + beforeState.ToString());

        //client action: Show "Result Window" here...

        //if CSReqWantGame  want = 3 - RestartGame
        //change state Room
        //if CSReqWantGame want = 2  - ExitGame
        //change state Home
    }

    public void OnExit()
    {
        Debug.Log("[CLIENT_STATE]PlayResult Exit");
        //Close Result Window
    }

    public void OnUpdate()
    {
        Debug.Log("[CLIENT_STATE]PlayResult Update");
    }
    #endregion
}