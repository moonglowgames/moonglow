﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;

public class MissionUnit : MonoBehaviour
{
    [SerializeField]
    private LocalizationUnit _missionType;
    [SerializeField]
    private Text _title, _description, _progress, _complete;
    [SerializeField]
    private Image _rewardItem1, _rewardItem2;
    [SerializeField]
    private Text _rewardItem1Count, _rewardItem2Count;
    [SerializeField]
    private RectTransform _progressContent;
    [SerializeField]
    private Image _startTip, _endTip;
    private float _progressBarWidth;

    public bool needUpdate = true;

    public Mission mission { get; set; }

    void Awake()
    {
        _progressBarWidth = _progressContent.parent.GetComponent<RectTransform>().sizeDelta.x;
    }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        _missionType.UpdateUI(mission.missionType.ToString());
        _title.text = mission.title;
        //_description.text = mission.description + System.Environment.NewLine + mission.startDate + " ~ " + mission.endDate;
        //_description.text = mission.description + System.Environment.NewLine + Convert.ToDateTime(mission.startDate).ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss") + " ~ " + Convert.ToDateTime(mission.endDate).ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
        _description.text = string.Format("{0}{1}{2:g} ~ {3:g}", mission.description, Environment.NewLine, DateTime.Parse(mission.startDate).ToLocalTime(), DateTime.Parse(mission.endDate).ToLocalTime());
        if (mission.currentValue > 0) { _startTip.gameObject.SetActive(true); }
        _progressContent.sizeDelta = new Vector2(mission.currentValue >= mission.targetValue ? _progressBarWidth - 24f : mission.currentValue * _progressBarWidth / mission.targetValue, 0f);
        _endTip.gameObject.SetActive(mission.state == MissionState.Complete);
        _progress.text = mission.currentValue.ToString() + " / " + mission.targetValue.ToString();
        
        switch (mission.state)
        {
            case MissionState.Complete:
                _complete.text = "<color=#77ED5BFF>Complete</color>";
                _complete.gameObject.SetActive(true);
                break;
            case MissionState.Fail:
                _complete.text = "<color=#FF5252FF>Mission" + System.Environment.NewLine + "Fail</color>";
                _complete.gameObject.SetActive(true);
                break;
        }

        if (mission.itemIdx1 == 0) { return; }
        else
        {
            switch (mission.itemIdx1)
            {
                case 2:
                    _rewardItem1.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopGold, 1);
                    _rewardItem1Count.text = mission.itemCount1.ToString() + " Gold";
                    break;
                case 3:
                    _rewardItem1.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopRuby, 6);
                    _rewardItem1Count.text = mission.itemCount1.ToString() + " Rubies";
                    break;
                default:
                    if (mission.itemIdx1 < 200)
                    {
                        _rewardItem1.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, mission.itemIdx1 % 100 - 1);
                        _rewardItem1Count.text = "x" + mission.itemCount1.ToString();
                    }
                    else if (mission.itemIdx1 < 700)
                    {
                        _rewardItem1.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, mission.itemIdx1 % 100 - 1);
                        _rewardItem1Count.text = ItemManager.Instance.Get(mission.itemIdx1).name;
                    }
                    else if (mission.itemIdx1 < 900)
                    {
                        _rewardItem1.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, mission.itemIdx1);
                        _rewardItem1Count.text = LocalizationManager.Instance.Get("BADGE");
                    }
                    break;
            }
            _rewardItem1.enabled = true;
            if (mission.itemIdx2 == 0) { _rewardItem2.gameObject.SetActive(false); }
            else
            {
                _rewardItem2.gameObject.SetActive(true);
                switch (mission.itemIdx2)
                {
                    case 2:
                        _rewardItem2.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopGold, 1);
                        _rewardItem2Count.text = mission.itemCount2.ToString() + " Gold";
                        break;
                    case 3:
                        _rewardItem2.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopRuby, 6);
                        _rewardItem2Count.text = mission.itemCount2.ToString() + " Rubies";
                        break;
                    default:
                        if (mission.itemIdx2 < 200)
                        {
                            _rewardItem2.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, mission.itemIdx2 % 100 - 1);
                            _rewardItem2Count.text = "x" + mission.itemCount2.ToString();
                        }
                        else if (mission.itemIdx2 < 700)
                        {
                            _rewardItem2.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, mission.itemIdx2 % 100 - 1);
                            _rewardItem2Count.text = ItemManager.Instance.Get(mission.itemIdx2).name;
                        }
                        else if (mission.itemIdx2 < 900)
                        {
                            _rewardItem2.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, mission.itemIdx2);
                            _rewardItem2Count.text = LocalizationManager.Instance.Get("BADGE");
                        }
                        break;
                }
                _rewardItem2.enabled = true;
            }
        }
    }
}