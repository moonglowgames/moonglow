﻿using UnityEngine;
using System;
using System.Collections.Generic;

class AchieveManager : Singleton<AchieveManager>
{
    private Dictionary<int, AchieveInfo> _items = new Dictionary<int, AchieveInfo>();
    private const string _FILE = @"Data/AchieveData";

    void Awake()
    {
        LoadFromFile();
    }

    void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        char[] charsToTrim = { ' ', '\t' };

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        AchieveInfo tempLevelExpInfo = null;

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);

            if (ln.Length == 0) continue;

            if (ln[0] == ';')
            {
                // ignore comment line
            }
            else
            {
                tempLevelExpInfo = new AchieveInfo();
                if (tempLevelExpInfo.Decode(ln) == true)
                {
                    _items[tempLevelExpInfo.iDx] = tempLevelExpInfo;
                }
                else
                {
                    Debug.Log(ln);
                }
            }
        }
    }

    public AchieveInfo Get(int iDx)
    {
        if (_items.ContainsKey(iDx))
        {
            return _items[iDx];
        }
        else
        {
            return null;
        }
    }
}