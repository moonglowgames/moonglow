﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class AchieveUnit : MonoBehaviour
{
    [SerializeField]
    private Text _title, _description, _star, _progress, _complete;
    [SerializeField]
    private Image _rewardItem1, _rewardItem2;
    [SerializeField]
    private Text _rewardItem1Count, _rewardItem2Count;
    [SerializeField]
    private RectTransform _progressContent;
    [SerializeField]
    private Image _startTip, _endTip;
    private float _progressBarWidth;

    public AchieveType achieveType;
    public bool needUpdate = true;

    public Achievement achievement { get; set; }

    void Awake()
    {
        _progressBarWidth = _progressContent.parent.GetComponent<RectTransform>().sizeDelta.x;
    }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        AchieveInfo achieveInfo = AchieveManager.Instance.Get(achievement.achieveIdx);
        if (achieveInfo == null)
        {
            Debug.Log("AchieveInfo is null AchieveUnit.cs");
            return;
        }

        achieveType = achieveInfo.achieveType;
        _title.text = achieveInfo.title;
        _description.text = achieveInfo.description;

        //★☆ Unicode symbols.
        string stars = achieveInfo.currentStar > 0 ? "★" : "☆";
        for (int i = 1; i < achieveInfo.maxStar; i++)
        {
            stars += i < achieveInfo.currentStar ? " ★" : " ☆";
        }
        _star.text = stars;

        if (achievement.currentValue > 0) { _startTip.gameObject.SetActive(true); }
        _progressContent.sizeDelta = new Vector2(achievement.currentValue >= achievement.targetValue ? _progressBarWidth - 24f : achievement.currentValue * _progressBarWidth / achievement.targetValue, 0f);
        _endTip.gameObject.SetActive(achieveInfo.currentStar == achieveInfo.maxStar);
        _progress.text = achievement.currentValue.ToString() + " / " + achievement.targetValue.ToString();

        _complete.enabled = achieveInfo.currentStar == achieveInfo.maxStar;

        if (achieveInfo.itemIdx1 == 0) { _rewardItem1.gameObject.SetActive(false); }
        else
        {
            switch (achieveInfo.itemIdx1)
            {
                case 2:
                    _rewardItem1.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopGold, 1);
                    _rewardItem1Count.text = achieveInfo.itemCount1.ToString() + " "+LocalizationManager.Instance.Get("GOLD");
                    break;
                case 3:
                    _rewardItem1.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopRuby, 6);
                    _rewardItem1Count.text = achieveInfo.itemCount1.ToString() + " " + LocalizationManager.Instance.Get("MOON_RUBY");
                    break;
                default:
                    if (achieveInfo.itemIdx1 < 200)
                    {
                        _rewardItem1.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, achieveInfo.itemIdx1 % 100 - 1);
                        _rewardItem1Count.text = "x" + achieveInfo.itemCount1.ToString();
                    }
                    else if (achieveInfo.itemIdx1 < 700)
                    {
                        _rewardItem1.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, achieveInfo.itemIdx1 % 100 - 1);
                        _rewardItem1Count.text = ItemManager.Instance.Get(achieveInfo.itemIdx1).name;
                    }
                    else if (achieveInfo.itemIdx1 < 900)
                    {
                        _rewardItem1.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, achieveInfo.itemIdx1);
                        _rewardItem1Count.text = LocalizationManager.Instance.Get("BADGE");
                    }
                    break;
            }
            _rewardItem1.enabled = true;
            if (achieveInfo.itemIdx2 == 0) { _rewardItem2.gameObject.SetActive(false); }
            else
            {
                _rewardItem2.gameObject.SetActive(true);
                switch (achieveInfo.itemIdx2)
                {
                    case 2:
                        _rewardItem2.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopGold, 1);
                        _rewardItem2Count.text = achieveInfo.itemCount2.ToString() + " " + LocalizationManager.Instance.Get("GOLD");
                        break;
                    case 3:
                        _rewardItem2.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopRuby, 6);
                        _rewardItem2Count.text = achieveInfo.itemCount2.ToString() + " " + LocalizationManager.Instance.Get("MOON_RUBY");
                        break;
                    default:
                        if (achieveInfo.itemIdx2 < 200)
                        {
                            _rewardItem2.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, achieveInfo.itemIdx2 % 100 - 1);
                            _rewardItem2Count.text = "x" + achieveInfo.itemCount2.ToString();
                        }
                        else if (achieveInfo.itemIdx2 < 700)
                        {
                            _rewardItem2.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, achieveInfo.itemIdx2 % 100 - 1);
                            _rewardItem2Count.text = ItemManager.Instance.Get(achieveInfo.itemIdx2).name;
                        }
                        else if (achieveInfo.itemIdx2 < 900)
                        {
                            _rewardItem2.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, achieveInfo.itemIdx2);
                            _rewardItem2Count.text = LocalizationManager.Instance.Get("BADGE");
                        }
                        break;
                }
                _rewardItem2.enabled = true;
            }
        }
    }
}