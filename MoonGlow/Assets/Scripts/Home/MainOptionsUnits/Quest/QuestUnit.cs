﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class QuestUnit : MonoBehaviour
{
    [SerializeField]
    private Toggle _missionToggle;
    [SerializeField]
    private ScrollRect missionTab, achievementTab;
    private RectTransform _achievementListRectTransform, _missionListRectTransform;
    
    private float _questBoxWidth;
    private bool _needCSReq = true;
    
    public AchieveUnit achievementPrefab;
    public MissionUnit missionPrefab;

    void Awake()
    {
        GameControlUnit.Instance.Quest = this;
        _achievementListRectTransform = achievementTab.content;
        _missionListRectTransform = missionTab.content;
        _questBoxWidth = _missionListRectTransform.sizeDelta.x;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void ClearAchievements()
    {
        foreach (Transform child in _achievementListRectTransform) { Destroy(child.gameObject); }
        _achievementListRectTransform.sizeDelta = new Vector2(_questBoxWidth, 0f);
    }

    public void AddAchievement(Achievement achievement)
    {
        AchieveUnit achievementObject = Instantiate(achievementPrefab);
        achievementObject.transform.SetParent(_achievementListRectTransform, false);
        achievementObject.achievement = achievement;
        achievementObject.needUpdate = true;

        Vector2 achievementListSize = _achievementListRectTransform.sizeDelta;
        achievementListSize.y += 193f;
        _achievementListRectTransform.sizeDelta = achievementListSize;
    }

    public void ClearMissions()
    {
        foreach (Transform child in _missionListRectTransform) { Destroy(child.gameObject); }
        _missionListRectTransform.sizeDelta = new Vector2(_questBoxWidth, 0f);
    }

    public void AddMission(Mission mission)
    {
        MissionUnit missionObject = Instantiate(missionPrefab);
        missionObject.transform.SetParent(_missionListRectTransform, false);
        missionObject.mission = mission;
        missionObject.needUpdate = true;

        Vector2 missionListSize = _missionListRectTransform.sizeDelta;
        missionListSize.y += 193f;
        _missionListRectTransform.sizeDelta = missionListSize;
    }

    public void SetDefaultTab()
    {
        _missionToggle.isOn = true;
        missionTab.verticalNormalizedPosition = 1f;
        achievementTab.verticalNormalizedPosition = 1f;
    }

    public void MissionTab(bool isOn)
    {
        if (isOn)
        {
            ClearMissions();
            PacketManager.Instance.SendCSReqMissionList();
        }
        missionTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab); }
        _needCSReq = true;
    }

    public void AchievementTab(bool isOn)
    {
        if (isOn)
        {
            ClearAchievements();
            PacketManager.Instance.SendCSReqAchieveList();
        }
        achievementTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab); }
        _needCSReq = false;
    }

    public void ReqUpdate()
    {
        if (_needCSReq)
        {
            ClearMissions();
            PacketManager.Instance.SendCSReqMissionList();
        }
        //PacketManager.Instance.SendCSReqAchieveList();
    }
}