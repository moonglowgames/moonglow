﻿using System;

public enum AchieveType
{
    Unknown = 0, Win = 1, Lose = 2, ConsecutiveWin = 3, Level = 4, Friends = 5, PlayTime = 6, EPElixirUseCount = 7, SOElixirUseCount = 8,
    RosemaryOilUseCount = 9, SLElixirUseCount = 10, EtherealInkUseCount = 11, AllJokerFace = 12, CheckIn = 13, PlayCount = 14, Tutorial = 15, ItemUseCount = 16, ConCheckIn = 17
}

public class AchieveInfo
{
    //;idx achieveType title description currentStar maxStar targetValue itemIdx1 itemCount1 itemIdx2 itemCount2 disable
    public int iDx;
    public AchieveType achieveType;
    public string title;
    public string description;
    public int currentStar;
    public int maxStar;
    public int targetValue;
    public int itemIdx1;
    public int itemCount1;
    public int itemIdx2;
    public int itemCount2;
    public bool disable;

    const int DECODE_PARSE_COUNT = 12;
    public char[] charsToTrim = { ' ', '\t' };

    public bool Decode(string cvsLineData_)
    {
        bool retvalue = false;

        if (cvsLineData_ != string.Empty)
        {
            cvsLineData_.Trim(charsToTrim);

            switch (cvsLineData_[0])
            {
                case ';':
                    break;
                default:
                    string[] parsings = cvsLineData_.Split(',');
                    int parscount = 0;
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        try
                        {
                            this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.achieveType = (AchieveType)Enum.Parse(typeof(AchieveType), parsings[parscount++].Trim(charsToTrim));
                            //this.title = parsings[parscount++].Trim(charsToTrim);
                            //this.description = parsings[parscount++].Trim(charsToTrim);
                            parscount++;
                            this.title = AchieveLocalizationManager.Instance.GetHeader(this.iDx);
                            parscount++;
                            this.description = AchieveLocalizationManager.Instance.GetDescription(this.iDx);
                            this.currentStar = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.maxStar = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.targetValue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.itemIdx1 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.itemCount1 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.itemIdx2 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.itemCount2 = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.disable = (Convert.ToInt32(parsings[parscount++].Trim(charsToTrim)) == 0) ? false : true;

                            retvalue = true;
                        }
                        catch (Exception /* e */ )
                        {
                            retvalue = false;
                        }
                    }
                    break;
            }
        }
        return retvalue;
    }
}