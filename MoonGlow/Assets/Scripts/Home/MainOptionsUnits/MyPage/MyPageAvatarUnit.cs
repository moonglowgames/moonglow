﻿using UnityEngine;
using UnityEngine.UI;

public class MyPageAvatarUnit : MyPageItemUnit
{
    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(info.inventoryIdx);
        if (itemInfo == null)
        {
            Debug.Log("ItemInfo is null MyPageUnit.cs");
            return;
        }

        _name.text = itemInfo.name;
        _description.text = itemInfo.description;
        _focusFrame.enabled = info.select;

        //_useButton.GetComponent<Image>().sprite = select ? HomeSpriteFactory.Instance.Get(SpriteType.MenuControl, 3) : HomeSpriteFactory.Instance.Get(SpriteType.MenuControl, 0);

        _image.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatarMax, info.inventoryIdx % 100);
    }

    override public void UseAction()
    {
        if (!info.select) { PacketManager.Instance.SendCSReqUseItem(info.inventoryIdx, !info.select); }
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}