﻿#pragma warning disable 649

using UnityEngine;

public class MyPageItemTabUnit : MonoBehaviour
{
    [SerializeField]
    private RectTransform _itemParent;

    public TabType tabType;
    public GameObject itemPrefab;
    public int[] iDx;

    void Start()
    {
        Vector2 spellSize = _itemParent.sizeDelta;
        spellSize.y += 421f * iDx.Length;
        _itemParent.sizeDelta = spellSize;

        for (int i = 0; i <= iDx.GetUpperBound(0); i++)
        {
            GameObject spell = Instantiate(itemPrefab);
            spell.transform.SetParent(_itemParent, false);
            MyPageItemUnit tempUnit;
            switch (tabType)
            {
                case TabType.Items:
                    tempUnit = spell.GetComponent<MyPageItemUnit>();
                    break;
                case TabType.Character:
                    tempUnit = spell.GetComponent<MyPageAvatarUnit>();
                    break;
                case TabType.Pet:
                    tempUnit = spell.GetComponent<MyPagePetUnit>();
                    break;
                case TabType.CardSkin:
                    tempUnit = spell.GetComponent<MyPageCardSkinUnit>();
                    break;
                case TabType.Badge:
                    tempUnit = spell.GetComponent<MyPageBadgeUnit>();
                    //spell.GetComponent<MyPageBadgeUnit>().UpdateImage(iDx[i]);
                    break;
                default:
                    tempUnit = spell.GetComponent<MyPageItemUnit>();
                    break;
            }
            tempUnit.info.inventoryIdx = iDx[i];
            GameControlUnit.Instance.MyPage.AddItem(tempUnit, tabType);
            tempUnit.needUpdate = true;
        }
    }
}