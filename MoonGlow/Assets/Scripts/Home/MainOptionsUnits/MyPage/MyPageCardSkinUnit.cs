﻿using UnityEngine;

public class MyPageCardSkinUnit : MyPageItemUnit
{
    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(info.inventoryIdx);
        if (itemInfo == null)
        {
            Debug.Log("ItemInfo is null MyPageUnit.cs");
            return;
        }

        _name.text = itemInfo.name;
        _description.text = itemInfo.description;
        _focusFrame.enabled = info.select;

        //_useButton.sprite = select ? HomeSpriteFactory.Instance.Get(SpriteType.MenuControl, 3) : HomeSpriteFactory.Instance.Get(SpriteType.MenuControl, 0);
        //_useButton.GetComponentInChildren<LocalizationUnit>().UpdateUI(select ? "REMOVE" : "USE");

        _image.sprite = HomeSpriteFactory.Instance.Get(SpriteType.CardSkin, info.inventoryIdx % 100 - 1);
        _image.enabled = true;
    }

    override public void ToggleMaterial(bool state)
    {
        _image.material = state ? null : ItemSpriteFactory.Instance.GrayScale;
        //_useButton.interactable = state;
    }

    override public void UseAction()
    {
        PacketManager.Instance.SendCSReqUseItem(info.inventoryIdx, !info.select);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}