﻿#pragma warning disable 649

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyPageProfileUnit : MonoBehaviour
{
    [SerializeField]
    private Image _avatar;
    [SerializeField]
    private Image[] _pets;
    [SerializeField]
    private Image _badge, _cardSkin;
    [SerializeField]
    private Text _name, _level, _exp, _stamina, _record, _winningRate, _hpPlus, _spPlus;
    [SerializeField]
    private Slider _sliderDown, _sliderUp;

    private int _avatarIdx;
    private List<int> _petList;
    private int _badgeIdx, _cardSkinIdx;
    private int _xp = int.MinValue;

    private bool _reqTimer;
    private float[] _timeLeft;

    public bool needUpdate;

    public MyInfo Info { get; set; }

    void Update()
    {
        if (needUpdate == true)
        {
            needUpdate = false;
            UpdateView();
        }
        if (_reqTimer)
        {
            UpdateTimer();
        }
    }

    void UpdateView()
    {
        if (_avatarIdx != Info.faceIdx)
        {
            _avatarIdx = Info.faceIdx;
            _avatar.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatar, _avatarIdx % 100);
        }
        _name.text = Info.nickName;
        _level.text = "Lv. " + Info.level.ToString();
        _exp.text = Info.exp.ToString() + "/" + Info.nextLevelExp.ToString();
        _stamina.text = Info.stamina.ToString() + "/" + Info.maxStamina.ToString();
        _record.text = Info.winCount.ToString() + "/" + Info.totalCount.ToString();
        _winningRate.text = Info.winRate;

        if (_xp != Info.exp)
        {
            _xp = Info.exp;
            if (_xp < 0)
            {
                _sliderDown.maxValue = Mathf.Abs(Info.downLevelExp);
                _sliderDown.value = Mathf.Abs(_xp);
                _sliderDown.transform.FindChild("FullTip").gameObject.SetActive(_sliderDown.normalizedValue == 1);
                _sliderDown.transform.FindChild("Handle Slide Area/Handle/Text").GetComponent<Text>().text = _xp.ToString();
                _sliderDown.transform.FindChild("Handle Slide Area/Handle").gameObject.SetActive(true);
                _sliderUp.value = 0;
                _sliderUp.transform.FindChild("Handle Slide Area/Handle").gameObject.SetActive(false);
            }
            else
            {
                _sliderUp.maxValue = Info.nextLevelExp;
                _sliderUp.value = _xp;
                _sliderUp.transform.FindChild("FullTip").gameObject.SetActive(_sliderUp.normalizedValue == 1);
                _sliderUp.transform.FindChild("Handle Slide Area/Handle/Text").GetComponent<Text>().text = _xp.ToString();
                _sliderUp.transform.FindChild("Handle Slide Area/Handle").gameObject.SetActive(true);
                _sliderDown.value = 0;
                _sliderDown.transform.FindChild("Handle Slide Area/Handle").gameObject.SetActive(false);
            }
        }

        if (_petList != Info.petList)
        {
            _petList = Info.petList;
            foreach (Image pet in _pets) { pet.gameObject.SetActive(false); }
            for (int i = 0; i < _petList.Count; i++)
            {
                _pets[i].sprite = ItemSpriteFactory.Instance.Get(SpriteType.PetMini, _petList[i] % 100 - 1);
                _pets[i].gameObject.SetActive(true);
            }
        }

        if (_badgeIdx != Info.badgeIdx)
        {
            _badgeIdx = Info.badgeIdx;
            if (_badgeIdx > 0)
            {
                _badge.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, _badgeIdx);
                _badge.gameObject.SetActive(true);
            }
            else
            {
                _badge.gameObject.SetActive(false);
            }
        }

        if (_cardSkinIdx != Info.cardSkinIdx)
        {
            _cardSkinIdx = Info.cardSkinIdx;
            if (_cardSkinIdx > 0)
            {
                _cardSkin.sprite = HomeSpriteFactory.Instance.Get(SpriteType.CardSkinMini, _cardSkinIdx % 100 - 1);
                _cardSkin.gameObject.SetActive(true);
            }
            else
            {
                _cardSkin.gameObject.SetActive(false);
            }
        }

        _timeLeft = new float[Info.itemAbilityList.Count];
        for(int i=0; i < Info.itemAbilityList.Count; i++)
        {
            string[] s = Info.itemAbilityList[i].remainTime.Trim('[', ']').Split(':');
            _timeLeft[i] += Convert.ToSingle(s[0]) * 3600 + Convert.ToSingle(s[1]) * 60 + Convert.ToSingle(s[2]);
            if (_timeLeft[i] > 0) { _reqTimer = true; }
        }
        if (_reqTimer)
        {
            return;
        }
        else
        {
            //_hpPlus.text = string.Empty;
            //_spPlus.text = string.Empty;
            //foreach (ItemAbility item in Info.itemAbilityList)
            //{
            //    _info.text += item.itemInfo;
            //    _info.text += item.remainTime;
            //    _info.text += Environment.NewLine;
            //}
            _hpPlus.text = Info.itemAbilityList[0].remainTime;
            _spPlus.text = Info.itemAbilityList[1].remainTime;
        }
    }

    void UpdateTimer()
    {
        _reqTimer = false;
        for (int i = 0; i <= _timeLeft.GetUpperBound(0); i++)
        {
            if (_timeLeft[i] > 0)
            {
                _reqTimer = true;
                _timeLeft[i] -= Time.deltaTime;
            }
        }

        _hpPlus.text = " [ 00 : 00 ]";
        _spPlus.text = " [ 00 : 00 ]";
        //for (int i = 0; i < Info.itemAbilityList.Count; i++)
        //{
        //    _info.text += Info.itemAbilityList[i].itemInfo;
        //    if (_timeLeft[i] > 0)
        //    {
        //        int hours = (int)_timeLeft[i] / 3600;
        //        int minutes = (int)(_timeLeft[i] % 3600) / 60;
        //        _info.text += string.Format(" [ {0:00} : {1:00} ]", hours, minutes);
        //    }
        //    else { _info.text += " [00:00:00]"; }
        //    _info.text += Environment.NewLine;
        //}
        if (_timeLeft[0] > 0)
        {
            int hours = (int)_timeLeft[0] / 3600;
            int minutes = (int)(_timeLeft[0] % 3600) / 60;
            _hpPlus.text = string.Format(" [ {0:00} : {1:00} ]", hours, minutes);
        }
        if (_timeLeft[1] > 0)
        {
            int hours = (int)_timeLeft[1] / 3600;
            int minutes = (int)(_timeLeft[1] % 3600) / 60;
            _spPlus.text = string.Format(" [ {0:00} : {1:00} ]", hours, minutes);
        }
    }
}