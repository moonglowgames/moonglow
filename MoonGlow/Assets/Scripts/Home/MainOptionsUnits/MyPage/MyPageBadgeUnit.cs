﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class MyPageBadgeUnit : MyPageItemUnit
{
    [SerializeField]
    private Image[] _badges;

    private int _badgeIdx = int.MinValue;

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(info.inventoryIdx);
        if (itemInfo == null)
        {
            Debug.Log(info.inventoryIdx.ToString() + " ItemInfo is null MyPageBadgeUnit.cs");
            return;
        }

        _name.text = itemInfo.name;
        _description.text = itemInfo.description;
        _focusFrame.enabled = info.select;

        //for (int i = 0; i <= _badges.GetUpperBound(0); i++)
        //{
        //    _badges[i].material = i > info.inventoryIdx % 10 - 1 ? ItemSpriteFactory.Instance.GrayScale : null;
        //}

        if (_badgeIdx != info.inventoryIdx)
        {
            _badgeIdx = info.inventoryIdx;
            //for (int i = _badgeIdx % 10 - 1; i >= 0; i--)
            //{
            //    _badges[_badgeIdx % 10 - i - 1].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, _badgeIdx - i);
            //}
            for (int i = 0 ; i < _badgeIdx % 10; i++)
            {
                _badges[i].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, _badgeIdx - _badgeIdx % 10 + i + 1);
            }
        }
    }

    public void UpdateImage(int iDx)
    {
        //for (int i = 0; i <= _badges.GetUpperBound(0); i++)
        //{
        //    _badges[i].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, iDx + i + 1);
        //}
    }

    override public void ToggleMaterial(bool state)
    {
        //foreach (Image badge in _badges) { badge.material = state ? null : ItemSpriteFactory.Instance.GrayScale; }
    }

    override public void UseAction()
    {
        PacketManager.Instance.SendCSReqUseItem(info.inventoryIdx, !info.select);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}