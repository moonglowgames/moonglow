﻿public enum Bagde
{
    Victory = 71,
    Defeat = 72,
    Gold = 73,
    ReachLevel = 74,
    Ruby = 75,
    GamesPlayed = 76,
    PlayTime = 77,
    Friends = 78,
    Ranking = 79,
    UseItems = 80,
    Attendance = 81,
    ContinuousAttendance = 82,
    ConsecutiveWins = 83,
    ConsecutiveDefeat = 84,
    AfterHoursJoin = 85
}