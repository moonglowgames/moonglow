﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class MyPageItemUnit : MonoBehaviour
{
    [SerializeField]
    protected Image _image;
    [SerializeField]
    protected FocusFrameUnit _focusFrame;
    [SerializeField]
    protected Text _name, _description, _quantity;
    //[SerializeField]
    //protected Button _useButton;
   
    public TabType tabType;
    public InventoryInfo info { get; set; }
    public bool needUpdate = true;

    protected void Awake()
    {
        info = new InventoryInfo();
    }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(info.inventoryIdx);
        if (itemInfo == null)
        {
            Debug.Log("ItemInfo is null MyPageUnit.cs");
            return;
        }

        _name.text = itemInfo.name;
        _description.text = itemInfo.description;
        _quantity.text = "x" + info.count.ToString();
        _image.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Item, info.inventoryIdx % 100 - 1);
        _image.enabled = true;
    }

    void ReqUseAction()
    {
        if (info.count > 0) { PacketManager.Instance.SendCSReqUseItem(info.inventoryIdx, true); }
    }

    virtual public void UseAction()
    {
        if (info.count > 0) { GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "ReqUseAction", null, "USE_ITEM"); }
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    virtual public void ToggleMaterial(bool state)
    {
        return;
    }
}