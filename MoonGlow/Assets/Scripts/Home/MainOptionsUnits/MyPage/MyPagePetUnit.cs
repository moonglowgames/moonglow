﻿#pragma warning disable 649

using System;
using UnityEngine;
using UnityEngine.UI;

public enum PetPrefab { WolverineJr, TinkerBelle, Trity, MiewCat, BlackDrake, Kebby, Kerato, MoonRabbit, MrPark, HellHound, Drago, Gobby, Pingu, Koora, CowDog, Wolverine, Succubeez, Draco }

public class MyPagePetUnit : MyPageItemUnit
{
    [SerializeField]
    private RectTransform _avatar;
    private Animator _petAnimator;

    private int _petIdx = int.MinValue;

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(info.inventoryIdx);
        if (itemInfo == null)
        {
            Debug.Log("ItemInfo is null MyPageUnit.cs");
            return;
        }

        _name.text = itemInfo.name;
        _description.text = itemInfo.description;
        _focusFrame.enabled = info.select;

        //_useButton.GetComponentInChildren<LocalizationUnit>().UpdateUI(select ? "REMOVE" : "USE");

        if (_petIdx != info.inventoryIdx)
        {
            _petIdx = info.inventoryIdx;
            string prefab_name = string.Format("PetPrefabs/{0:00}{1}UI", _petIdx % 100, Enum.GetName(typeof(PetPrefab), _petIdx % 100));
            var pet = (GameObject)Instantiate(Resources.Load(prefab_name));
            var pet_transform = pet.GetComponent<RectTransform>();
            pet_transform.SetParent(_avatar);
            pet_transform.localScale = new Vector3(1.5f, 1.5f);
            pet_transform.anchoredPosition = Vector2.zero;
            foreach (Image cutImage in pet_transform.GetComponentsInChildren<Image>()) { cutImage.material = ItemSpriteFactory.Instance.GrayScale; }
            _petAnimator = pet.GetComponent<Animator>();
        }
        if (_petAnimator != null) { _petAnimator.SetTrigger(info.select ? "Action" : "Stop"); }
    }

    override public void ToggleMaterial(bool state)
    {
        foreach (Image cutImage in _avatar.GetComponentsInChildren<Image>()) { cutImage.material = state ? null : ItemSpriteFactory.Instance.GrayScale; }
        //_useButton.interactable = state;
    }

    override public void UseAction()
    {
        PacketManager.Instance.SendCSReqUseItem(info.inventoryIdx, !info.select);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}