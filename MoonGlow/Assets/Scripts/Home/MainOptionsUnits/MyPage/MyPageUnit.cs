﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MyPageUnit : MonoBehaviour
{
    [SerializeField]
    private Toggle _itemToggle;
    [SerializeField]
    private ScrollRect itemTab, avatarTab, petTab, badgeTab, cardSkinTab;
    [SerializeField]
    private MyPageProfileUnit _profile;

    private TabType _tabType = TabType.Items;
    private Dictionary<int, MyPageItemUnit> _items = new Dictionary<int, MyPageItemUnit>();
    private bool _needCSReq = true;

    public MyPageProfileUnit Profile { get { return _profile; } }

    void Awake()
    {
        GameControlUnit.Instance.MyPage = this;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void SetDefaultTab()
    {
        _itemToggle.isOn = true;
        itemTab.verticalNormalizedPosition = 1f;
        avatarTab.verticalNormalizedPosition = 1f;
        petTab.verticalNormalizedPosition = 1f;
        badgeTab.verticalNormalizedPosition = 1f;
    }

    public void ReqUpdate()
    {
        PacketManager.Instance.SendCSReqMyInfo();
        if (_needCSReq) { PacketManager.Instance.SendCSReqInventory(_tabType); }
    }

    public void DisableImages(TabType tab)
    {
        foreach (MyPageItemUnit item in _items.Values)
        {
            if (item.tabType == tab) { item.ToggleMaterial(false); }
        }
    }

    public void AddItem(MyPageItemUnit item, TabType tab)
    {
        switch (tab)
        {
            case TabType.Badge:
                int iDx = item.info.inventoryIdx / 10;
                _items.Add(iDx, item);
                break;
            default:
                _items.Add(item.info.inventoryIdx, item);
                break;
        }
    }

    public MyPageItemUnit GetItem(int iDx, TabType tab)
    {
        switch (tab)
        {
            case TabType.Badge:
                iDx /= 10;
                if (_items.ContainsKey(iDx)) { return _items[iDx]; }
                else { return null; }
            default:
                if (_items.ContainsKey(iDx)) { return _items[iDx]; }
                else { return null; }
        }
    }

    public void ItemsTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = TabType.Items;
            PacketManager.Instance.SendCSReqInventory(_tabType);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = true;
        }
        itemTab.gameObject.SetActive(isOn);
    }

    public void AvatarTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = TabType.Character;
            PacketManager.Instance.SendCSReqInventory(_tabType);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        avatarTab.gameObject.SetActive(isOn);
    }

    public void PetTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = TabType.Pet;
            PacketManager.Instance.SendCSReqInventory(_tabType);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        //petTab.transform.FindChild("PetList").GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        petTab.gameObject.SetActive(isOn);
    }

    public void BadgeTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = TabType.Badge;
            PacketManager.Instance.SendCSReqInventory(_tabType);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        badgeTab.gameObject.SetActive(isOn);
    }

    public void CardSkinTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = TabType.CardSkin;
            PacketManager.Instance.SendCSReqInventory(_tabType);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        cardSkinTab.gameObject.SetActive(isOn);
    }

    public void HelpButton()
    {
        HomeUIManagerUnit.Instance.HelpBookOption(8);
    }
}