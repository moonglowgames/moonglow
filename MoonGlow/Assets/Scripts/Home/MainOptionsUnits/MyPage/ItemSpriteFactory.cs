﻿using UnityEngine;

public class ItemSpriteFactory : Singleton<ItemSpriteFactory>
{
    private Sprite _errorSprite;
    private Sprite[] _characterAvatar;
    private Sprite[] _characterAvatarMini;
    private Sprite[] _characterAvatarMax;
    private Sprite _myPageActiveItem;
    private Sprite _myPageInactiveItem;
    private Sprite[] _items;
    private Sprite[] _jokerAvatar;
    private Sprite[] _pets;
    private Sprite[] _petMinis;
    private Sprite[] _cardSkins;
    private Sprite[] _badges;
    private Material _grayScale;

    void Awake()
    {
        _errorSprite = Resources.Load<Sprite>("Images/Error");
        _characterAvatar = Resources.LoadAll<Sprite>("Images/CharacterAvatars");
        _characterAvatarMini = Resources.LoadAll<Sprite>("Images/PlayAvatars");
        _characterAvatarMax = Resources.LoadAll<Sprite>("Images/Avatars");
        _items = Resources.LoadAll<Sprite>("Images/Items");
        _jokerAvatar = Resources.LoadAll<Sprite>("Images/JokerFace");
        _pets = Resources.LoadAll<Sprite>("Images/Pets");
        _petMinis = Resources.LoadAll<Sprite>("Images/PetMini");
        _cardSkins = Resources.LoadAll<Sprite>("Images/Card/CardSkin");
        _badges = Resources.LoadAll<Sprite>("Images/Badges");
        _myPageActiveItem = Resources.Load<Sprite>("Images/ActiveFrame/MyPageActiveItem");
        _myPageInactiveItem = Resources.Load<Sprite>("Images/ActiveFrame/MyPageInactiveItem");
        _grayScale = Resources.Load<Material>("Images/GrayScale/GrayScale");
    }

    public Material GrayScale { get { return _grayScale; } }

    public Sprite Get(SpriteType type, int iDx)
    {
        switch (type)
        {
            case SpriteType.Item:
                return GetItemSprite(iDx);
            case SpriteType.CharacterAvatar:
                return GetCharacterAvatarSprite(iDx);
            case SpriteType.CharacterAvatarMini:
                return GetCharacterAvatarMiniSprite(iDx);
            case SpriteType.CharacterAvatarMax:
                return GetCharacterAvatarMaxSprite(iDx);
            case SpriteType.JokerFace:
                return GetJokerFaceSprite(iDx);
            case SpriteType.Pet:
                return GetPetSprite(iDx);
            case SpriteType.PetMini:
                return GetPetMiniSprite(iDx);
            case SpriteType.CardSkin:
                return GetCardSkinSprite(iDx);
            case SpriteType.Badge:
                return GetBadgeSprite(iDx);
            case SpriteType.MyPageActiveItem:
                return GetActiveItemSprite(iDx);
            default:
                Debug.LogError("Sprite type mismatch ItemSpriteFactory.cs");
                return null;
        }
    }

    Sprite GetItemSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _items.Length)
        {
            return _items[iDx];
        }
        else
        {
            Debug.LogError("Item index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCharacterAvatarSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _characterAvatar.Length)
        {
            return _characterAvatar[iDx];
        }
        else
        {
            Debug.LogError("Character avatar index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCharacterAvatarMiniSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _characterAvatarMini.Length)
        {
            return _characterAvatarMini[iDx];
        }
        else
        {
            Debug.LogError("Character avatar mini index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCharacterAvatarMaxSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _characterAvatarMax.Length)
        {
            return _characterAvatarMax[iDx];
        }
        else
        {
            Debug.LogError("Character avatar index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetJokerFaceSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _jokerAvatar.Length)
        {
            return _jokerAvatar[iDx];
        }
        else
        {
            Debug.LogError("Joker avatar index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetPetSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _pets.Length)
        {
            return _pets[iDx];
        }
        else
        {
            Debug.LogError("Pet index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetPetMiniSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _petMinis.Length)
        {
            return _petMinis[iDx];
        }
        else
        {
            Debug.LogError("Pet Mini index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCardSkinSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _cardSkins.Length)
        {
            return _cardSkins[iDx];
        }
        else
        {
            Debug.LogError("Card Skin index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetBadgeSprite(int iDx)
    {
        iDx -= 711;
        iDx = (iDx / 10) * 5 + iDx % 10; // (Badge_type) * Badge_max_level + Badge_current_level
        if (iDx >= 0 && iDx < _badges.Length)
        {
            return _badges[iDx];
        }
        else
        {
            Debug.LogError("Badge index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetActiveItemSprite(int iDx)
    {
        switch (iDx)
        {
            case 0:
                return _myPageInactiveItem;
            case 1:
                return _myPageActiveItem;
            default:
                Debug.LogError("Frame index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
                return _errorSprite;
        }
    }
}