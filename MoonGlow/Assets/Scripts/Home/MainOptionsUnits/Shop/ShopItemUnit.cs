﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class ShopItemUnit : MonoBehaviour
{
    [SerializeField]
    private RectTransform _countPanel;
    [SerializeField]
    private Text _name, _description, _price, _quantity;
    [SerializeField]
    private Image _priceType, _image, _sale, _best;
    [SerializeField]
    private LocalizationUnit _buyButton, _countLabel;

    private int _count = 1;

    public bool needUpdate = true;

    public ShopListInfo ShopInfo { get; set; }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(ShopInfo.elementIdx);
        if (itemInfo == null)
        {
            Debug.Log("ItemInfo is null ShopUnit.cs " + ShopInfo.elementIdx.ToString());
            return;
        }

        _name.text = itemInfo.name;
        _description.text = itemInfo.shopDescription == "0" ? itemInfo.description : itemInfo.shopDescription;
        _price.text = ShopInfo.tabID == TabType.Ruby ? ShopInfo.price.ToString("N2") : ShopInfo.price.ToString("N0");
        _buyButton.UpdateUI();

        if (ShopSpriteFactory.Instance != null)
        {
            _priceType.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopPriceType, (int)itemInfo.payType - 1);
            switch (ShopInfo.tabID)
            {
                case TabType.Items:
                    _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, ShopInfo.elementIdx % 100 - 1);
                    _countPanel.gameObject.SetActive(true);
                    _quantity.transform.parent.gameObject.SetActive(true);
                    _quantity.text = _count.ToString();
                    break;
                case TabType.Joker:
                    _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopJoker, ShopInfo.elementIdx % 100 - 1);
                    _countLabel.UpdateUI("ITEM_ONCE");
                    break;
                case TabType.Gold:
                    _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopGold, ShopInfo.elementIdx % 100 - 1);
                    _countLabel.UpdateUI("ITEM_AD");
                    break;
                case TabType.Ruby:
                    _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopRuby, ShopInfo.elementIdx % 100 - 1);
                    switch (ShopInfo.payType)
                    {
                        case PayType.AD_RUBY:
                            _countLabel.UpdateUI("ITEM_AD");
                            break;
                        case PayType.Cash:
                            _countLabel.UpdateUI("ITEM_APP");
                            break;
                    }
                    break;
                case TabType.Pet:
                    _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.Pet, ShopInfo.elementIdx);
                    _countLabel.UpdateUI("ITEM_ONCE");
                    break;
                case TabType.CardSkin:
                    _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.CardSkin, ShopInfo.elementIdx % 100 - 1);
                    _countLabel.UpdateUI("ITEM_ONCE");
                    break;
            }
            _image.enabled = true;
        }
        else { Debug.Log("ShopSpriteFactory is null MiniCardUnit.cs"); }

        switch (ShopInfo.specialType)
        {
            case SpecialType.Normal:
                _best.enabled = false;
                _sale.enabled = false;
                break;
            case SpecialType.Sale:
                _best.enabled = false;
                _sale.enabled = true;
                break;
            case SpecialType.Best:
                _best.enabled = true;
                _sale.enabled = false;
                break;
            default:
                _best.enabled = false;
                _sale.enabled = false;
                break;
        }

        if (transform.GetSiblingIndex() == 0)
        {
            transform.FindChild("Hinge1").gameObject.SetActive(false);
            transform.FindChild("Hinge2").gameObject.SetActive(false);
        }
    }

    public void BuyAction()
    {
        GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "ReqBuyAction", null, "BUY_ITEM");
        SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton);
    }

    void ReqBuyAction()
    {
        switch (ShopInfo.payType)
        {
            case PayType.Cash:
#if UNITY_ANDROID || UNITY_IOS
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    PacketManager.Instance.SendCSReqIAP(ShopInfo.elementIdx, IAPType.AppStore);
                }
                else
                if (Application.platform == RuntimePlatform.Android)
                {
                    PacketManager.Instance.SendCSReqIAP(ShopInfo.elementIdx, IAPType.GooglePlay);
                }
#endif
                break;
            default:
                PacketManager.Instance.SendCSReqBuyElement(ShopInfo.elementIdx, _count);
                break;
        }
    }

    public void CountOption(int increment)
    {
        if ((_count + increment) < 1 || (_count + increment >= 10)) { return; }
        _count += increment;
        _quantity.text = _count.ToString();
        _price.text = (ShopInfo.price * _count).ToString();
    }
}