﻿using UnityEngine;
using System;
using System.Collections.Generic;

class ItemManager : Singleton<ItemManager>
{
    private Dictionary<int, ItemElementInfo> _items = new Dictionary<int, ItemElementInfo>();
    private const string _FILE = @"Data/ItemData";

    void Awake()
    {
        LoadFromFile();
    }

    void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        char[] charsToTrim = { ' ', '\t' };

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        ItemElementInfo tempShopInfo = null;

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);

            if (ln.Length == 0) { continue; }

            if (ln[0] == ';')
            {
                // ignore comment line
            }
            else
            {
                tempShopInfo = new ItemElementInfo();
                if (tempShopInfo.Decode(ln) == true)
                {
                    _items[tempShopInfo.iDx] = tempShopInfo;
                }
                else
                {
                    Debug.Log(ln);
                }
            }
        }
    }

    public ItemElementInfo Get(int iDx)
    {
        if (_items.ContainsKey(iDx))
        {
            return _items[iDx];
        }
        else
        {
            return null;
        }
    }
}