﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShopUnit : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup _main;
    [SerializeField]
    private GameObject _backButton;
    [SerializeField]
    private LocalizationUnit _title;
    [SerializeField]
    private ScrollRect itemsTab, rubyTab, goldTab, jokerTab, petTab, cardSkinTab;

    private Dictionary<TabType, RectTransform> _RectTransforms = new Dictionary<TabType, RectTransform>(6);

    private float _itemHeight;

    //private Dictionary<int, ShopItemUnit> _shopItems = new Dictionary<int, ShopItemUnit>();

    public ShopItemUnit itemPrefab;

    void Awake()
    {
        GameControlUnit.Instance.Shop = this;
        _RectTransforms[TabType.Items] = itemsTab.content;
        _RectTransforms[TabType.Ruby] = rubyTab.content;
        _RectTransforms[TabType.Gold] = goldTab.content;
        _RectTransforms[TabType.Joker] = jokerTab.content;
        _RectTransforms[TabType.Pet] = petTab.content;
        _RectTransforms[TabType.CardSkin] = cardSkinTab.content;
        _itemHeight = _RectTransforms[TabType.Items].sizeDelta.y;
    }

    void Start()
    {
        GameControlUnit.Instance.Alarm.ClearNotiAlarm(Option.Shop);

        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void SetDefaultTab()
    {
        _main.GetComponent<Toggle>().isOn = true;
        jokerTab.horizontalNormalizedPosition = 0f;
        itemsTab.horizontalNormalizedPosition = 0f;
        rubyTab.horizontalNormalizedPosition = 0f;
        goldTab.horizontalNormalizedPosition = 0f;
        petTab.horizontalNormalizedPosition = 0f;
        cardSkinTab.horizontalNormalizedPosition = 0f;
    }

    public void ReqUpdate()
    {
        PacketManager.Instance.SendCSReqShopList();
    }

    public void Clear()
    {
        foreach (RectTransform itemList in _RectTransforms.Values)
        {
            foreach (Transform child in itemList) { Destroy(child.gameObject); }
            itemList.sizeDelta = new Vector2(0f, _itemHeight);
        }
    }

    public void AddItem(ShopListInfo itemInfo)
    {
        //GameObject item = Instantiate(itemPrefab) as GameObject;
        //item.GetComponent<RectTransform>().SetParent(_RectTransforms[itemInfo.tabID]);
        //item.GetComponent<RectTransform>().localScale = Vector3.one;
        //ShopItemUnit tempUnit = item.GetComponent<ShopItemUnit>();
        //tempUnit.ShopInfo = itemInfo;
        //tempUnit.needUpdate = true;
        ShopItemUnit item = Instantiate(itemPrefab);
        item.transform.SetParent(_RectTransforms[itemInfo.tabID], false);
        item.ShopInfo = itemInfo;
        item.needUpdate = true;

        Vector2 itemSize = _RectTransforms[itemInfo.tabID].sizeDelta;
        itemSize.x += 465f;
        _RectTransforms[itemInfo.tabID].sizeDelta = itemSize;
    }

    public void MainTab(bool isOn)
    {
        _title.UpdateUI("SHOP");
        _backButton.SetActive(!isOn);
        _main.alpha = isOn ? 1 : 0;
        _main.blocksRaycasts = isOn;

        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.Close); }
    }

    public void JokerTab(bool isOn)
    {
        _title.UpdateUI("JOKER");
        jokerTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton); }
    }

    public void ItemsTab(bool isOn)
    {
        _title.UpdateUI("ITEM");
        itemsTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton); }
    }

    public void RubyTab(bool isOn)
    {
        _title.UpdateUI("MOON_RUBY");
        rubyTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton); }
    }

    public void GoldTab(bool isOn)
    {
        _title.UpdateUI("GOLD");
        goldTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton); }
    }

    public void PetTab(bool isOn)
    {
        _title.UpdateUI("PET");
        petTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton); }
    }

    public void CardSkinTab(bool isOn)
    {
        _title.UpdateUI("CARD_SKIN");
        cardSkinTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton); }
    }
}