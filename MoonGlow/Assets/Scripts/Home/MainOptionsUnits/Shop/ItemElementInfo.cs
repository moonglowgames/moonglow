﻿using System;

public class ItemElementInfo
{
    public int iDx;
    public TabType tabType;
    public int enabled;
    public int rqrLevel;
    public string name;
    public PayType payType;
    public int quantity;
    public double price;
    public SpecialType specialType;
    public double salePrice;
    public int baseCard;
    public SpellType option;
    public string description;
    public string shopDescription;

    const int DECODE_PARSE_COUNT = 13;
    public char[] charsToTrim = { ' ', '\t' };

    public bool Decode(string cvsLineData_)
    {
        bool retvalue = false;

        if (cvsLineData_ != string.Empty)
        {
            cvsLineData_.Trim(charsToTrim);

            switch (cvsLineData_[0])
            {
                case ';':
                    break;
                default:
                    string[] parsings = cvsLineData_.Split(',');
                    int parscount = 0;
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        try
                        {
                            this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.tabType = (TabType)Enum.Parse(typeof(TabType), parsings[parscount++].Trim(charsToTrim));
                            this.enabled = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.rqrLevel = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            parscount++;
                            //this.name = parsings[parscount++].Trim(charsToTrim);
                            this.payType = (PayType)Enum.Parse(typeof(PayType),parsings[parscount++].Trim(charsToTrim));
                            this.quantity = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.price = Convert.ToDouble(parsings[parscount++].Trim(charsToTrim)); ;
                            this.specialType = (SpecialType)Enum.Parse(typeof(SpecialType),parsings[parscount++].Trim(charsToTrim)); ;
                            this.salePrice = Convert.ToDouble(parsings[parscount++].Trim(charsToTrim)); ;
                            this.baseCard = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.option = (SpellType)Enum.Parse(typeof(SpellType), parsings[parscount++].Trim(charsToTrim));
                            //this.description = parsings[parscount++].Trim(charsToTrim);

                            this.name = ItemLocalizationManager.Instance.GetName(this.iDx);
                            this.name = this.name.Replace('^', ',');
                            this.description = ItemLocalizationManager.Instance.GetDescription(this.iDx);
                            this.description = this.description.Replace("@", System.Environment.NewLine);
                            this.description = this.description.Replace('^', ',');
                            this.shopDescription = ItemLocalizationManager.Instance.GetShopDescription(this.iDx);
                            this.shopDescription = this.shopDescription.Replace("@", System.Environment.NewLine);
                            this.shopDescription = this.shopDescription.Replace('^', ',');
                            retvalue = true;
                        }
                        catch (Exception /* e */ )
                        {
                            retvalue = false;
                        }
                    }
                    break;
            }
        }
        return retvalue;
    }
}