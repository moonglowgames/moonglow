﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class CardSkinManager : Singleton<CardSkinManager>
{
    private Dictionary<int, Color32[]> _dict = new Dictionary<int, Color32[]>();
    private const string _FILE = @"Data/CardSkinData";
    private const int DECODE_PARSE_COUNT = 5;

    void Awake()
    {
        LoadFromFile();
    }

    public void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        char[] charsToTrim = { ' ', '\t' };

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);
            if (ln[0] == ';') { continue; }
            else
            {
                if (ln != string.Empty)
                {
                    string[] parsings = ln.Split(',');
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        int iDx = Convert.ToInt32(parsings[0].Trim(charsToTrim));
                        _dict[iDx] = new Color32[DECODE_PARSE_COUNT - 1];
                        for (int i = 0; i < DECODE_PARSE_COUNT - 1; i++)
                        {
                            string[] colors = parsings[i + 1].Split('^');
                            _dict[iDx][i] = new Color32(Convert.ToByte(colors[0]), Convert.ToByte(colors[1]), Convert.ToByte(colors[2]), Convert.ToByte(colors[3]));
                        }
                    }
                }
            }
        }
    }

    public Color32 Get(int iDx, Resource body)
    {
        if (_dict.ContainsKey(iDx))
        {
            return _dict[iDx][(int)body];
        }
        else
        {
            Debug.LogError("Card skin error: " + iDx.ToString() + "Resource: " + body.ToString());
            return Color.white;
        }
    }
}