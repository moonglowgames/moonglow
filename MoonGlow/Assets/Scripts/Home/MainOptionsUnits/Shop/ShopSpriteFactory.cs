﻿using UnityEngine;
using System.Collections.Generic;

public class ShopSpriteFactory : Singleton<ShopSpriteFactory>
{
    private Sprite _errorSprite;
    private Sprite[] _priceType;
    private Sprite[] _items;
    private Sprite[] _joker;
    private Sprite[] _gold;
    private Sprite[] _ruby;
    private Dictionary<int, Sprite> _pet = new Dictionary<int, Sprite>(4);
    private Sprite[] _cardSkin;

    void Awake()
    {
        _errorSprite = Resources.Load<Sprite>("Images/Error");
        _priceType = Resources.LoadAll<Sprite>("Images/Shop/PriceType");
        _items = Resources.LoadAll<Sprite>("Images/Shop/Items");
        _joker = Resources.LoadAll<Sprite>("Images/Shop/JokerFace");
        _gold = Resources.LoadAll<Sprite>("Images/Shop/Gold");
        _ruby = Resources.LoadAll<Sprite>("Images/Shop/Ruby");
        _cardSkin = Resources.LoadAll<Sprite>("Images/Shop/CardSkin");
        //temp
        _pet[601] = Resources.Load<Sprite>("Images/Shop/Pet/601_TamBelle");
        _pet[608] = Resources.Load<Sprite>("Images/Shop/Pet/608_Mr.Park");
        _pet[610] = Resources.Load<Sprite>("Images/Shop/Pet/610_Drago");
        _pet[616] = Resources.Load<Sprite>("Images/Shop/Pet/616_Succubeez");
    }

    public Sprite Get(SpriteType type, int iDx)
    {
        switch (type)
        {
            case SpriteType.ShopPriceType:
                return GetShopPriceTypeSprite(iDx);
            case SpriteType.ShopItem:
                return GetShopItemSprite(iDx);
            case SpriteType.ShopJoker:
                return GetShopJokerSprite(iDx);
            case SpriteType.ShopGold:
                return GetShopGoldSprite(iDx);
            case SpriteType.ShopRuby:
                return GetShopRubySprite(iDx);
            case SpriteType.Pet:
                return GetShopPetSprite(iDx);
            case SpriteType.CardSkin:
                return GetShopCardSkinSprite(iDx);
            default:
                Debug.LogError("Sprite type mismatch ShopSpriteFactory.cs");
                return null;
        }
    }

    Sprite GetShopPriceTypeSprite(int iDx)
    {
        if (iDx > 2) { iDx = 2; }
        if (iDx >= 0 && iDx < _priceType.Length)
        {
            return _priceType[iDx];
        }
        else
        {
            Debug.LogError("Shop price index outside boundaries " + iDx.ToString() + " ShopSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetShopItemSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _items.Length)
        {
            return _items[iDx];
        }
        else
        {
            Debug.LogError("Shop item index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetShopJokerSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _joker.Length)
        {
            return _joker[iDx];
        }
        else
        {
            Debug.LogError("Shop joker index outside boundaries " + iDx.ToString() + " ShopSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetShopGoldSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _gold.Length)
        {
            return _gold[iDx];
        }
        else
        {
            Debug.LogError("Shop gold index outside boundaries " + iDx.ToString() + " ShopSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetShopRubySprite(int iDx)
    {
        iDx /= 2;
        if (iDx >= 0 && iDx < _ruby.Length)
        {
            return _ruby[iDx];
        }
        else
        {
            Debug.LogError("Shop ruby index outside boundaries " + iDx.ToString() + " ShopSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetShopPetSprite(int iDx)
    {
        if (_pet.ContainsKey(iDx))
        {
            return _pet[iDx];
        }
        else
        {
            Debug.LogError("Shop pet index outside boundaries " + iDx.ToString() + " ShopSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetShopCardSkinSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _cardSkin.Length)
        {
            return _cardSkin[iDx];
        }
        else
        {
            Debug.LogError("Shop card skin index outside boundaries " + iDx.ToString() + " ShopSpriteFactory.cs");
            return _errorSprite;
        }
    }
}