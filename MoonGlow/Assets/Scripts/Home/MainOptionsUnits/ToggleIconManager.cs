﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ToggleIconManager : MonoBehaviour
{
    [SerializeField]
    private Image _icon;
    [SerializeField]
    private Color _onColor, _offColor;

    void Awake()
    {
        if (_icon == null) { _icon = GetComponent<Image>(); }
    }

    public void UpdateIcon(bool state)
    {
        _icon.color = state ? _onColor : _offColor;
    }
}