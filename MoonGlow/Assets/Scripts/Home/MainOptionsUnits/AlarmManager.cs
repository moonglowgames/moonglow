﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;

public class AlarmManager : MonoBehaviour
{
    [SerializeField]
    private Image[] _notifications;

    void Awake()
    {
        GameControlUnit.Instance.Alarm = this;

        if (GameControlUnit.Instance.mailAlarm != null) { ShowNotiAlarm(Option.Mail, GameControlUnit.Instance.mailAlarm); }
        if (GameControlUnit.Instance.friendAlarm != null) { ShowNotiAlarm(Option.Friend, GameControlUnit.Instance.friendAlarm); }
        if (GameControlUnit.Instance.shopAlarm != null) { ShowNotiAlarm(Option.Shop, GameControlUnit.Instance.shopAlarm); }
        if (GameControlUnit.Instance.newsAlarm != null) { ShowNotiAlarm(Option.News, GameControlUnit.Instance.newsAlarm); }
    }

    public void ClearNotiAlarm(Option option)
    {
        switch (option)
        {
            case Option.News:
                EncryptedPlayerPrefs.SetString(PlayerPrefsKey.newsAlarm, DateTime.Now.ToString());
                _notifications[0].gameObject.SetActive(false);
                break;
            case Option.Mail:
                _notifications[1].gameObject.SetActive(false);
                break;
            case Option.Friend:
                _notifications[2].gameObject.SetActive(false);
                break;
            case Option.Shop:
                EncryptedPlayerPrefs.SetString(PlayerPrefsKey.shopAlarm, DateTime.Now.ToString());
                _notifications[3].gameObject.SetActive(false);
                break;
        }
    }

    public void ShowNotiAlarm(Option option, string alarm)
    {
        switch (option)
        {
            case Option.News:
                DateTime serverDate = DateTime.Parse(alarm);
                if (serverDate > DateTime.Parse(EncryptedPlayerPrefs.GetString(PlayerPrefsKey.newsAlarm, "2016-06-01")))
                {
                    _notifications[0].gameObject.SetActive(true);
                }
                break;
            case Option.Mail:
                _notifications[1].GetComponentInChildren<Text>().text = alarm;
                _notifications[1].gameObject.SetActive(alarm != "0");
                break;
            case Option.Friend:
                _notifications[2].GetComponentInChildren<Text>().text = alarm;
                _notifications[2].gameObject.SetActive(alarm != "0");
                break;
            case Option.Shop:
                serverDate = DateTime.Parse(alarm);
                if (serverDate > DateTime.Parse(EncryptedPlayerPrefs.GetString(PlayerPrefsKey.shopAlarm, "2016-06-01")))
                {
                    _notifications[3].gameObject.SetActive(true);
                }
                break;
        }
    }
}