﻿#pragma warning disable 649

using UnityEngine;

public class MailItemUnit : MyPageItemUnit
{
    [SerializeField]
    private LocalizationUnit _receive;

    public Mail mail { get; set; }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        _name.text = mail.subject;
        _description.text = mail.content;
        _receive.UpdateUI(mail.isGot ? "RECEIVED_MAIL" : "RECEIVE_MAIL");

        switch (mail.itemIdx1)
        {
            case 2:
                _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopGold, 1);
                if (mail.itemCount1 > 1)
                {
                    _quantity.text = mail.itemCount1.ToString();
                    _quantity.transform.parent.gameObject.SetActive(true);
                }
                break;
            case 3:
                _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopRuby, 3);
                if (mail.itemCount1 > 1)
                {
                    _quantity.text = mail.itemCount1.ToString();
                    _quantity.transform.parent.gameObject.SetActive(true);
                }
                break;
            default:
                if (mail.itemIdx1 < 200)
                {
                    _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, mail.itemIdx1 % 100 - 1);
                    if (mail.itemCount1 > 1)
                    {
                        _quantity.text = "x" + mail.itemCount1.ToString();
                        _quantity.transform.parent.gameObject.SetActive(true);
                    }
                }
                else if (mail.itemIdx1 < 700)
                {
                    _image.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, mail.itemIdx1 % 100 - 1);
                }
                else if (mail.itemIdx1 < 900)
                {
                    _image.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, mail.itemIdx1);
                }
                break;
        }
        _image.enabled = true;
    }

    override public void UseAction()
    {
        PacketManager.Instance.SendCSReqGetItem(mail.mailIdx);
    }
}