﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class MailUnit : MonoBehaviour
{
    [SerializeField]
    private RectTransform _mailListRectTransform;
    [SerializeField]
    private Text _title;
    private float _mailBoxWidth;

    public string Title
    {
        set { _title.text = value; }
    }

    public MailItemUnit mailPrefab;

    void Awake ()
    {
        GameControlUnit.Instance.Mail = this;
        _mailBoxWidth = _mailListRectTransform.sizeDelta.x;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void Clear()
    {
        foreach (Transform child in _mailListRectTransform) { Destroy(child.gameObject); }
        _mailListRectTransform.sizeDelta = new Vector2(_mailBoxWidth, 0f);
    }

    public void AddMail(Mail mail)
    {
        MailItemUnit mailItem = Instantiate(mailPrefab);
        mailItem.transform.SetParent(_mailListRectTransform, false);
        mailItem.mail = mail;
        mailItem.needUpdate = true;

        Vector2 mailBoxSize = _mailListRectTransform.sizeDelta;
        mailBoxSize.y += 200f;
        _mailListRectTransform.sizeDelta = mailBoxSize;
    }

    public void ReceiveAll()
    {
        PacketManager.Instance.SendCSReqGetAllItems();
        SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton);
    }

    public void RemoveOld()
    {
        PacketManager.Instance.SendCSReqRemoveOld();
        SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton);
    }

    public void ReqUpdate()
    {
        Clear();
        PacketManager.Instance.SendCSReqMailList();
    }
}