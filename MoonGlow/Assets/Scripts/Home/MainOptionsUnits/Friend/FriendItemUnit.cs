﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;

public class FriendItemUnit : MonoBehaviour
{
    [SerializeField]
    private Image _avatar;
    [SerializeField]
    private Text _name, _date;
    [SerializeField]
    private Button _receiveButton, _sendButton, _actionButton;
    [SerializeField]
    private LocalizationUnit _actionButtonText;

    public Friend friend { get; set; }
    public FriendTabType tabType = FriendTabType.MyFriends;

    public bool editMode;
    public bool needUpdate = true;

	void Update ()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        _name.text = "<color=#DCAC00FF>Lv. " + friend.level.ToString() + "</color> " + friend.friendNick;
        _date.text = DateTime.Parse(friend.lastLoginDate).ToLocalTime().ToString("f");

        _avatar.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatarMini, friend.faceIdx % 100);

        switch (tabType)
        {
            case FriendTabType.MyFriends:
                _receiveButton.gameObject.SetActive(!editMode);
                _sendButton.gameObject.SetActive(!editMode);
                _actionButton.gameObject.SetActive(editMode);
                _actionButtonText.UpdateUI("DEL_FRIEND");

                _receiveButton.gameObject.SetActive(friend.enableGetItem);
                //_receiveButton.GetComponentInChildren<LocalizationUnit>().UpdateUI();
                _sendButton.interactable = friend.enableSendItem;
                _sendButton.GetComponentInChildren<LocalizationUnit>().UpdateUI();
                break;
            case FriendTabType.RequestFriends:
                _receiveButton.gameObject.SetActive(false);
                _sendButton.gameObject.SetActive(false);
                _actionButton.gameObject.SetActive(true);

                switch (friend.friendType)
                {
                    case FriendType.Accepting:
                        _actionButton.interactable = true;
                        _actionButtonText.UpdateUI("ACCEPT_FRIEND");
                        break;
                    case FriendType.Requesting:
                        _actionButton.interactable = false;
                        _actionButtonText.UpdateUI("REQ");
                        break;
                }

                break;
            case FriendTabType.RecommendFriends:
                _receiveButton.gameObject.SetActive(false);
                _sendButton.gameObject.SetActive(false);
                _actionButton.gameObject.SetActive(true);
                _actionButtonText.UpdateUI("REQ_FRIEND");
                break;
        }
    }

    public void ToggleEditMode(bool state)
    {
        _receiveButton.gameObject.SetActive(!state && friend.enableGetItem);
        _sendButton.gameObject.SetActive(!state);
        _actionButton.gameObject.SetActive(state);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void UseAction()
    {
        switch (tabType)
        {
            case FriendTabType.MyFriends:
                PacketManager.Instance.SendCSReqDeleteFriend(tabType, friend.iDx);
                break;
            case FriendTabType.RequestFriends:
                PacketManager.Instance.SendCSReqAcceptFriend(tabType, friend.iDx);
                break;
            case FriendTabType.RecommendFriends:
                PacketManager.Instance.SendCSReqRequestFriend(tabType, friend.iDx);
                break;
        }
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void ReceiveGift()
    {
        PacketManager.Instance.SendCSReqGetGift(tabType, friend.iDx);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void SendGift()
    {
        PacketManager.Instance.SendCSReqSendGift(tabType, friend.iDx);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void ReqProfile()
    {
        PacketManager.Instance.SendCSReqPlayerProfile(friend.friendNick);
    }
}