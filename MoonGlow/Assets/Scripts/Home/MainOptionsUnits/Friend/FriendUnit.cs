﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class FriendUnit : MonoBehaviour
{
    [SerializeField]
    private Toggle _myFriendsToggle, _myFriendsEdit;
    [SerializeField]
    private ScrollRect myFriendsTab, requestFriendsTab, recommendFriendsTab;
    [SerializeField]
    private Text _title;
    private RectTransform _myFriendsListRectTransform,_requestFriendsListRectTransform,_recommendFriendsListRectTransform;

    private FriendTabType _tabType;
    private float _friendListWidth;
    private bool _needCSReq = true;

    public FriendItemUnit friendPrefab;

    public string Title
    {
        set { _title.text = value; }
    }

    void Awake()
    {
        GameControlUnit.Instance.Friend = this;

        _myFriendsListRectTransform = myFriendsTab.content;
        _requestFriendsListRectTransform = requestFriendsTab.content;
        _recommendFriendsListRectTransform = recommendFriendsTab.content;
        _friendListWidth = _myFriendsListRectTransform.sizeDelta.x;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void SetDefaultTab()
    {
        _myFriendsToggle.isOn = true;
        _tabType = FriendTabType.MyFriends;
        myFriendsTab.verticalNormalizedPosition = 1f;
        requestFriendsTab.verticalNormalizedPosition = 1f;
        recommendFriendsTab.verticalNormalizedPosition = 1f;
    }

    public void ClearFriends(FriendTabType tabType)
    {
        switch (tabType)
        {
            case FriendTabType.MyFriends:
                foreach (Transform child in _myFriendsListRectTransform) { Destroy(child.gameObject); }
                _myFriendsListRectTransform.sizeDelta = new Vector2(_friendListWidth, 0f);
                break;
            case FriendTabType.RequestFriends:
                foreach (Transform child in _requestFriendsListRectTransform) { Destroy(child.gameObject); }
                _requestFriendsListRectTransform.sizeDelta = new Vector2(_friendListWidth, 0f);
                break;
            case FriendTabType.RecommendFriends:
                foreach (Transform child in _recommendFriendsListRectTransform) { Destroy(child.gameObject); }
                _recommendFriendsListRectTransform.sizeDelta = new Vector2(_friendListWidth, 0f);
                break;
        }
    }

    public void AddFriend(Friend friend, FriendTabType tabType)
    {
        FriendItemUnit friendItem = Instantiate(friendPrefab);
        friendItem.tabType = tabType;
        friendItem.friend = friend;
        friendItem.editMode = _myFriendsEdit.isOn;
        friendItem.needUpdate = true;

        Vector2 friendListSize;
        switch (tabType)
        {
            case FriendTabType.MyFriends:
                friendItem.transform.SetParent(_myFriendsListRectTransform, false);
                friendListSize = _myFriendsListRectTransform.sizeDelta;
                friendListSize.y += 198f;
                _myFriendsListRectTransform.sizeDelta = friendListSize;
                break;
            case FriendTabType.RequestFriends:
                friendItem.transform.SetParent(_requestFriendsListRectTransform, false);
                friendListSize = _requestFriendsListRectTransform.sizeDelta;
                friendListSize.y += 198f;
                _requestFriendsListRectTransform.sizeDelta = friendListSize;
                break;
            case FriendTabType.RecommendFriends:
                friendItem.transform.SetParent(_recommendFriendsListRectTransform, false);
                friendListSize = _recommendFriendsListRectTransform.sizeDelta;
                friendListSize.y += 198f;
                _recommendFriendsListRectTransform.sizeDelta = friendListSize;
                break;
        }
    }

    public void ReqUpdate()
    {
        if (_needCSReq)
        {
            ClearFriends(FriendTabType.MyFriends);
            PacketManager.Instance.SendCSReqFriendList(FriendTabType.MyFriends);
        }
    }

    public void Edit(bool isOn)
    {
        foreach (FriendItemUnit friend in _myFriendsListRectTransform.GetComponentsInChildren<FriendItemUnit>())
        {
            friend.ToggleEditMode(isOn);
        }
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void ReceiveAllGifts()
    {
        PacketManager.Instance.SendCSReqGetAllGifts(FriendTabType.MyFriends);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void SendAllGifts()
    {
        PacketManager.Instance.SendCSReqSendAllGifts(FriendTabType.MyFriends);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void MyFriendsTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = FriendTabType.MyFriends;
            ClearFriends(_tabType);
            PacketManager.Instance.SendCSReqFriendList(_tabType);
            _myFriendsEdit.gameObject.SetActive(true);
            if (_myFriendsEdit.isOn) { _myFriendsEdit.isOn = false; }
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = true;
        }
        myFriendsTab.gameObject.SetActive(isOn);
    }

    public void RequestFriendsTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = FriendTabType.RequestFriends;
            ClearFriends(_tabType);
            PacketManager.Instance.SendCSReqFriendList(_tabType);
            _myFriendsEdit.gameObject.SetActive(false);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        requestFriendsTab.gameObject.SetActive(isOn);
    }

    public void RecommendFriendsTab(bool isOn)
    {
        if (isOn)
        {
            _tabType = FriendTabType.RecommendFriends;
            ClearFriends(_tabType);
            PacketManager.Instance.SendCSReqFriendList(_tabType);
            _myFriendsEdit.gameObject.SetActive(false);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        recommendFriendsTab.gameObject.SetActive(isOn);
    }
}