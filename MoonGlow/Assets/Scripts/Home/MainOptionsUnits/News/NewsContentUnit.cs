﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class NewsContentUnit : MonoBehaviour
{
    private RectTransform _transform;
    private RawImage _image;
    private string _url = string.Empty;

    public bool needUpdate = true;

	void Awake ()
    {
        _transform = GetComponent<RectTransform>();
    }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        _image = this.gameObject.AddComponent<RawImage>();
        _image.enabled = false;
        //string url = "http://www.moonglowgames.com/images/main_logo_moonglow-u246.png";

        if (_url != string.Empty)
        {
            Debug.Log("Start downloading image from: " + _url);
            StartCoroutine(downloadImg(_url));
        }
    }

    //    StartCoroutine(CheckConnectionToMasterServer());
    private IEnumerator CheckConnectionToMasterServer()
    {
        Ping pingMasterServer = new Ping("192.168.0.16");
        Debug.Log(pingMasterServer.ip);
        float startTime = Time.time;
        while (!pingMasterServer.isDone && Time.time < startTime + 5.0f)
        {
            yield return new WaitForSeconds(0.1f);
        }
        if (pingMasterServer.isDone)
        {
            //new InternetConnectionMessage();
        }
        else
        {
            //new NoInternetConnectionMessage();
        }
    }

    IEnumerator downloadImg(string url)
    {
        Texture2D texture = new Texture2D(1, 1);
        WWW www = new WWW(url);
        yield return www;
        www.LoadImageIntoTexture(texture);

        _image.texture = texture;
        _image.enabled = true;
        _transform.pivot = new Vector2(0.5f, 1f);
        _transform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0f, 0f);
        _transform.sizeDelta = new Vector2(1162f, texture.height);
    }

    public void Initialize(RectTransform parent, News news)
    {
        _transform.SetParent(parent, false);

        switch (news.newsType)
        {
            case 1:
                _url = news.url;
                needUpdate = true;
                break;
            case 2:
                Debug.Log(news.url);
                break;
        }
    }
}