﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
    
public class NewsUnit : MonoBehaviour
{
    [SerializeField]
    private Text _pageCounter, _title, _loading;
    [SerializeField]
    private RectTransform _frame;
    [SerializeField]
    private ScrollRect _scroll;
    private int _currentPage;

    private NewsContentUnit[] _news;

    public List<News> newsList { get; set; }

    void Awake()
    {
        //transform.FindChild("Canvas").GetComponent<RectTransform>().sizeDelta = LayoutUnit.GetSize();
        GameControlUnit.Instance.News = this;
        PacketManager.Instance.SendCSReqNews();
    }

    void Start()
    {
        GameControlUnit.Instance.Alarm.ClearNotiAlarm(Option.News);

        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void ReqUpdate()
    {
        PacketManager.Instance.SendCSReqNews();
    }

    public void ClearNews()
    {
        foreach (Transform child in _frame) { Destroy(child.gameObject); }
        _loading.enabled = true;
    }

    public void UpdateNews()
    {
        ClearNews();
        //int i = 0;
        //foreach (var news in newsList)
        //{
        //    i++;
        //    new GameObject("NewsPage" + i.ToString()).AddComponent<NewsContentUnit>().Initialize(_frame, news);
        //}
        if (newsList.Count > 0)
        {
            _news = new NewsContentUnit[newsList.Count];
            for (int j = 0; j < newsList.Count; j++)
            {
                _news[j] = new GameObject("NewsPage" + j.ToString()).AddComponent<NewsContentUnit>();
                _news[j].Initialize(_frame, newsList[j]);
                //_loading.enabled = false;
            }

            //_news = _frame.GetComponentsInChildren<NewsContentUnit>();
            _currentPage = 0;
            SetPage(_currentPage);
        }
    }

    void SetPage(int page)
    {
        _title.text = newsList[page].title;
        _pageCounter.text = "<color=#ffa500ff>" + (page + 1).ToString() + "</color>/" + newsList.Count.ToString();// + " Page";

        foreach (var News in _news) { News.gameObject.SetActive(false); }
        _news[page].gameObject.SetActive(true);
        _scroll.content = _news[page].GetComponent<RectTransform>();
        _scroll.verticalNormalizedPosition = 1f;
    }

    public void TurnPage(int forward)
    {
        if (newsList == null) { return; }
        if ((_currentPage + forward) < 0 || (_currentPage + forward >= newsList.Count)) { return; }
        _currentPage += forward;
        SetPage(_currentPage);
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
    }
}