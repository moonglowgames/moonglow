﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;

public class ChannelItemUnit : MonoBehaviour
{
    [SerializeField]
    private Image _image;
    [SerializeField]
    private Text _name, _description, _total;
    [SerializeField]
    private Button _joinButton;

    public bool needUpdate = true;

    public Chanel channelInfo { get; set; }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        _image.sprite = HomeSpriteFactory.Instance.Get(SpriteType.RankChannel, (int)channelInfo.type);
        _name.text = channelInfo.name;
        _description.text = "Lv. " + channelInfo.minLevel.ToString() + "~" + channelInfo.maxLevel.ToString() + " " + channelInfo.description + System.Environment.NewLine;
        //_description.text += channelInfo.startDate + " ~ " + channelInfo.endDate;
        //_description.text += Convert.ToDateTime(channelInfo.startDate).ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss") + " ~ " + Convert.ToDateTime(channelInfo.endDate).ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
        _description.text += string.Format("{0:f} ~ {1:f}", DateTime.Parse(channelInfo.startDate).ToLocalTime(), DateTime.Parse(channelInfo.endDate).ToLocalTime());
        _total.text = LocalizationManager.Instance.Get("TOTAL") + " " + channelInfo.currentCount + "/" + channelInfo.maxCount;
        if (channelInfo.enable)
        {
            _joinButton.interactable = !channelInfo.isJoin;
            _joinButton.GetComponentInChildren<LocalizationUnit>().UpdateUI(channelInfo.isJoin ? "JOINED" : "JOIN");
        }
        else
        {
            _joinButton.gameObject.SetActive(false);
        }
        
    }

    public void JoinClick()
    {
        PacketManager.Instance.SendCSReqJoinChanel(channelInfo.chanelIdx);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}