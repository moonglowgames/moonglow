﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class RewardUnit : MonoBehaviour
{
    [SerializeField]
    private Text _rank;
    [SerializeField]
    private Image[] _itemIcon;
    [SerializeField]
    private Text[] _itemCount;

    public bool needUpdate = true;

    public Reward rewardInfo { get; set; }
    
    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        switch (rewardInfo.rankType)
        {
            case RankType.Count:
                switch (rewardInfo.rank)
                {
                    case 1:
                        _rank.text = "<color=#BD3D3DFF>1st</color>";
                        break;
                    case 2:
                        _rank.text = "<color=#BD3D3DFF>2nd</color>";
                        break;
                    case 3:
                        _rank.text = "<color=#BD3D3DFF>3rd</color>";
                        break;
                }
                break;
            case RankType.Percent:
                _rank.text = rewardInfo.rank.ToString() + "%";
                break;
        }

        if (rewardInfo.itemIdx1 == 0)
        {
            _itemIcon[0].enabled = false;
            _itemCount[0].enabled = false;
        }
        else
        {
            switch (rewardInfo.itemIdx1)
            {
                case 2:
                    _itemIcon[0].sprite = HomeSpriteFactory.Instance.Get(SpriteType.Reward, 0);
                    _itemCount[0].text = rewardInfo.itemCount1.ToString();
                    break;
                case 3:
                    _itemIcon[0].sprite = HomeSpriteFactory.Instance.Get(SpriteType.Reward, 1);
                    _itemCount[0].text = rewardInfo.itemCount1.ToString();
                    break;
                default:
                    if (rewardInfo.itemIdx1 < 200)
                    {
                        _itemIcon[0].sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, rewardInfo.itemIdx1 % 100 - 1);
                    }
                    else if (rewardInfo.itemIdx1 < 700)
                    {
                        _itemIcon[0].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, rewardInfo.itemIdx1 % 100 - 1);
                    }
                    else if (rewardInfo.itemIdx1 < 900)
                    {
                        _itemIcon[0].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, rewardInfo.itemIdx1);
                    }
                    _itemCount[0].text = rewardInfo.itemCount1 > 1 ? "x" + rewardInfo.itemCount1.ToString() : string.Empty;
                    break;
            }
            _itemIcon[0].enabled = true;
            _itemCount[0].enabled = true;
        }

        if (rewardInfo.itemIdx2 == 0)
        {
            _itemIcon[1].enabled = false;
            _itemCount[1].enabled = false;
        }
        else
        {
            switch (rewardInfo.itemIdx2)
            {
                case 2:
                    _itemIcon[1].sprite = HomeSpriteFactory.Instance.Get(SpriteType.Reward, 0);
                    _itemCount[1].text = rewardInfo.itemCount2.ToString();
                    break;
                case 3:
                    _itemIcon[1].sprite = HomeSpriteFactory.Instance.Get(SpriteType.Reward, 1);
                    _itemCount[1].text = rewardInfo.itemCount2.ToString();
                    break;
                default:
                    if (rewardInfo.itemIdx2 < 200)
                    {
                        _itemIcon[1].sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, rewardInfo.itemIdx2 % 100 - 1);
                    }
                    else if (rewardInfo.itemIdx2 < 700)
                    {
                        _itemIcon[1].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, rewardInfo.itemIdx2 % 100 - 1);
                    }
                    else if (rewardInfo.itemIdx2 < 900)
                    {
                        _itemIcon[1].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, rewardInfo.itemIdx2);
                    }
                    _itemCount[1].text = rewardInfo.itemCount2 > 1 ? "x" + rewardInfo.itemCount2.ToString() : string.Empty;
                    break;
            }
            _itemIcon[1].enabled = true;
            _itemCount[1].enabled = true;
        }

        if (rewardInfo.itemIdx3 == 0)
        {
            _itemIcon[2].enabled = false;
            _itemCount[2].enabled = false;
        }
        else
        {
            switch (rewardInfo.itemIdx3)
            {
                case 2:
                    _itemIcon[2].sprite = HomeSpriteFactory.Instance.Get(SpriteType.Reward, 0);
                    _itemCount[2].text = rewardInfo.itemCount3.ToString();
                    break;
                case 3:
                    _itemIcon[2].sprite = HomeSpriteFactory.Instance.Get(SpriteType.Reward, 1);
                    _itemCount[2].text = rewardInfo.itemCount3.ToString();
                    break;
                default:
                    if (rewardInfo.itemIdx3 < 200)
                    {
                        _itemIcon[2].sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, rewardInfo.itemIdx3 % 100 - 1);
                    }
                    else if (rewardInfo.itemIdx3 < 700)
                    {
                        _itemIcon[2].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, rewardInfo.itemIdx3 % 100 - 1);
                    }
                    else if (rewardInfo.itemIdx3 < 900)
                    {
                        _itemIcon[2].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, rewardInfo.itemIdx3);
                    }
                    _itemCount[2].text = rewardInfo.itemCount3 > 1 ? "x" + rewardInfo.itemCount3.ToString() : string.Empty;
                    break;
            }
            _itemIcon[2].enabled = true;
            _itemCount[2].enabled = true;
        }
    }
}