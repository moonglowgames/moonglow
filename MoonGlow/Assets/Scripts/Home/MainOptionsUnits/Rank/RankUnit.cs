﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class RankUnit : MonoBehaviour
{
    [SerializeField]
    private Toggle _defaultToggle;
    [SerializeField]
    private ChannelUnit _channel;
    [SerializeField]
    private RankListUnit _rankList;
    [SerializeField]
    private MyRankUnit _myRank;

    private bool _needCSReq = true;

    public ChannelUnit Channel { get { return _channel; } }
    public RankListUnit RankList { get { return _rankList; } }
    public MyRankUnit MyRank { get { return _myRank; } }

    void Awake()
    {
        GameControlUnit.Instance.Rank = this;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void SetDefaultTab()
    {
        _defaultToggle.isOn = true;
        //channelTab.transform.FindChild("ChannelList").GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        //rankListTab.transform.FindChild("RankList").GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        //myRankTab.transform.FindChild("myRankTab").GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
    }

    public void ReqUpdate()
    {
        if (_needCSReq)
        {
            //PacketManager.Instance.SendCSReqRewardList();
            PacketManager.Instance.SendCSReqChanelList();
        }
    }

    public void ChannelTab(bool isOn)
    {
        if (isOn)
        {
            _channel.ClearChannelList();
            PacketManager.Instance.SendCSReqChanelList();
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = true;
        }
        _channel.gameObject.SetActive(isOn);
    }

    public void RankListTab(bool isOn)
    {
        if (isOn)
        {
            _rankList.ClearRankList();
            PacketManager.Instance.SendCSReqRankList();
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        _rankList.gameObject.SetActive(isOn);
    }

    public void MyRankTab(bool isOn)
    {
        if (isOn)
        {
            //_myRank.ClearRewards();
            //PacketManager.Instance.SendCSReqRewardList();
            _myRank.ClearRankList();
            PacketManager.Instance.SendCSReqMyRankInfo();
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        _myRank.gameObject.SetActive(isOn);
    }

    public void HelpButton()
    {
        HomeUIManagerUnit.Instance.HelpBookOption(7);
    }
}