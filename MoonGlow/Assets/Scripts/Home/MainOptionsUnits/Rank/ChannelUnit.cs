﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class ChannelUnit : MonoBehaviour
{
    [SerializeField]
    private Text _title;
    [SerializeField]
    private RectTransform _channelListRectTransform;
    private float _channelListWidth;

    public string ChanelName
    {
        get { return _title.text; }
        set { _title.text = value; }
    }

    public ChannelItemUnit channelPrefab;

    void Awake()
    {
        _channelListWidth = _channelListRectTransform.sizeDelta.x;
    }

    public void ClearChannelList()
    {
        foreach (Transform child in _channelListRectTransform) { Destroy(child.gameObject); }
        _channelListRectTransform.sizeDelta = new Vector2(_channelListWidth, 0f);
    }

    public void AddChannelItem(Chanel channel)
    {
        ChannelItemUnit channelItem = Instantiate(channelPrefab);
        channelItem.transform.SetParent(_channelListRectTransform, false);
        channelItem.channelInfo = channel;
        channelItem.needUpdate = true;

        Vector2 channelListSize = _channelListRectTransform.sizeDelta;
        channelListSize.y += 198f;
        _channelListRectTransform.sizeDelta = channelListSize;
    }
}