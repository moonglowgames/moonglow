﻿#pragma warning disable 649

using UnityEngine;

public class MyRankUnit : MonoBehaviour
{
    [SerializeField]
    private RectTransform _rankListRectTransform, _rewardTab, _rewardListRectTransform;
    private float _rankListWidth, _rewardListWidth;

    public Color[] BgColors = new Color[3];

    public MyRankItemUnit rankPrefab;
    public RewardUnit rewardPrefab;

    void Awake()
    {
        _rankListWidth = _rankListRectTransform.sizeDelta.x;
        _rewardListWidth = _rewardListRectTransform.sizeDelta.x;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void UpdateCurrentRank(string channelName_, int rankCount_, int rankPercent_, int rankPoint_, int rankWin_, int rankLose_, string winRate_)
    {
        MyRankItemUnit myRankItem = Instantiate(rankPrefab);
        myRankItem.transform.SetParent(_rankListRectTransform, false);
        myRankItem.myRankInfo = new MyRank(0, channelName_, rankCount_, rankPercent_, rankPoint_, rankWin_, rankLose_, winRate_, false, false, false);
        myRankItem.MakeCurrent();
        myRankItem.needUpdate = true;

        Vector2 rankListSize = _rankListRectTransform.sizeDelta;
        rankListSize.y += 85f;
        _rankListRectTransform.sizeDelta = rankListSize;
    }

    public void ClearRankList()
    {
        foreach (Transform child in _rankListRectTransform) { Destroy(child.gameObject); }
        _rankListRectTransform.sizeDelta = new Vector2(_rankListWidth, 0f);
    }

    public void AddRankItem(MyRank rank)
    {
        MyRankItemUnit myRankItem = Instantiate(rankPrefab);
        myRankItem.transform.SetParent(_rankListRectTransform, false);
        myRankItem.myRankInfo = rank;
        myRankItem.needUpdate = true;

        Vector2 rankListSize = _rankListRectTransform.sizeDelta;
        rankListSize.y += 85f;
        _rankListRectTransform.sizeDelta = rankListSize;
    }

    public void ToggleRewardWindow(bool state)
    {
        _rewardTab.gameObject.SetActive(state);
    }

    public void ClearRewards()
    {
        foreach (Transform child in _rewardListRectTransform) { Destroy(child.gameObject); }
        _rewardListRectTransform.sizeDelta = new Vector2(_rewardListWidth, 0f);
    }

    public void AddReward(Reward reward)
    {
        RewardUnit rewardItem = Instantiate(rewardPrefab);
        rewardItem.transform.SetParent(_rewardListRectTransform, false);
        rewardItem.rewardInfo = reward;
        rewardItem.needUpdate = true;

        Vector2 rewardListSize = _rewardListRectTransform.sizeDelta;
        rewardListSize.y += 43f;
        _rewardListRectTransform.sizeDelta = rewardListSize;
    }
}