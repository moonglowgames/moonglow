﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class RankItemUnit : MonoBehaviour
{
    [SerializeField]
    private Image _place;
    [SerializeField]
    private Text _placeNumber, _userName, _rankPoint, _victory, _defeat, _winRate;

    public int place;
    public bool needUpdate = true;
    
    public Rank rankInfo { get; set; }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        if (place > 3)
        {
            _placeNumber.text = place.ToString();
            _place.gameObject.SetActive(false);
            _placeNumber.gameObject.SetActive(true);
        }
        else
        {
            _place.sprite = HomeSpriteFactory.Instance.Get(SpriteType.Rank, place);
            _place.gameObject.SetActive(true);
            _placeNumber.gameObject.SetActive(false);
        }
        _userName.text = "Lv." + rankInfo.userLevel.ToString() + System.Environment.NewLine + rankInfo.userNick;
        _rankPoint.text = rankInfo.rankPoint.ToString() + System.Environment.NewLine + rankInfo.rankPercent + "%";
        _victory.text = rankInfo.rankWin.ToString();
        _defeat.text = rankInfo.rankLose.ToString();
        _winRate.text = rankInfo.winRate;
        //_description.text = "Lvl." + rankInfo.userLevel.ToString() + " " + rankInfo.userNick + "\t<" + rankInfo.rankPoint.ToString() + " Ranking Point>\t" + rankInfo.rankPercent.ToString() + "%";
    }
}