﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class MyRankItemUnit : MonoBehaviour
{
    [SerializeField]
    private Image _bg, _place, _rewardComplete;
    [SerializeField]
    private Text _placeNumber, _stateLabel, _leagueName, _rankPoint, _victory, _defeat, _winRate;
    [SerializeField]
    private Button _reward, _rewardList;

    private bool _current = false;
    private bool _needCSReq = true;

    public bool needUpdate = true;

    public MyRank myRankInfo { get; set; }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        if (myRankInfo.rankCount > 3)
        {
            _placeNumber.text = myRankInfo.rankCount.ToString();
            _place.gameObject.SetActive(false);
            _placeNumber.gameObject.SetActive(true);
        }
        else
        {
            _place.sprite = HomeSpriteFactory.Instance.Get(SpriteType.Rank, myRankInfo.rankCount);
            _place.gameObject.SetActive(true);
            _placeNumber.gameObject.SetActive(false);
        }

        _leagueName.text = myRankInfo.chanelName;
        _rankPoint.text = myRankInfo.rankPoint.ToString() + System.Environment.NewLine + myRankInfo.rankPercent + "%";
        _victory.text = myRankInfo.rankWin.ToString();
        _defeat.text = myRankInfo.rankLose.ToString();
        _winRate.text = myRankInfo.winRate;

        if (myRankInfo.itemExist)
        {
            if (!myRankInfo.itemGot)
            {
                _reward.gameObject.SetActive(true);
                _reward.GetComponentInChildren<LocalizationUnit>().UpdateUI("REWARD");
            }
            else { _rewardComplete.gameObject.SetActive(true); }
        }
        else { _reward.gameObject.SetActive(false); }

        if (myRankInfo.isBest)
        {
            _stateLabel.text = "Best";
            _stateLabel.color = GameControlUnit.Instance.Rank.MyRank.BgColors[3];
            _stateLabel.gameObject.SetActive(true);
        }
        if (_current) { _bg.color = GameControlUnit.Instance.Rank.MyRank.BgColors[2]; }
        else { _bg.color = GameControlUnit.Instance.Rank.MyRank.BgColors[myRankInfo.isBest ? 1 : 0]; }
    }

    public void MakeCurrent()
    {
        _current = true;
        _stateLabel.text = "Now";
        _stateLabel.color = GameControlUnit.Instance.Rank.MyRank.BgColors[4];
        _stateLabel.gameObject.SetActive(true);
        _rewardList.gameObject.SetActive(true);
        _rewardList.GetComponentInChildren<LocalizationUnit>().UpdateUI("REWARD_LIST");
    }

    public void GetReward()
    {
        if (_current)
        {
            if (_needCSReq)
            {
                _needCSReq = false;
                PacketManager.Instance.SendCSReqRewardList();
            }
            GameControlUnit.Instance.Rank.MyRank.ToggleRewardWindow(true);
        }
        else
        {
            PacketManager.Instance.SendCSReqGetRankItem(myRankInfo.rankIdx);
        }
    }
}