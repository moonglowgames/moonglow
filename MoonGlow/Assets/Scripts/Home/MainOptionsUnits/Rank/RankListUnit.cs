﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class RankListUnit : MonoBehaviour
{
    private Text _chanelName;
    [SerializeField]
    private RectTransform _rankListRectTransform;
    private float _rankListWidth;
    private int _iDx;

    public string ChanelName
    {
        get { return _chanelName.text; }
        set { _chanelName.text = value; }
    }

    public RankItemUnit rankPrefab;

    void Awake()
    {
        _rankListWidth = _rankListRectTransform.sizeDelta.x;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void ClearRankList()
    {
        _iDx = 0;
        foreach (Transform child in _rankListRectTransform) { Destroy(child.gameObject); }
        _rankListRectTransform.sizeDelta = new Vector2(_rankListWidth, 0f);
    }

    public void AddRankItem(Rank rank)
    {
        _iDx++;
        RankItemUnit rankItem = Instantiate(rankPrefab);
        rankItem.transform.SetParent(_rankListRectTransform, false);
        rankItem.rankInfo = rank;
        rankItem.place = _iDx;
        rankItem.needUpdate = true;

        Vector2 rankListSize = _rankListRectTransform.sizeDelta;
        rankListSize.y += 85;
        _rankListRectTransform.sizeDelta = rankListSize;
    }
}