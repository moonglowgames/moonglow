﻿#pragma warning disable 649

using UnityEngine;

public class JokerFaceUnit : MyPageItemUnit
{
    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(info.inventoryIdx);
        if (itemInfo == null)
        {
            Debug.Log("ItemInfo is null JokerFaceUnit.cs");
            return;
        }
        
        _name.text = itemInfo.name;
        _description.text = itemInfo.description;
        _focusFrame.enabled = info.select;
        _image.sprite = ItemSpriteFactory.Instance.Get(SpriteType.JokerFace, info.inventoryIdx % 100 - 1);
        _image.enabled = true;
    }

    override public void ToggleMaterial(bool state)
    {
        _image.material = state ? null : ItemSpriteFactory.Instance.GrayScale;
    }

    override public void UseAction()
    {
        PacketManager.Instance.SendCSReqUseItem(info.inventoryIdx, !info.select);
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
    }
}