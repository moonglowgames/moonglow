﻿#pragma warning disable 649

using UnityEngine;

public class JokerFaceTabUnit : MonoBehaviour
{
    [SerializeField]
    private JokerUnit _joker;
    [SerializeField]
    private RectTransform _itemParent;

    public JokerFaceUnit itemPrefab;
    public int[] iDx;

    void Awake()
    {
        Vector2 spellSize = _itemParent.sizeDelta;
        spellSize.y += 421f * iDx.Length;
        _itemParent.sizeDelta = spellSize;

        for (int i = 0; i <= iDx.GetUpperBound(0); i++)
        {
            JokerFaceUnit jokerFace = Instantiate(itemPrefab);
            jokerFace.transform.SetParent(_itemParent, false);
            jokerFace.info.inventoryIdx = iDx[i];
            _joker.AddFace(jokerFace);
            jokerFace.needUpdate = true;
        }
    }
}