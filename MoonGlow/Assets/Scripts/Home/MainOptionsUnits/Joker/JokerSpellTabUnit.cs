﻿#pragma warning disable 649

using UnityEngine;

public class JokerSpellTabUnit : MonoBehaviour
{
    [SerializeField]
    private RectTransform _spellParent;
    private float _spellParentWidth;

    public JokerSpellUnit spellPrefab;

    void Awake()
    {
        _spellParentWidth = _spellParent.sizeDelta.x;
    }

    public void Clear()
    {
        GameControlUnit.Instance.Joker.ClearSpells();
        foreach (Transform child in _spellParent) { Destroy(child.gameObject); }
        _spellParent.sizeDelta = new Vector2(_spellParentWidth, 0f);
    }

    public void AddSpell(int iDx)
    {
        JokerSpellUnit spell = Instantiate(spellPrefab);
        spell.transform.SetParent(_spellParent, false);
        spell.iDx = iDx;
        GameControlUnit.Instance.Joker.AddSpell(spell);
        spell.needUpdate = true;

        Vector2 spellSize = _spellParent.sizeDelta;
        spellSize.y += 193f;
        _spellParent.sizeDelta = spellSize;
    }
}