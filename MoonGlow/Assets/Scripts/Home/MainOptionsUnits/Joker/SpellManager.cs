﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class SpellManager : Singleton<SpellManager>
{
    private Dictionary<int, SpellInfo> _spells = new Dictionary<int, SpellInfo>();
    private const string _FILE = @"Data/JokerData";

    void Awake()
    {
        LoadFromFile();
    }

    public void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        SpellInfo tempJokerInfo = null;

        char[] charsToTrim = { ' ', '\t' };
        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);

            if (ln.Length == 0) { continue; }

            if (ln[0] != ';')
            {
                tempJokerInfo = new SpellInfo();
                if (tempJokerInfo.Decode(ln)) { _spells[tempJokerInfo.iDx] = tempJokerInfo; }
            }
        }
    }

    public SpellInfo Get(int iDx)
    {
        if (_spells.ContainsKey(iDx))
        {
            return _spells[iDx];
        }
        else
        {
            Debug.LogError("Didn't find Spell Info: " + iDx.ToString());
            return null;
        }
    }

    public List<int> Get(SpellType type)
    {
        List<int> _result = new List<int>();

        foreach (KeyValuePair<int, SpellInfo> entry in _spells)
        {
            if(entry.Key % 100 == 0)
            {
                if ((type & entry.Value.spellType) == entry.Value.spellType) { _result.Add(entry.Key); }
            }
        }

        return _result;
    }
}