﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class JokerUnit : MonoBehaviour
{
    [SerializeField]
    private Toggle _faceToggle;
    [SerializeField]
    private ScrollRect faceTab, spellTab;
    [SerializeField]
    private Text _inkItem, _ink;
    [SerializeField]
    private Button _resetButton;
    [SerializeField]
    private JokerProfileUnit _profile;

    private Dictionary<int, JokerFaceUnit> _avatars = new Dictionary<int, JokerFaceUnit>();
    private Dictionary<int, JokerSpellUnit> _spells = new Dictionary<int, JokerSpellUnit>();
    private bool _needCSReq = true;

    public Color[] BgColors = new Color[3];
    public JokerProfileUnit Profile { get { return _profile; } }

    public string Ink
    {
        get { return _ink.text; }
        set { _ink.text = value; }
    }

    public int InkAmount { get; set; }

    void Awake()
    {
        GameControlUnit.Instance.Joker = this;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void AddFace(JokerFaceUnit item)
    {
        _avatars.Add(item.info.inventoryIdx, item);
    }

    public void ClearSpells()
    {
        _spells.Clear();
    }

    public void AddSpell(JokerSpellUnit spell)
    {
        _spells.Add(spell.iDx / 100, spell);
    }

    public void SetDefaultTab()
    {
        //_faceToggle.isOn = true;
        //faceTab.verticalNormalizedPosition = 1f;
    }

    public JokerFaceUnit GetFace(int iDx)
    {
        if (_avatars.ContainsKey(iDx)) { return _avatars[iDx]; }
        else { return null; }
    }

    public JokerSpellUnit GetSpell(int iDx)
    {
        iDx /= 100;
        if (_spells.ContainsKey(iDx)) { return _spells[iDx]; }
        else { return null; }
    }

    public void SetInkItemCount(int inkItemCount)
    {
        _inkItem.text = "x" + inkItemCount.ToString();
        _resetButton.interactable = inkItemCount > 0;
    }

    public void DisableJokerFaces()
    {
        foreach (JokerFaceUnit jokerFace in _avatars.Values) { jokerFace.ToggleMaterial(false); }
    }

    public void ResetSpells()
    {
        foreach (KeyValuePair<int, JokerSpellUnit> spell in _spells)
        {
            spell.Value.iDx = spell.Key * 100;
            spell.Value.select = false;
            spell.Value.needUpdate = true;
        }
    }

    public void FaceTab(bool isOn)
    {
        if (isOn)
        {
            PacketManager.Instance.SendCSReqInventory(TabType.Joker);
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = true;
        }
        faceTab.gameObject.SetActive(isOn);
    }

    public void SpellTab(bool isOn)
    {
        if (isOn)
        {
            SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
            _needCSReq = false;
        }
        spellTab.gameObject.SetActive(isOn);
    }

    public void ReqUpdate()
    {
        PacketManager.Instance.SendCSReqJokerInfo();
        if (_needCSReq) { PacketManager.Instance.SendCSReqInventory(TabType.Joker); }
    }

    public void ResetButton()
    {
        GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "ReqResetSpell", null, "RESET_SPELL");
    }

    void ReqResetSpell()
    {
        PacketManager.Instance.SendCSReqUseItem(101, true);
    }

    public void ApplySpellButton()
    {
        if (!_profile.editMode) { return; }
        GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "ReqApplySpell", null, "APPLY_SPELL");
    }

    void ReqApplySpell()
    {
        PacketManager.Instance.SendCSReqApplySpells();
    }

    public void CancelSpellButton()
    {
        PacketManager.Instance.SendCSReqCancelUpgrade();
    }

    public void HelpButton()
    {
        HomeUIManagerUnit.Instance.HelpBookOption(6);
    }
}