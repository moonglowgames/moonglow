﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class JokerProfileUnit : MonoBehaviour
{
    [SerializeField]
    private JokerSpellTabUnit _spellTab;
    [SerializeField]
    private Image _jokerFace, _editImage;
    [SerializeField]
    private Text _priceMD, _priceM, _priceRE;
    [SerializeField]
    private Text[] _spells;

    private int _jokerFaceIdx;
    private SpellType _spellType;

    public bool needUpdate = true;
    public int jokerFaceIdx;
    public bool editMode;
    public int priceMD, priceM, priceRE;

    public List<int> spellIdxList { get; set; }

    void Awake()
    {
        spellIdxList = new List<int>();
    }

    void Update()
    {
        if (needUpdate == true)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        if (_jokerFaceIdx != jokerFaceIdx)
        {
            _jokerFaceIdx = jokerFaceIdx;
            if (_jokerFaceIdx != 0)
            {
                _jokerFace.enabled = true;
                _jokerFace.sprite = ItemSpriteFactory.Instance.Get(SpriteType.JokerFace, jokerFaceIdx % 100 - 1);

                ItemElementInfo info = ItemManager.Instance.Get(_jokerFaceIdx);
                if (info != null)
                {
                    _spellType = info.option;
                    _spellTab.Clear();
                    foreach (int i in SpellManager.Instance.Get(_spellType))
                    {
                        _spellTab.AddSpell(i);
                    }
                }
            }
            else
            {
                _jokerFace.enabled = false;
                _spellTab.Clear();
            }
        }

        priceMD = 0; priceM = 0; priceRE = 0;

        for (int i = 0; i <= _spells.GetUpperBound(0); i++)
        {
            _spells[i].text = LocalizationManager.Instance.Get("EMPTY_SPELL").Replace("*", (i * 10 + 1).ToString());//1, 11, 21
            _spells[i].fontStyle = FontStyle.Italic;
        }

        for (int i = 0; i < spellIdxList.Count; i++)
        {
            if (i > 2) { Debug.LogError("Only 3 spells Allowed! JokerProfileUnit.cs" + i.ToString()); }
            SpellInfo spellInfo = SpellManager.Instance.Get(spellIdxList[i]);
            if (spellInfo == null)
            {
                Debug.Log("ItemInfo is null MyPageUnit.cs");
                return;
            }
            switch (spellInfo.spellType)
            {
                case SpellType.MoonDust:
                    priceMD += spellInfo.ability.priceMD;
                    break;
                case SpellType.Mana:
                    priceM += spellInfo.ability.priceM;
                    break;
                case SpellType.RodEnergy:
                    priceRE += spellInfo.ability.priceRE;
                    break;
            }
            _spells[i].fontStyle = FontStyle.Normal;
            _spells[i].text = spellInfo.Description;
        }

        _priceMD.text = priceMD == 0 ? "0" : priceMD.ToString();
        _priceM.text = priceM == 0 ? "0" : priceM.ToString();
        _priceRE.text = priceRE == 0 ? "0" : priceRE.ToString();

        _editImage.enabled = editMode;
    }
}