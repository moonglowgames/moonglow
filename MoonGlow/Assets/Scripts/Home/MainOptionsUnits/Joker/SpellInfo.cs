﻿using System;

public class SpellAbility
{
    //PriceMD	PriceM	PriceRE	HPEnemy	Damage	Mana	Zenstone	MoonDust	Moonstone	RodEnergy	MoonCrystal	ManaEnemy	ZenstoneEnemy	MoonDustEnemy	MoonstoneEnemy	RodEnergyEnemy	MoonCrystalEnemy
    public int priceMD;
    public int priceM;
    public int priceRE;
    public int hpEnemy;
    public int damage;
    public int manaBlue;
    public int zenstoneBlue;
    public int moonDustRed;
    public int moonstoneRed;
    public int rodEnergyGreen;
    public int moonCrystalGreen;
    public int manaEnemyBlue;
    public int zenstoneEnemyBlue;
    public int moonDustEnemyRed;
    public int moonstoneEnemyRed;
    public int rodEnergyEnemyGreen;
    public int moonCrystalEnemyGreen;
}

public class SpellInfo
{
    //idx,ReqInk,LastReqInk,NextReqInk,SpellType,Level,MaxLevel,LastSpellIdx,NextSpellIdx,Special,Target,Name,Description
    public int iDx;
    public int reqInk;
    public int lastReqInk;
    public int nextReqInk;
    public SpellType spellType;
    public int level;
    public int maxLevel;
    public int lastSpellIdx;
    public int nextSpellIdx;
    public SpellAbility ability = new SpellAbility();
    public int special;
    public TargetOption target;
    public string Name;
    public string Description;

    const int DECODE_PARSE_COUNT = 30;
    public char[] charsToTrim = { ' ', '\t' };

    public bool Decode(string cvsLineData_)
    {
        bool retvalue = false;

        if (cvsLineData_ != string.Empty)
        {
            cvsLineData_.Trim(charsToTrim);

            switch (cvsLineData_[0])
            {
                case ';':
                    break;
                default:
                    string[] parsings = cvsLineData_.Split(',');
                    int parscount = 0;
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        try
                        {
                            this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.reqInk = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.lastReqInk = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.nextReqInk = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.spellType = (SpellType)Enum.Parse(typeof(SpellType), parsings[parscount++].Trim(charsToTrim));
                            this.level = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.maxLevel = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.lastSpellIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.nextSpellIdx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.priceMD = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.priceM = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.priceRE = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.hpEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.damage = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.manaBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.zenstoneBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.moonDustRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.moonstoneRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.rodEnergyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.moonCrystalGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.manaEnemyBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.zenstoneEnemyBlue = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.moonDustEnemyRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.moonstoneEnemyRed = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.rodEnergyEnemyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.ability.moonCrystalEnemyGreen = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.special = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.target = (TargetOption)Enum.Parse(typeof(TargetOption), parsings[parscount++].Trim(charsToTrim));

                            //this.Name = parsings[parscount++].Trim(charsToTrim);
                            //this.Description = parsings[parscount++].Trim(charsToTrim);
                            this.Name = JokerLocalizationManager.Instance.GetName(this.iDx);
                            this.Description = JokerLocalizationManager.Instance.GetDescription(this.iDx);
                            this.Description = this.Description.Replace("@", System.Environment.NewLine);
                            retvalue = true;
                        }
                        catch (Exception  e)
                        {
                            Console.WriteLine(e.ToString());
                            retvalue = false;
                        }
                    }
                    break;
            }
        }
        return retvalue;
    }

    public AbilityToolTip ToolTipMe
    {
        get
        {
            AbilityToolTip tip = AbilityToolTip.NONE;
            if (this.ability.moonstoneRed != 0) { tip |= AbilityToolTip.MSPOWER; }
            if (this.ability.moonDustRed != 0) { tip |= AbilityToolTip.MOONDUST; }
            if (this.ability.zenstoneBlue != 0) { tip |= AbilityToolTip.ZSPOWER; }
            if (this.ability.manaBlue != 0) { tip |= AbilityToolTip.MANA; }
            if (this.ability.moonCrystalGreen != 0) { tip |= AbilityToolTip.MCPOWER; }
            if (this.ability.rodEnergyGreen != 0) { tip |= AbilityToolTip.RODENERGY; }
            return tip;
        }
    }

    public AbilityToolTip ToolTipEnemy
    {
        get
        {
            AbilityToolTip tip = AbilityToolTip.NONE;
            if (this.ability.damage != 0) { tip |= AbilityToolTip.DAMAGE; }
            if (this.ability.hpEnemy != 0) { tip |= AbilityToolTip.HP; }
            if (this.ability.moonstoneEnemyRed != 0) { tip |= AbilityToolTip.MSPOWER; }
            if (this.ability.moonDustEnemyRed != 0) { tip |= AbilityToolTip.MOONDUST; }
            if (this.ability.zenstoneEnemyBlue != 0) { tip |= AbilityToolTip.ZSPOWER; }
            if (this.ability.manaEnemyBlue != 0) { tip |= AbilityToolTip.MANA; }
            if (this.ability.moonCrystalEnemyGreen != 0) { tip |= AbilityToolTip.MCPOWER; }
            if (this.ability.rodEnergyEnemyGreen != 0) { tip |= AbilityToolTip.RODENERGY; }
            return tip;
        }
    }
}