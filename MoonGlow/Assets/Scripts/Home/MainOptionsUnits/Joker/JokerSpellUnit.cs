﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class JokerSpellUnit : MonoBehaviour
{
    [SerializeField]
    private FocusFrameUnit _focusFrame;
    [SerializeField]
    private Text _reqInk, _price, _level, _description;//_name
    [SerializeField]
    private Image _bg;
    [SerializeField]
    private Outline _reqInkOutline;
    [SerializeField]
    private LocalizationUnit _reqInkLabel, _priceLabel;

    private bool canDowngrade, canUpdate;

    public bool needUpdate = true;
    public int iDx;
    public bool select;

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        SpellInfo spellInfo = SpellManager.Instance.Get(iDx);
        SpellInfo spellInfoNext = null;
        if (spellInfo.nextSpellIdx != 0) { spellInfoNext = SpellManager.Instance.Get(spellInfo.nextSpellIdx); }
        if (spellInfo == null)
        {
            Debug.Log("ItemInfo is null JokerSpellUnit.cs");
            return;
        }

        //_name.text = spellInfo.Name;
        _description.text = spellInfo.Description;
        _reqInkLabel.UpdateUI();
        _reqInk.text = spellInfo.nextReqInk.ToString();
        _level.text = spellInfo.level.ToString() + "<color=black>/" + spellInfo.maxLevel.ToString() + "</color>";

        //_nextLevel.enabled = spellInfo.level != spellInfo.maxLevel;
        //_nextLevel.text = "<color=#D0A562FF>Next</color> Lv. " + (spellInfo.level + 1).ToString();
        //_useButton.interactable = spellInfo.level == spellInfo.maxLevel ? false : true;
        canDowngrade = spellInfo.level > 0;
        canUpdate = spellInfo.level < spellInfo.maxLevel;
        //_useButton.interactable = spellInfo.level != spellInfo.maxLevel && spellInfo.nextReqInk <= GameControlUnit.Instance.Joker.InkAmount;

        _priceLabel.UpdateUI();
        switch (spellInfo.spellType)
        {
            case SpellType.MoonDust:
                _price.text = spellInfoNext == null ? string.Empty : "<color=red>" + spellInfoNext.ability.priceMD.ToString() + "</color>";
                _bg.color = GameControlUnit.Instance.Joker.BgColors[0];
                _reqInkOutline.effectColor = GameControlUnit.Instance.Joker.BgColors[0];
                break;
            case SpellType.Mana:
                _price.text = spellInfoNext == null ? string.Empty : "<color=blue>" + spellInfoNext.ability.priceM.ToString() + "</color>";
                _bg.color = GameControlUnit.Instance.Joker.BgColors[1];
                _reqInkOutline.effectColor = GameControlUnit.Instance.Joker.BgColors[1];
                break;
            case SpellType.RodEnergy:
                _price.text = spellInfoNext == null ? string.Empty : "<color=green>" + spellInfoNext.ability.priceRE.ToString() + "</color>";
                _bg.color = GameControlUnit.Instance.Joker.BgColors[2];
                _reqInkOutline.effectColor = GameControlUnit.Instance.Joker.BgColors[2];
                break;
            default:
                _price.text = "0";
                break;
        }

        _focusFrame.enabled = select;
    }

    public void SetSpell(int idx)
    {
        iDx = idx;
        select = true;
        needUpdate = true;
    }

    public void UpgradeSpell()
    {
        if (!canUpdate) { return; }
        PacketManager.Instance.SendCSReqUpgradeSpell(iDx);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void DowngradeSpell()
    {
        if (!canDowngrade) { return; }
        PacketManager.Instance.SendCSReqDowngradeSpell(iDx);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}