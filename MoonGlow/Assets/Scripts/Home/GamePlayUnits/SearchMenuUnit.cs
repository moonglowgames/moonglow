﻿using UnityEngine;
using System.Collections.Generic;

public class SearchMenuUnit : MonoBehaviour
{
	private SelectOption _gameType;
	private SelectOption _playMode;
	private SelectOption _playTime;
	//private SelectOption _view;
	private ReqRoomList _roomList;

	void Awake()
	{
		_roomList = new ReqRoomList("CSReqRoomList", EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session), new List<GameType>(), new List<PlayMode>(), new List<PlayTime>(), new List<bool>(), 5);

		_gameType = transform.FindChild("Canvas/OptionChoice/GameType/GameTypes").GetComponent<SelectOption>();
		_playMode = transform.FindChild("Canvas/OptionChoice/PlayMode").GetComponent<SelectOption>();
		_playTime = transform.FindChild("Canvas/OptionChoice/PlayTime").GetComponent<SelectOption>();
		//_view = transform.FindChild("Canvas/OptionChoice/View").GetComponent<SelectOption>();
	}

	public void ColmaretOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Colmaret);}
		else {	_roomList.gameTypeList.Remove(GameType.Colmaret); }
	}
	
	public void JohaOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Joha);}
		else {	_roomList.gameTypeList.Remove(GameType.Joha); }
	}

	public void CheshireOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Cheshire);}
		else {	_roomList.gameTypeList.Remove(GameType.Cheshire); }
	}
	
	public void ZatooOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Zatoo);}
		else {	_roomList.gameTypeList.Remove(GameType.Zatoo); }
	}
	
	public void ArmdreeOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Armdree);}
		else {	_roomList.gameTypeList.Remove(GameType.Armdree); }
	}
	
	public void TarassenOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Tarassen);}
		else {	_roomList.gameTypeList.Remove(GameType.Tarassen); }
	}
	
	public void KrazanOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Krazan);}
		else {	_roomList.gameTypeList.Remove(GameType.Krazan); }
	}
	
	public void YananOption(bool isOn)
	{
		if (isOn==true)	{_roomList.gameTypeList.Add(GameType.Yanan);}
		else {	_roomList.gameTypeList.Remove(GameType.Yanan); }
	}
	
	public void Single2Option(bool isOn)
	{
		if (isOn==true)	{_roomList.playModeList.Add(PlayMode.Single2);}
		else {	_roomList.playModeList.Remove(PlayMode.Single2); }
	}
	
	public void Single3Option(bool isOn)
	{
		if (isOn==true)	{_roomList.playModeList.Add(PlayMode.Single3);}
		else {	_roomList.playModeList.Remove(PlayMode.Single3); }
	}
	
	public void Single4Option(bool isOn)
	{
		if (isOn==true)	{_roomList.playModeList.Add(PlayMode.Single4);}
		else {	_roomList.playModeList.Remove(PlayMode.Single4); }
	}
	
	public void TeamOption(bool isOn)
	{
		if (isOn==true)	{_roomList.playModeList.Add(PlayMode.Team);}
		else {	_roomList.playModeList.Remove(PlayMode.Team); }
	}
	
	public void Time30SOption(bool isOn)
	{
		if (isOn==true)	{_roomList.playTimeList.Add(PlayTime.Sec_30);}
		else {	_roomList.playTimeList.Remove(PlayTime.Sec_30); }
	}
	
	public void Time60SOption(bool isOn)
	{
		if (isOn==true)	{_roomList.playTimeList.Add(PlayTime.Sec_60);}
		else {	_roomList.playTimeList.Remove(PlayTime.Sec_60); }
	}

	public void DiscloseYes(bool isOn)
	{
		if (isOn==true)	{_roomList.disCloseList.Add(true);}
		else {	_roomList.disCloseList.Remove(true); }
	}

	public void DiscloseNo(bool isOn)
	{
		if (isOn==true)	{_roomList.disCloseList.Add(false);}
		else {	_roomList.disCloseList.Remove(false); }
	}

	public void Complete()
	{
		SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
		PacketManager.Instance.SendCSReqRoomList(_roomList);
	}

    public void SetDefaultTab()
    {
        _gameType.TurnDefaultOn();
        _playMode.TurnDefaultOn();
        _playTime.TurnDefaultOn();
        //_view.TurnDefaultOn();
    }

	public void SelectAll()
	{
        //_roomList.gameTypeList.Clear();
        //_roomList.gameTypeList.Add(GameType.All);
        //_roomList.playModeList.Clear();
        //_roomList.playModeList.Add(PlayMode.All);
        //_roomList.playTimeList.Clear();
        //_roomList.playTimeList.Add(PlayTime.All);
        ////_roomList.disCloseList.Clear();
        ////_roomList.disCloseList.Add(true);
        ////_roomList.disCloseList.Add(false);
        //Complete();
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}