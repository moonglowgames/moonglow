﻿#pragma warning disable 649

using UnityEngine;
using System.Collections.Generic;

public class RoomListUnit : MonoBehaviour
{
    [SerializeField]
    private RectTransform _roomsRectTransform;
    private float _roomWidth;
    private bool _refreshDelay;
    private ReqRoomList _roomList;

    public Color[] BgColors;
    public JoinConfirmUnit joinConfirmPrefab;
    public RoomUnit roomPrefab;

    void Awake()
    {
        GameControlUnit.Instance.RoomList = this;
        _roomWidth = _roomsRectTransform.sizeDelta.x;
        _roomList = new ReqRoomList("CSReqRoomList", EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session), new List<GameType>(), new List<PlayMode>(), new List<PlayTime>(), new List<bool>(), 5);
    }

    void Start()
    {
        JoinConfirmUnit joinConfirm = Instantiate(joinConfirmPrefab);
        //Set priority over tabs
        joinConfirm.transform.SetParent(this.transform.parent.parent, false);
        joinConfirm.ToggleWindow(false);
    }

    void CancelRefreshDelay()
    {
        _refreshDelay = false;
    }

    public void Refresh()
    {
        if (_refreshDelay) { return; }
        Invoke("CancelRefreshDelay", 2f);
        _refreshDelay = true;
        PacketManager.Instance.SendCSReqRoomList(_roomList);
    }

    public void Clear()
    {
        foreach (Transform child in _roomsRectTransform) { Destroy(child.gameObject); }
        _roomsRectTransform.sizeDelta = new Vector2(_roomWidth, 0f);
    }

    public void AddRoom(RoomInfo roomInfo)
    {
        RoomUnit roomItem = Instantiate(roomPrefab);
        roomItem.transform.SetParent(_roomsRectTransform, false);
        roomItem.name = roomInfo.roomName;
        roomItem.roomName = roomInfo.roomName;
        roomItem.gameType = roomInfo.gameType;
        roomItem.playMode = roomInfo.playMode;
        roomItem.playTime = roomInfo.playTime;
        roomItem.stake = roomInfo.stake;
        roomItem.disclose = roomInfo.disClose;
        for (int i = 0; i < roomInfo.userInfoList.Count; i++)
        {
            //roomItem.playerList[i] = "<color=#B00000FF>Lv. " + roomInfo.userInfoList[i].Level.ToString() + "</color> " + roomInfo.userInfoList[i].Name;
            if (roomInfo.userInfoList[i].Level != 0)
            {
                roomItem.playerList[i] = string.Format("<color=#B00000FF>Lv. {0}</color> {1}", roomInfo.userInfoList[i].Level.ToString(), roomInfo.userInfoList[i].Name);
            }
            else { roomItem.playerList[i] = roomInfo.userInfoList[i].Name; } 
        }
        roomItem.isMine = roomInfo.isMine;
        roomItem.isPlaying = roomInfo.isPlaying;
        roomItem.needUpdate = true;

        Vector2 roomSize = _roomsRectTransform.sizeDelta;
        roomSize.y += 110f;
        _roomsRectTransform.sizeDelta = roomSize;
    }
}