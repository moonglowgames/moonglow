﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;

public class CreateMenuUnit : MonoBehaviour
{
    [SerializeField]
    private Toggle _colmaretToggle, _johaToggle, _cheshireToggle, _zatooToggle, _armdreeToggle, _tarassenToggle, _krazanToggle, _yananToggle;
    [SerializeField]
    private Toggle _30sToggle, _60sToggle, _singlePlayToggle;
    [SerializeField]
    private Text _stakeText;
    [SerializeField]
    private LocalizationUnit _gameTypeName;
    [SerializeField]
    private Text _winCondition;

    private int[] _stakes = { 0, 10, 50, 100, 500, 1000 };
    private int _currentStakeIdx;

    public GameType gameType = GameType.Colmaret;
    public PlayTime playTime = PlayTime.Sec_30;
    public int stake;
    public bool singlePlay;

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void ColmaretOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Colmaret;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void JohaOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Joha;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void CheshireOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Cheshire;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void ZatooOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Zatoo;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void ArmdreeOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Armdree;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void TarassenOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Tarassen;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void KrazanOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Krazan;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void YananOption(bool isOn)
    {
        if (isOn)
        {
            gameType = GameType.Yanan;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.gameType, Enum.GetName(typeof(GameType), gameType));
        }
    }

    public void UpdateWinCondition(bool isOn)
    {
        if (isOn)
        {
            _gameTypeName.UpdateUI(Enum.GetName(typeof(GameType), gameType).ToUpper());
            _winCondition.text = WinConditionManager.Instance.Get(gameType).ToString();
        }
    }

    public void Time30SOption(bool isOn)
    {
        if (isOn)
        {
            playTime = PlayTime.Sec_30;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.playTime, Enum.GetName(typeof(PlayTime), playTime));
        }
    }

    public void Time60SOption(bool isOn)
    {
        if (isOn)
        {
            playTime = PlayTime.Sec_60;
            EncryptedPlayerPrefs.SetString(PlayerPrefsKey.playTime, Enum.GetName(typeof(PlayTime), playTime));
        }
    }

    public void StakeOption(int increment)
    {
        if ((_currentStakeIdx + increment) < 0 || (_currentStakeIdx + increment >= _stakes.Length)) { return; }
        _currentStakeIdx += increment;
        stake = _stakes[_currentStakeIdx];
        _stakeText.text = stake.ToString();
        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.stake, stake.ToString());
    }

    public void SinglePlayOption(bool isOn)
    {
        singlePlay = isOn;
    }

    public void SetDefaultTab(bool isOn)
    {
        if (isOn)
        {
            switch ((GameType)Enum.Parse(typeof(GameType), EncryptedPlayerPrefs.GetString(PlayerPrefsKey.gameType, "2")))
            {
                case GameType.Colmaret:
                    _colmaretToggle.isOn = true;
                    break;
                case GameType.Joha:
                    _johaToggle.isOn = true;
                    break;
                case GameType.Cheshire:
                    _cheshireToggle.isOn = true;
                    break;
                case GameType.Zatoo:
                    _zatooToggle.isOn = true;
                    break;
                case GameType.Armdree:
                    _armdreeToggle.isOn = true;
                    break;
                case GameType.Tarassen:
                    _tarassenToggle.isOn = true;
                    break;
                case GameType.Krazan:
                    _krazanToggle.isOn = true;
                    break;
                case GameType.Yanan:
                    _yananToggle.isOn = true;
                    break;
            }

            UpdateWinCondition(isOn);
            switch ((PlayTime)Enum.Parse(typeof(PlayTime), EncryptedPlayerPrefs.GetString(PlayerPrefsKey.playTime, "3")))
            {
                case PlayTime.Sec_30:
                    _30sToggle.isOn = true;
                    break;
                case PlayTime.Sec_60:
                    _60sToggle.isOn = true;
                    break;
            }

            stake = Convert.ToInt32(EncryptedPlayerPrefs.GetString(PlayerPrefsKey.stake, "0"));
            _stakeText.text = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.stake, "0");
            for (int i = 0; i <= _stakes.GetUpperBound(0); i++)
            {
                if (_stakes[i] == stake) { _currentStakeIdx = i; }
            }
            _singlePlayToggle.isOn = false;
        }
    }

    public void Complete()
    {
        SoundControlUnit.Instance.PlayUiClip(UiClip.Complete);
        PacketManager.Instance.SendCSReqRoomCreate(gameType, PlayMode.Single2, playTime, stake, true, true, singlePlay);//disclose, lockGame);
    }
}