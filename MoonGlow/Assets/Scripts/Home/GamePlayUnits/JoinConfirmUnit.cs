﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class JoinConfirmUnit : MonoBehaviour
{
    [SerializeField]
    private Image _avatar;
    [SerializeField]
    private Text _info;
    [SerializeField]
    private LocalizationUnit _ok, _cancel;

    private bool _isHost;

    public string askerUserID { get; set; }

    void Awake()
    {
        GameControlUnit.Instance.JoinConfirm = this;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void ToggleWindow(bool state)
    {
        this.gameObject.SetActive(state);
    }

    public void JoinCancelView()
    {
        _isHost = false;
        _info.text = LocalizationManager.Instance.Get("WAIT");
        _avatar.transform.parent.gameObject.SetActive(false);
        _cancel.transform.parent.gameObject.SetActive(false);
        _ok.UpdateUI("CANCEL");
        ToggleWindow(true);
    }

    public void JoinReqView(int askerLevel, string askerNick, int askerFaceIdx)
    {
        _isHost = true;
        _info.text = "<color=#DCAC00FF>Lv. " + askerLevel.ToString() + "</color> " + LocalizationManager.Instance.Get("HAS_JOIN").Replace("*", askerNick);
        _avatar.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatarMini, askerFaceIdx % 100);
        _avatar.transform.parent.gameObject.SetActive(true);
        _ok.UpdateUI("ACCEPT");
        _cancel.transform.parent.gameObject.SetActive(true);
        _cancel.UpdateUI("REJECT");
        ToggleWindow(true);
    }

    public void JoinResponseButton(bool rspResult)
    {
        if (_isHost) { PacketManager.Instance.SendCSRspPlayerJoin(askerUserID, rspResult); }
        else { PacketManager.Instance.SendCSReqCancelJoin(); }
        ToggleWindow(false);
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }
}