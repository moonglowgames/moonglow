﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class UserUnit : MonoBehaviour
{
    [SerializeField]
	private Image _avatar, _badge;
    [SerializeField]
    private Text _name, _level, _stamina, _moonRuby, _gold;
	//private Text _etherealInk;
 //   private Text _saqqaraTablet;
 //   private Text _rosemaryOil;
 //   private Text _steamPack;
 //   private Text _eveningPrimroseElixir;
 //   private Text _suassureaObliviataElixir;
	private int _avatarIdx, _badgeIdx = -1;

    public UserInfo Info;

	public bool needUpdate;
	
    void Awake()
    {
        GameControlUnit.Instance.User = this;
    }

	void Update ()
	{
		if(needUpdate == true)
		{
			needUpdate = false;
			UpdateView();
		}
	}

    void UpdateView()
    {
        if (_avatarIdx != Info.avatarIdx)
        {
            _avatarIdx = Info.avatarIdx;
            _avatar.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatarMini, _avatarIdx % 100);
        }
        _name.text = Info.userName;
        _level.text = "Lv. " + Info.userLevel.ToString();
        if (_badgeIdx != Info.badgeIdx)
        {
            _badgeIdx = Info.badgeIdx;
            if (_badgeIdx > 0)
            {
                _badge.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, _badgeIdx);
                _badge.gameObject.SetActive(true);
                _level.rectTransform.offsetMin = new Vector2(38f, -38f);
            }
            else
            {
                _badge.gameObject.SetActive(false);
                _level.rectTransform.offsetMin = new Vector2(0f, -38f);
            }
        }
        _stamina.text = Info.stamina.ToString() + "/" + Info.maxStamina.ToString();
        _moonRuby.text = Info.moonRuby.ToString("N0");
        _gold.text = Info.gold.ToString("N0");
        //_etherealInk.text = "x" + items[(int)UserItem.etherealInk];
        //_saqqaraTablet.text = "x" + items[(int)UserItem.saqqaraTablet];
        //_rosemaryOil.text = "x" + items[(int)UserItem.rosemaryOil];
        //_steamPack.text = "x" + items[(int)UserItem.stimPack];
        //_eveningPrimroseElixir.text = "x" + items[(int)UserItem.eveningPrimroseElixir];
        //_suassureaObliviataElixir.text = "x" + items[(int)UserItem.suassureaObliviataElixir];
        GameControlUnit.Instance.PlayerInfo = this.Info;
    }

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                transform.FindChild("ElementsCanvas").GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 3.35f);
                break;
            case RatioType.Standart800:
                transform.FindChild("ElementsCanvas").GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 3.75f);
                break;
            case RatioType.aPhone:
                transform.FindChild("ElementsCanvas").GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 4.02f);
                break;
            case RatioType.aPad:
                transform.FindChild("ElementsCanvas").GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 4.55f);
                break;
        }
    }
}