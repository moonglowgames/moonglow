﻿#pragma warning disable 649

using UnityEngine;

public enum Option {Setting, Mail, Quest, MyPage, Rank, Friend, Joker, Shop, News}

public class OptionMenuUnit : MonoBehaviour
{
    [SerializeField]
    private RectTransform _tabParent;
    private SettingsMenuUnit _settings;
    private MailUnit _mail;
    private QuestUnit _quest;
    private MyPageUnit _myPage;
    private RankUnit _rank;
    private FriendUnit _friend;
    private JokerUnit _joker;
    private ShopUnit _shop;
    private NewsUnit _news;

    private GameObject _lastTab;

    public SettingsMenuUnit Settings;
    public MailUnit Mail;
    public QuestUnit Quest;
    public MyPageUnit MyPage;
    public RankUnit Rank;
    public FriendUnit Friend;
    public JokerUnit Joker;
    public ShopUnit Shop;
    public NewsUnit News;

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void OpenTab(int tabIdx)
    {
        if (_lastTab != null) { _lastTab.SetActive(false); }

        HomeUIManagerUnit.Instance.ToggleFrame(true);
        switch ((Option)tabIdx)
        {
            case Option.Setting:
                SettingsOption();
                _lastTab = _settings.gameObject;
                break;
            case Option.Mail:
                MailOption();
                _lastTab = _mail.gameObject;
                break;
            case Option.Quest:
                QuestOption();
                _lastTab = _quest.gameObject;
                break;
            case Option.MyPage:
                HomeUIManagerUnit.Instance.ToggleFrame(false);
                MyPageOption();
                _lastTab = _myPage.gameObject;
                break;
            case Option.Rank:
                RankOption();
                _lastTab = _rank.gameObject;
                break;
            case Option.Friend:
                FriendOption();
                _lastTab = _friend.gameObject;
                break;
            case Option.Joker:
                HomeUIManagerUnit.Instance.ToggleFrame(false);
                JokerOption();
                _lastTab = _joker.gameObject;
                break;
            case Option.Shop:
                ShopOption();
                _lastTab = _shop.gameObject;
                break;
            case Option.News:
                NewsOption();
                _lastTab = _news.gameObject;
                break;
        }
    }

    public void OnClose()
    {
        if (_lastTab != null) { _lastTab.SetActive(false); }
        _lastTab = null;
        HomeUIManagerUnit.Instance.ToggleFrame(true);
        SoundControlUnit.Instance.PlayUiClip(UiClip.Close);
    }


    public void SettingsOption()
	{
        if(_settings == null)
        {
            _settings = Instantiate(Settings);
            _settings.transform.SetParent(_tabParent, false);
        }
        _settings.gameObject.SetActive(true);
        _settings.SetDefaultTab();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void MailOption()
    {
        if (_mail == null)
        {
            _mail = Instantiate(Mail);
            _mail.transform.SetParent(_tabParent, false);
        }
        _mail.ReqUpdate();
        _mail.gameObject.SetActive(true);
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void QuestOption()
    {
        if (_quest == null)
        {
            _quest = Instantiate(Quest);
            _quest.transform.SetParent(_tabParent, false);
        }
        _quest.ReqUpdate();
        _quest.gameObject.SetActive(true);
        _quest.SetDefaultTab();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void MyPageOption()
	{
        if (_myPage == null)
        {
            _myPage = Instantiate(MyPage);
            _myPage.transform.SetParent(_tabParent, false);
        }
        _myPage.ReqUpdate();
        _myPage.gameObject.SetActive(true);
        _myPage.SetDefaultTab();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void RankOption()
    {
        if (_rank == null)
        {
            _rank = Instantiate(Rank);
            _rank.transform.SetParent(_tabParent, false);
        }
        _rank.ReqUpdate();
        _rank.gameObject.SetActive(true);
        _rank.SetDefaultTab();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void FriendOption()
    {
        if (_friend == null)
        {
            _friend = Instantiate(Friend);
            _friend.transform.SetParent(_tabParent, false);
        }
        _friend.ReqUpdate();
        _friend.gameObject.SetActive(true);
        _friend.SetDefaultTab();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void JokerOption()
    {
        if (_joker == null)
        {
            _joker = Instantiate(Joker);
            _joker.transform.SetParent(_tabParent, false);
        }
        _joker.ReqUpdate();
        _joker.gameObject.SetActive(true);
        _joker.SetDefaultTab();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void ShopOption()
    {
        if (_shop == null)
        {
            _shop = Instantiate(Shop);
            _shop.transform.SetParent(_tabParent, false);
        }
        _shop.ReqUpdate();
        _shop.gameObject.SetActive(true);
        _shop.SetDefaultTab();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void NewsOption()
    {
        if (_news == null)
        {
            _news = Instantiate(News);
            _news.transform.SetParent(_tabParent, false);
        }
        //_news.ReqUpdate();
        _news.gameObject.SetActive(true);
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void FaceBookOption()
    {
        //Application.OpenURL("https://www.facebook.com/moonglowgames2015");
        PacketManager.Instance.SendCSReqUrl(UrlType.HomePage);
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void ManualOption()
    {
        PacketManager.Instance.SendCSReqUrl(UrlType.Manual);
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }
}