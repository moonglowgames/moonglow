﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;

public enum JoinType {Join, JoinSpectate, Full};

public class RoomUnit : MonoBehaviour
{
    [SerializeField]
    private Image _bg, _gameTypeImage;
    [SerializeField]
    private Button _joinButton;
   [SerializeField]
    private LocalizationUnit _joinButtonText, _gameTypeText;
    [SerializeField]
    private Text _playModeText, _stake;
    [SerializeField]
    private Text[] _players;

    public bool needUpdate = true;
    public string roomName;
    public JoinType joinType;
    public GameType gameType;
    public PlayMode playMode;
    public PlayTime playTime;
    public int stake;
    public bool disclose;
    public string[] playerList = new string[4];
    public bool isMine;
    public bool isPlaying;

    void Update()
    {
        if (needUpdate == true)
        {
            needUpdate = false;
            _bg.color = GameControlUnit.Instance.RoomList.BgColors[isMine ? 1 : 0];
            _gameTypeImage.sprite = HomeSpriteFactory.Instance.Get(SpriteType.GameType, (int)gameType);
            _gameTypeText.UpdateUI(Enum.GetName(typeof(GameType), gameType).ToUpper());
            PlayModeText();

            _joinButtonText.UpdateUI(isMine? "CANCEL" : "JOIN");
            _joinButton.transform.parent.gameObject.SetActive(!isPlaying);
        }
    }

    void PlayModeText()
    {
        switch (playMode)
        {
            case PlayMode.Single2:
                //_playModeText.text = "2S";
                _players[0].text = playerList[0];
                _players[1].text = playerList[1];
                break;
            case PlayMode.Single3:
                //_playModeText.text = "3S";
                _players[0].text = playerList[0];
                _players[1].text = playerList[1];
                _players[2].text = playerList[2];
                break;
            case PlayMode.Single4:
                //_playModeText.text = "4S";
                _players[0].text = playerList[0];
                _players[1].text = playerList[1];
                _players[2].text = playerList[2];
                _players[3].text = playerList[3];
                break;
            case PlayMode.Team:
                //_playModeText.text = "Team";
                _players[0].text = playerList[0];
                _players[1].text = playerList[1];
                _players[2].text = playerList[2];
                _players[3].text = playerList[3];
                break;
            default:
                break;
        }
        string time = LocalizationManager.Instance.Get("SEC");
        switch (playTime)
        {
            case PlayTime.Sec_30:
                _playModeText.text = time.Replace("*", "30");
                break;
            case PlayTime.Sec_60:
                _playModeText.text = time.Replace("*", "60");
                break;
        }
        _stake.text = stake.ToString();
    }

    public void JoinClick()
    {
        if (isMine) { PacketManager.Instance.SendCSNotiCancelRoom(); }
        else { PacketManager.Instance.SendCSReqJoinRoom(roomName, 1); }
        SoundControlUnit.Instance.PlayUiClip(UiClip.Join);
    }
}