﻿using UnityEngine;

public class HomeSpriteFactory : Singleton<HomeSpriteFactory>
{
	private Sprite _errorSprite;
    private Sprite[] _gameType;
	private Sprite[] _menuControl;
    private Sprite[] _rank;
    private Sprite[] _rankChannel;
    private Sprite[] _reward;
    private Sprite[] _cardSkin;
    private Sprite[] _cardSkinMini;

    void Awake()
	{
		_errorSprite = Resources.Load<Sprite>("Images/Error");
        _gameType = Resources.LoadAll<Sprite>("Images/GameTypes");
		_menuControl = Resources.LoadAll<Sprite>("Images/MenuControl");
        _rank = Resources.LoadAll<Sprite>("Images/Rank");
        _rankChannel = Resources.LoadAll<Sprite>("Images/RankChannel");
        _reward = Resources.LoadAll<Sprite>("Images/Reward");
        _cardSkin = Resources.LoadAll<Sprite>("Images/CardSkin");
        _cardSkinMini = Resources.LoadAll<Sprite>("Images/CardSkinMini");
    }

 	public Sprite Get(SpriteType type, int iDx){
        switch (type)
        {
            case SpriteType.GameType:
                return GetGameTypeSprite(iDx);
            case SpriteType.MenuControl:
                return GetMenuControlSprite(iDx);
            case SpriteType.Rank:
                return GetRankSprite(iDx);
            case SpriteType.RankChannel:
                return GetRankChannelSprite(iDx);
            case SpriteType.Reward:
                return GetRewardSprite(iDx);
            case SpriteType.CardSkin:
                return GetCardSkinSprite(iDx);
            case SpriteType.CardSkinMini:
                return GetCardSkinMiniSprite(iDx);
            default:
                Debug.LogError("Sprite type mismatch HomeSpriteFactory.cs");
                return null;
        }
	}
	
    Sprite GetGameTypeSprite(int iDx)
    {
        //enum GameType {Unknown = 0, All = 1, Colmaret = 2, Joha = 3, Cheshire = 4, Zatoo = 5, Armdree = 6, Tarassen = 7, Krazan = 8, Yanan = 9}
        iDx -= 2;
		if (iDx >= 0 && iDx < _gameType.Length)
		{
			return _gameType[iDx];
		}else{
			Debug.LogError("Sprite game type index outside boundaries " +iDx.ToString()+ " HomeSpriteFactory.cs");
			return _errorSprite;
		}
	}

	Sprite GetMenuControlSprite (int iDx)
	{
		if (iDx >= 0 && iDx < _menuControl.Length)
		{
			return _menuControl[iDx];
		}else{
			Debug.LogError("Menu Control index outside boundaries " +iDx.ToString()+ " HomeSpriteFactory.cs");
			return _errorSprite;
		}
	}

    Sprite GetRankSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _rank.Length)
        {
            return _rank[iDx];
        }
        else
        {
            Debug.LogError("Rank index outside boundaries " + iDx.ToString() + " HomeSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetRankChannelSprite(int iDx)
    {
        iDx -= 1;
        if (iDx >= 0 && iDx < _rankChannel.Length)
        {
            return _rankChannel[iDx];
        }
        else
        {
            Debug.LogError("Rank channel index outside boundaries " + iDx.ToString() + " HomeSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetRewardSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _reward.Length)
        {
            return _reward[iDx];
        }
        else
        {
            Debug.LogError("Reward index outside boundaries " + iDx.ToString() + " HomeSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCardSkinSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _cardSkin.Length)
        {
            return _cardSkin[iDx];
        }
        else
        {
            Debug.LogError("Card Skin index outside boundaries " + iDx.ToString() + " HomeSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCardSkinMiniSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _cardSkinMini.Length)
        {
            return _cardSkinMini[iDx];
        }
        else
        {
            Debug.LogError("Card Skin Mini index outside boundaries " + iDx.ToString() + " HomeSpriteFactory.cs");
            return _errorSprite;
        }
    }
}