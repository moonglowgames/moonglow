﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HomeUIManagerUnit : Singleton<HomeUIManagerUnit>
{
    [SerializeField]
    private Text _connectedUsers;
    [SerializeField]
    private GameObject _defaultFrame;
    [SerializeField]
    private Canvas UserElements;
    [SerializeField]
    private CanvasGroup MenuFrame;
    private HelpBookUnit _helpBookUnit;
    private PlayerProfileUnit _playerProfileUnit;

    public PlayerProfileUnit profilePrefab;
    public HelpBookUnit helpBookPrefab;

    public string UserCount
    {
        get { return _connectedUsers.text; }
        set { _connectedUsers.text = value; }
    }

    void Awake()
    {
        if (EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.tutorial, 0) == 0)
        {
            SceneManager.LoadScene(3);
        }
        LocalizationManager.Instance.ChangeLanguage();
    }

    void Start()
    {
        if (GameControlUnit.Instance.UserAccess != UserType.User)
        {
            _connectedUsers.transform.parent.gameObject.SetActive(true);
        }
    }

    public void ToggleDefaultUI(bool state, bool showElements = true)
    {
        UserElements.enabled = showElements;
        MenuFrame.alpha = state ? 1 : 0;
        MenuFrame.blocksRaycasts = state;
    }

    public void ToggleFrame(bool state)
    {
        _defaultFrame.SetActive(state);
    }

    public void HelpBookOption(int page)
    {
        if (_helpBookUnit == null)
        {
            _helpBookUnit = Instantiate(helpBookPrefab);
        }
        _helpBookUnit.gameObject.SetActive(true);
        _helpBookUnit.OpenPage(page);
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuButton);
    }

    public void ShowPlayerProfile(PlayerProfile profile)
    {
        if (_playerProfileUnit == null)
        {
            _playerProfileUnit = Instantiate(profilePrefab);
            _playerProfileUnit.transform.SetParent(_defaultFrame.transform.parent, false);
        }
        _playerProfileUnit.UpdateView(profile);
        _playerProfileUnit.ToggleWindow(true);
    }
}