﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class MenuOptionUnit : MonoBehaviour
{
    [SerializeField]
    public OptionMenuUnit optionsTab;
    [SerializeField]
    public CreateMenuUnit createTab;
    [SerializeField]
    private Text _menuButtonText, _createButtonText;
    [SerializeField]
    private Color _onToggleText;

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void RoomListTab(bool isOn)
	{
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab); }
	}

    public void SidePanelToggle(bool isOn)
    {
        this.gameObject.SetActive(isOn);
    }

    public void OptionsTab(bool isOn)
	{
        _menuButtonText.color = isOn ? Color.black : _onToggleText;
        optionsTab.gameObject.SetActive(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab); }
    }

	public void CreateTab(bool isOn)
	{
        _createButtonText.color = isOn ? Color.black : _onToggleText;
        createTab.gameObject.SetActive(isOn);
        createTab.SetDefaultTab(isOn);
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab); }
    }

	public void CloseButton()
	{
        SidePanelToggle(false);
		SoundControlUnit.Instance.PlayUiClip(UiClip.Cancel);
	}
}