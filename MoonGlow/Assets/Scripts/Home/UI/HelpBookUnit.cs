﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class HelpBookUnit : MonoBehaviour
{
    [SerializeField]
    private Text _helpText, _pageCounter;
    [SerializeField]
    private Image _helpArt, _leftIcon, _rightIcon;

    private string[] _helpBook;
    private int _currentPage;

    public TextAsset[] helpFiles;
    public Sprite[] artFiles;

    void Awake()
    {
        switch((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                _helpBook = helpFiles[0].text.Split('#');
                break;
            case Language.Korean:
                _helpBook = helpFiles[1].text.Split('#');
                break;
        }

        if (artFiles.Length != _helpBook.Length) { Debug.LogError("Helpbook text and artwork count doesn't match."); }
    }

    void Start()
    {
        transform.FindChild("Canvas").GetComponent<RectTransform>().sizeDelta = LayoutUnit.GetSize();
        //foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        //{
        //    textUnit.UpdateUI();
        //}
    }

    void UpdateView()
    {
        _leftIcon.gameObject.SetActive(_currentPage != 0);
        _rightIcon.gameObject.SetActive(_currentPage != _helpBook.Length - 1);
        _pageCounter.text = "<color=#F9E5CFFF>" + (_currentPage + 1).ToString() + "</color>/" + _helpBook.Length.ToString() + " Page";
        _helpText.text = _helpBook[_currentPage];
        _helpArt.sprite = artFiles[_currentPage];
    }

    public void OpenPage(int page)
    {
        _currentPage = page;
        UpdateView();
    }

    public void TurnPage(int forward)
    {
        if ((_currentPage + forward) < 0 || (_currentPage + forward >= _helpBook.Length)) { return; }
        _currentPage += forward;
        UpdateView();
        SoundControlUnit.Instance.PlayUiClip(UiClip.MenuTab);
    }
}