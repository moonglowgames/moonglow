﻿using UnityEngine;
using UnityEngine.UI;

public class SelectOption : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    void Awake()
    {
        if(_text == null) { _text = GetComponent<Text>(); }
    }

    public void TurnDefaultOn()
    {
        ToggleAll(false);
        //defaultToggle.isOn = true;
    }

    public void ToggleAll(bool state)
    {
        foreach (Toggle toggle in GetComponentsInChildren<Toggle>()) { if (toggle.interactable) { toggle.isOn = state; } }
    }

    public void OnValueChanged(bool isOn)
    {
        _text.color = isOn ? Color.red : Color.black;
        SoundControlUnit.Instance.PlayUiClip(UiClip.GameTypeButton);
    }
}