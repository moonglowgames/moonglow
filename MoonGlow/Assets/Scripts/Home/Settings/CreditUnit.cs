﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class CreditUnit : MonoBehaviour
{
    [SerializeField]
    private SettingsMenuUnit _settings;
    [SerializeField]
    Animator _petAnimator;
    [SerializeField]
    private Text _creditText;

    public TextAsset creditsEng, creditsKo;

    void Start()
    {
        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                _creditText.text = creditsEng.text;
                break;
            case Language.Korean:
                _creditText.text = creditsKo.text;
                break;
        }
    }

    void OnEnable()
    {
        _petAnimator.SetTrigger("Action");
    }

    public void ToggleView(bool state)
    {
        this.gameObject.SetActive(state);
    }

    public void ChangeLanguage(int i)
    {
        _settings.OptionClose();
    }
}