﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Language { English, Korean }
public enum Country { Korea, USA, China, Japan, France, Germany}

public class SettingsMenuUnit : MonoBehaviour
{
    [SerializeField]
    private GameObject _mainTab;
    [SerializeField]
    private Text _version, _userId;
    [SerializeField]
    private LanguageUnit _languageUnit;
    [SerializeField]
    private CreditUnit _creditUnit;
    [SerializeField]
    private Toggle _sound, _music, _vibration;
    [SerializeField]
    private Text _language;
    [SerializeField]
    private LocalizationUnit _title;

    void Start()
    {
        _version.text = LocalizationManager.Instance.Get("CLIENT_VER") + " " + EncryptedPlayerPrefs.GetString(PlayerPrefsKey.clientVersion);
        _userId.text = "ID [" + EncryptedPlayerPrefs.GetString(PlayerPrefsKey.account) + "]";
        _sound.isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.sound, 1) == 1;
        _music.isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.music, 1) == 1;
        _vibration.isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.vibration, 1) == 1;
        UpdateLanguage();

        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void SetDefaultTab()
    {
        //transform.FindChild("Canvas/SettingsList").GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        OptionClose();
    }

    public void SoundOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.sound, isOn ? 1 : 0);
        SoundControlUnit.Instance.isOn = isOn;
    }

    public void MusicOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.music, isOn ? 1 : 0);
        MusicControlUnit.Instance.isOn = isOn;
        if (isOn)
        {
            MusicControlUnit.Instance.Play();
        }
        else
        {
            MusicControlUnit.Instance.Stop();
        }
    }

    public void PushNotificationOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.pushNoti, isOn ? 1 : 0);
    }

    public void VibrationOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.vibration, isOn ? 1 : 0);
    }

    public void SaveBatteryOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt("SaveBattery", isOn ? 1 : 0);
    }

    public void LogOutOption()
    {
        GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "LogOutProcedure", null, "LOG_OUT");
    }

    void LogOutProcedure()
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.autoLogin, 0);
        PacketManager.Instance.SendCSReqLogout();
        GameControlUnit.Instance.ForceLogin = true;
        SceneManager.LoadScene(0);
    }

    public void CreditsOption()
    {
        _title.UpdateUI("CREDITS");
        _mainTab.SetActive(false);
        _creditUnit.ToggleView(true);
    }

    public void LanguageOption()
    {
        _title.UpdateUI("LANGUAGE");
        _mainTab.SetActive(false);
        _languageUnit.LoadLanguageSettings();
        _languageUnit.ToggleView(true);
    }

    public void OptionClose()
    {
        _title.UpdateUI("SETTINGS");
        _mainTab.SetActive(true);
        _languageUnit.ToggleView(false);
        _creditUnit.ToggleView(false);
    }

    public void UpdateLanguage()
    {
        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                _language.text = "English";
                break;
            case Language.Korean:
                _language.text = "한국어";
                break;
        }
    }

    public void LaunchTutorial()
    {
        SceneManager.LoadScene(3);
    }

    public void LaunchIntro()
    {
        GameControlUnit.Instance.LastScene = 1;
        SceneManager.LoadScene(4);
    }
}