﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class LanguageUnit : MonoBehaviour
{
    [SerializeField]
    private SettingsMenuUnit _settings;
    [SerializeField]
    private Text[] _languages;

    public void LoadLanguageSettings()
    {
        if (_languages == null) { return; }
        for (int i = 0; i <= _languages.GetUpperBound(0); i++)
        {
            _languages[i].color = i == EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0) ? Color.red : Color.black;
        }
    }

    public void ToggleView(bool state)
    {
        this.gameObject.SetActive(state);
    }

    public void Chosen(int i)
	{
        GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "ChangeLanguage", i, "LANG_CONFIRM");
        SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton);
    }

    public void ChangeLanguage(int i)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.language, i);
        _settings.UpdateLanguage();
        _settings.OptionClose();
        LocalizationManager.Instance.ChangeLanguage();
        PacketManager.Instance.SendCSNotiChangeLanguage((Language)i);
    }
}