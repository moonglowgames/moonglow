﻿using UnityEngine;
using UnityEngine.UI;

public class SettingOption : MonoBehaviour
{
	private Image _image;
    private LocalizationUnit _localization;

    public Sprite buttonOn;
	public Sprite buttonOff;

	void Awake()
	{
		_image = GetComponent<Image>();
        _localization = transform.FindChild("Text").GetComponent<LocalizationUnit>();
    }

	public void OnClick(bool isOn)
	{
		_image.sprite = isOn? buttonOn : buttonOff;
        if (isOn) { SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton); }
        _localization.UpdateUI(isOn ? "ON" : "OFF");
    }
}