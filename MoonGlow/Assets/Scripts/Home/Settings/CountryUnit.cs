using UnityEngine;
using UnityEngine.UI;

public class CountryUnit : MonoBehaviour
{
    private Canvas _canvas;
    private SettingsMenuUnit _settings;

    //private Country _currentCountry;

    void Awake()
	{
        _canvas = GetComponent<Canvas>();
        _settings = GetComponentInParent<SettingsMenuUnit>();
    }

    public void LoadCountrySettings(Country country)
    {
        //_currentCountry = country;
        //Toggle[] _countries = transform.FindChild("Countries").GetComponentsInChildren<Toggle>();
        //_countries[(int)_currentCountry].isOn = true;
        transform.FindChild("Countries/" + country.ToString()).GetComponent<Toggle>().isOn = true;
    }

    public void ToggleView(bool state)
    {
        _canvas.enabled = state;
    }

	public void Chosen(int i)
	{
        //_currentCountry = (Country)i;
	}

	public void Complete()
	{
		//_settings.UpdateCountry(_currentCountry);
		_settings.OptionClose();
	}
}
