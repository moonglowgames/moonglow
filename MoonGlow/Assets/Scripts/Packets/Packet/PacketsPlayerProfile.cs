﻿using LitJson;
using System;
using System.Collections.Generic;

//CSReqPlayerProfile
public partial class CSReqPlayerProfile : Packet
{
    public string session = string.Empty;
    public string playerNick = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqPlayerProfile;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["playerNick"] = this.playerNick;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            playerNick = jsonData["playerNick"].ToString();

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class PlayerProfile
{
    public string name { get; set; }
    public int faceIdx { get; set; }
    public int level { get; set; }
    public string nickName { get; set; }
    public int winCount { get; set; }
    public int loseCount { get; set; }
    public int badgeIdx { get; set; }
    public List<int> petList { get; set; }
    public PlayerProfile() { }
    public PlayerProfile(string packetName, int faceIdx_, int level_, string nickName_, int winCount_, int loseCount_, int badgeIdx_, List<int> petList_)
    {
        name = packetName;
        faceIdx = faceIdx_;
        level = level_;
        nickName = nickName_;
        winCount = winCount_;
        loseCount = loseCount_;
        badgeIdx = badgeIdx_;
        petList = petList_;
    }
}

//SCNotiPlayerProfile
public partial class SCNotiPlayerProfile : Packet
{
    public PlayerProfile playerProfile = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiPlayerProfile;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(playerProfile);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            playerProfile = JsonMapper.ToObject<PlayerProfile>(Json);

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}