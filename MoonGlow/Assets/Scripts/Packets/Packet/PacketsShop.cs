﻿using LitJson;
using System;
using System.Collections.Generic;

public enum TabType { Items = 1, Joker = 2, Gold = 3, Ruby = 4, Character = 5, Pet = 6, Badge = 7, CardSkin = 8 }
public enum PayType { None , Gold , Ruby, Cash, AD_GOLD, AD_RUBY}
public enum SpecialType { Normal = 0, Sale = 1, Best = 2, SoldOut = 3, SaleAndBest = 4, SaleAndSoldOut = 5 }
public enum ShopResult { Unknown = 0, OK=1, LackRuby = 2, LackGold = 3, CancelPayment = 4, AlreadyExist = 5, Fail = 6 }
public enum IAPType { Unknown = 0, AppStore = 1, GooglePlay = 2 }
public enum IAPResult { Unknown = 0, OK = 1, Fail = 2, Cancel = 3 }

//CSReqShopList
public partial class CSReqShopList : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqShopList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class ShopListInfo 
{
    public TabType tabID { get; set; } 
    public int elementIdx { get; set; }
    public PayType payType  { get; set; } // ( Gold = 0, Ruby = 1, Cash = 2, AD = 3 )
    public int quantity { get; set; }
    public double price { get; set; }
    public SpecialType specialType { get; set; } //( Normal = 0, Sale = 1, Best = 2 )
    public double salePrice { get; set; }

    public ShopListInfo() { }
    public ShopListInfo(TabType tabID_, int elementIdx_, PayType payType_, int quantity_, double price_, SpecialType specialType_, double salePrice_)
    {
        tabID = tabID_;
        elementIdx = elementIdx_;
        payType = payType_;
        this.quantity = quantity_;
        price = price_;
        specialType = specialType_;
        salePrice = salePrice_;
    }
}

public class ShopList 
{
    public string name = string.Empty;
    public List<ShopListInfo> shopList { get; set; }

    public ShopList() { }
    public ShopList(string packetName_, List<ShopListInfo> shopList_) 
    {
        name = packetName_;
        shopList = shopList_;
    }
}

//SCNotiShopList
public partial class SCNotiShopList : Packet
{
    public ShopList shopList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiShopList;
        base.Init();
    }
   
    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = JsonMapper.ToJson(shopList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            shopList = JsonMapper.ToObject<ShopList>(Json);
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqBuyElement 
public partial class CSReqBuyElement : Packet
{
    public string session = string.Empty;
    public int elementIdx = 0;
    public int count = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqBuyElement;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["elementIdx"] = this.elementIdx;
        element["count"] = this.count;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            elementIdx = Convert.ToInt32(jsonData["elementIdx"].ToString());
            count = int.Parse(jsonData["count"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspBuyElement 
public partial class SCRspBuyElement : Packet
{
    public ShopResult result = ShopResult.OK;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspBuyElement;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["result"] = this.result.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            result = (ShopResult)Enum.Parse(typeof(ShopResult), jsonData["result"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCNotiShowAD
public partial class SCNotiShowAD : Packet
{
    public bool isOK = false;
    public string guid { get; set; }
    public int todayRemainCount = 0;
    public string remainTime { get; set; }

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiShowAD;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["isOK"] = this.isOK;
        element["guid"] = this.guid;
        element["todayRemainCount"] = this.todayRemainCount;
        element["remainTime"] = this.remainTime;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            isOK = Boolean.Parse(jsonData["isOK"].ToString());
            guid = jsonData["guid"].ToString();
            todayRemainCount = int.Parse(jsonData["todayRemainCount"].ToString());
            remainTime = jsonData["remainTime"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqFinishAD
public partial class CSNotiFinishAD : Packet
{
    public string session = string.Empty;
    public string guid = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSNotiFinishAD;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["guid"] = this.guid;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            guid = jsonData["guid"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class SCNotiGetADItem : Packet
{
    public int itemIdx = 0;
    public int itemCount = 0;
    public EffectPosition pos = EffectPosition.PlayCenter;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiGetADItem;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["itemIdx"] = this.itemIdx;
        element["itemCount"] = this.itemCount;
        element["pos"] = this.pos.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            itemIdx = int.Parse(jsonData["itemIdx"].ToString());
            itemCount = int.Parse(jsonData["itemCount"].ToString());
            pos = (EffectPosition)Enum.Parse(typeof(EffectPosition), jsonData["pos"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqIAP = 520
public partial class CSReqIAP : Packet
{
    public string session = string.Empty;
    public IAPType iAPType = IAPType.Unknown;
    public int elementIdx = 0;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqIAP;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["elementIdx"] = this.elementIdx;
        element["iAPType"] = this.iAPType.ToString();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            elementIdx = int.Parse(jsonData["elementIdx"].ToString());
            iAPType = (IAPType)Enum.Parse(typeof(IAPType), jsonData["iAPType"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCRspIAP = 521
public partial class SCRspIAP : Packet
{
    public string guid = string.Empty;
    public int elementIdx = 0;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspIAP;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["guid"] = this.guid;
        element["elementIdx"] = this.elementIdx;
        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            guid = jsonData["guid"].ToString();
            elementIdx = int.Parse(jsonData["elementIdx"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqIAPResult = 522
public partial class CSReqIAPResult : Packet
{
    public string session = string.Empty;
    public string guid = string.Empty;
    public IAPResult result = IAPResult.Unknown;
    
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqIAPResult;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["guid"] = this.guid;
        element["result"] = this.result.ToString();
        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            guid = jsonData["guid"].ToString();
            result = (IAPResult)Enum.Parse(typeof(IAPResult), jsonData["result"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCRspIAPResult = 523
public partial class SCRspIAPResult : Packet
{
    public string guid = string.Empty;
    public ShopResult result = ShopResult.Unknown;
    
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspIAPResult;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["guid"] = this.guid;
        element["result"] = this.result.ToString();
        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            guid = jsonData["guid"].ToString();
            result = (ShopResult)Enum.Parse(typeof(ShopResult), jsonData["result"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}