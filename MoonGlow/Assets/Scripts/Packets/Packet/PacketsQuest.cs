﻿using LitJson;
using System;
using System.Collections.Generic;

public enum MissionType { MISSION = 1, EVENT = 2 }
public enum MissionState { Normal = 0, Complete = 1, Fail = 2 }

//CSReqAchieveList 
public partial class CSReqAchieveList : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqAchieveList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class Achievement
{
    public int achieveIdx { get; set; }
    public int currentValue { get; set; }
    public int targetValue { get; set; }
    public Achievement() { }
    public Achievement(int achieveIdx_, int currentValue_, int targetValue_)
    {
        achieveIdx = achieveIdx_;
        currentValue = currentValue_;
        targetValue = targetValue_;
    }
}
public class AchieveList
{
    public string name { get; set; }
    public List<Achievement> achieveList { get; set; }
    public AchieveList() { }
    public AchieveList(string packetName, List<Achievement> achieveList_)
    {
        name = packetName;
        achieveList = achieveList_;
    }
}

//SCNotiAchieveList
public partial class SCNotiAchieveList : Packet
{
    public AchieveList achieveList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiAchieveList;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(achieveList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            achieveList = JsonMapper.ToObject<AchieveList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqMissionList
public partial class CSReqMissionList : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqMissionList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class Mission
{
    public MissionType missionType { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string startDate { get; set; }
    public string endDate { get; set; }
    public int currentValue { get; set; }
    public int targetValue { get; set; }
    public int itemIdx1 { get; set; }
    public int itemCount1 { get; set; }
    public int itemIdx2 { get; set; }
    public int itemCount2 { get; set; }
    public MissionState state { get; set; }
    public Mission() { }
    public Mission(MissionType missionType_, string title_, string description_, string startDate_, string endDate_, int currentValue_, int targetValue_, int itemIdx1_, int itemCount1_, int itemIdx2_, int itemCount2_, MissionState state_)
    {
        missionType = missionType_;
        title = title_;
        description = description_;
        startDate = startDate_;
        endDate = endDate_;
        currentValue = currentValue_;
        targetValue = targetValue_;
        itemIdx1 = itemIdx1_;
        itemCount1 = itemCount1_;
        itemIdx2 = itemIdx2_;
        itemCount2 = itemCount2_;
        state = state_;
    }
}
public class MissionList
{
    public string name { get; set; }
    public List<Mission> missionList { get; set; }
    public MissionList() { }
    public MissionList(string packetName, List<Mission> missionList_)
    {
        name = packetName;
        missionList = missionList_;
    }
}

//SCNotiMissionList
public partial class SCNotiMissionList : Packet
{
    public MissionList missionList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiMissionList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(missionList);
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            missionList = JsonMapper.ToObject<MissionList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}