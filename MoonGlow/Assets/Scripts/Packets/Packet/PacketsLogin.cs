using System;
using LitJson;

public enum ServerState { Unkown = 0, Normal = 1, Ready = 2, ShutDown = 3 }
public enum AccountType { Unknown = 0, Email = 1, Google = 2, Facebook = 3, Guest = 4 }
public enum ConnectType { Unkown = 0, iOS = 100, Android = 200, WinStore = 300, Web = 400, PC = 500, Mac = 600 }
public enum LoginState { Unknown = 0, OK = 1, Fail = 2, NoID = 3, NoNick = 4, MissPassword = 5, IDExist = 6, NickExist = 7, IDAndNickExist = 8, ServerReadyFail = 9, OldVersion = 10, Ban = 11, GuestOK = 12 }
public enum UserType { User = 0, Operator = 1, OperatorManager = 2, Developer = 3, DeveloperManager = 4 }
public enum SceneType { Unknown = 0, Close = 1, loading = 2, Home = 3 }
public enum UrlType { Unknown = 0, HomePage = 1, AppStore = 2, GooglePlay = 3, MSStore = 4, Manual = 5, Facebook = 6 }

/* 
 * 클라이언트가 접속하면 서버의 정보를 알려준다. 
 * Show Server’s Info when Client connect 
 * [SCNotiHello] ( svrrname,  ver, state, info  )
	svrrname = ‘MoonGlowLoginServer’
		ver  = ‘MGLV1.0.0’
		state = [ Unkown= 0, Normal = 1 , Ready = 2 , ShutDown = 3 ]
		info = ‘Don’t Connect 7:00 PM’
*/
public partial class SCNotiHello : Packet
{
	public string svrName = string.Empty;// GlobalConfig.Instance.ServerName;
	public string ver = string.Empty;// ServerConfig.Instance.ServerVirsion;
	public ServerState state  = ServerState.Unkown; // ServerStateManager.Instacnce.ServerState;
	public string info = string.Empty;//ServerConfig.Instance.ServiceInfo;

	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiHello;
		//call base init...
		base.Init ();
	}

	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		element ["svrName"] = this.svrName;
		element ["ver"] = this.ver;
		element ["state"] = this.state.ToString();
		element ["info"] = this.info;

		retvalue = element.ToJson ();
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
	    //{'name':'SCNotiHello','svrname':'MoonGlowLoginServer','ver':'MGLV1.0.0','state':1,'info':'Don?t Connect 7:00 PM'}
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			svrName = jsonData ["svrName"].ToString ();
			ver = jsonData["ver"].ToString();
			state = (ServerState)Enum.Parse(typeof(ServerState),jsonData["state"].ToString());
			info = jsonData["info"].ToString();
			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		throw new NotImplementedException ();
	}
	#endregion
}

//CSReqRegister
public partial class CSReqRegister : Packet
{
    public AccountType accountType = AccountType.Unknown;
    public string account = string.Empty;
    public string password = string.Empty;
    public string nick = string.Empty;
    public int faceIdx = 0;
    public string deviceID = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqRegister;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["accountType"] = this.accountType.ToString();
        element["account"] = this.account;
        element["password"] = this.password;
        element["nick"] = this.nick;
        element["faceIdx"] = this.faceIdx;
        element["deviceID"] = this.deviceID;
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        //{'name':'SCNotiHello','svrname':'MoonGlowLoginServer','ver':'MGLV1.0.0','state':1,'info':'Don?t Connect 7:00 PM'}
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            accountType = (AccountType)Enum.Parse(typeof(AccountType), jsonData["accountType"].ToString());
            account = jsonData["account"].ToString();
            password = jsonData["password"].ToString();
            nick = jsonData["nick"].ToString();
            faceIdx = int.Parse(jsonData["faceIdx"].ToString());
            deviceID = jsonData["deviceID"].ToString();
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}


//SCRspRegister
public partial class SCRspRegister : Packet
{
    public LoginState state = LoginState.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspRegister;
        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["state"] = this.state.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        //{'name':'SCNotiHello','svrname':'MoonGlowLoginServer','ver':'MGLV1.0.0','state':1,'info':'Don?t Connect 7:00 PM'}
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            state = (LoginState)Enum.Parse(typeof(LoginState), jsonData["state"].ToString());
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqLogin = 110
public partial class CSReqLogin : Packet
{
    public AccountType accountType = AccountType.Unknown;
    public string account = string.Empty;
    public string password = string.Empty;
    public string deviceID = string.Empty;
    public ConnectType conType = ConnectType.Unkown;
    public string conVer = string.Empty;
    public Language language = Language.English;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqLogin;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["accountType"] = this.accountType.ToString();
        element["account"] = this.account;
        element["password"] = this.password;
        element["deviceID"] = this.deviceID;
        element["conType"] = this.conType.ToString();
        element["conVer"] = this.conVer;
        element["language"] = this.language.ToString();

        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            accountType = (AccountType)Enum.Parse(typeof(AccountType), jsonData["accountType"].ToString());
            account = jsonData["account"].ToString();
            password = jsonData["password"].ToString();
            deviceID = jsonData["deviceID"].ToString();
            conType = (ConnectType)Enum.Parse(typeof(ConnectType), jsonData["conType"].ToString());
            conVer = jsonData["conVer"].ToString();
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiLogin = 111
 * 서버의 로그인 응답
 * Server respons CSReqLogin
 * [ SCRspLogin ]  ( state , session, connectUrl )
	session : ‘1234XERTT’
	connectUrl : ‘http://192.168.0.23:5000’
	a. ‘tcp://192.168.0.23:4500’  = require TCP/IP Connect 
	b. ‘ws://192.168.0.48:2300’ = require SocketIO Connect
	c. ‘https://192.168.0.23/login’ = https RESTful
	d. ‘http://192.168.0.23/game’ = http RESTful
*/
public partial class SCNotiLogin : Packet
{
    public LoginState state = LoginState.Unknown;
    public UserType userType = UserType.User;
    public string userID = string.Empty;
    public string session = string.Empty;
    public string connectUrl = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiLogin;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["state"] = this.state.ToString();
        element["userType"] = this.userType.ToString();
        element["userID"] = this.userID;
        element["session"] = this.session;
        element["connectUrl"] = this.connectUrl;

        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            state = (LoginState)Enum.Parse(typeof(LoginState), jsonData["state"].ToString());
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());
            userID = jsonData["userID"].ToString();
            session = jsonData["session"].ToString();
            connectUrl = jsonData["connectUrl"].ToString();

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiDownload
public partial class SCNotiDownload : Packet
{
    public ConnectType connectType = ConnectType.Unkown;
    public string downloadUrl = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiDownload;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["connectType"] = this.connectType.ToString();
        element["downloadUrl"] = this.downloadUrl;

        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            connectType = (ConnectType)Enum.Parse(typeof(ConnectType), jsonData["connectType"].ToString());
            downloadUrl = jsonData["downloadUrl"].ToString();

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqLogout
public partial class CSReqLogout : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqLogout;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiLogout
public partial class SCNotiLogout : Packet
{
    public bool result = false;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiLogout;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["result"] = this.result;
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            result = Boolean.Parse(jsonData["result"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiConnections = 140
public partial class SCNotiConnections : Packet
{
    public int connections = 0;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiConnections;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["connections"] = this.connections;
        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            connections = int.Parse(jsonData["connections"].ToString());

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCReqPing = 150
public partial class SCReqPing : Packet
{
    public int pingCount = 0;
    public string msg = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCReqPing;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["pingCount"] = this.pingCount;
        element["msg"] = this.msg;
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            pingCount = int.Parse(jsonData["pingCount"].ToString());
            msg = jsonData["msg"].ToString();

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSRspPang = 151
public partial class CSRspPang : Packet
{
    public string session = string.Empty;
    public int pingCount = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSRspPang;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["pingCount"] = this.pingCount;
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            pingCount = int.Parse(jsonData["pingCount"].ToString());

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiReconnect = 160
public partial class SCNotiReconnect : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiReconnect;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiQuit = 170
public partial class SCNotiQuit : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiQuit;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class CSReqFindPassword : Packet
{
    public string account = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqFindPassword;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["account"] = this.account;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            account = jsonData["account"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqChangePassword
public partial class CSReqChangePassword : Packet
{
    public string account = string.Empty;
    public string oldPassword = string.Empty;
    public string newPassword = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqChangePassword;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["account"] = this.account;
        element["oldPassword"] = this.oldPassword;
        element["newPassword"] = this.newPassword;

        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            account = jsonData["account"].ToString();
            oldPassword = jsonData["oldPassword"].ToString();
            newPassword = jsonData["newPassword"].ToString();
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class SCRspPasswordResult : Packet
{
    public PacketTypeList pckType = PacketTypeList.Unknown;
    public LoginState state = LoginState.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspPasswordResult;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["pckType"] = this.pckType.ToString();
        element["state"] = this.state.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            state = (LoginState)Enum.Parse(typeof(LoginState), jsonData["state"].ToString());
            pckType = (PacketTypeList)Enum.Parse(typeof(PacketTypeList), jsonData["pckType"].ToString());

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * CSReqSessionLogin = 200
 * Request Login GameServer with Session
 * [CSReqSessionLogin]( session )
	session
*/
public partial class CSReqSessionLogin  : Packet
{
	public string session = string.Empty;
    public string userID = string.Empty;
    public Language language = Language.English;

    #region implemented abstract members of Packet
    public override void Init ()
	{
		packetType = PacketTypeList.CSReqSessionLogin;

		base.Init ();
	}

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["userID"] = this.userID;
        element["language"] = this.language.ToString();

        retvalue = element.ToJson();

        return retvalue;
    }

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}

	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
        if (name == Name)
        {
            session = jsonData["session"].ToString();
            userID = jsonData["userID"].ToString();
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());
            retvalue = true;
        }
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * SCNotiSessionLogin = 201
 * Respons of SessionLogin and Server Notify Session Change
 * [SCNotiSessionLogin] ( status , newSession )
	status : ( fail = 0 , sucess = 1 )
	newsession : if ( status == sucess ) newSession 
*/
public partial class SCNotiSessionLogin  : Packet
{
	public bool status = true;
	public string newsession = string.Empty;
    public UserType userType = UserType.User;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiSessionLogin;

		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();

		element["status"] = this.status;
		element ["newsession"] = this.newsession;
        element["userType"] = this.userType.ToString();

        retvalue = element.ToJson ();
		return retvalue;
	}

	protected override string ElementToTelnet ()
	{
        throw new NotImplementedException();
    }

	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name) {
			status = Convert.ToBoolean(jsonData["status"].ToString());
			newsession = jsonData["newsession"].ToString();
            userType = (UserType)Enum.Parse(typeof(UserType), jsonData["userType"].ToString());

            retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * CSReqGotoScene = 210
 * 게임로딩을 끝내고 신에 들어가고자 요청 한다.
 * Game Ready and Client wnat to go to some SCene
 * [CSReqGotoScene] (session, SCene )
	session
	scene: [  Unknow = 0, Close = 1 , loading = 2 , Home = 3... ]
*/
public partial class CSReqGotoScene : Packet
{
	public string session = string.Empty;
	public SceneType scene =  SceneType.Unknown;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.CSReqGotoScene ;

		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();

		element["session"] = this.session;
		element ["scene"] = this.scene.ToString();
		
		retvalue = element.ToJson ();
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name) {
			session =jsonData["session"].ToString();
			scene =  (SceneType)Enum.Parse(typeof(SceneType), jsonData["scene"].ToString());
			
			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * SCNotiGotoScene = 211
 * 서버가 특정신에 들어가라고 알려준다.
 * Server notity that client go to some SCene
 * [SCNotiGotoScene] (scene )
	scene: [ Unknow = 0, Close = 1, loading = 2 , Home = 3 …]
*/
public partial class SCNotiGotoScene : Packet
{
    public SceneType scene = SceneType.Unknown;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiGotoScene;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["scene"] = this.scene.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            scene = (SceneType)Enum.Parse(typeof(SceneType), jsonData["scene"].ToString());
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqClientPause
public partial class CSReqClientPause : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqClientPause;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqClientFocus
public partial class CSReqClientFocus : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqClientFocus;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqClientFocus
public partial class SCNotiClientFocus : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqClientFocus;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqUrl
public partial class CSReqUrl : Packet
{
    public string session = string.Empty;
    public UrlType urlType = UrlType.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqUrl;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["urlType"] = this.urlType.ToString();
        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            urlType = (UrlType)Enum.Parse(typeof(UrlType), jsonData["urlType"].ToString());

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiUrl
public partial class SCNotiUrl : Packet
{
    public UrlType urlType = UrlType.Unknown;
    public string url = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiUrl;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["urlType"] = this.urlType.ToString();
        element["url"] = this.url;
        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            urlType = (UrlType)Enum.Parse(typeof(UrlType), jsonData["urlType"].ToString());
            url = jsonData["url"].ToString();

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}