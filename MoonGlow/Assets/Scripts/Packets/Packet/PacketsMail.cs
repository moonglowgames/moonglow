﻿using LitJson;
using System;
using System.Collections.Generic;

//CSReqMailList
public partial class CSReqMailList : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqMailList;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

public class Mail
{
    public int mailIdx { get; set; }
    public string subject { get; set; }
    public string content { get; set; }
    public int itemIdx1 { get; set; }
    public int itemCount1 { get; set; }
    public bool isGot { get; set; }
    public Mail() { }
    public Mail(int mailIdx_, string subject_, string content_, int itemIdx1_, int itemCount1_, bool isGot_)
    {
        mailIdx = mailIdx_;
        subject = subject_;
        content = content_;
        itemIdx1 = itemIdx1_;
        itemCount1 = itemCount1_;
        isGot = isGot_;
    }
}

public class MailList
{
    public List<Mail> mailList { get; set; }
    public string name { get; set; }
    public MailList() { }
    public MailList(string packetName, List<Mail> mailList_)
    {
        name = packetName;
        mailList = mailList_;
    }
}
//SCNotiMailList 
public partial class SCNotiMailList : Packet
{
    public MailList mailList = null;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiMailList;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(mailList);
        //retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            mailList = JsonMapper.ToObject<MailList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqGetItem  
public partial class CSReqGetItem : Packet
{
    public string session = string.Empty;
    public int mailIdx = 0;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqGetItem;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["mailIdx"] = this.mailIdx;
        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            mailIdx = int.Parse(jsonData["mailIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCRspGetItem   
public partial class SCRspGetItem : Packet
{
    public bool getResult = false;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspGetItem;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["getResult"] = this.getResult.ToString();

        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            getResult = Boolean.Parse(jsonData["getResult"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqGetAllItems    
public partial class CSReqGetAllItems : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqGetAllItems;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCRspGetAllItems     
public partial class SCRspGetAllItems : Packet
{
    public bool getResult = false;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCRspGetAllItems;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["getResult"] = this.getResult.ToString();

        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            getResult = Boolean.Parse(jsonData["getResult"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqRemoveList      
public partial class CSReqRemoveOld : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqRemoveOld;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();

        return retvalue;
        //throw new NotImplementedException ();
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}