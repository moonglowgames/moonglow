﻿using System;
using LitJson;
using System.Collections.Generic;

[Flags]
public enum SpellType { Unknown = 0, MoonDust = 1, Mana = 2, RodEnergy = 4 }
public enum SpellResult { Unknown = 0, OK = 1, LackInk = 2, MaxSpellLevel = 3, NoUpgrade = 4, Fail = 5 }

//CSReqJokerInfo
public partial class CSReqJokerInfo : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqJokerInfo;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class SpellIdxList
{
    public string name { get; set; }
    public int ink { get; set; }
    public int tryInkCount { get; set; }
    public int totalInk { get; set; }

    public List<int> spellIdxList { get; set; }

    public SpellIdxList() { }
    public SpellIdxList(string packetName, int ink_, int tryInckCount_, int totalInk_, List<int> spellIdxList_)
    {
        name = packetName;
        ink = ink_;
        tryInkCount = tryInckCount_;
        totalInk = totalInk_;
        spellIdxList = spellIdxList_;
    }
}

//SCNotiJokerInfo
public partial class SCNotiJokerInfo : Packet
{
    public int jokerFace = 0;
    public bool isTry = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiJokerInfo;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["jokerFace"] = this.jokerFace;
        element["isTry"] = this.isTry;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            jokerFace = int.Parse(jsonData["jokerFace"].ToString());
            isTry = Boolean.Parse(jsonData["isTry"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCNotiSpellList 
public partial class SCNotiSpellList : Packet
{
    public SpellIdxList spellIdxList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiSpellList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(spellIdxList);
        //retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            spellIdxList = JsonMapper.ToObject<SpellIdxList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqUpgradeSpell 
public partial class CSReqUpgradeSpell : Packet
{
    public string session = string.Empty;
    public int currentSpellIdx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqUpgradeSpell;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["currentSpellIdx"] = this.currentSpellIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            currentSpellIdx = Convert.ToInt32(jsonData["currentSpellIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqDowngradeSpell
public partial class CSReqDowngradeSpell : Packet
{
    public string session = string.Empty;
    public int currentSpellIdx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqDowngradeSpell;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["currentSpellIdx"] = this.currentSpellIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            currentSpellIdx = Convert.ToInt32(jsonData["currentSpellIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCRspUpgradeSpell 
public partial class SCRspUpdateSpell : Packet
{
    public SpellResult applyResult = SpellResult.Unknown;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspUpdateSpell;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["applyResult"] = this.applyResult.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            applyResult = (SpellResult)Enum.Parse(typeof(SpellResult), jsonData["applyResult"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public partial class CSReqJokerCancel : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqJokerCancel;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public partial class CSReqJokerApply : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqJokerApply;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspApplySpells 
public partial class SCRspJokerApply : Packet
{
    public bool applyResult = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspJokerApply;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["applyResult"] = this.applyResult;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            applyResult = Boolean.Parse(jsonData["applyResult"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}