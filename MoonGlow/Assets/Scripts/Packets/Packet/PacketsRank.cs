﻿using LitJson;
using System;
using System.Collections.Generic;

public enum ChanelType { Unknown = 0, Beginner = 1, Intermediate = 2, Advance = 3 }
public enum RankType { Unknown = 0, Count = 1, Percent = 2 }
public enum ChanelResult { Unknown = 0, OK = 1, NotAccept = 2, LevelLimit = 3, Fail = 4 }

//CSReqChanelList 
public partial class CSReqChanelList : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqChanelList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class Chanel
{
    public int chanelIdx { get; set; }
    public ChanelType type { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public int minLevel { get; set; }
    public int maxLevel { get; set; }
    public string startDate { get; set; }
    public string endDate { get; set; }
    public int currentCount { get; set; }
    public int maxCount { get; set; }
    public bool isJoin { get; set; }
    public bool enable { get; set; }
    public Chanel() { }
    public Chanel(int chanelIdx_, ChanelType type_, string name_, string description_, int minLevel_, int maxLevel_, string startDate_, string endDate_, int currentCount_, int maxCount_, bool isJoin_, bool enable_)
    {
        chanelIdx = chanelIdx_;
        type = type_;
        name = name_;
        description = description_;
        minLevel = minLevel_;
        maxLevel = maxLevel_;
        startDate = startDate_;
        endDate = endDate_;
        currentCount = currentCount_;
        maxCount = maxCount_;
        isJoin = isJoin_;
        enable = enable_;
    }
}

public class ChanelList
{
    public string name { get; set; }
    public List<Chanel> chanelList { get; set; }
    public ChanelList() { }
    public ChanelList(string packetName, List<Chanel> chanelList_)
    {
        name = packetName;
        chanelList = chanelList_;
    }
}

//SCNotiChanelList 
public partial class SCNotiChanelList : Packet
{
    public ChanelList chanelList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiChanelList;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(chanelList);
        //retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            chanelList = JsonMapper.ToObject<ChanelList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqJoinChanel  
public partial class CSReqJoinChanel : Packet
{
    public string session = string.Empty;
    public int chanelIdx = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqJoinChanel;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["chanelIdx"] = this.chanelIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            chanelIdx = int.Parse(jsonData["chanelIdx"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqRankList   
public partial class CSReqRankList : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqRankList;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class Rank
{
    public int userLevel { get; set; }
    public string userNick { get; set; }
    public int rankPoint { get; set; }
    public int rankWin { get; set; }
    public int rankLose { get; set; }
    public string winRate { get; set; }
    public int rankPercent { get; set; }
    public Rank() { }
    public Rank(int userLevel_, string userNick_, int rankPoint_, int rankWin_, int rankLose_, string winRate_, int rankPercent_)
    {
        userLevel = userLevel_;
        userNick = userNick_;
        rankPoint = rankPoint_;
        rankWin = rankWin_;
        rankLose = rankLose_;
        winRate = winRate_;
        rankPercent = rankPercent_;
    }
}

public class RankList
{
    public string name { get; set; }
    public string chanelName { get; set; }
    public List<Rank> rankList { get; set; }
    public RankList() { }
    public RankList(string packetName, string chanelName_, List<Rank> rankList_)
    {
        name = packetName;
        chanelName = chanelName_;
        rankList = rankList_;
    }
}
//SCNotiRankList    
public partial class SCNotiRankList : Packet
{
    public RankList rankList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiRankList;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(rankList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            rankList = JsonMapper.ToObject<RankList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqRewardList     
public partial class CSReqRewardList : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqRewardList;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class Reward
{
    public int rank { get; set; }
    public RankType rankType { get; set; }
    public int itemIdx1 { get; set; }
    public int itemCount1 { get; set; }
    public int itemIdx2 { get; set; }
    public int itemCount2 { get; set; }
    public int itemIdx3 { get; set; }
    public int itemCount3 { get; set; }
    public Reward() { }
    public Reward(int rank_, RankType rankType_, int itemIdx1_, int itemCount1_, int itemIdx2_, int itemCount2_, int itemIdx3_, int itemCount3_)
    {
        rank = rank_;
        rankType = rankType_;
        itemIdx1 = itemIdx1_;
        itemCount1 = itemCount1_;
        itemIdx2 = itemIdx2_;
        itemCount2 = itemCount2_;
        itemIdx3 = itemIdx3_;
        itemCount3 = itemCount3_;
    }
}
public class RewardList
{
    public string name { get; set; }
    public List<Reward> rewardList { get; set; }
    public RewardList() { }
    public RewardList(string packetName, List<Reward> rewardList_)
    {
        name = packetName;
        rewardList = rewardList_;
    }
}
//SCNotiRewardLst      
public partial class SCNotiRewardList : Packet
{
    public RewardList rewardList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiRewardList;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(rewardList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            rewardList = JsonMapper.ToObject<RewardList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqMyRankInfo       
public partial class CSReqMyRankInfo : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqMyRankInfo;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class MyRank
{
    public int rankIdx { get; set; }
    public string chanelName { get; set; }
    public int rankCount { get; set; }
    public int rankPercent { get; set; }
    public int rankPoint { get; set; }
    public int rankWin { get; set; }
    public int rankLose { get; set; }
    public string winRate { get; set; }
    public bool isBest { get; set; }
    public bool itemExist { get; set; }
    public bool itemGot { get; set; }
    public MyRank() { }
    public MyRank(int rankIdx_, string chanelName_, int rankCount_, int rankPercent_, int rankPoint_, int rankWin_, int rankLose_, string winRate_, bool isBest_, bool itemExist_, bool itemGot_)
    {
        rankIdx = rankIdx_;
        chanelName = chanelName_;
        rankCount = rankCount_;
        rankPercent = rankPercent_;
        rankPoint = rankPoint_;

        rankWin = rankWin_;
        rankLose = rankLose_;
        winRate = winRate_;

        isBest = isBest_;
        itemExist = itemExist_;
        itemGot = itemGot_;
    }
}

public class RankHistory
{
    public string name { get; set; }
    public string currentChannelName { get; set; }
    public int currentRankCount { get; set; }
    public int currentRankPercent { get; set; }
    public int currentRankPoint { get; set; }
    public int currentRankWin { get; set; }
    public int currentRankLose { get; set; }
    public string currentWinRate { get; set; }
    public List<MyRank> rankHistory { get; set; }
    public RankHistory() { }
    public RankHistory(string packetName, string currentChannelName_, int currentRankCount_, int currentRankPercent_, /*float*/int currentRankPoint_, int currentRankWin_, int currentRankLose_, string currentWinRate_, List<MyRank> rankHistory_)
    {
        name = packetName;
        currentChannelName = currentChannelName_;
        currentRankCount = currentRankCount_;
        currentRankPercent = currentRankPercent_;
        currentRankPoint = currentRankPoint_;

        currentRankWin = currentRankWin_;
        currentRankLose = currentRankLose_;
        currentWinRate = currentWinRate_;

        rankHistory = rankHistory_;
    }
}

//SCNotiMyRankInfo
public partial class SCNotiMyRankInfo : Packet
{
    public RankHistory rankHistory = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiMyRankInfo;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(rankHistory);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            rankHistory = JsonMapper.ToObject<RankHistory>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqGetRankItem        
public partial class CSReqGetRankItem : Packet
{
    public string session = string.Empty;
    public int rankIdx = 0;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqGetRankItem;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["rankIdx"] = this.rankIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            rankIdx = int.Parse(jsonData["rankIdx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspRankResult         
public partial class SCRspRankResult : Packet
{
    public ChanelResult result = ChanelResult.Unknown;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspRankResult;
        base.Init();

        //throw new NotImplementedException ();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["result"] = this.result.ToString();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    //Decode
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            result = (ChanelResult)Enum.Parse(typeof(ChanelResult), jsonData["result"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}