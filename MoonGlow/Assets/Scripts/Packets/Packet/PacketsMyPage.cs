﻿using LitJson;
using System;
using System.Collections.Generic;

//CSReqMyInfo
public partial class CSReqMyInfo : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqMyInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class ItemAbility
{
    public string itemInfo { get; set; }
    public string remainTime { get; set; }
	public ItemAbility(){}
    public ItemAbility(string itemInfo_, string remainTime_)
    {
        itemInfo = itemInfo_;
        remainTime = remainTime_;
    }
}

public class PetAbility
{
    public string petInfo { get; set; }
	public PetAbility(){}
    public PetAbility(string petInfo_)
    {
        petInfo = petInfo_;
    }
}

public class ProfileInfo
{
    public string profileItem { get; set; }
    public string profileDetail { get; set; }

    //test first
	public ProfileInfo(){}
    public ProfileInfo(string profileItem_,string profileDetail_)
    {
        profileItem = profileItem_;
        profileDetail = profileDetail_;
    }
}

public class MyInfo
{
    public string name { get; set; }
    public int faceIdx { get; set; }
    public int exp { get; set; }
    public int nextLevelExp { get; set; }
    public int downLevelExp { get; set; }
    public int stamina { get; set; }
    public int maxStamina { get; set; }
    public string nextStaminaTime { get; set; }
    public int winCount { get; set; }
    public int totalCount { get; set; }
    public string winRate { get; set; }
    public int level { get; set; }
    public string nickName { get; set; }
    public string userID { get; set; }
    public List<int> petList { get; set; }
    public int badgeIdx { get; set; }
    public int cardSkinIdx { get; set; }
    public List<ItemAbility> itemAbilityList { get; set; }
    public List<PetAbility> petAbilityList { get; set; }
    public List<ProfileInfo> profileList { get; set; }

    public MyInfo() { }
    public MyInfo(string packetName, int faceIdx_, int exp_, int nextLevelExp_, int downLevelExp_, int stamina_, int maxStamina_, string nextStaminaTime_, int winCount_, int totalCount_, string winRate_, int level_, string nickName_, string userID_, List<int> petList_, int badgeIdx_, int cardSkinIdx_, List<ItemAbility> itemAbilityList_, List<PetAbility> petAbilityList_, List<ProfileInfo> profileList_)
    {
        name = packetName;
        faceIdx = faceIdx_;
        exp = exp_;
        nextLevelExp = nextLevelExp_;
        downLevelExp = downLevelExp_;
        stamina = stamina_;
        maxStamina = maxStamina_;
        nextStaminaTime = nextStaminaTime_;
        winCount = winCount_;
        totalCount = totalCount_;
        winRate = winRate_;
        level = level_;
        nickName = nickName_;
        userID = userID_;
        petList = petList_;
        badgeIdx = badgeIdx_;
        cardSkinIdx = cardSkinIdx_;
        itemAbilityList = itemAbilityList_;
        petAbilityList = petAbilityList_;
        profileList = profileList_;
    }
}

//SCNotiMyInfo 
public partial class SCNotiMyInfo : Packet
{
    public MyInfo myInfo = null; 
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiMyInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(myInfo);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            myInfo = JsonMapper.ToObject<MyInfo>(Json);
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqInventory 
public partial class CSReqInventory : Packet
{
    public string session = string.Empty;
    public TabType tabType = TabType.Items;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqInventory;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (TabType)Enum.Parse(typeof(TabType), jsonData["tabType"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class InventoryInfo
{
    public int inventoryIdx { get; set; }
    public int count { get; set; }
    public bool select { get; set; }

    public InventoryInfo() { }
    public InventoryInfo(int inventoryIdx_, int count_, bool select_)
    {
        inventoryIdx = inventoryIdx_;
        count = count_;
        select = select_;
    }
}

public class InventoryList
{
	public string name { get; set; }
	public TabType tabType { get; set; }
    public List<InventoryInfo> inventoryList { get; set; }

    public InventoryList() { }
    public InventoryList(string packetName, TabType tabType_, List<InventoryInfo> inventoryList_)
    {
        name = packetName;
        tabType = tabType_;
        inventoryList = inventoryList_;
    }
}

//SCNotiInventory  
public partial class SCNotiInventory : Packet
{
    public InventoryList inventoryList = null;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiInventory;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = JsonMapper.ToJson(inventoryList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            inventoryList = JsonMapper.ToObject<InventoryList>(Json);
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqUseItem 
public partial class CSReqUseItem : Packet
{
    public string session = string.Empty;
    public int itemIdx = 0;
    public bool use = false;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqUseItem;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["itemIdx"] = this.itemIdx;
        element["use"] = this.use;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            itemIdx = Convert.ToInt32(jsonData["itemIdx"].ToString());
            use = Boolean.Parse(jsonData["use"].ToString());

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCRspUseItem  
public partial class SCRspUseItem : Packet
{
    public bool result = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspUseItem;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["result"] = this.result.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            result = Boolean.Parse(jsonData["result"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}