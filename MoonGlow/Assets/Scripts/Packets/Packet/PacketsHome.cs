using LitJson;
using System;
using System.Collections.Generic;

public enum GameType {Unknown = 0, All = 1, Colmaret = 2, Joha = 3, Cheshire = 4, Zatoo = 5, Armdree = 6, Tarassen = 7, Krazan = 8, Yanan = 9}
public enum PlayMode {Unknown = 0, All = 1 , Single2 = 2, Single3 = 3, Single4 = 4, Team = 5}
public enum PlayTime {Unknown = 0, All = 1, Sec_30 = 2 , Sec_60 = 3}
public enum ElementType {Unknow = 0, Stamina = 1, Gold = 2, MoonRuby = 3, HP = 4, SP = 5}
public enum MessageType { Unknown = 0, LackStamina = 1, LowLevel = 2, MaxPetCountAchieved = 3, AlreaySet = 4, LackGold = 5, JoinReject = 6, NoRoom = 7, FullRoom = 8, SendJoinOk = 9, AlreadyPlay = 10, RoomCreator = 11 }

/* 
 * CSNotiSceneEnter = 212
 * 클라이언트가 특정신에 들어갔다고 알려준다. 클라이언트 로딩 끝 
 * Client notify it’s SCene
 * [CSNotiSceneEnter] ( session, scene)
	session:
	scene : [ Unknow = 0, Close = 1 , loading = 2 , Home = 3 …]
*/
public partial class CSNotiSceneEnter : Packet {
	public string session = string.Empty;
	public SceneType scene =  SceneType.Unknown;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.CSNotiSceneEnter ;
		
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		
		element ["session"] = this.session;
		element ["scene"] = this.scene.ToString();
		
		retvalue = element.ToJson ();
		return retvalue;
	}

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}

	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			session = jsonData["session"].ToString();
			scene =  (SceneType)Enum.Parse(typeof(SceneType), jsonData["scene"].ToString());
			
			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		throw new NotImplementedException ();
	}
	#endregion
}

public partial class SCNotiMenuAlarm : Packet
{
    public Option option;
    public string alarm;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiMenuAlarm;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["option"] = this.option.ToString();
        element["alarm"] = this.alarm;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            option = (Option)Enum.Parse(typeof(Option), jsonData["option"].ToString());
            alarm = jsonData["alarm"].ToString();
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiUserInfo = 300
 * 클라이언트에 유저정보를 알려줌
 * [ SCNotiUserInfo] (faceIndex , userName , level , exp , nextLevelExp)
	faceIndex
	userName
	level
	exp
	nextLevelExp
*/
public partial class SCNotiUserInfo : Packet
{
    public int faceIndex;
    public string userName = string.Empty;
    public int badgeIdx;
    public int level;
    public int exp;
    public int nextLevelExp;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiUserInfo;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["faceIndex"] = this.faceIndex;
        element["userName"] = this.userName;
        element["badgeIdx"] = this.badgeIdx;
        element["level"] = this.level;
        element["exp"] = this.exp;
        element["nextLevelExp"] = this.nextLevelExp;

        retvalue = element.ToJson();
        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            faceIndex = int.Parse(jsonData["faceIndex"].ToString());
            userName = jsonData["userName"].ToString();
            badgeIdx = int.Parse(jsonData["badgeIdx"].ToString());
            level = int.Parse(jsonData["level"].ToString());
            exp = int.Parse(jsonData["exp"].ToString());
            nextLevelExp = int.Parse(jsonData["nextLevelExp"].ToString());

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiItemInfo = 301
 * 클라이언트에 아이템 정보를 알려줌
 * [SCNotiItemInfo] ( List< itemIdx , count > )
	List< itemIdx , count > 
*/
public class ItemInfo
{
    public int itemIndex { get; set; }
    public int count { get; set; }

    public ItemInfo() { }

    public ItemInfo(int itemIndex_, int count_)
    {
        itemIndex = itemIndex_;
        count = count_;
    }
}

public class ItemList
{
	public string name;
	public List<ItemInfo> itemList { get; set;}
	
	public ItemList(){}
	
	public ItemList( string packetName , List<ItemInfo> itemList_ )
	{
		name = packetName;
		itemList = itemList_;
    }
}

public partial class SCNotiItemInfo : Packet
{
    public ItemList itemList = null;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiItemInfo;
        base.Init();
    }

    protected override void InitTest()
    {
        List<ItemInfo> itemInfolist = new List<ItemInfo>();
        itemInfolist.Add(new ItemInfo(1, 5));
        itemInfolist.Add(new ItemInfo(2, 10));

        itemList = new ItemList(Name, itemInfolist);
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(itemList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        // Json = @"{""itemList"":[{""itemIndex"":1,""count"":5},{""itemIndex"":2,""count"":10}],""name"":""SCNotiItemInfo""}";ss
        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            itemList = JsonMapper.ToObject<ItemList>(Json);
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiUserElement = 302
 * 클라이언트에 유저의 스테미너 아이템 루비 정보등을 알려줌
 * [SCNotiUserElement ] ( elementList< ElemType , Value , MaxValue > )
	elementList
		elementType : [ Unkown = 0 , Stamina = 1, Coin = 2, Ruby = 3 , HP = 4 , SP = 5 ]
		value : 10
		maxValue : 100
*/
public class ElementInfo
{
    public ElementType elementType { get; set; }
    public int value { get; set; }
    public int maxValue { get; set; }

    public ElementInfo() { }

    public ElementInfo(ElementType elementType_, int value_)
    {
        elementType = elementType_;
        value = value_;
    }
    public ElementInfo(ElementType elementType_, int value_, int maxValue_)
    {
        elementType = elementType_;
        value = value_;
        maxValue = maxValue_;
    }
}

public class ElementList
{
    public string name;
    public List<ElementInfo> elementList { get; set; }

    public ElementList() { }

    public ElementList(string packetName, List<ElementInfo> elementList_)
    {
        name = packetName;
        elementList = elementList_;
    }
}

public partial class SCNotiUserElement : Packet
{
    public ElementList elementList = null;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiUserElement;
        base.Init();
    }
    protected override void InitTest()
    {
        List<ElementInfo> elementInfolist = new List<ElementInfo>();
        elementInfolist.Add(new ElementInfo(ElementType.Stamina, 10, 100));
        elementInfolist.Add(new ElementInfo(ElementType.Gold, 10));

        elementList = new ElementList(Name, elementInfolist);
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(elementList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        // Json = @"{""elementList"":[{""elementType"":1,""value'":10,""maxValue"":100},{""elementType"":2,""value"":10,""maxValue"":0}],""name"":""SCNotiUserElement""}";
        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            elementList = JsonMapper.ToObject<ElementList>(Json);
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

public class ChatIconSet
{
    public string name { get; set; }
    public List<int> iconSet { get; set; }
    public ChatIconSet() { }
    public ChatIconSet(string packetName_, List<int> iconSet_)
    {
        name = packetName_;
        iconSet = iconSet_;
    }
}

//SCNotiChatIconSet = 303
public partial class SCNotiChatIconSet : Packet
{
    public ChatIconSet chatIconSet = null;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiChatIconSet;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(chatIconSet);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);
        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            chatIconSet = JsonMapper.ToObject<ChatIconSet>(Json);
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * CSReqRoomList = 320
 * 클라이언트가 룸리스트를 요청함 
 * [CSReqRoomList] ( session, List<gameType> , List<playMode>, List<playTime>, List<disClose> , count )
    session
    List<gameType>: ( Unknown = 0,  All= 1 , Mercury = 2, Venus = 3, Earth = 4, Mars = 5, Jupiter = 6, Saturn = 7, Uranus = 8, Neptune = 9 )
    List<playMode>: ( Unknown = 0, All = 1 , Single = 2, Team = 3 )
    List<playTime>: ( Unknown = 0, All = 1, 30Sec = 2 , 60 Sec = 3 )
    disClose:
    count = 5 ( list count )
*/
public class ReqRoomList
{
    //packet name
    public string name = string.Empty;
    public string session = string.Empty;
    public List<GameType> gameTypeList { get; set; }
    public List<PlayMode> playModeList { get; set; }
    public List<PlayTime> playTimeList { get; set; }
    public List<bool> disCloseList { get; set; }
    public int count;

    public ReqRoomList() { }

    public ReqRoomList(string packetName, string session_, List<GameType> gameTypeList_, List<PlayMode> playModeList_, List<PlayTime> playTimeList_, List<bool> disCloseList_, int count_)
    {
        name = packetName;

        session = session_;
        gameTypeList = gameTypeList_;
        playModeList = playModeList_;
        playTimeList = playTimeList_;
        disCloseList = disCloseList_;
        count = count_;
    }
}

public partial class CSReqRoomList : Packet
{
    public ReqRoomList reqRoomList = null;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqRoomList;

        base.Init();
    }

    protected override void InitTest()
    {
        List<GameType> gameTypeList = new List<GameType>();
        gameTypeList.Add(GameType.Cheshire);
        gameTypeList.Add(GameType.Armdree);

        List<PlayMode> playModeList = new List<PlayMode>();
        playModeList.Add(PlayMode.Single2);
        playModeList.Add(PlayMode.Single3);

        List<PlayTime> playTimeList = new List<PlayTime>();
        playTimeList.Add(PlayTime.Sec_30);
        playTimeList.Add(PlayTime.Sec_60);

        List<bool> disCloseList = new List<bool>();
        disCloseList.Add(true);
        disCloseList.Add(false);

        reqRoomList = new ReqRoomList(Name, "testsession", gameTypeList, playModeList, playTimeList, disCloseList, 5);
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(reqRoomList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            reqRoomList = JsonMapper.ToObject<ReqRoomList>(Json);
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiRoomList = 321
 * 서버가 룸리스트를 알려준다.
 * [SCNotiRoomList] ( RoomList<roomName, gameType, …., UserList < userName, Level >>) 
	RoomList<roomName, gameType, …., UserList < userName, Level >>
		roomName
		gameType
		playMode
		playTime
		disClose
		UserList < userName, Level >
			userName
			Level
*/
public class RoomUserInfo
{
    public string Name { get; set; }
    public int Level { get; set; }

    public RoomUserInfo() { }

    public RoomUserInfo(string userName, int userLevel)
    {
        Name = userName;
        Level = userLevel;
    }
}

public class RoomInfo
{
    public string roomName { get; set; }
    public GameType gameType { get; set; }
    public PlayMode playMode { get; set; }
    public PlayTime playTime { get; set; }
    public int stake { get; set; }
    public bool disClose { get; set; }
    public int goalHP { get; set; }
    public int goalRP { get; set; }
    public List<RoomUserInfo> userInfoList { get; set; }
    public bool isMine { get; set; }
    public bool isPlaying { get; set; }

    public RoomInfo() { }

    public RoomInfo(string roomName_, GameType gameType_, PlayMode playMode_, PlayTime playTime_, int stake_, bool disClose_, List<RoomUserInfo> userInfolist_, bool isMine_, bool isPlaying_)
    {
        roomName = roomName_;
        gameType = gameType_;
        playMode = playMode_;
        playTime = playTime_;
        stake = stake_;
        disClose = disClose_;
        userInfoList = userInfolist_;
        isMine = isMine_;
        isPlaying = isPlaying_;
    }
}

public class RoomList
{
    public string name;
    public List<RoomInfo> roomList { get; set; }

    public RoomList() { }

    public RoomList(string packetName, List<RoomInfo> roomList_)
    {
        name = packetName;
        roomList = roomList_;
    }
}

public partial class SCNotiRoomList : Packet
{
    public RoomList roomList = null;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiRoomList;
        base.Init();
    }

    public void AddUserInfo(ref List<RoomUserInfo> roomUserInfoList, RoomUserInfo userInfo)
    {
        if (roomUserInfoList == null) roomUserInfoList = new List<RoomUserInfo>();

        roomUserInfoList.Add(userInfo);
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = JsonMapper.ToJson(roomList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            roomList = JsonMapper.ToObject<RoomList>(Json);
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/*CSReqRoomCreate = 330
 * 클라이언트가 룸을 하나 만들어줄것을 요청한다.
 * [CSReqRoomCreate]( session, gameType, playMode, playTime, disClose, isLock)
	session
	gameType
	playMode
	playTime
	disClose
	isLock: (public = 0, private =1)
*/
public partial class CSReqRoomCreate : Packet
{
    public string session = string.Empty;
    public GameType gameType = new GameType();
    public PlayMode playMode = new PlayMode();
    public PlayTime playTime = new PlayTime();
    public int stake = 0;
    public bool disClose;
    public bool isLock;
    public bool singlePlay;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqRoomCreate;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["gameType"] = this.gameType.ToString();
        element["playMode"] = this.playMode.ToString();
        element["playTime"] = this.playTime.ToString();
        element["stake"] = this.stake;
        element["disClose"] = this.disClose;
        element["isLock"] = this.isLock;
        element["singlePlay"] = this.singlePlay;

        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            session = jsonData["session"].ToString();
            gameType = (GameType)Enum.Parse(typeof(GameType), jsonData["gameType"].ToString());
            playMode = (PlayMode)Enum.Parse(typeof(PlayMode), jsonData["playMode"].ToString());
            playTime = (PlayTime)Enum.Parse(typeof(PlayTime), jsonData["playTime"].ToString());
            stake = int.Parse(jsonData["stake"].ToString());
            disClose = Convert.ToBoolean(jsonData["disClose"]);
            isLock = Convert.ToBoolean(jsonData["isLock"]);
            singlePlay = Convert.ToBoolean(jsonData["singlePlay"]);
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * CSReqQuickStart = 340
 * Client request quick start game 
 * [CSReqQuickStart ](session)
 	session
*/
public partial class CSReqQuickStart : Packet
{
    public string session = string.Empty;
    public GameType gameType = GameType.Unknown;
    public PlayMode playMode = PlayMode.Unknown;
    public PlayTime playTime = PlayTime.Unknown;
    public int stake = 0;
    public bool disClose = false;
    public bool isLock = false;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqQuickStart;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["gameType"] = this.gameType.ToString();
        element["playMode"] = this.playMode.ToString();
        element["playTime"] = this.playTime.ToString();
        element["stake"] = this.stake;
        element["disClose"] = this.disClose;
        element["isLock"] = this.isLock;

        retvalue = element.ToJson();
        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            session = jsonData["session"].ToString();
            gameType = (GameType)Enum.Parse(typeof(GameType), jsonData["gameType"].ToString());
            playMode = (PlayMode)Enum.Parse(typeof(PlayMode), jsonData["playMode"].ToString());
            playTime = (PlayTime)Enum.Parse(typeof(PlayTime), jsonData["playTime"].ToString());
            stake = int.Parse(jsonData["stake"].ToString());
            disClose = Boolean.Parse(jsonData["disClose"].ToString());
            isLock = Boolean.Parse(jsonData["isLock"].ToString());
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * CSReqJoinRoom = 350
 * Client request join a room
 * [CSReqJoinRoom](session, roomName, joinType)
	session
	roomName
	joinType (1 = player, 2 = inspector)
*/
public partial class CSReqJoinRoom : Packet
{

    public string session;
    public string roomName;
    public int joinType;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqJoinRoom;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["roomName"] = this.roomName;
        element["joinType"] = this.joinType;

        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            session = jsonData["session"].ToString();
            roomName = jsonData["roomName"].ToString();
            joinType = int.Parse(jsonData["joinType"].ToString());

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiPlayerJoin = 351
public partial class SCNotiPlayerJoin : Packet
{
    public string askerUserID = string.Empty;
    public int askerFaceIdx = 0;
    public string askerNick = string.Empty;
    public int askerLevel = 0;
    //public int askerWinCount = 0;
    //public int askerLoseCount = 0;
    //public string askerWinRate = string.Empty;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiPlayerJoin;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["askerUserID"] = this.askerUserID;
        element["askerFaceIdx"] = this.askerFaceIdx;
        element["askerNick"] = this.askerNick;
        element["askerLevel"] = this.askerLevel;
        //element["askerWinCount"] = this.askerWinCount;
        //element["askerLoseCount"] = this.askerLoseCount;
        //element["askerWinRate"] = this.askerWinRate;

        retvalue = element.ToJson();
        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            askerUserID = jsonData["askerUserID"].ToString();
            askerFaceIdx = int.Parse(jsonData["askerFaceIdx"].ToString());
            askerNick = jsonData["askerNick"].ToString();
            askerLevel = int.Parse(jsonData["askerLevel"].ToString());
            //askerWinCount = int.Parse(jsonData["askerWinCount"].ToString());
            //askerLoseCount = int.Parse(jsonData["askerLoseCount"].ToString());
            //askerWinRate = jsonData["askerWinRate"].ToString();

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSRspPlayerJoin = 352
public partial class CSRspPlayerJoin : Packet
{
    public string session = string.Empty;
    public string askerUserID = string.Empty;
    public bool rspResult = false;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSRspPlayerJoin;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["askerUserID"] = this.askerUserID;
        element["rspResult"] = this.rspResult.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            askerUserID = jsonData["askerUserID"].ToString();
            rspResult = Boolean.Parse(jsonData["rspResult"].ToString());

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSNotiCancelRoom = 353
public partial class CSNotiCancelRoom : Packet
{
    public string session = string.Empty;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSNotiCancelRoom;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqCancelJoin
public partial class CSReqCancelJoin : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqCancelJoin;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiPlayerJoinCancel
public partial class SCNotiPlayerJoinCancel : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiPlayerJoinCancel;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = element.ToJson();

        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiEnterRoom = 360
 * 클라이언트가 룸에 들어가라고 알려준다. ( 게임 시작전 준비해라 )
 * 이때에는 클라언트가 씬을 바꿔줘야됨
 * [SCNotiEnterRoom] ( gameType, playMode, playTime, disClose, lock )
	gameType
	playMode
	playTime
	disClose
	lock
*/
public partial class SCNotiEnterRoom : Packet
{
    public GameType gameType = new GameType();
    public PlayMode playMode = new PlayMode();
    public PlayTime playTime = new PlayTime();
    public int stake = 0;
    public bool disClose;
    public bool isLock;
    public int goalHP;
    public int goalRP;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiEnterRoom;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["gameType"] = this.gameType.ToString();
        element["playMode"] = this.playMode.ToString();
        element["playTime"] = this.playTime.ToString();
        element["stake"] = this.stake;
        element["disClose"] = this.disClose;
        element["isLock"] = this.isLock;
        element["goalHP"] = this.goalHP;
        element["goalRP"] = this.goalRP;

        retvalue = element.ToJson();

        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            gameType = (GameType)Enum.Parse(typeof(GameType), jsonData["gameType"].ToString());
            playMode = (PlayMode)Enum.Parse(typeof(PlayMode), jsonData["playMode"].ToString());
            playTime = (PlayTime)Enum.Parse(typeof(PlayTime), jsonData["playTime"].ToString());
            stake = int.Parse(jsonData["stake"].ToString());
            disClose = Convert.ToBoolean(jsonData["disClose"].ToString());
            isLock = Convert.ToBoolean(jsonData["isLock"].ToString());
            goalHP = int.Parse(jsonData["goalHP"].ToString());
            goalRP = int.Parse(jsonData["goalRP"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * CSNotiEnterRoom = 361
 * 클라이언트 룸에 잘 들어갔다고 알려준다. ( 클라이언트마다 로딩타임등이 틀림 )
 * [CSNotiEnterRoom]( session )
 	session
*/
public partial class CSNotiEnterRoom : Packet
{

    public string session = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSNotiEnterRoom;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            session = jsonData["session"].ToString();
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiMessage = 370
public partial class SCNotiMessage : Packet
{
    public MessageType msgType = MessageType.Unknown;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiMessage;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["msgType"] = this.msgType.ToString();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            msgType = (MessageType)Enum.Parse(typeof(MessageType), jsonData["msgType"].ToString());
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqReadTutorial
public partial class CSReqReadTutorial : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqReadTutorial;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqNews
public partial class CSReqNews : Packet
{
    #region implemented abstract members of Packet

    public string session = string.Empty;

    public override void Init()
    {
        packetType = PacketTypeList.CSReqNews;
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public class News
{
    //newsType: image = 1, html = 2
    public int newsType { get; set; }
    public string title { get; set; }
    public string url { get; set; }
    public News() { }
    public News(int newsType_, string title_, string url_)
    {
        newsType = newsType_;
        title = title_;
        url = url_;
    }
}

public class NewsList
{
    public List<News> newsList { get; set; }
    public string name { get; set; }
    public NewsList() { }
    public NewsList(string packetName, List<News> newsList_)
    {
        name = packetName;
        newsList = newsList_;
    }
}

//SCNotiNews
public partial class SCNotiNews : Packet
{
    public NewsList newsList = null;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiNews;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(newsList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            newsList = JsonMapper.ToObject<NewsList>(Json);
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSNotiChangeLanguage
public partial class CSNotiChangeLanguage : Packet
{
    public string session = string.Empty;
    public Language language = Language.English;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSNotiChangeLanguage;
        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["language"] = this.language.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            language = (Language)Enum.Parse(typeof(Language), jsonData["language"].ToString());
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');
        throw new NotImplementedException();
    }
    #endregion
}