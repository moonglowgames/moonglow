using System;
using LitJson;
using System.Collections.Generic;

public enum GameAction { Unknown = 0, Exit = 1, EndGame = 2, Restart = 3 }
public enum Result { Lose, Draw, Win }
public enum GameEndReason { Unknown, IHPFull, IResFull, IHPZero, IExit, IExitWithinTenPlay, EnemyHPFull, EnemyResFull, EnemyHPZero, EnemyExit, EnemyExitWithinTenPlay }

/* 
 * SCNotiPlayerList =400
 * 룸의 플레이어의 정보를 알려줌
 * [SCNotiPlayerList]( playerList< PlayerNumber , FaceImageIndex, Name, Level , isUser >)
	playerList
		playerNumber ( 1 ~ 4 )
		faceImageIndex
		playerName
		playerLevel
		isUser : 1 ( user ) , 0 ( autoplay npc )

*/
public class PlayerInfo
{
	public int playerNumber { get; set; }
	public int faceImageIndex { get; set; }
	public string playerName;
	public int playerLevel;
	public bool isUser;
	public bool isMe;
	
	public PlayerInfo(){}
	
	public PlayerInfo( int playerNumber_, int faceImageIndex_, string playerName_, int playerLevel_, bool isUser_, bool isMe_)
	{
		playerNumber = playerNumber_;
		faceImageIndex = faceImageIndex_;
		playerName = playerName_;
		playerLevel = playerLevel_;
		isUser = isUser_;
		isMe = isMe_;
	}
}
public class PlayerList
{
	public string name;
	public List<PlayerInfo> playerList { get; set;}
	
	public PlayerList(){}
	
	public PlayerList( string packetName , List<PlayerInfo> playerList_ )
	{
		name = packetName;
		playerList = playerList_;
	}
}

public partial class SCNotiPlayerList : Packet
{
	public PlayerList playerList = null;

	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiPlayerList ;
		
		//call base init...
		base.Init ();
	}

	protected override void InitTest ()
	{
		List<PlayerInfo> playerInfolist = new List<PlayerInfo>();
		playerInfolist.Add(new PlayerInfo(1,1,"Player 1",1,true, true));
		playerInfolist.Add(new PlayerInfo(2,2,"Player 2",2,false, true));
		
		playerList = new PlayerList ( Name, playerInfolist );
		
		/*
		 * {"playerList":
		 	[{"playerNumber":1,"faceImageIndex":1,"playerName":"Player 1","playerLevel":1,"isUser":true},{"playerNumber":2,"faceImageIndex":2,"playerName":"Player 2","playerLevel":2,"isUser":true}
		 	],
		 	"name":"SCNotiPlayerList"}
		 */
	}

	/*
	public override void Excute ()
	{
		throw new NotImplementedException ();


	}
	*/
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		
		retvalue =  JsonMapper.ToJson(playerList);
		return retvalue;
	}

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}

	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		// Json = "{""playerList"":[{""playerNumber"":1,""faceImageIndex"":1,""playerName"":"Player 1"",""playerLevel"":1,""isUser"":true},
				 //{""playerNumber"":2,""faceImageIndex"":2,""playerName"":""Player 2"",""playerLevel"":2,""isUser"":true}],""name"":""SCNotiPlayerList""}"
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			playerList = JsonMapper.ToObject<PlayerList>(Json);

			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * SCNotiPlayerStatus = 401
 * 프레이어의 정보를 변경한다.
 * [SCNotiPlayerStatus]( playerNumber, HP,SP,MD,MA,RE,MS,ZS,MC,effectID)
	playerNumber
	HP
	SP
	MD
	MA
	RE
	MS
	ZS
	MC
    effectID ( 0 = None, 1 ~ effect number … )
*/
public partial class SCNotiPlayerStatus : Packet {
	public int playerNumber;
	public int HP;
	public int SP;
	public int MD;
	public int MA;
	public int RE;
	public int MS;
	public int ZS;
	public int MC;
    public int effectIdx;
    public int globalEffectIdx;

    #region implemented abstract members of Packet
    public override void Init ()
	{
		packetType = PacketTypeList.SCNotiPlayerStatus ;
		
		//call base init...
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		
		element ["playerNumber"] = this.playerNumber;
		element ["HP"] = this.HP;
		element ["SP"] = this.SP;
		element ["MD"] = this.MD;
		element ["MA"] = this.MA;
		element ["RE"] = this.RE;
		element ["MS"] = this.MS;
		element ["ZS"] = this.ZS;
		element ["MC"] = this.MC;
        element["effectIdx"] = this.effectIdx;
        element["globalEffectIdx"] = this.globalEffectIdx;

        retvalue = element.ToJson ();
		
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name) {
			playerNumber = int.Parse(jsonData["playerNumber"].ToString());
			HP = int.Parse(jsonData["HP"].ToString());
			SP = int.Parse(jsonData["SP"].ToString());
			MD = int.Parse(jsonData["MD"].ToString());
			MA = int.Parse(jsonData["MA"].ToString());
			RE = int.Parse(jsonData["RE"].ToString());
			MS = int.Parse(jsonData["MS"].ToString());
			ZS = int.Parse(jsonData["ZS"].ToString());
			MC = int.Parse(jsonData["MC"].ToString());
            effectIdx = int.Parse(jsonData["effectIdx"].ToString());
            globalEffectIdx = int.Parse(jsonData["globalEffectIdx"].ToString());

            retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		
		throw new NotImplementedException ();
	}
	#endregion
}

public class PlayerItemInfo
{
    public string name { get; set; }
    public int playerNumber { get; set; }
    public bool hpPlus { get; set; }
    public bool spPlus { get; set; }
    public List<int> petList { get; set; }
    public int badgeIdx { get; set; }
    public PlayerItemInfo() { }
    public PlayerItemInfo(string packetName, int playerNumber_, bool hpPlus_, bool spPlus_, List<int> petList_, int badgeIdx_)
    {
        name = packetName;
        playerNumber = playerNumber_;
        hpPlus = hpPlus_;
        spPlus = spPlus_;
        petList = petList_;
        badgeIdx = badgeIdx_;
    }
}

//SCNotiPlayerItemInfo
public partial class SCNotiPlayerItemInfo : Packet
{
    public PlayerItemInfo playerItemInfo = null;
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiPlayerItemInfo;

        //call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(playerItemInfo);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            playerItemInfo = JsonMapper.ToObject<PlayerItemInfo>(Json);

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiStartGame = 410
 * 게임 시작
 * [SCNotiStartGame]

*/
public partial class SCNotiStartGame : Packet {
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiStartGame ;
		
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		
		retvalue = element.ToJson ();
		return retvalue;
	}

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}

	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			retvalue = true;
		}
		return retvalue;
	}

	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		
		throw new NotImplementedException ();
	}
	#endregion
}

public partial class SCNotiContinueGame : Packet
{
    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiStartGame;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

/* 
 * SCNotiDeckShuffle( 생략가능 ) = 411
 * 셔플을 msec 동안 한다.
 * [SCNotiDeckShuffle] ( msec )
	msec : msec동안 셔플해라 

*/
public partial class SCNotiDeckShuffle : Packet
{
	public int msec;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiDeckShuffle ;
		
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();

		element ["msec"] = this.msec;
		
		retvalue = element.ToJson ();
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			
			msec = int.Parse(jsonData["msec"].ToString ());

			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * SCNotiDecToHand = 420
 * 플레이어에게 덱으로부터 카드를 준다.
 * [SCNotiDecToHand]( playerNumber, cardList<  cardIndex  , handPosition >)
	playerNumber
	cardList<cardIndex, handPosition>
		cardIndex
		handPosition

*/
public class CardListInfo
{
	public int cardIndex   {get; set;}
    public int attachItemIdx { get; set; }
    public int handPosition{ get; set;}
	
	public CardListInfo(){}
	
	public CardListInfo( int cardIndex_, int attachItemIdx_, int handPosition_ )
	{
		cardIndex = cardIndex_;
        attachItemIdx = attachItemIdx_;
        handPosition = handPosition_;
	}
}

public enum ShowAnimationType{ ShowCard, AnimationFromDeck }
public class CardList
{
	public string name;
	public int playerNumber;
	public ShowAnimationType showType;
	public List<CardListInfo> cardList { get; set;}
    public int cardSkinIdx { get; set; }

    public int jokerFaceIdx { get; set; }
    public List<int> spellIdxList { get; set; }

    public CardList(){}
	
	public CardList( string packetName , int playerNumber_, ShowAnimationType showType_, List<CardListInfo> cardList_,int cardSkinIdx_, int jokerFaceIdx_, List<int> spellIdxList_)
    {
		name = packetName;
		playerNumber = playerNumber_;
		showType = showType_;
		cardList = cardList_;
        cardSkinIdx = cardSkinIdx_;

        jokerFaceIdx = jokerFaceIdx_;
        spellIdxList = spellIdxList_;
	}
}
public partial class SCNotiDeckToHand : Packet
{
	public CardList cardList = null;

	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiDeckToHand ;
		//call base init...
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		retvalue =  JsonMapper.ToJson(cardList);
		return retvalue;
	}

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			cardList = JsonMapper.ToObject<CardList>(Json);
			retvalue = true;
		} 
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * SCNotiPlayerFocus = 430
 * 플레이를 할 사람을 지정한다.
 * 클라이언트는 시간을 30초 또는 60초로 리셋한다.
 * [SCNotiPlayerFocus ] ( playerNumber )
	playerNumber

*/
public partial class SCNotiPlayerFocus : Packet
{
	public int playerNumber;
	public bool onlyDiscard;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiPlayerFocus ;
		
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		
		element ["playerNumber"] = this.playerNumber;
		element["onlyDiscard"] = this.onlyDiscard;

		retvalue = element.ToJson ();
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name) {
			
			playerNumber = int.Parse(jsonData["playerNumber"].ToString ());
			onlyDiscard = Boolean.Parse(jsonData["onlyDiscard"].ToString());

			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * CSReqUseCard = 440
 * 특정타겟이나 덱에 카드를 적용한다.
 * [CSReqUseCard]( session , handPosition , targetNumber )
	session
	handPosition ( 1 ~ 6 )
	targetNumber = [ deck: 0 , 1~ 4 PlayerNumber )

*/
public partial class CSReqUseCard : Packet {
	public string session;
	public int handPosition;
	public int targetNumber;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.CSReqUseCard ;
		
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		
		element ["session"] = this.session;
		element ["handPosition"] = this.handPosition;
		element ["targetNumber"] = this.targetNumber;
		
		retvalue = element.ToJson ();
		
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name) {
			session = jsonData["session"].ToString();
			handPosition = int.Parse(jsonData["handPosition"].ToString ());
			targetNumber = int.Parse(jsonData["targetNumber"].ToString ());
			
			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		throw new NotImplementedException ();
	}
	#endregion
}

public class PlayCardInfo
{
    public string name { get; set; }
    public bool success { get; set; }
    public int playerNumber { get; set; }
    public int targetNumber { get; set; }
    public int cardIdx { get; set; }
    public int effectIdx { get; set; }
    public int jokerFaceIdx { get; set; }
    public List<int> spellIdxList { get; set; }
    public PlayCardInfo() { }
    public PlayCardInfo(string packetName_, bool success_, int playerNumber_, int targetNumber_, int cardIdx_, int effectIdx_, int jokerFaceIdx_, List<int> spellIdxList_)
    {
        name = packetName_;
        success = success_;
        playerNumber = playerNumber_;
        targetNumber = targetNumber_;
        cardIdx = cardIdx_;
        effectIdx = effectIdx_;
        jokerFaceIdx = jokerFaceIdx_;
        spellIdxList = spellIdxList_;
    }
}

public partial class SCNotiUseCard : Packet
{
    public PlayCardInfo playCardInfo = null;

    #region implemented abstract members of Packet
    public override void Init ()
	{
		packetType = PacketTypeList.SCNotiUseCard ;
		
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = JsonMapper.ToJson(playCardInfo);
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
            playCardInfo = JsonMapper.ToObject<PlayCardInfo>(Json);
            retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 * SCNotiDeckInfo = 442
 * 덱옆에 있는 카드 정보를 전송한다.
 * [SCNotiDeckInfo]( DeckList<cardIndex, attackPlayer, targetPlayer position> )
	DeckList<cardIndex, attackPlayer, targetPlayer, position> 
		cardIndex
		attackPlayer : 카드 사용자
		taregetPlayer : 카드사용 타겟
		position : 카드 표시위치 

*/
public class DeckInfo
{
    public int cardIndex { get; set; }
    public int attachItemIdx { get; set; }
    public int targetPosition { get; set; }
    
    public DeckInfo() { }
    public DeckInfo(int cardIndex_, int attachItemIdx_,/*int attackPlayer_, int targetPlayer_,*/ int targetPosition_)
    {
        cardIndex = cardIndex_;
        attachItemIdx = attachItemIdx_;
        targetPosition = targetPosition_;
    }
}

public class DeckList
{
	public string name;
	public List<DeckInfo> deckList { get; set;}
    public int cardSkinIdx { get; set; }
    public int jokerFaceIdx { get; set; }
    public List<int> spellIdxList { get; set; }

    public DeckList(){}
	public DeckList(string packetName, List<DeckInfo> deckList_,int cardSkinIdx_, int jokerFaceIdx_, List<int> spellIdxList_)
	{
		name = packetName;
		deckList = deckList_;
        cardSkinIdx = cardSkinIdx_;
        jokerFaceIdx = jokerFaceIdx_;
        spellIdxList = spellIdxList_;
    }
}

public partial class SCNotiDeckInfo : Packet
{
	public DeckList deckList = null;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiDeckInfo ;
		
		base.Init ();
	}

    protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		retvalue = JsonMapper.ToJson (deckList);
		return retvalue;
	}

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		string name = jsonData ["name"].ToString ();
		if (name == Name) {
			deckList = JsonMapper.ToObject<DeckList>(Json);
			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 *  SCNotiGameEnd = 450
 * 게임이 끝났음을 알림
 * [SCNotiGameEnd]( winnerNumber )
	winnerNumber 

*/
public partial class SCNotiGameEnd : Packet {
	public int winnerNumber;
	
	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.SCNotiGameEnd ;
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		element ["winnerNumber"] = this.winnerNumber;
		retvalue = element.ToJson ();
		return retvalue;
	}

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			winnerNumber = int.Parse(jsonData["winnerNumber"].ToString());
			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		throw new NotImplementedException ();
	}
	#endregion
}

public class PlayFriend
{
    public int iDx { get; set; }
    public int friendLevel { get; set; }
    public int friendFaceIdx { get; set; }
    public string friendNick { get; set; }

    public PlayFriend() { }
    public PlayFriend(int iDx_, int friendLevel_, int friendFaceIdx_, string friendNick_)
    {
        iDx = iDx_;
        friendLevel = friendLevel_;
        friendFaceIdx = friendFaceIdx_;
        friendNick = friendNick_;
    }
}

public class GameResult
{
    public string name { get; set; }
    public int exp { get; set; }
    public int downLevelExp { get; set; }
    public int upLevelExp { get; set; }
    public int rankPoint { get; set; }
    public int coin { get; set; }
    public int ruby { get; set; }
    public int stake { get; set; }
    public bool levelUp { get; set; }
    public bool levelDown { get; set; }
    public ItemInfo itemInfo { get; set; }
    public int currentLevel { get; set; }
    public string playTime { get; set; }

    public bool adOK { get; set; }
    public GameEndReason reason { get; set; }
    public int reasonValue { get; set; }

    public Result result { get; set; } 
    public List<PlayFriend> playFriendList { get; set; }

    public GameResult() { }
    public GameResult(string packetName, int exp_, int downLevelExp_, int upLevelExp_, /*float*/int rankPoint_, int coin_, int ruby_, int stake_, bool levelUp_, bool levelDown_, ItemInfo itemInfo_, int currentLevel_, string playTime_, bool adOK_, GameEndReason reason_, int reasonValue_, Result result_, List<PlayFriend> playFriendList_)
    {
        name = packetName;
        exp = exp_;
        downLevelExp = downLevelExp_;
        upLevelExp = upLevelExp_;
        rankPoint = rankPoint_;
        coin = coin_;
        ruby = ruby_;
        stake = stake_;
        levelUp = levelUp_;
        levelDown = levelDown_;
        itemInfo = itemInfo_;
        currentLevel = currentLevel_;
        playTime = playTime_;

        adOK = adOK_;
        reason = reason_;
        reasonValue = reasonValue_;
        result = result_;
        playFriendList = playFriendList_;
    }
}

/*  SCNotiGameResult = 451
 * 게임종료후 얻는 결과를 알려줌	
 * [SCNotiGameResult] ( exp , coin , isLevelUp , playTime   )
	exp  : reword exp
	coin : reword coin
	isLevelUp : 1 ( have levelup ) , 0 ( have not levelup )
	playTime
*/
public partial class SCNotiGameResult : Packet
{
    public GameResult gameResult = null;

    #region implemented abstract members of Packet
    public override void Init ()
	{
		packetType = PacketTypeList.SCNotiGameResult ;
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
        retvalue = JsonMapper.ToJson(gameResult);
        return retvalue;
	}

	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
            gameResult = JsonMapper.ToObject<GameResult>(Json);
            retvalue = true;
        }
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		
		throw new NotImplementedException ();
	}
	#endregion
}

/* 
 *  CSReqWantGame = 452
 * 게임을 다시 하고 싶다
 * 플레이어가 모두 나가면 방이 파괴된다.
 * [CSReqWantGame]( session, GameAction  want)
 	session
	want:  [ Unknown = 0 , exit =1 , EndGame=2, restart = 3]

*/
public partial class CSReqWantGame : Packet {
	public string session;
	public GameAction want = GameAction.Unknown;

	#region implemented abstract members of Packet
	public override void Init ()
	{
		packetType = PacketTypeList.CSReqWantGame ;
		base.Init ();
	}
	
	protected override string ElementToJson ()
	{
		string  retvalue = base.ElementToJson ();
		
		element ["session"] = this.session;
		element ["want"] = this.want.ToString();
		
		retvalue = element.ToJson ();
		return retvalue;
	}
	protected override string ElementToTelnet ()
	{
		throw new NotImplementedException ();
	}
	protected override bool JsonToElement (string Json)
	{
		bool retvalue = false;
		JsonData jsonData = JsonMapper.ToObject (Json);
		
		string name = jsonData ["name"].ToString ();
		if (name == Name)
        {
			session = jsonData["session"].ToString();
			want = (GameAction)Enum.Parse(typeof(GameAction),jsonData["want"].ToString());
			
			retvalue = true;
		}
		return retvalue;
	}
	protected override bool TelnetToElement (string Telnet)
	{
		// []
		//int startdataidx = Telnet.CompareTo('[');
		//int enddataidx  = Telnet.CompareTo(']');
		//string data = Telnet.GetStrign( startindxex, endindex );
		// string[] stringlist = Telnet.Split(',');
		throw new NotImplementedException ();
	}
	#endregion
}

//CSReqQuickInventory
public partial class CSReqQuickInventory : Packet
{
    public string session = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqQuickInventory;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');

        throw new NotImplementedException();
    }
    #endregion
}

public class QuickItemInfo
{
    public int itemIdx { get; set; }
    public int count { get; set; }
    public string remainTime { get; set; }
    public PayType payType { get; set; }
    public double price { get; set; }
    public SpecialType specialType { get; set; }
}
public class QuickInventory
{
    public string name { get; set; }
    List<QuickItemInfo> quickItemList { get; set; }
    public QuickInventory() { }
    public QuickInventory(string packetName, List<QuickItemInfo> quickItemList_)
    {
        name = packetName;
        quickItemList = quickItemList_;
    }
}
//SCNotiQuickInventory
public partial class SCNotiQuickInventory : Packet
{
    public QuickInventory quickInventory = null;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiQuickInventory;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        retvalue = JsonMapper.ToJson(quickInventory);
        return retvalue;
    }
    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }
    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            quickInventory = JsonMapper.ToObject<QuickInventory>(Json);

            retvalue = true;
        }
        return retvalue;
    }
    protected override bool TelnetToElement(string Telnet)
    {
        // []
        //int startdataidx = Telnet.CompareTo('[');
        //int enddataidx  = Telnet.CompareTo(']');
        //string data = Telnet.GetStrign( startindxex, endindex );
        // string[] stringlist = Telnet.Split(',');
        throw new NotImplementedException();
    }
    #endregion
}