using System;
using LitJson;

public enum Command { Message = 0, BanTime = 1, BanAfterGame = 2 }
public enum ChatChannel { Kor, Eng, Any, All }

public partial class CSNotiChangeChatChannel : Packet
{
    public string session = string.Empty;
    public ChatChannel chatChannel = ChatChannel.Any;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSNotiChangeChatChannel;
        // element = new JsonData ();
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["chatChannel"] = this.chatChannel.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            chatChannel = (ChatChannel)Enum.Parse(typeof(ChatChannel), jsonData["chatChannel"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class CSNotiChat : Packet
{
    public string session = string.Empty;
    public string chatMsg = string.Empty;
    public int iconIdx = 0;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSNotiChat;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["chatMsg"] = this.chatMsg;
        element["iconIdx"] = this.iconIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            session = jsonData["session"].ToString();
            chatMsg = jsonData["chatMsg"].ToString();
            iconIdx = int.Parse(jsonData["iconIdx"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

public partial class SCNotiChat : Packet
{
    public string nick = string.Empty;
    public int userType = 0;
    public int playerNumber = 0;
    public string chatMsg = string.Empty;
    public int iconIdx = 0;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiChat;

        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["nick"] = this.nick;
        element["userType"] = this.userType;
        element["playerNumber"] = this.playerNumber;
        element["chatMsg"] = this.chatMsg;
        element["iconIdx"] = this.iconIdx;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name = jsonData["name"].ToString();
        if (name == Name)
        {
            nick = jsonData["nick"].ToString();
            userType = int.Parse(jsonData["userType"].ToString());
            playerNumber = int.Parse(jsonData["playerNumber"].ToString());
            chatMsg = jsonData["chatMsg"].ToString();
            iconIdx = int.Parse(jsonData["iconIdx"].ToString());
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiServerNoti
public partial class SCNotiServerNoti : Packet
{
    public Command command = Command.Message;
    public string value = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiServerNoti;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["command"] = this.command.ToString();
        element["value"] = this.value;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            command = (Command)Enum.Parse(typeof(Command), jsonData["command"].ToString());
            value = jsonData["value"].ToString();

            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//CSReqCMD
public partial class CSReqCMD : Packet
{
    public string session = string.Empty;
    public string msg = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.CSReqCMD;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["msg"] = this.msg;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            msg = jsonData["msg"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }
    #endregion
}

//SCNotiCMD
public partial class SCNotiCMD : Packet
{
    public string msg = string.Empty;

    #region implemented abstract members of Packet
    public override void Init()
    {
        packetType = PacketTypeList.SCNotiCMD;
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["msg"] = this.msg;

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            msg = jsonData["msg"].ToString();
            retvalue = true;
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}