﻿using LitJson;
using System;
using System.Collections.Generic;

public enum FriendTabType { Unknown = 0, MyFriends = 1, RequestFriends = 2, RecommendFriends = 3 }
public enum FriendType { Friend = 0, Requesting = 1, Accepting = 2, Recommend = 3 }
public enum FriendResult { Unknown = 0, OK = 1, NotExist = 2, NotAccept = 3, Fail = 4 }

//CSReqFriendList
public partial class CSReqFriendList : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqFriendList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

public class Friend
{
    public int iDx { get; set; }
    public string friendNick { get; set; }
    public int faceIdx { get; set; }
    public int level { get; set; }
    public string lastLoginDate { get; set; }
    public FriendType friendType { get; set; }
    public bool enableSendItem { get; set; }
    public bool enableGetItem { get; set; }
    public int winCount { get; set; }
    public int loseCount { get; set; }
    public int drawCount { get; set; }
    public Friend() { }
    public Friend(int iDx_, string friendNick_, int faceIdx_, int level_, string lastLoginDate_, FriendType friendType_, bool enableSendItem_, bool enableGetItem_, int winCount_, int loseCount_, int drawCount_)
    {
        iDx = iDx_;
        friendNick = friendNick_;
        faceIdx = faceIdx_;
        level = level_;
        lastLoginDate = lastLoginDate_;
        friendType = friendType_;
        enableSendItem = enableSendItem_;
        enableGetItem = enableGetItem_;
        winCount = winCount_;
        loseCount = loseCount_;
        drawCount = drawCount_;
    }
}

public class FriendList
{
    public string name { get; set; }
    public FriendTabType tabType { get; set; }
    public int maxFriendCount { get; set; }
    public List<Friend> friendList { get; set; }
    public FriendList() { }
    public FriendList(string packetName, FriendTabType tabType_, int maxFriendCount_, List<Friend> friendList_)
    {
        name = packetName;
        tabType = tabType_;
        maxFriendCount = maxFriendCount_;
        friendList = friendList_;
    }
}

//SCNotiFriendList 
public partial class SCNotiFriendList : Packet
{
    public FriendList friendList = null;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCNotiFriendList;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();

        //retvalue = element.ToJson();
        retvalue = JsonMapper.ToJson(friendList);
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            friendList = JsonMapper.ToObject<FriendList>(Json);
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqRequestFriend
public partial class CSReqRequestFriend : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;
    public int iDx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqRequestFriend;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();
        element["iDx"] = this.iDx;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());
            iDx = int.Parse(jsonData["iDx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqAcceptFriend 
public partial class CSReqAcceptFriend : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;
    public int iDx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqAcceptFriend;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();
        element["iDx"] = this.iDx;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());
            iDx = int.Parse(jsonData["iDx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqDeleteFriend 
public partial class CSReqDeleteFriend : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;
    public int iDx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqDeleteFriend;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();
        element["iDx"] = this.iDx;
        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());
            iDx = int.Parse(jsonData["iDx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqSendGift 
public partial class CSReqSendGift : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;
    public int iDx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqSendGift;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();
        element["iDx"] = this.iDx;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());
            iDx = int.Parse(jsonData["iDx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqGetGift 
public partial class CSReqGetGift : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;
    public int iDx = 0;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqGetGift;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();
        element["iDx"] = this.iDx;
        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());
            iDx = int.Parse(jsonData["iDx"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqSendAllGifts 
public partial class CSReqSendAllGifts : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqSendAllGifts;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");

        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//CSReqGetAllGifts 
public partial class CSReqGetAllGifts : Packet
{
    public string session = string.Empty;
    public FriendTabType tabType = FriendTabType.Unknown;

    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.CSReqGetAllGifts;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["session"] = this.session;
        element["tabType"] = this.tabType.ToString();

        retvalue = element.ToJson();
        return retvalue;

    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            session = jsonData["session"].ToString();
            tabType = (FriendTabType)Enum.Parse(typeof(FriendTabType), jsonData["tabType"].ToString());

            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}

//SCRspFriendResut
public partial class SCRspFriendResut : Packet
{
    public PacketTypeList pckType = PacketTypeList.Unknown;
    public FriendResult result = FriendResult.Unknown;
    #region implemented abstract members of Packet

    public override void Init()
    {
        packetType = PacketTypeList.SCRspFriendResut;
        // element = new JsonData ();
        // call base init...
        base.Init();
    }

    protected override string ElementToJson()
    {
        string retvalue = base.ElementToJson();
        element["pckType"] = this.pckType.ToString();
        element["result"] = this.result.ToString();

        retvalue = element.ToJson();
        return retvalue;
    }

    protected override string ElementToTelnet()
    {
        throw new NotImplementedException();
    }

    protected override bool JsonToElement(string Json)
    {
        bool retvalue = false;
        JsonData jsonData = JsonMapper.ToObject(Json);

        string name_ = jsonData["name"].ToString();
        if (Name == name_)
        {
            pckType = (PacketTypeList)Enum.Parse(typeof(PacketTypeList), jsonData["pckType"].ToString());
            result = (FriendResult)Enum.Parse(typeof(FriendResult), jsonData["result"].ToString());
            retvalue = true;
        }
        else
        {
            // throw new Exception("Packet name error");
        }
        return retvalue;
    }

    protected override bool TelnetToElement(string Telnet)
    {
        throw new NotImplementedException();
    }

    #endregion
}