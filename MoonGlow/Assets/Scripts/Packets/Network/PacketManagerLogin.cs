using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //SCNotiHello
    public void SendSCNotiHello(string svrname, string ver, ServerState state, string info)
    {
        SCNotiHello pck = new SCNotiHello();
        pck.svrName = svrname;
        pck.ver = ver;
        pck.state = state;
        pck.info = svrname;
        Debug.Log("SEND SCNotiHello OUT: " + "SERVER NAME: " + pck.svrName + "SERVER VERSION: " + pck.ver + "SERVER STATE" + pck.state + "SERVER INFO" + pck.info);

        send(pck);
    }

    //CSReqRegister
    public void SendCSReqRegister(AccountType accountType, string account, string password, string nick, int faceIdx, string deviceID)
    {
        CSReqRegister pck = new CSReqRegister();
        pck.accountType = accountType;
        pck.account = account;
        pck.password = password;
        pck.nick = nick;
        pck.faceIdx = faceIdx;
        pck.deviceID = deviceID;
        Debug.Log("SEND CSReqRegister OUT: " + "Account: " + pck.account + " Nick: " + pck.nick);

        send(pck);
    }

    //CSReqLogin
    public void SendCSReqLogin(AccountType accountType, string account, string password, string deviceID)
    {
        CSReqLogin pck = new CSReqLogin();
        pck.accountType = accountType;
        pck.account = account;
        pck.password = password;
        pck.deviceID = deviceID;
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                pck.conType = ConnectType.Android;
                break;
            case RuntimePlatform.IPhonePlayer:
                pck.conType = ConnectType.iOS;
                break;
            default:
                pck.conType = ConnectType.PC;
                break;
        }
        pck.conVer = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.clientVersion);
        pck.language = (Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 1);

        Debug.Log("SEND CSReqLogin OUT: " + "id: " + "Account: " + pck.account + " Version: " + pck.conVer);

        send(pck);
    }

    //SCRspLogin
    public void SendSCRspLogin(LoginState state, string session, string connectUrl)
    {
        SCNotiLogin pck = new SCNotiLogin();
        pck.state = state;
        pck.session = session;
        pck.connectUrl = connectUrl;

        Debug.Log("SEND SCRspLogin OUT: " + "state: " + pck.state + "session: " + pck.session + "connectUrl: " + pck.connectUrl);

        send(pck);
    }

    //CSReqSessionLogin
    public void SendCSReqSessionLogin(string session, string userID, Language language)
    {
        CSReqSessionLogin pck = new CSReqSessionLogin();
        pck.session = session;
        pck.userID = userID;
        pck.language = language;

        Debug.Log("SEND CSReqSessionLogin OUT: " + "session: " + pck.session + "userID: " + pck.userID + "language: " + pck.language);

        send(pck);
    }

    //SCNotiSessionLogin
    public void SendSCNotiSessionLogin(bool status, string newsession)
    {
        SCNotiSessionLogin pck = new SCNotiSessionLogin();
        pck.status = status;
        pck.newsession = newsession;

        Debug.Log("SEND SCNotiSessionLogin OUT: " + "status: " + pck.status + "newsession: " + pck.newsession);

        send(pck);
    }

    //CSRspPang
    public void SendCSRspPang(int pingCount)
    {
        CSRspPang pck = new CSRspPang();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.pingCount = pingCount;

        Debug.Log("SEND CSRspPang OUT: " + "session: " + pck.session + "PingCount: " + pck.pingCount);

        send(pck);
    }

    //CSReqFindPassword
    public void SendCSReqFindPassword(string account)
    {
        CSReqFindPassword pck = new CSReqFindPassword();
        pck.account = account;

        Debug.Log("SEND CSReqFindPassword OUT: " + "account: " + pck.account);

        send(pck);
    }

    //CSReqChangePassword
    public void SendCSReqChangePassword(string account, string oldPassword, string newPassword)
    {
        CSReqChangePassword pck = new CSReqChangePassword();
        pck.account = account;
        pck.oldPassword = oldPassword;
        pck.newPassword = newPassword;

        Debug.Log("SEND CSReqChangePassword OUT: " + "account: " + pck.account);

        send(pck);
    }

    //CSReqGotoScene
    public void SendCSReqGotoScene(SceneType scene)
    {
        CSReqGotoScene pck = new CSReqGotoScene();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.scene = scene;

        Debug.Log("SEND CSReqGotoScene OUT: " + "session: " + pck.session + "scene: " + pck.scene);

        send(pck);
    }

    //SCNotiGotoScene
    public void SendSCNotiGotoScene(SceneType scene)
    {
        SCNotiGotoScene pck = new SCNotiGotoScene();
        pck.scene = scene;

        Debug.Log("SEND SCNotiGotoScene OUT: " + "scene: " + pck.scene);

        send(pck);
    }

    //CSReqClientPause
    public void SendCSReqClientPause()
    {
        CSReqClientPause pck = new CSReqClientPause();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqClientPause OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqClientFocus
    public void SendCSReqClientFocus()
    {
        CSReqClientFocus pck = new CSReqClientFocus();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqClientFocus OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqUrl
    public void SendCSReqUrl(UrlType urlType)
    {
        CSReqUrl pck = new CSReqUrl();

        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.urlType = urlType;

        Debug.Log("SEND SCNotiGotoScene OUT: " + "session: " + pck.session + "UrlType: " + pck.urlType);
        send(pck);
    }

    //CSReqLogout
    public void SendCSReqLogout()
    {
        CSReqLogout pck = new CSReqLogout();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqLogout OUT: " + "session: " + pck.session);
        send(pck);
    }
}