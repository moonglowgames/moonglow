﻿using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqShopList
    public void SendCSReqMailList()
    {
        Debug.Log("SEND CSReqMailList READY: ");

        CSReqMailList pck = new CSReqMailList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqMailList OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqGetItem
    public void SendCSReqGetItem(int mailItemIdx)
    {
        Debug.Log("SEND CSReqGetItem READY: " + "mailItemIdx: " + mailItemIdx);

        CSReqGetItem pck = new CSReqGetItem();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.mailIdx = mailItemIdx;

        Debug.Log("SEND CSReqGetItem OUT: " + "session: " + pck.session + "mailItemIdx: " + pck.mailIdx);
        send(pck);
    }

    //CSReqGetAllItems
    public void SendCSReqGetAllItems()
    {
        Debug.Log("SEND CSReqGetAllItems READY: ");

        CSReqGetAllItems pck = new CSReqGetAllItems();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqGetAllItems OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqRemoveOld
    public void SendCSReqRemoveOld()
    {
        Debug.Log("SEND CSReqRemoveOld READY: " + "mailItemIdx: ");

        CSReqRemoveOld pck = new CSReqRemoveOld();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqRemoveOld OUT: " + "session: " + pck.session);
        send(pck);
    }
}