﻿using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqJokerInfo
    public void SendCSReqJokerInfo()
    {
        CSReqJokerInfo pck = new CSReqJokerInfo();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqJokerInfo OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqUpgradeSpell
    public void SendCSReqUpgradeSpell(int spellIdx)
    {
        CSReqUpgradeSpell pck = new CSReqUpgradeSpell();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.currentSpellIdx = spellIdx;

        Debug.Log("SEND CSReqUpgradeSpell OUT: " + "session: " + pck.session + "SpellIdx: " + pck.currentSpellIdx.ToString());
        send(pck);
    }

    //CSReqDowngradeSpell
    public void SendCSReqDowngradeSpell(int spellIdx)
    {
        CSReqDowngradeSpell pck = new CSReqDowngradeSpell();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.currentSpellIdx = spellIdx;

        Debug.Log("SEND CSReqDowngradeSpell OUT: " + "session: " + pck.session + "SpellIdx: " + pck.currentSpellIdx.ToString());
        send(pck);
    }

    //CSReqCancelUpgrade
    public void SendCSReqCancelUpgrade()
    {
        CSReqJokerCancel pck = new CSReqJokerCancel();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqCancelUpgrade OUT: " + "session: " + pck.session + "SpellType: ");
        send(pck);
    }

    //CSReqApplySpells
    public void SendCSReqApplySpells()
    {
        CSReqJokerApply pck = new CSReqJokerApply();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqApplySpells OUT: " + "session: " + pck.session);
        send(pck);
    }
}