﻿using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqPlayerProfile
    public void SendCSReqPlayerProfile(string playerNick)
    {
        CSReqPlayerProfile pck = new CSReqPlayerProfile();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.playerNick = playerNick;

        Debug.Log("SEND CSReqPlayerProfile OUT: " + "session: " + pck.session + "PlayerNick: " + pck.playerNick);
        send(pck);
    }
}