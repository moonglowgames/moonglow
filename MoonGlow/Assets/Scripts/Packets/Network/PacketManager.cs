using System;
using System.Net.Sockets;
using UnityEngine;
using LitJson;

public partial class PacketManager : Singleton<PacketManager>
{
    private int _sendFailCount = 0;
    public Socket _client = null;
    public EncodeType encodeType = EncodeType.Json;
    public bool runTests = false;

    public bool isConnect
    {
        get { return (_client == null) ? false : ( _client.Connected && TcpManager.isConnected ); }
    }

    void Awake()
    {
        if (runTests) { TestFunc(); }
    }

    public void Reconnect()
    {
        string nowConnectString = EncryptedPlayerPrefs.GetString("NOW_CONNECT_STR");
        int nowConnectPort = EncryptedPlayerPrefs.GetInt("NOW_CONNECT_PORT");


        TcpManager.Disconnect(_client);
        Connect(nowConnectString, nowConnectPort);
    }

    public void Connect(string connectIP, int connectPort)
    {
        EncryptedPlayerPrefs.SetString("NOW_CONNECT_STR", connectIP);
        EncryptedPlayerPrefs.SetInt("NOW_CONNECT_PORT", connectPort);
        PlayerPrefs.Save();

        _sendFailCount = 0;
        //string connectIP = ConfigManager.Instance.Get (configKey.LoginServerIP);
        //int connectPort =int.Parse(ConfigManager.Instance.Get (configKey.LoginServerPort));
        Debug.Log("TCP CONNECTED connect ...");
        TcpManager.StartClient(connectIP, connectPort, out _client);

        //		if (_client == null) {
        //			Debug.Log ("Connection fail");
        //			isConnect = false;
        //		} else {
        //			Debug.Log("Connection success");
        //			isConnect = true;
        //		}
    }
    public void DisConnect()
    {
        TcpManager.Disconnect(_client);
    }

    public void send(IPacket ipacket)
    {
        string sendMsg = ipacket.Encode(encodeType);
        //Debug.Log ("Send :" + sendMsg);
        if (TcpManager.Send(sendMsg) == false)
        {
            _sendFailCount++;
            if (_sendFailCount >= 3) { Reconnect(); }
        }
        else
        {
            _sendFailCount = 0;
        }
    }

    public void SendMe(IPacket ipacket)
    {
        ipacket.Excute();
    }

    public void Update()
    {
        Receive();
        TcpManager.SendRun(_client);
    }

    public void Receive()
    {
        // 1. get packet from packetTypelist buffer
        string receivestr = TcpManager.Pop();
        if (receivestr == string.Empty) { return; }
        Debug.Log("POP: " + receivestr);

        // 2. json converting 
        JsonData jsonData = null;
        try
        {
            jsonData = JsonMapper.ToObject(receivestr);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            jsonData = null;
        }

        if (jsonData == null)
        {
            Debug.LogError("JsonData receive error!" + receivestr);
            return;
        }

        // 3. get packet name
        string packetName = jsonData["name"].ToString();
        if (packetName == string.Empty)
        {
            Debug.LogError("packet name error! : " + receivestr);
            return;
        }

        // 4. create packet with packetname
        IPacket pck = null;
        try
        {
            pck = (IPacket)Activator.CreateInstance(Type.GetType(packetName));
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            pck = null;
        }

        // 5. decode packet
        if (pck != null)
        {
            pck.Decode(encodeType, receivestr);
        }
        else { Debug.LogError("Packet object is null!"); }

        // 6. excute packet
        //Debug.Log ("Packet Execute :"+pck.Encode(EncodeType.Json));
        pck.Excute();
    }

    public void TestFunc()
    {
        SCNotiRoomList pck_NotiRoomList = new SCNotiRoomList();

        string json = pck_NotiRoomList.Encode(EncodeType.Json);
        Debug.Log("TestFunc SCNotiRoomList json1: " + json);

        RoomList roomList = JsonMapper.ToObject<RoomList>(json);
        //pck.Decode (EncodeType.Json,json);
        Debug.Log("TestFunc SCNotiRoomList json2: " + JsonMapper.ToJson(roomList));

        /*************************************************************/

        SCNotiItemInfo pck_NotiItemInfo = new SCNotiItemInfo();

        json = pck_NotiItemInfo.Encode(EncodeType.Json);
        Debug.Log("TestFunc SCNotiItemInfo json1: " + json);

        ItemList itemList = JsonMapper.ToObject<ItemList>(json);
        //pck.Decode (EncodeType.Json,json);
        Debug.Log("TestFunc SCNotiItemInfo json2: " + JsonMapper.ToJson(itemList));

        /*************************************************************/

        SCNotiUserElement pck_NotiUserElement = new SCNotiUserElement();

        json = pck_NotiUserElement.Encode(EncodeType.Json);
        Debug.Log("TestFunc SCNotiUserElement json1: " + json);

        ElementList elementList = JsonMapper.ToObject<ElementList>(json);
        //pck.Decode (EncodeType.Json,json);
        Debug.Log("TestFunc SCNotiUserElement json2: " + JsonMapper.ToJson(elementList));

        /*************************************************************/

        SCNotiPlayerList pck_SCNotiPlayerList = new SCNotiPlayerList();

        json = pck_SCNotiPlayerList.Encode(EncodeType.Json);
        Debug.Log("TestFunc SCNotiPlayerList json1: " + json);

        PlayerList playerList = JsonMapper.ToObject<PlayerList>(json);
        //pck.Decode (EncodeType.Json,json);
        Debug.Log("TestFunc SCNotiPlayerList json2: " + JsonMapper.ToJson(playerList));

        /*************************************************************/

        SCNotiDeckToHand pck_SCNotiDecToHand = new SCNotiDeckToHand();

        json = pck_SCNotiDecToHand.Encode(EncodeType.Json);
        Debug.Log("TestFunc SCNotiDecToHand json1: " + json);

        CardList cardList = JsonMapper.ToObject<CardList>(json);
        //pck.Decode (EncodeType.Json,json);
        Debug.Log("TestFunc SCNotiDecToHand json2: " + JsonMapper.ToJson(cardList));

        JsonData jsonData = JsonMapper.ToObject(json);
        Debug.Log("************************************playerNumber: " + jsonData["playerNumber"]);

        /*************************************************************/

        SCNotiDeckInfo pck_SCNotiDeckInfo = new SCNotiDeckInfo();

        json = pck_SCNotiDeckInfo.Encode(EncodeType.Json);
        Debug.Log("TestFunc SCNotiDeckInfo json1: " + json);

        DeckList deckList = JsonMapper.ToObject<DeckList>(json);
        //pck.Decode (EncodeType.Json,json);
        Debug.Log("TestFunc SCNotiDeckInfo json2: " + JsonMapper.ToJson(deckList));
    }
}