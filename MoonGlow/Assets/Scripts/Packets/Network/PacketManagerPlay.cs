using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
	//SCNotiPlayerList
	public void SendSCNotiPlayerList (PlayerList playerList)
	{
		Debug.Log ("SEND SCNotiPlayerList READY: " + "playerList: " + playerList);
		//send to clients
		SCNotiPlayerList pck = new SCNotiPlayerList ();
		pck.playerList = playerList;
		
		Debug.Log ("SEND SCNotiPlayerList OUT: " + "playerList: " + pck.playerList);
		
		send(pck);
	}

	//SCNotiPlayerStatus
	public void SendSCNotiPlayerStatus (int playerNumber, int HP, int SP, int MD, int MA, int RE, int MS, int ZS, int MC)
	{
		Debug.Log ("SEND SCNotiPlayerStatus READY: " + "playerNumber: " + playerNumber + "HP: " + HP + "SP: " + SP 
		           + "MD: " + MD + "MA: " + MA + "RE: " + RE + "MS: " + MS + "ZS: " + ZS + "MC: " + MC);
		//send to clients
		SCNotiPlayerStatus pck = new SCNotiPlayerStatus ();

		pck.playerNumber = playerNumber;
		pck.HP = HP;
		pck.SP = SP;
		pck.MD = MD;
		pck.MA = MA;
		pck.RE = RE;
		pck.MS = MS;
		pck.ZS = ZS;
		pck.MC = MC;
		
		Debug.Log ("SEND SCNotiPlayerStatus OUT: " + "playerNumber: " + pck.playerNumber + "HP: " + pck.HP + "SP: " + pck.SP 
		           + "MD: " + pck.MD + "MA: " + pck.MA + "RE: " + pck.RE + "MS: " + pck.MS + "ZS: " + pck.ZS + "MC: " + pck.MC);
		
		send(pck);
	}

	//SCNotiStartGame
	public void SendSCNotiStartGame()
	{
		Debug.Log ("SEND SCNotiStartGame READY! ");
		//send to clients
		SCNotiStartGame pck = new SCNotiStartGame ();
		

		
		Debug.Log ("SEND SCNotiStartGame OUT! ");
		
		send(pck);
	}

	//SCNotiDeckShuffle
	public void SendSCNotiDeckShuffle(int msec)
	{
		Debug.Log ("SEND SCNotiDeckShuffle READY: " + "msec: " + msec);
		//send to clients
		SCNotiDeckShuffle pck = new SCNotiDeckShuffle ();
		
		pck.msec = msec;
		
		Debug.Log ("SEND SCNotiDeckShuffle OUT: " + "msec: " + msec);
		
		send(pck);
	}

	//SCNotiDecToHand
	public void SendSCNotiDecToHand(CardList cardList)
	{
		Debug.Log ("SEND SCNotiDecToHand READY: " + "cardList: " + cardList);
		//send to clients
		SCNotiDeckToHand pck = new SCNotiDeckToHand ();
		
		pck.cardList = cardList;
		
		Debug.Log ("SEND SCNotiDecToHand OUT: " + "playerNumber: " + pck.cardList.playerNumber + "cardList" + pck.cardList.cardList);
		
		send(pck);
	}

	//SCNotiPlayerFocus
	public void SendSCNotiPlayerFocus(int playerNumber)
	{
		Debug.Log ("SEND SCNotiPlayerFocus READY: " + "playerNumber: " + playerNumber);
		//send to clients
		SCNotiPlayerFocus pck = new SCNotiPlayerFocus ();
		
		pck.playerNumber = playerNumber;
		
		Debug.Log ("SEND SCNotiPlayerFocus OUT: " + "playerNumber: " + pck.playerNumber);
		
		send(pck);
	}

	//CSReqUseCard
	public void SendCSReqUseCard(int handPosition, int targetNumber)
	{
		CSReqUseCard pck = new CSReqUseCard ();
		
		pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
		pck.handPosition = handPosition;
		pck.targetNumber = targetNumber;
		
		Debug.Log ("SEND CSReqUseCard OUT: " + "session: " + pck.session + "handPosition: " + pck.handPosition + "targetNumber: " + pck.targetNumber);
		
		send(pck);
	}

	////SCNotiUseCard
	//public void SendSCNotiUseCard(int playerNumber, int targetNumber, int cardIndex)
	//{
	//	Debug.Log ("SEND SCNotiUseCard READY: " + "playerNumber: " + playerNumber + "targetNumber: " + targetNumber + "cardIndex: " + cardIndex);
	//	//send to clients
	//	SCNotiUseCard pck = new SCNotiUseCard ();
		
	//	pck.playerNumber = playerNumber;
	//	pck.targetNumber = targetNumber;
	//	pck.cardIndex = cardIndex;
		
	//	Debug.Log ("SEND SCNotiUseCard OUT: " + "playerNumber: " + pck.playerNumber + "targetNumber: " + pck.targetNumber + "cardIndex: " + pck.cardIndex);
	//	send(pck);
	//}

	//SCNotiDeckInfo
	public void SendSCNotiDeckInfo(DeckList deckList)
	{
		Debug.Log ("SEND SCNotiDeckInfo READY: " + "deckList: " + deckList);
		//send to clients
		SCNotiDeckInfo pck = new SCNotiDeckInfo ();
		
		pck.deckList = deckList;
		
		Debug.Log ("SEND SCNotiDeckInfo OUT: " + "deckList: " + pck.deckList);
		send(pck);
	}

	//SCNotiGameEnd
	public void SendSCNotiGameEnd(int winnerNumber)
	{
		Debug.Log ("SEND SCNotiGameEnd READY: " + "winnerNumber: " + winnerNumber);
		//send to clients
		SCNotiGameEnd pck = new SCNotiGameEnd ();
		
		pck.winnerNumber = winnerNumber;
		
		Debug.Log ("SEND SCNotiGameEnd OUT: " + "winnerNumber: " + pck.winnerNumber);
		send(pck);
	}

	////SCNotiGameResult
	//public void SendSCNotiGameResult(int exp, int coin, bool isLevelUp, int playTime)
	//{
	//	Debug.Log ("SEND SCNotiGameResult READY: " + "exp: " + exp + "coin: " + coin + "isLevelUp: " + isLevelUp + "playTime: " + playTime);
	//	//send to clients
	//	SCNotiGameResult pck = new SCNotiGameResult ();
		
	//	pck.exp = exp;
	//	pck.coin = coin;
	//	pck.isLevelUp = isLevelUp; 
	//	pck.playTime = playTime;
		
	//	Debug.Log ("SEND SCNotiGameResult OUT: " + "exp: " + pck.exp + "coin: " + pck.coin + "isLevelUp: " + pck.isLevelUp + "playTime: " + pck.playTime);
	//	send(pck);
	//}

	//CSReqWantGame
	public void SendCSReqWantGame(GameAction want)
	{
		CSReqWantGame pck = new CSReqWantGame ();

        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.want = want;

		Debug.Log ("SEND CSReqWantGame OUT: " + "session: " + pck.session + "GameAction: " + pck.want);
		send(pck);

		/*
		//change state home 
		if (pck.want == GameAction.Exit) {
			StateManagerUnit.Instance.SetState(ClientState.Home);
		}
		//change state home 
		if (pck.want == GameAction.EndGame) {
			StateManagerUnit.Instance.SetState(ClientState.Home);
		}
		//hange state room 
		if (pck.want == GameAction.Restart) {
			StateManagerUnit.Instance.SetState(ClientState.Room);
		}
		*/
	}

    //CSReqQuickInventory
    public void SendCSReqQuickInventory()
    {
        CSReqQuickInventory pck = new CSReqQuickInventory();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqQuickInventory OUT: " + "session: " + pck.session);
        send(pck);
    }
}