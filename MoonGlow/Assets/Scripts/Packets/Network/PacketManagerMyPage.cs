﻿using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqMyInfo
    public void SendCSReqMyInfo()
    {
        CSReqMyInfo pck = new CSReqMyInfo();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqMyInfo OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqInventory
    public void SendCSReqInventory(TabType tabType)
    {
        CSReqInventory pck = new CSReqInventory();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;

        Debug.Log("SEND CSReqInventory OUT: " + "session: " + pck.session + "TabType: " + pck.tabType.ToString());
        send(pck);
    }

    //CSReqUseItem
    public void SendCSReqUseItem(int elementIdx, bool use)
    {
        CSReqUseItem pck = new CSReqUseItem();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.itemIdx = elementIdx;
        pck.use = use;

        Debug.Log("SEND CSReqUseItem OUT: " + "session: " + "elementIdx: " + pck.itemIdx + "use: " + pck.use);
        send(pck);
    }
}