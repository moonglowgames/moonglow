﻿using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqFriendList
    public void SendCSReqFriendList(FriendTabType tabType)
    {
        CSReqFriendList pck = new CSReqFriendList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;

        Debug.Log("SEND CSReqFriendList OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString());
        send(pck);
    }

    //CSReqRequestFriend 
    public void SendCSReqRequestFriend(FriendTabType tabType, int iDx)
    {
        CSReqRequestFriend pck = new CSReqRequestFriend();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;
        pck.iDx = iDx;

        Debug.Log("SEND CSReqRequestFriend OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString() + "Idx: " + pck.iDx);
        send(pck);
    }

    //CSReqAcceptFriend  
    public void SendCSReqAcceptFriend(FriendTabType tabType, int iDx)
    {
        CSReqAcceptFriend pck = new CSReqAcceptFriend();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;
        pck.iDx = iDx;

        Debug.Log("SEND CSReqAcceptFriend OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString() + "Idx: " + pck.iDx);
        send(pck);
    }

    //CSReqDeleteFriend  
    public void SendCSReqDeleteFriend(FriendTabType tabType, int iDx)
    {
        CSReqDeleteFriend pck = new CSReqDeleteFriend();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;
        pck.iDx = iDx;

        Debug.Log("SEND CSReqDeleteFriend OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString() + "Idx: " + pck.iDx);
        send(pck);
    }

    //CSReqSendGift  
    public void SendCSReqSendGift(FriendTabType tabType, int iDx)
    {
        CSReqSendGift pck = new CSReqSendGift();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;
        pck.iDx = iDx;

        Debug.Log("SEND CSReqSendGift OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString() + "Idx: " + pck.iDx);
        send(pck);
    }

    //CSReqGetGift  
    public void SendCSReqGetGift(FriendTabType tabType, int iDx)
    {
        CSReqGetGift pck = new CSReqGetGift();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;
        pck.iDx = iDx;

        Debug.Log("SEND CSReqGetGift OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString() + "Idx: " + pck.iDx);
        send(pck);
    }

    //CSReqSendAllGifts  
    public void SendCSReqSendAllGifts(FriendTabType tabType)
    {
        CSReqSendAllGifts pck = new CSReqSendAllGifts();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;

        Debug.Log("SEND CSReqSendAllGifts OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString());
        send(pck);
    }

    //CSReqGetAllGifts  
    public void SendCSReqGetAllGifts(FriendTabType tabType)
    {
        CSReqGetAllGifts pck = new CSReqGetAllGifts();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.tabType = tabType;

        Debug.Log("SEND CSReqGetAllGifts OUT: " + "session: " + pck.session + "FriendTabType: " + pck.tabType.ToString());
        send(pck);
    }
}