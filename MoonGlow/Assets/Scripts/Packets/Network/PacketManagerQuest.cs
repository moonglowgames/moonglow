﻿using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqAchieveList
    public void SendCSReqAchieveList()
    {
        Debug.Log("SEND CSReqAchieveList READY: ");

        CSReqAchieveList pck = new CSReqAchieveList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqAchieveList OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqMissionList
    public void SendCSReqMissionList()
    {
        Debug.Log("SEND CSReqMissionList READY: ");

        CSReqMissionList pck = new CSReqMissionList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqMissionList OUT: " + "session: " + pck.session);
        send(pck);
    }
}