﻿using UnityEngine;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqChanelList
    public void SendCSReqChanelList()
    {
        CSReqChanelList pck = new CSReqChanelList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqChanelList OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqJoinChanel 
    public void SendCSReqJoinChanel(int chanelIdx)
    {
        CSReqJoinChanel pck = new CSReqJoinChanel();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.chanelIdx = chanelIdx;

        Debug.Log("SEND CSReqJoinChanel OUT: " + "session: " + pck.session + "ChanelIdx: " + pck.chanelIdx.ToString());
        send(pck);
    }

    //CSReqRankList  
    public void SendCSReqRankList()
    {
        CSReqRankList pck = new CSReqRankList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqRankList OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqRewardList  
    public void SendCSReqRewardList()
    {
        CSReqRewardList pck = new CSReqRewardList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqRewardList OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqMyRankInfo  
    public void SendCSReqMyRankInfo()
    {
        CSReqMyRankInfo pck = new CSReqMyRankInfo();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqMyRankInfo OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqGetRankItem 
    public void SendCSReqGetRankItem(int rankIdx)
    {
        CSReqGetRankItem pck = new CSReqGetRankItem();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.rankIdx = rankIdx;

        Debug.Log("SEND CSReqGetRankItem OUT: " + "session: " + pck.session + "RankIdx: " + pck.rankIdx.ToString());
        send(pck);
    }
}