using UnityEngine;
using System;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSNotiSceneEnter
    public void SendCSNotiSceneEnter(SceneType scene)
    {
        CSNotiSceneEnter pck = new CSNotiSceneEnter();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.scene = scene;

        Debug.Log("SEND CSNotiSceneEnter OUT: " + "session: " + pck.session + "scene: " + pck.scene);
        send(pck);
    }

    //SCNotiUserInfo
    public void SendSCNotiUserInfo(int faceIndex, string userName, int level, int exp, int nextLevelExp)
    {
        SCNotiUserInfo pck = new SCNotiUserInfo();
        pck.faceIndex = faceIndex;
        pck.userName = userName;
        pck.level = level;
        pck.exp = exp;
        pck.nextLevelExp = nextLevelExp;

        Debug.Log("SEND SCNotiUserInfo OUT: " + "faceIndex: " + pck.faceIndex + "userName: " + pck.userName
                   + "level: " + pck.level + "exp: " + pck.exp + "nextLevelExp: " + pck.nextLevelExp);
        send(pck);
    }

    //SCNotiItemInfo
    public void SendSCNotiItemInfo(ItemList itemList)
    {
        SCNotiItemInfo pck = new SCNotiItemInfo();
        pck.itemList = itemList;

        Debug.Log("SEND SCNotiItemInfo OUT: " + "itemList: " + pck.itemList);
        send(pck);
    }

    //SCNotiUserElement
    public void SendSCNotiUserElement(ElementList elementList)
    {
        SCNotiUserElement pck = new SCNotiUserElement();
        pck.elementList = elementList;

        Debug.Log("SEND SCNotiUserElement OUT: " + "elementList: " + pck.elementList);
        send(pck);
    }

    //CSReqRoomList
    public void SendCSReqRoomList(ReqRoomList roomList)
    {
        CSReqRoomList pck = new CSReqRoomList();
        pck.reqRoomList = roomList;

        Debug.Log("SEND CSReqRoomList OUT: " + "roomList: " + pck.reqRoomList);
        send(pck);
    }

    //SCNotiRoomList
    public void SendSCNotiRoomList(RoomList roomList_)
    {
        SCNotiRoomList pck = new SCNotiRoomList();
        pck.roomList = roomList_;

        Debug.Log("SEND SCNotiRoomList OUT: " + "roomList: " + pck.roomList);
        send(pck);
    }

    //CSReqRoomCreate
    public void SendCSReqRoomCreate(GameType gameType, PlayMode playMode, PlayTime playTime, int stake, bool disClose, bool isLock, bool singlePlay)
    {
        CSReqRoomCreate pck = new CSReqRoomCreate();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.gameType = gameType;
        pck.playMode = playMode;
        pck.playTime = playTime;
        pck.stake = stake;
        pck.disClose = disClose;
        pck.isLock = isLock;
        pck.singlePlay = singlePlay;

        Debug.Log("SEND CSReqRoomCreate OUT: " + "session: " + pck.session + "gameType: " + pck.gameType + "playMode: " + pck.playMode
                   + "playTime: " + pck.playTime + "stake: " + pck.stake + "disClose: " + pck.disClose + "isLock: " + pck.isLock);
        send(pck);

        //change state to ReadyToRoom
        StateManagerUnit.Instance.SetState(ClientState.ReadyToRoom);
    }

    //CSReqQuickStart
    public void SendCSReqQuickStart()
    {
        CSReqQuickStart pck = new CSReqQuickStart();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.gameType = (GameType)Enum.Parse(typeof(GameType), EncryptedPlayerPrefs.GetString(PlayerPrefsKey.gameType, "2"));
        pck.playMode = PlayMode.Single2;
        pck.playTime = (PlayTime)Enum.Parse(typeof(PlayTime), EncryptedPlayerPrefs.GetString(PlayerPrefsKey.playTime, "3"));
        pck.stake = Convert.ToInt16(EncryptedPlayerPrefs.GetString(PlayerPrefsKey.stake, "0"));

        Debug.Log("SEND CSReqQuickStart OUT: " + "session: " + pck.session + "gameType: " + pck.gameType + "playMode: " + pck.playMode
                   + "playTime: " + pck.playTime + "stake: " + pck.stake + "disClose: " + pck.disClose + "isLock: " + pck.isLock);
        send(pck);
        //change state to ReadyToRoom
        StateManagerUnit.Instance.SetState(ClientState.ReadyToRoom);
    }

    //CSReqJoinRoom
    public void SendCSReqJoinRoom(string roomName, int joinType)
    {
        CSReqJoinRoom pck = new CSReqJoinRoom();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.roomName = roomName;
        pck.joinType = joinType;

        Debug.Log("SEND CSReqJoinRoom OUT: " + "session: " + pck.session + "roomName: " + pck.roomName + "joinType: " + pck.joinType);
        send(pck);

        //change state to ReadyToRoom
        StateManagerUnit.Instance.SetState(ClientState.ReadyToRoom);
    }

    //CSRspPlayerJoin
    public void SendCSRspPlayerJoin(string askerUserID, bool rspResult)
    {
        CSRspPlayerJoin pck = new CSRspPlayerJoin();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.askerUserID = askerUserID;
        pck.rspResult = rspResult;

        Debug.Log("SEND CSRspPlayerJoin OUT: " + "session: " + pck.session + "askerUserID: " + pck.askerUserID + "rspResult: " + pck.rspResult);
        send(pck);
    }

    //CSNotiCancelRoom
    public void SendCSNotiCancelRoom()
    {
        CSNotiCancelRoom pck = new CSNotiCancelRoom();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSNotiCancelRoom OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqCancelJoin
    public void SendCSReqCancelJoin()
    {
        CSReqCancelJoin pck = new CSReqCancelJoin();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqCancelJoin OUT: " + "session: " + pck.session);
        send(pck);
    }

    //SCNotiEnterRoom
    public void SendSCNotiEnterRoom(GameType gameType, PlayMode playMode, PlayTime playTime, bool disClose, bool isLock)
    {
        SCNotiEnterRoom pck = new SCNotiEnterRoom();
        pck.gameType = gameType;
        pck.playMode = playMode;
        pck.playTime = playTime;
        pck.disClose = disClose;
        pck.isLock = isLock;

        Debug.Log("SEND SCNotiEnterRoom OUT: " + "gameType: " + pck.gameType + "playMode: " + pck.playMode
                   + "playTime: " + pck.playTime + "disClose: " + pck.disClose + "isLock: " + pck.isLock);
        send(pck);
    }

    //CSNotiEnterRoom
    public void SendCSNotiEnterRoom()
    {
        CSNotiEnterRoom pck = new CSNotiEnterRoom();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSNotiEnterRoom OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqReadTutorial
    public void SendCSReqReadTutorial()
    {
        CSReqReadTutorial pck = new CSReqReadTutorial();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqReadTutorial OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqNews
    public void SendCSReqNews()
    {
        CSReqNews pck = new CSReqNews();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqNews OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSNotiChangeLanguage
    public void SendCSNotiChangeLanguage(Language lang)
    {
        CSNotiChangeLanguage pck = new CSNotiChangeLanguage();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.language = lang;

        Debug.Log("SEND CSNotiChangeLanguage OUT: " + "session: " + pck.session + "Language: " + pck.language);
        send(pck);
    }
}