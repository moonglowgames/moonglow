﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Collections.Generic;
using UnityEngine;

// State object for receiving data from remote device.
public class StateObject
{
    // Client socket.
    public Socket workSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 1024;
    // Receive buffer.
    public byte[] buffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder sb = new StringBuilder();
}

public class TcpManager
{
    // The port number for the remote device.
    private static int port = 11000;
    //private static string ip = null;
    public static bool isConnected = false;
    // private Socket client = null;

    // ManualResetEvent instances signal completion.
    public static ManualResetEvent connectDone = new ManualResetEvent(false);
    public static ManualResetEvent sendDone = new ManualResetEvent(false);
    public static ManualResetEvent receiveDone = new ManualResetEvent(false);

    // The response from the remote device.
    private static List<string> response = new List<string>();

    public static void SetPacket(string packetstr)
    {
        Thread.BeginCriticalRegion();
        response.Add(packetstr);
        Thread.EndCriticalRegion();
    }

    private static void RemoveRemainPackets()
    {
        if (response != null) response.Clear();
        if (_SendList != null) _SendList.Clear();
    }

    public static void StartClient(string connectStr, int connectPort, out Socket client)
    {
        //Debug.Log ("IP: "+ connectStr + ":" + connectPort);
        // Connect to a remote device.
        try
        {
            // erase all packet...
            RemoveRemainPackets();

            port = connectPort;
            //ip = connectIP;
            // Establish the remote endpoint for the socket.
            // The name of the remote device is "host.contoso.com".
            IPAddress[] addresslist;
            IPAddress ipAddress;
            if (IPAddress.TryParse(connectStr, out ipAddress))
            {
                //IPHostEntry ipHostInfo = Dns.GetHostEntry(connectStr);//Dns.Resolve(connectIP);
                //addresslist = ipHostInfo.AddressList;
            }
            else
            {
                addresslist = Dns.GetHostAddresses(connectStr);
                if (addresslist.Length <= 0) { client = null; return; }

                foreach (var address in addresslist)
                {
                    if (address.AddressFamily == AddressFamily.InterNetwork || address.AddressFamily == AddressFamily.InterNetworkV6)
                    {
                        ipAddress = address;
                        break;
                    }
                }
            }

            if (ipAddress == null)
            {
                client = null;
                Debug.LogError("IPAddress is Null ");
                return;
            }

            IPEndPoint remoteEP = null;
            remoteEP = new IPEndPoint(ipAddress, port);
            Debug.Log("before new client created");
            // Create a TCP/IP socket.
            client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            Debug.Log("new client created");
            // Connect to the remote endpoint.
            client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);

            Debug.Log("after begin connect");
            // check 5000 msec
            bool sucess = connectDone.WaitOne(5000, true);
            if (!sucess)
            {
                client.Close();
                Debug.Log(" Don't Connect 5000 msec ");
                isConnected = false;
            }
            else
            {
                isConnected = true;
            }
            isSendBusy = false;
            // Send test data to the remote device.
            //			Send(client,"This is a test<EOF>");
            //			sendDone.WaitOne();

            // Receive the response from the remote device.
            //Receive(client);
            //receiveDone.WaitOne();

            // Write the response to the console.
            //Console.WriteLine("Response received : {0}", response);

            // Release the socket.
            //			client.Shutdown(SocketShutdown.Both);
            //			client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            client = null;
        }
    }

    public static void Disconnect(Socket client)
    {
        isConnected = false;
        Debug.Log("TCP CONNECT DISCONNECT !!");
        if (client != null)
        {
            if (client.Connected)
            {
                try
                {
                    client.Shutdown(SocketShutdown.Both);
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message);
                }
                finally
                {
                    client.Close();
                }
            }
        }

        // erase all packet...
        RemoveRemainPackets();
    }

    private static void ConnectCallback(IAsyncResult ar)
    {
        try
        {
            Debug.Log("start connect call back");
            // Retrieve the socket from the state object.
            Socket client = (Socket)ar.AsyncState;

            // Complete the connection.
            client.EndConnect(ar);
            Debug.Log("connect call back");
            Console.WriteLine("Socket connected to {0}", client.RemoteEndPoint.ToString());

            // Signal that the connection has been made.
            connectDone.Set();

            Debug.Log("connect call back finished");

            Receive(client);
            //Debug.Log("MESSAGE RECEIVED?");
        }
        catch (Exception e) { Console.WriteLine(e.ToString()); }
    }

    public static string Pop()
    {
        string returnvalue = string.Empty;
        Thread.BeginCriticalRegion();

        if (response.Count > 0)
        {
            returnvalue = response[0];
            response.RemoveAt(0);
        }

        Thread.EndCriticalRegion();

        return returnvalue;
    }

    private static void Receive(Socket client)
    {
        try
        {
            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = client;

            // Begin receiving the data from the remote device.
            client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public static char[] splitChars = { '\r', '\n' };

    private static string PacketBreaker(string packetstr_)
    {
        string retstr = string.Empty;
        string passtr = string.Empty;
        try
        {
            //Debug.Log (packetstr_);
            // if packet end than packet will include '\r'
            //Debug.LogError( "packetlenth:"+packetlenth.ToString());
            int packetlenth = 0;
            do
            {
                packetlenth = packetstr_.IndexOf('\n');
                // Debug.LogError("T:"+packetstr_.Length.ToString()+"," + packetlenth.ToString() );

                if (packetlenth > 0)
                {
                    passtr = packetstr_.Substring(0, packetlenth - 1);

                    //Debug.LogError( passtr);
                    string desc = passtr.Decription();
                    SetPacket(desc);

                    if (packetlenth + 1 == packetstr_.Length)
                    {
                        packetstr_ = string.Empty;
                    }
                    else
                    {
                        packetstr_ = packetstr_.Substring(packetlenth + 1, packetstr_.Length - packetlenth - 1);
                        //Debug.LogError("OVER 2:"+packetstr_);
                    }
                }
                else if (packetlenth == 0)
                {
                    packetstr_ = "\n";
                }

            } while (packetlenth > 0);

            retstr = packetstr_;
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            Debug.LogError("EXCEPTION PACKET:" + packetstr_);
            Debug.LogError("EXCEPTION PACKET PAS:" + passtr);
            retstr = string.Empty;
        }

        return retstr;
    }

    private static void ReceiveCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket client = state.workSocket;
            string remainstr = string.Empty;

            // Read data from the remote device.
            if (!client.Connected) { return; }

            int bytesRead = client.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There might be more data, so store the data received so far.
                state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));

                //response.Add(state.sb.ToString());

                remainstr = PacketBreaker(state.sb.ToString());
                state.sb.Length = 0;
                if (remainstr.Length > 0) state.sb.Append(remainstr);


                // Get the rest of the data.
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                                    new AsyncCallback(ReceiveCallback), state);

                //Debug.Log("RECEIVE MESSAGE1: " + state.sb.ToString());
            }
            else
            {
                // All the data has arrived; put it in response.
                if (state.sb.Length > 1)
                {
                    //Debug.Log("RECEIVE MESSAGE2: " + state.sb.ToString());

                    //response.Add(state.sb.ToString());
                    PacketBreaker(state.sb.ToString());
                    state.sb = new StringBuilder();
                }

                isConnected = false;
                // Signal that all bytes have been received.
                receiveDone.Set();
            }
        }
        catch (ObjectDisposedException de)
        {
            Debug.LogException(de);
            //isConnected = false;
        }
        catch (Exception e)
        {
            //isConnected  = false;
            Debug.LogError("TCP_EXCEPTION:" + e.Message);
            //Debug.LogError("TCP CONNECT DISCONNECT 2!");
            //Console.WriteLine(e.ToString());
        }
    }

    public static bool Send(string data)
    {
        if (isConnected == false)
        {
            Debug.LogError("TCP CONNECT CLOSED!");
            return false;
        }

        SetSendPacket(data);
        return true;
    }

    private static List<String> _SendList = new List<string>();
    private static void SetSendPacket(string sendstr)
    {
        Thread.BeginCriticalRegion();

        _SendList.Add(sendstr);

        Thread.EndCriticalRegion();
    }

    static bool isSendBusy = false;

    public static void SendRun(Socket client)
    {
        if (_SendList.Count <= 0 || isSendBusy) return;

        isSendBusy = true;

        string nowpaket = string.Empty;

        Thread.BeginCriticalRegion();

        nowpaket = _SendList[0];
        _SendList.RemoveAt(0);

        Thread.EndCriticalRegion();

        // data = "11▦" + data;
        // Convert the string data to byte data using ASCII encoding.
        string encdata = nowpaket.Encription() + "\r\n";
        byte[] byteData = Encoding.UTF8.GetBytes(encdata);

        // Begin sending the data to the remote device.
        client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), client);
    }

    private static void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.
            Socket client = (Socket)ar.AsyncState;

            // Complete sending the data to the remote device.
            int bytesSent = client.EndSend(ar);
            Console.WriteLine("Sent {0} bytes to server.", bytesSent);

            // Signal that all bytes have been sent.
            sendDone.Set();
        }
        catch (Exception e)
        {
            isConnected = false;
            Debug.LogError("TCP CONNECT DISCONNECT 3!");
            Console.WriteLine(e.ToString());
        }

        isSendBusy = false;
    }
}