﻿using UnityEngine;
using System;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSReqShopList
    public void SendCSReqShopList()
    {
        CSReqShopList pck = new CSReqShopList();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);

        Debug.Log("SEND CSReqShopList OUT: " + "session: " + pck.session);
        send(pck);
    }

    //CSReqBuyElement
    public void SendCSReqBuyElement(int elementIdx, int count)
    {
        CSReqBuyElement pck = new CSReqBuyElement();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.elementIdx = elementIdx;
        pck.count = count;

        Debug.Log("SEND CSReqBuyElement OUT: " + "session: " + pck.session + "ElementIdx: " + pck.elementIdx + "Count: " + pck.count);
        send(pck);
    }

    //CSNotiFinishAD
    public void SendCSNotiFinishAD(string guid)
    {
        CSNotiFinishAD pck = new CSNotiFinishAD();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.guid = guid;

        Debug.Log("SEND CSReqFinishAD OUT: " + "session: " + pck.session + "Guid: " + pck.guid);
        send(pck);
    }

    //CSReqIAP
    public void SendCSReqIAP(int elementIdx, IAPType iAPType)
    {
        CSReqIAP pck = new CSReqIAP();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.elementIdx = elementIdx;
        pck.iAPType = iAPType;

        Debug.Log("SEND CSReqIAP OUT: " + "session: " + pck.session + "ElementIdx: " + pck.elementIdx + "IAPType: " + Enum.GetName(typeof(IAPType), pck.iAPType));
        send(pck);
    }

    //CSReqIAPResult
    public void SendCSReqIAPResult(string guid, IAPResult result)
    {
        EncryptedPlayerPrefs.DeleteKey(PlayerPrefsKey.iapInfoBuy);
        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.iapInfoResult, guid + '\n' + Enum.GetName(typeof(IAPResult), result));
        PlayerPrefs.Save();

        CSReqIAPResult pck = new CSReqIAPResult();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.guid = guid;
        pck.result = result;

        Debug.Log("SEND CSReqIAPResult OUT: " + "session: " + pck.session + "Guid: " + pck.guid + "IAPResult: " + Enum.GetName(typeof(IAPResult), pck.result));
        send(pck);
    }
}