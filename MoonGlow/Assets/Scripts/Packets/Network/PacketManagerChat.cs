﻿using UnityEngine;
using System;

public partial class PacketManager : Singleton<PacketManager>
{
    //CSNotiChat
    public void SendCSNotiChat(string msg, int iconIdx = 0)
    {
        CSNotiChat pck = new CSNotiChat();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.chatMsg = msg;
        pck.iconIdx = iconIdx;
        Debug.Log("SEND CSNotiChat OUT: " + pck.session + " " + pck.chatMsg);
        send(pck);
    }

    //CSReqCMD 
    public void SendCSReqCMD(string msg)
    {
        CSReqCMD pck = new CSReqCMD();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.msg = msg;
        Debug.Log("SEND CSReqCMD OUT: " + " message: " + pck.msg);
        send(pck);
    }

    //CSNotiChangeChatChannel
    public void SendCSNotiChangeChatChannel(ChatChannel channel)
    {
        CSNotiChangeChatChannel pck = new CSNotiChangeChatChannel();
        pck.session = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.session);
        pck.chatChannel = channel;
        Debug.Log("SEND CSNotiChangeChatChannel OUT: " + " channel: " + Enum.GetName(typeof(ChatChannel), pck.chatChannel));
        send(pck);
    }
}