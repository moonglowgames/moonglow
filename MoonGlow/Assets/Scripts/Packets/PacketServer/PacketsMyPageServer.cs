﻿#define SERVER 

#if SERVER 

using UnityEngine;

//CSReqMyInfo
public partial class CSReqMyInfo : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqMyInfo RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqInventory 
public partial class CSReqInventory : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqInventory RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqUseItem 
public partial class CSReqUseItem : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqUseItem RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}
#endif