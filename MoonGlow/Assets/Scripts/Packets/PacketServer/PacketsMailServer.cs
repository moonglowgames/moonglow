﻿#define SERVER 

#if SERVER 

using UnityEngine;

//CSReqMailList 
public partial class CSReqMailList : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqMailList  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqGetItem  
public partial class CSReqGetItem : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqGetItem  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqGetAllItems  
public partial class CSReqGetAllItems : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqGetAllItems   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqRemoveOld   
public partial class CSReqRemoveOld : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqRemoveOld   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}
#endif