#define SERVER 

#if SERVER 

using UnityEngine;

//CSNotiSceneEnter
public  partial class  CSNotiSceneEnter : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSNotiSceneEnter RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqRoomList
public  partial class  CSReqRoomList  : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSReqRoomList RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqRoomCreate
public  partial class  CSReqRoomCreate  : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSReqRoomCreate RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqQuickStart
public  partial class  CSReqQuickStart  : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSReqQuickStart RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqJoinRoom
public  partial class CSReqJoinRoom  : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSReqJoinRoom RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqJoinRoom
public partial class CSRspPlayerJoin : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqJoinRoom RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqJoinRoom
public partial class CSNotiCancelRoom : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqJoinRoom RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqCancelJoin
public partial class CSReqCancelJoin : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqCancelJoin RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSNotiEnterRoom
public  partial class  CSNotiEnterRoom  : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSNotiEnterRoom RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqReadTutorial
public partial class CSReqReadTutorial : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqReadTutorial RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqNews
public partial class CSReqNews : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqNews RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSNotiChangeLanguage
public partial class CSNotiChangeLanguage : Packet
{
    public override void Excute()
    {
        Debug.Log("CSNotiChangeLanguage RECEIVED..WAIT ACTION ON SERVER...");
    }
}

#endif