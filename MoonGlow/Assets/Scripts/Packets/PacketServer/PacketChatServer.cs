#define SERVER

#if SERVER

using System;
using UnityEngine;

//CSNotiChat
public partial class CSNotiChat:Packet
{
	public override void Excute ()
	{
		Debug.Log ("CSNotiChat received...");
	}
}

//CSReqCMD
public partial class CSReqCMD : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqCMD received...");
    }
}

//CSNotiChangeChatChannel
public partial class CSNotiChangeChatChannel : Packet
{
    public override void Excute()
    {
        Debug.Log("CSNotiChangeChatChannel received...");
    }
}
#endif