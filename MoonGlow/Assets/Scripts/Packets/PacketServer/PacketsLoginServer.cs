﻿#define SERVER 

#if SERVER 

using UnityEngine;

//CSReqRegister
public partial class CSReqRegister : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSReqRegister RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqLogin
public partial class CSReqLogin : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqLogin RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqLogout
public partial class CSReqLogout : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqLogout RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqSessionLogin
public  partial class  CSReqSessionLogin  : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSReqSessionLogin RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSRspPang
public partial class CSRspPang : Packet
{
    public override void Excute()
    {
        Debug.Log("CSRspPang RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqFindPassword
public partial class CSReqFindPassword : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqFindPassword RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqChangePassword
public partial class CSReqChangePassword : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqChangePassword RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqGotoScene
public  partial class  CSReqGotoScene : Packet {
	public override void Excute ()
	{
		Debug.Log ("CSReqGotoScene RECEIVED..WAIT ACTION ON SERVER...");
	}
}

//CSReqClientPause
public partial class CSReqClientPause : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqClientPause RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqClientFocus
public partial class CSReqClientFocus : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqClientFocus RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqUrl
public partial class CSReqUrl : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqUrl RECEIVED..WAIT ACTION ON SERVER...");
    }
}
#endif