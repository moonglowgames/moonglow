﻿#define SERVER 

#if SERVER 

using UnityEngine;

//CSReqJokerInfo
public partial class CSReqJokerInfo : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqJokerInfo RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqUpgradeSpell 
public partial class CSReqUpgradeSpell : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqUpgradeSpell RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqDowngradeSpell
public partial class CSReqDowngradeSpell : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqDowngradeSpell RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqCancelUpgrade 
public partial class CSReqJokerCancel : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqCancelUpgrade RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqApplySpells 
public partial class CSReqJokerApply : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqApplySpells RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}
#endif