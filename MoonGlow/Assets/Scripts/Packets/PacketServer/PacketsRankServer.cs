﻿#define SERVER 

#if SERVER 

using UnityEngine;

//CSReqChanelList 
public partial class CSReqChanelList : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqChanelList  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqJoinChanel  
public partial class CSReqJoinChanel : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqJoinChanel  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqRankList  
public partial class CSReqRankList : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqRankList   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqRewardList 
public partial class CSReqRewardList : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqRewardList  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqMyRankInfo  
public partial class CSReqMyRankInfo : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqMyRankInfo  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqGetRankItem  
public partial class CSReqGetRankItem : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqGetRankItem   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}
#endif