﻿#define SERVER 

#if SERVER 

using UnityEngine;

//CSReqShopList
public partial class CSReqShopList : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqShopList RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqBuyElement
public partial class CSReqBuyElement : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqBuyElement RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSNotiFinishAD
public partial class CSNotiFinishAD : Packet
{
    public override void Excute()
    {
        Debug.Log("CSNotiFinishAD RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqIAP
public partial class CSReqIAP : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqIAP RECEIVED..WAIT ACTION ON SERVER...");
    }
}

//CSReqIAPResult
public partial class CSReqIAPResult : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqIAPResult RECEIVED..WAIT ACTION ON SERVER...");
    }
}
#endif