﻿#define SERVER 

#if SERVER 

using UnityEngine;

//CSReqFriendList 
public partial class CSReqFriendList : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqFriendList  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqRequestFriend  
public partial class CSReqRequestFriend : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqRequestFriend  RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqAcceptFriend  
public partial class CSReqAcceptFriend : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqAcceptFriend   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqDeleteFriend   
public partial class CSReqDeleteFriend : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqDeleteFriend   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqSendGift   
public partial class CSReqSendGift : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqSendGift   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqGetGift   
public partial class CSReqGetGift : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqGetGift   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqSendAllGifts   
public partial class CSReqSendAllGifts : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqSendAllGifts   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}

//CSReqGetAllGifts   
public partial class CSReqGetAllGifts : Packet
{
    public override void Excute()
    {
        Debug.Log("CSReqGetAllGifts   RECEIVED..WAIT ACTION ON SERVER...");
        //throw new NotImplementedException();
    }
}
#endif