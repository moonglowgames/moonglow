﻿#define CLIENT

#if CLIENT 
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public partial class SCNotiHello : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiHello RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (svrName.ToLower().Contains("login"))
        {
            StateManagerUnit.Instance.SetState(ClientState.UserLogin);
        }
        else
        {
            StateManagerUnit.Instance.SetState(ClientState.SessionLogin);
        }
    }
}

public partial class SCRspRegister : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspRegister RECEIVED......................................................WAIT ACTION ON CLIENT...");
        //"loading scene"
        Debug.Log("LoginState: " + this.state);
        switch (this.state)
        {
            case LoginState.OK:
                LoginManager.Instance.ToggleWindow(false);

                string account = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.account);
                string password = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.password);
                string deviceID = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.deviceID);

                //Reset quick start history & settings
                EncryptedPlayerPrefs.SetDefaultStrings();

                PacketManager.Instance.SendCSReqLogin(AccountType.Email, account, password, deviceID);
                break;
            case LoginState.GuestOK:
                LoginManager.Instance.ToggleWindow(false);

                EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.guestLogin, 1);
                deviceID = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.deviceID);
                //Reset quick start history
                EncryptedPlayerPrefs.SetDefaultStrings();

                PacketManager.Instance.SendCSReqLogin(AccountType.Guest, deviceID, string.Empty, deviceID);
                break;
            case LoginState.IDExist:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "ID_EXISTS");
                LoginManager.Instance.ToggleWindow(true);
                break;
            case LoginState.NickExist:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "NICK_EXISTS");
                LoginManager.Instance.ToggleWindow(true);
                break;
            case LoginState.IDAndNickExist:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "NICK_EXISTS");
                LoginManager.Instance.ToggleWindow(true);
                break;
            case LoginState.OldVersion:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "OLD_VERSION");
                LoginManager.Instance.ToggleWindow(true);
                break;
            case LoginState.Ban:
                //GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "BAN");
                break;
            default:
                LoginManager.Instance.ToggleWindow(true);
                break;
        }
    }
}

//SCRspLogin
public  partial class  SCNotiLogin : Packet
{
	public override void Excute ()
	{
		Debug.Log ("SCRspLogin RECEIVED......................................................WAIT ACTION ON CLIENT.");
        GameControlUnit.Instance.UserAccess = this.userType;
        
        switch (this.state)
        {
            case LoginState.OK:
                EncryptedPlayerPrefs.SetString(PlayerPrefsKey.loginState, Enum.GetName(typeof(LoginState), LoginState.OK));
                EncryptedPlayerPrefs.SetString(PlayerPrefsKey.userID, this.userID);
                EncryptedPlayerPrefs.SetString(PlayerPrefsKey.session, this.session);
                EncryptedPlayerPrefs.SetString(PlayerPrefsKey.connectUrl, this.connectUrl);

                PacketManager.Instance.DisConnect();
                string[] urlArray = this.connectUrl.Split(':');
                if (urlArray.Length == 2)
                {
                    string connectIP = urlArray[0];
                    int connectPort = int.Parse(urlArray[1]);

                    EncryptedPlayerPrefs.SetString(PlayerPrefsKey.connectIP, connectIP);
                    EncryptedPlayerPrefs.SetString(PlayerPrefsKey.connectPort, connectPort.ToString());

                    PacketManager.Instance.Connect(connectIP, connectPort);

                    LoginManager.Instance.ToggleWindow(false);
                }
                break;
            case LoginState.OldVersion:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "OLD_VERSION");
                break;
            case LoginState.NoID:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "NO_ID");
                LoginManager.Instance.ToggleWindow(true);
                break;
            case LoginState.MissPassword:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "NO_PASSWORD");
                LoginManager.Instance.ToggleWindow(true);
                break;
            default:
                LoginManager.Instance.ToggleWindow(true);
                break;
        }
    }
}

//SCNotiDownload
public partial class SCNotiDownload
{
    public override void Excute()
    {
        Debug.Log("SCNotiDownload RECEIVED......................................................WAIT ACTION ON CLIENT.");
        if (SceneManager.GetActiveScene().buildIndex != 0) { return; }
        //Application.OpenURL(" market://details?id=com.MoonGlow.MoonGlow”);
        //                      itms - apps://itunes.apple.com/app/id1122872372
        GlobalMessageUnit.Instance.ShowMessage(LoadingManager.Instance.gameObject, "OpenStore", this.downloadUrl, "OLD_VERSION");
    }
}


//SCRspPasswordResult
public partial class SCRspPasswordResult : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspPasswordResult RECEIVED......................................................WAIT ACTION ON CLIENT.");
        Debug.Log(this.state.ToString());
        switch (this.state)
        {
            case LoginState.OK:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "SUCCESS");
                break;
            case LoginState.MissPassword:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "NO_PASSWORD");
                break;
        }
    }
}

//SCNotiSessionLogin
public partial class  SCNotiSessionLogin  : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiSessionLogin RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (this.status)
        {
            PlayerPrefs.SetString(PlayerPrefsKey.session, this.newsession);
            StateManagerUnit.Instance.SetState(ClientState.AssetLoading);
            GameControlUnit.Instance.UserAccess = this.userType;
        }
        else
        {
            //go to login server
            PacketManager.Instance.DisConnect();
            StateManagerUnit.Instance.SetState(ClientState.Loading);
        }
    }
}

//SCNotiConnections
public partial class SCNotiConnections : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiConnections RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }
        HomeUIManagerUnit.Instance.UserCount = this.connections.ToString();
    }
}

//SCReqPing
public partial class SCReqPing : Packet
{
    public override void Excute()
    {
        Debug.Log("SCReqPing RECEIVED......................................................WAIT ACTION ON CLIENT...");
        PacketManager.Instance.SendCSRspPang(this.pingCount);
    }
}

//SCNotiReconnect
public partial class SCNotiReconnect : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiReconnect RECEIVED......................................................WAIT ACTION ON CLIENT...");

        //string connectIP = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.connectIP);
        //int connectPort = int.Parse(EncryptedPlayerPrefs.GetString(PlayerPrefsKey.connectPort));
        //PacketManager.Instance.Connect(connectIP, connectPort);
        PacketManager.Instance.DisConnect();
        StateManagerUnit.Instance.SetState(ClientState.Loading);
    }
}

//SCNotiQuit
public partial class SCNotiQuit : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiQuit RECEIVED...Forcing client app to close...");
        Application.Quit();
    }
}

//SCNotiGotoScene
public  partial class  SCNotiGotoScene : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiGotoScene RECEIVED......................................................WAIT ACTION ON CLIENT...");
		//LoadingManager.Instance.GetSCNotiGotoScene (this.scene);

		switch ( this.scene )
		{
		case SceneType.loading:
			StateManagerUnit.Instance.SetState (ClientState.Loading);
			break;
		case SceneType.Home:
			StateManagerUnit.Instance.SetState (ClientState.Home);
			break;
		case SceneType.Close:
			// logout and quit
			break;
		default:
			break;
		}
	}
}

//SCNotiClientFocus
public partial class SCNotiClientFocus : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiClientFocus RECEIVED......................................................WAIT ACTION ON CLIENT...");
        StateManagerUnit.Instance.RecvSCNotiClientFocus();
    }
}

//SCNotiUrl
public partial class SCNotiUrl : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiUrl RECEIVED......................................................WAIT ACTION ON CLIENT...");
        switch (this.urlType)
        {
            case UrlType.Facebook:
                Application.OpenURL(this.url);
                break;
            case UrlType.HomePage:
                Application.OpenURL(this.url);
                break;
            case UrlType.Manual:
                Application.OpenURL(this.url);
                break;
            default:
                break;
        }
    }
}

//SCNotiLogout
public partial class SCNotiLogout : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiLogout RECEIVED......................................................WAIT ACTION ON CLIENT...");
    }
}

#endif