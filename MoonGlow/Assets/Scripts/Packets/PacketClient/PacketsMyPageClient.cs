﻿using UnityEngine;

//SCNotiMyInfo 
public partial class SCNotiMyInfo : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiMyInfo RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.MyPage.Profile.Info = this.myInfo;
        GameControlUnit.Instance.MyPage.Profile.needUpdate = true;
    }
}

//SCNotiInventory 
public partial class SCNotiInventory : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiInventory RECEIVED......................................................WAIT ACTION ON CLIENT...");
        switch (this.inventoryList.tabType)
        {
            case TabType.Joker:
                GameControlUnit.Instance.Joker.DisableJokerFaces();

                foreach (InventoryInfo info in this.inventoryList.inventoryList)
                {
                    GameControlUnit.Instance.Joker.GetFace(info.inventoryIdx).info = info;
                    GameControlUnit.Instance.Joker.GetFace(info.inventoryIdx).ToggleMaterial(true);
                    GameControlUnit.Instance.Joker.GetFace(info.inventoryIdx).needUpdate = true;

                }
                break;
            //go to MyPage(Avatar, Pet, CardSkin, Badge)
            default:
                GameControlUnit.Instance.MyPage.DisableImages(this.inventoryList.tabType);

                foreach (InventoryInfo info in this.inventoryList.inventoryList)
                {
                    if (info.inventoryIdx == 500) { continue; }
                    GameControlUnit.Instance.MyPage.GetItem(info.inventoryIdx, this.inventoryList.tabType).info = info;
                    GameControlUnit.Instance.MyPage.GetItem(info.inventoryIdx, this.inventoryList.tabType).ToggleMaterial(true);
                    GameControlUnit.Instance.MyPage.GetItem(info.inventoryIdx, this.inventoryList.tabType).needUpdate = true;
                }
                break;
        }
    }
}

//SCRspUseItem 
public partial class SCRspUseItem : Packet
{
    public override void Excute()
    {
        Debug.Log(this.result.ToString());
    }
}