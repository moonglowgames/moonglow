﻿using System;
using UnityEngine;

//SCNotiShopList
public partial class SCNotiShopList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiShopList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Shop.Clear();
        foreach (ShopListInfo shopInfo in this.shopList.shopList)
        {
            if (shopInfo.tabID == TabType.Badge || shopInfo.tabID == TabType.Character) { continue; }
            GameControlUnit.Instance.Shop.AddItem(shopInfo);
        }
    }
}

//SCRspBuyElement 
public partial class SCRspBuyElement : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspBuyElement RECEIVED......................................................WAIT ACTION ON CLIENT...");

        Debug.Log(this.result.ToString());

        switch (this.result)
        {
            case ShopResult.OK:
                SoundControlUnit.Instance.PlayUiClip(UiClip.SuccessfullPurchase);
                break;
            case ShopResult.AlreadyExist:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "HAVE_ITEM");
                break;
            case ShopResult.LackRuby:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "LACK_RUBY");
                break;
            case ShopResult.LackGold:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "LACK_GOLD");
                break;
            case ShopResult.Fail:
                //GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "FAIL");
                break;
        }
    }
}

//SCNotiShowAD
public partial class SCNotiShowAD : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiShowAD RECEIVED......................................................WAIT ACTION ON CLIENT...");

#if UNITY_ANDROID || UNITY_IOS
        GameControlUnit.Instance.Ad.OnResponseShowAD(this.isOK, this.guid, this.todayRemainCount, this.remainTime);
#endif
    }
}

//SCNotiGetADItem
public partial class SCNotiGetADItem : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiGetADItem RECEIVED......................................................WAIT ACTION ON CLIENT...");

#if UNITY_ANDROID || UNITY_IOS
        GameControlUnit.Instance.Ad.GetReward(this.itemIdx);
#endif
    }
}

//SCRspIAP
public partial class SCRspIAP : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspIAP RECEIVED......................................................WAIT ACTION ON CLIENT...");

#if UNITY_ANDROID || UNITY_IOS
        EncryptedPlayerPrefs.SetString(PlayerPrefsKey.iapInfoBuy, this.guid + '\n' + this.elementIdx.ToString());
        PlayerPrefs.Save();

        GameControlUnit.Instance.Iap.BuyConsumable(this.guid, this.elementIdx);
#endif
    }
}

//SCRspIAPResult
public partial class SCRspIAPResult : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspIAPResult RECEIVED......................................................WAIT ACTION ON CLIENT...");

#if UNITY_ANDROID || UNITY_IOS
        EncryptedPlayerPrefs.DeleteKey(PlayerPrefsKey.iapInfoResult);
        PlayerPrefs.Save();
        Debug.Log("Item guid: " + this.guid + "ShopResult: " + Enum.GetName(typeof(ShopResult), this.result));
#endif
    }
}