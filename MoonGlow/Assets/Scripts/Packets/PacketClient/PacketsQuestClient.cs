﻿using UnityEngine;

//SCNotiAchieveList 
public partial class SCNotiAchieveList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiAchieveList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Quest.ClearAchievements();
        foreach (Achievement achievement in this.achieveList.achieveList)
        {
            GameControlUnit.Instance.Quest.AddAchievement(achievement);
        }
    }
}

//SCNotiMissionList 
public partial class SCNotiMissionList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiMissionList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Quest.ClearMissions();
        foreach (Mission mission in this.missionList.missionList)
        {
            GameControlUnit.Instance.Quest.AddMission(mission);
        }
    }
}