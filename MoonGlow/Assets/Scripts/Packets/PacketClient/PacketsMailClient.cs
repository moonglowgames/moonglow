﻿#define CLIENT

#if CLIENT 

using UnityEngine;

//SCNotiMailList 
public partial class SCNotiMailList : Packet
{
    public override void Excute()
    {
        int unread = 0;
        GameControlUnit.Instance.Mail.Clear();
        foreach (Mail mail in this.mailList.mailList)
        {
            GameControlUnit.Instance.Mail.AddMail(mail);
            if (!mail.isGot) { unread++; }
        }

        //GameControlUnit.Instance.Mail.Title = "You have " + unread.ToString() + " unread message";
        //GameControlUnit.Instance.Mail.Title += (unread > 1) ? "s." : ".";
        GameControlUnit.Instance.Mail.Title = LocalizationManager.Instance.Get("UNREAD").Replace("*", unread.ToString());
    }
}

//SCRspGetItem 
public partial class SCRspGetItem : Packet
{
    public override void Excute()
    {
        Debug.Log(this.getResult.ToString());
    }
}

//SCRspGetAllItems  
public partial class SCRspGetAllItems : Packet
{
    public override void Excute()
    {
        Debug.Log(this.getResult.ToString());
    }
}

#endif