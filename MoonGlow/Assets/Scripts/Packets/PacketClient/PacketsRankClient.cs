﻿#define CLIENT

#if CLIENT 

using UnityEngine;

//SCNotiChanelList 
public partial class SCNotiChanelList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiChanelList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Rank.Channel.ClearChannelList();
        foreach (var channel in this.chanelList.chanelList)
        {
            GameControlUnit.Instance.Rank.Channel.AddChannelItem(channel);
            if (channel.isJoin) { GameControlUnit.Instance.Rank.Channel.ChanelName = LocalizationManager.Instance.Get("CHANNEL_JOIN").Replace("*", channel.name); }
        }
    }
}

//SCNotiRankList 
public partial class SCNotiRankList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiRankList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        //GameControlUnit.Instance.Rank.RankList.ChanelName = this.rankList.chanelName;

        GameControlUnit.Instance.Rank.RankList.ClearRankList();
        foreach (var rank in this.rankList.rankList)
        {
            GameControlUnit.Instance.Rank.RankList.AddRankItem(rank);
        }
    }
}

//SCNotiRewardLst 
public partial class SCNotiRewardList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiRewardLst RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Rank.MyRank.ClearRewards();
        foreach (var reward in this.rewardList.rewardList)
        {
            GameControlUnit.Instance.Rank.MyRank.AddReward(reward);
        }
    }
}

//SCNotiMyRankInfo 
public partial class SCNotiMyRankInfo : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiMyRankInfo RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Rank.MyRank.ClearRankList();
        if (this.rankHistory.currentRankCount != 0)
        {
            GameControlUnit.Instance.Rank.MyRank.UpdateCurrentRank(this.rankHistory.currentChannelName, this.rankHistory.currentRankCount, this.rankHistory.currentRankPercent, this.rankHistory.currentRankPoint, this.rankHistory.currentRankWin, this.rankHistory.currentRankLose, this.rankHistory.currentWinRate);
        }
        foreach (var rank in this.rankHistory.rankHistory)
        {
            GameControlUnit.Instance.Rank.MyRank.AddRankItem(rank);
        }
    }
}

//SCRspRankResult 
public partial class SCRspRankResult : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspRankResult RECEIVED......................................................WAIT ACTION ON CLIENT...");

        switch (this.result)
        {
            case ChanelResult.OK:
                Debug.Log(ChanelResult.OK);
                break;
            case ChanelResult.NotAccept:
                Debug.Log(ChanelResult.NotAccept);
                break;
            default:
                break;
        }
    }
}
#endif