﻿#define CLIENT

#if CLIENT 

using UnityEngine;
using UnityEngine.SceneManagement;

//SCNotiPlayerProfile 
public partial class SCNotiPlayerProfile : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiPlayerProfile RECEIVED......................................................WAIT ACTION ON CLIENT...");

        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 1:
                HomeUIManagerUnit.Instance.ShowPlayerProfile(this.playerProfile);
                break;
            case 2:
                RoomManagerUnit.Instance.ShowPlayerProfile(this.playerProfile);
                break;
        }
    }
}
#endif