#define CLIENT

#if CLIENT 
using UnityEngine;

//SCNotiPlayerList
public  partial class  SCNotiPlayerList : Packet
{
	public override void Excute ()
	{
		Debug.Log ("SCNotiPlayerList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        //if(RoomManagerUnit.Instance.Map.Shift == -1)
        //{
        foreach (PlayerInfo playerInfo in this.playerList.playerList)
        {
            if (playerInfo.isMe == true)
            {
                RoomManagerUnit.Instance.Map.Shift = playerInfo.playerNumber - 1;
                break;
            }
        }
        //}

        foreach (PlayerInfo playerInfo in this.playerList.playerList)
		{
//			PlayerUnit playerUnit = GameControlUnit.Instance.PlayerList.GetPlayer(playerInfo.playerNumber-1);
			PlayerUnit playerUnit = GameControlUnit.Instance.PlayerList.GetPlayer(playerInfo.playerNumber);
			playerUnit.avatarIdx = playerInfo.faceImageIndex;
			playerUnit.level = playerInfo.playerLevel;
			playerUnit.playerName = playerInfo.playerName;
			playerUnit.isUser = playerInfo.isUser;
			playerUnit.isMe = playerInfo.isMe;
			playerUnit.needUpdate = true;
			playerUnit.gameObject.SetActive(true);
		}
	}
}

//SCNotiPlayerStatus 
public partial class SCNotiPlayerStatus : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiPlayerStatus RECEIVED......................................................WAIT ACTION ON CLIENT...");
        PlayerUnit playerUnit = GameControlUnit.Instance.PlayerList.GetPlayer(this.playerNumber);
        playerUnit.hitPoints = this.HP;
        playerUnit.shieldPoints = this.SP;
        playerUnit.moonDust = this.MD;
        playerUnit.mana = this.MA;
        playerUnit.rodEnergy = this.RE;
        playerUnit.moonStonePower = this.MS;
        playerUnit.zenStonePower = this.ZS;
        playerUnit.moonCrystalPower = this.MC;
        playerUnit.needUpdate = true;

        if (this.effectIdx != 0)
        {
            EffectManager.Instance.InstantiateEffect(this.effectIdx, this.playerNumber);
        }

        if (this.globalEffectIdx != 0)
        {
            EffectManager.Instance.InstantiateEffect(this.globalEffectIdx, this.playerNumber);
        }

        if (this.playerNumber == RoomManagerUnit.Instance.Map.Shift + 1)
        {
            GameControlUnit.Instance.Hand.UpdatePrices();
        }
    }
}

//SCNotiPlayerItemInfo 
public partial class SCNotiPlayerItemInfo : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiPlayerItemInfo RECEIVED......................................................WAIT ACTION ON CLIENT...");
        PlayerUnit playerUnit = GameControlUnit.Instance.PlayerList.GetPlayer(this.playerItemInfo.playerNumber);
        playerUnit.hpPlus = playerItemInfo.hpPlus;
        playerUnit.spPlus = playerItemInfo.spPlus;
        playerUnit.petList = playerItemInfo.petList;
        playerUnit.badgeIdx = playerItemInfo.badgeIdx;
        playerUnit.itemSetup = true;
    }
}

//SCNotiStartGame
public partial class SCNotiStartGame : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiStartGame RECEIVED......................................................WAIT ACTION ON CLIENT...");
        GameControlUnit.Instance.Hand.RevealCards();
        //GameControlUnit.Instance.HandManager.ToggleState(CameraUnit.Instance.Ratio != RatioType.aPhone && CameraUnit.Instance.Ratio != RatioType.aPad);
        GameControlUnit.Instance.GameNotice.ShowEffect(Notice.Start);
        //change state Play
        StateManagerUnit.Instance.SetState(ClientState.Play);
    }
}

//SCNotiContinueGame
public partial class SCNotiContinueGame : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiContinueGame RECEIVED......................................................WAIT ACTION ON CLIENT...");
        GameControlUnit.Instance.Hand.RevealCards();
        //change state Play
        StateManagerUnit.Instance.SetState(ClientState.Play);
    }
}

//SCNotiDeckShuffle
public partial class  SCNotiDeckShuffle : Packet {
	public override void Excute ()
	{
		Debug.LogError ("SCNotiDeckShuffle RECEIVED......................................................WAIT ACTION ON CLIENT...");
	}
}

//SCNotiDecToHand
public  partial class  SCNotiDeckToHand : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiDecToHand RECEIVED......................................................WAIT ACTION ON CLIENT...");

        foreach (CardListInfo cardInfo in this.cardList.cardList)
        {
            if (cardInfo.cardIndex == 999)
            {
                GameControlUnit.Instance.Hand.MakeJoker(cardInfo.handPosition - 1, this.cardList.jokerFaceIdx, this.cardList.spellIdxList);
            }
            else
            {
                GameControlUnit.Instance.Hand.GetCard(cardInfo.handPosition - 1).cardIdx = cardInfo.cardIndex;
            }
            GameControlUnit.Instance.Hand.GetCard(cardInfo.handPosition - 1).cardSkinIdx = cardList.cardSkinIdx;
            GameControlUnit.Instance.Hand.GetCard(cardInfo.handPosition - 1).StartCoroutine("UpdateSkin");
            GameControlUnit.Instance.Hand.GetCard(cardInfo.handPosition - 1).itemIdx = cardInfo.attachItemIdx;

            if (this.cardList.showType == ShowAnimationType.AnimationFromDeck)
            {
                GameControlUnit.Instance.Hand.GetFromDeck(cardInfo.handPosition - 1);
            }
        }
        RoomManagerUnit.Instance.GameStart = true;
	}
}

//SCNotiPlayerFocus
public partial class SCNotiPlayerFocus : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiPlayerFocus RECEIVED......................................................WAIT ACTION ON CLIENT...");
        GameControlUnit.Instance.PlayerList.SetPlayerTurn(this.playerNumber);
        GameControlUnit.Instance.Timer.StartTimer();
        if (this.playerNumber - 1 == RoomManagerUnit.Instance.Map.Shift)
        {
            if (this.onlyDiscard)
            {
                GameControlUnit.Instance.GameNotice.ShowEffect(Notice.Discard);
                GameControlUnit.Instance.Hand.SetDiscardState();
            }
            else
            {
                GameControlUnit.Instance.GameNotice.ShowEffect(GameControlUnit.Instance.GameNotice.MyTurn ? Notice.PlayAgain : Notice.YourTurn);
                GameControlUnit.Instance.Hand.SetNormalState();
            }
            GameControlUnit.Instance.GameNotice.MyTurn = true;
        }
        else { GameControlUnit.Instance.GameNotice.MyTurn = false; }
    }
}

//SCNotiUseCard
public  partial class  SCNotiUseCard : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiUseCard RECEIVED......................................................WAIT ACTION ON CLIENT...");
        //GameControlUnit.Instance.Token.UseCard(this.playCardInfo.cardIdx, RoomManagerUnit.Instance.Map.GetNumber(this.playCardInfo.playerNumber - 1), RoomManagerUnit.Instance.Map.GetNumber(this.playCardInfo.targetNumber - 1));
        if (!this.playCardInfo.success)
        {
            GameControlUnit.Instance.Hand.ResetLastCard();
            GameControlUnit.Instance.GameNotice.MyTurn = true;
        }
        if (this.playCardInfo.effectIdx != 0)
        {
            EffectManager.Instance.InstantiateEffect(this.playCardInfo.effectIdx, this.playCardInfo.targetNumber);
        }
	}
}

//SCNotiDeckInfo
public  partial class  SCNotiDeckInfo : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiDeckInfo RECEIVED......................................................WAIT ACTION ON CLIENT...");
		GameControlUnit.Instance.History.Clear();
		foreach(DeckInfo deckInfo in this.deckList.deckList)
		{
            GameControlUnit.Instance.History.ShowHistoryToken(deckInfo.targetPosition, deckInfo.cardIndex, deckInfo.attachItemIdx, this.deckList.cardSkinIdx, this.deckList.jokerFaceIdx, this.deckList.spellIdxList);
        }
	}
}

//SCNotiGameEnd
public  partial class  SCNotiGameEnd : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiGameEnd RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.GameNotice.ShowEffect(Notice.End);
	}
}

//SCNotiGameResult
public  partial class  SCNotiGameResult : Packet {
    public override void Excute()
    {
        Debug.Log("SCNotiGameResult RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Hand.ResetDragOption();
        GameControlUnit.Instance.History.Clear();

        GameControlUnit.Instance.Result.gameResult = this.gameResult;
        GameControlUnit.Instance.Result.ResultWindowToggle(true);
        GameControlUnit.Instance.Timer.StopTimer();
        
        //change state PlayResult
        StateManagerUnit.Instance.SetState(ClientState.PlayResult);
    }
}

//SCNotiQuickInventory
public partial class SCNotiQuickInventory : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiQuickInventory RECEIVED......................................................WAIT ACTION ON CLIENT...");
    }
}
#endif