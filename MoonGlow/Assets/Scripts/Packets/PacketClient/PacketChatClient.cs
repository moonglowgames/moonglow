#define CLIENT

#if CLIENT
using UnityEngine;

//SCNotiChat
public partial class SCNotiChat : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiChat RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Chat.ReceiveMessage(this.nick, this.playerNumber, this.chatMsg, this.iconIdx);
    }
}

//SCNotiServerNoti
public partial class SCNotiServerNoti : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiServerNoti RECEIVED......................................................WAIT ACTION ON CLIENT...");

        switch (this.command)
        {
            case Command.Message:
                GlobalMessageUnit.Instance.ShowServerMessage(this.value);
                break;
        }
    }
}

//SCNotiCMD
public partial class SCNotiCMD : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiCMD RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Chat.ReceiveCMD(this.msg);
    }
}
#endif