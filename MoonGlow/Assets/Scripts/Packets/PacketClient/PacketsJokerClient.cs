﻿using UnityEngine;

//SCNotiJokerInfo 
public partial class SCNotiJokerInfo : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiJokerInfo RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Joker.Profile.jokerFaceIdx = this.jokerFace;
        GameControlUnit.Instance.Joker.Profile.editMode = this.isTry;
        GameControlUnit.Instance.Joker.Profile.needUpdate = true;
    }
}

//SCNotiSpellList 
public partial class SCNotiSpellList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiSpellList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.Joker.ResetSpells();

        GameControlUnit.Instance.Joker.InkAmount = this.spellIdxList.ink;
        GameControlUnit.Instance.Joker.Ink = this.spellIdxList.ink.ToString();
        GameControlUnit.Instance.Joker.Ink += "/" + this.spellIdxList.totalInk.ToString();
        if (this.spellIdxList.tryInkCount != 0)
        {
            GameControlUnit.Instance.Joker.Ink += this.spellIdxList.tryInkCount < 0 ? " (" + this.spellIdxList.tryInkCount.ToString() + ")" : " (+" + this.spellIdxList.tryInkCount.ToString() + ")";
        }
        //GameControlUnit.Instance.Joker.Ink += this.spellIdxList.tryInkCount == 0 ? string.Empty : " (" + this.spellIdxList.tryInkCount.ToString() + ")";

        if (this.spellIdxList.spellIdxList != null)
        {
            foreach (int spell in this.spellIdxList.spellIdxList)
            {
                GameControlUnit.Instance.Joker.GetSpell(spell).SetSpell(spell);
            }
            GameControlUnit.Instance.Joker.Profile.spellIdxList = this.spellIdxList.spellIdxList;
            GameControlUnit.Instance.Joker.Profile.needUpdate = true;
        }
    }
}

//SCRspUpgradeSpell 
public partial class SCRspUpdateSpell : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspUpgradeSpell RECEIVED......................................................WAIT ACTION ON CLIENT...");

        Debug.Log(this.applyResult.ToString());
    }
}

//SCRspApplySpells 
public partial class SCRspJokerApply : Packet
{
    public override void Excute()
    {
        Debug.Log("SCRspApplySpells RECEIVED......................................................WAIT ACTION ON CLIENT...");

        Debug.Log(this.applyResult.ToString());
    }
}