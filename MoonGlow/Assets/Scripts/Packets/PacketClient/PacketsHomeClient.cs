﻿#define CLIENT

#if CLIENT 
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

//SCNotiMenuAlarm
public partial class SCNotiMenuAlarm : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiMenuAlarm Received.......................................................WAIT ACTION ON CLIENT...");

        switch (this.option)
        {
            case Option.Mail:
                GameControlUnit.Instance.mailAlarm = this.alarm;
                break;
            case Option.Friend:
                GameControlUnit.Instance.friendAlarm = this.alarm;
                break;
            case Option.Shop:
                GameControlUnit.Instance.shopAlarm = this.alarm;
                break;
            case Option.News:
                GameControlUnit.Instance.newsAlarm = this.alarm;
                break;
        }
        if (SceneManager.GetActiveScene().buildIndex == 1) { GameControlUnit.Instance.Alarm.ShowNotiAlarm(this.option, this.alarm); }
    }
}

//SCNotiUserInfo
public  partial class  SCNotiUserInfo : Packet {
	public override void Excute ()
	{
        Debug.Log("SCNotiUserInfo RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }

        GameControlUnit.Instance.User.Info.avatarIdx = this.faceIndex;
		GameControlUnit.Instance.User.Info.userName = this.userName;
        GameControlUnit.Instance.User.Info.badgeIdx = this.badgeIdx;
        GameControlUnit.Instance.User.Info.userLevel = this.level;
		GameControlUnit.Instance.User.Info.exp = this.exp;
		GameControlUnit.Instance.User.Info.nextLevelExp = this.nextLevelExp;
		GameControlUnit.Instance.User.needUpdate = true;
    }
}

//SCNotiItemInfo
public  partial class  SCNotiItemInfo : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiItemInfo RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }

        foreach (ItemInfo itemInfo in this.itemList.itemList)
		{
			GameControlUnit.Instance.User.Info.items[(itemInfo.itemIndex % 100) - 1] = itemInfo.count;
		}
        GameControlUnit.Instance.User.needUpdate = true;
    }
}

//SCNotiUserElement
public  partial class  SCNotiUserElement : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiUserElement RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }

		foreach(ElementInfo elementInfo in this.elementList.elementList)
		{
			switch(elementInfo.elementType)
			{
			case ElementType.Stamina:
				GameControlUnit.Instance.User.Info.stamina = elementInfo.value;
				GameControlUnit.Instance.User.Info.maxStamina = elementInfo.maxValue;
				break;
			case ElementType.MoonRuby:
				GameControlUnit.Instance.User.Info.moonRuby = elementInfo.value;
				break;
			case ElementType.Gold:
				GameControlUnit.Instance.User.Info.gold = elementInfo.value;
				break;
			default:
				Debug.LogError("Unknown Element type");
				break;
			}
		}
        GameControlUnit.Instance.User.needUpdate = true;
    }
}

public partial class SCNotiChatIconSet
{
    public override void Excute()
    {
        Debug.Log("SCNotiChatIconSet RECEIVED......................................................WAIT ACTION ON CLIENT...");
    }
}

//SCNotiRoomList
public  partial class  SCNotiRoomList  : Packet {
	public override void Excute ()
	{
        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }

        Debug.Log ("SCNotiRoomList RECEIVED......................................................WAIT ACTION ON CLIENT...");
		GameControlUnit.Instance.RoomList.Clear();
		foreach(RoomInfo roomInfo in this.roomList.roomList)
		{
			GameControlUnit.Instance.RoomList.AddRoom(roomInfo);
		}
	}
}

//SCNotiPlayerJoin
public partial class SCNotiPlayerJoin : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiPlayerJoin RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.JoinConfirm.askerUserID = this.askerUserID;
        GameControlUnit.Instance.JoinConfirm.JoinReqView(this.askerLevel, this.askerNick, this.askerFaceIdx);
    }
}

//SCNotiPlayerJoinCancel
public partial class SCNotiPlayerJoinCancel : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiPlayerJoinCancel RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.JoinConfirm.ToggleWindow(false);
    }
}

//SCNotiEnterRoom
public partial class  SCNotiEnterRoom  : Packet {
	public override void Excute ()
	{
		Debug.Log ("SCNotiEnterRoom RECEIVED......................................................WAIT ACTION ON CLIENT...");
		GameControlUnit.Instance.RoomInfo.gameType = this.gameType;
		GameControlUnit.Instance.RoomInfo.playMode = this.playMode;
		GameControlUnit.Instance.RoomInfo.playTime = this.playTime;
		GameControlUnit.Instance.RoomInfo.disClose = this.disClose;
		GameControlUnit.Instance.RoomInfo.goalHP = this.goalHP;
		GameControlUnit.Instance.RoomInfo.goalRP = this.goalRP;
        GameControlUnit.Instance.RoomInfo.stake = this.stake;

        //change state Room
        SceneManager.LoadScene(2);
        StateManagerUnit.Instance.SetState (ClientState.Room);
	}
}

//SCNotiMessage
public partial class SCNotiMessage : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiMessage RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }

        switch (this.msgType)
        {
            case MessageType.LackStamina:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "LACK_STAMINA");
                break;
            case MessageType.LowLevel:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "LOW_LEVEL");
                break;
            case MessageType.LackGold:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "STAKE_GOLD");
                break;
            case MessageType.JoinReject:
                GameControlUnit.Instance.JoinConfirm.ToggleWindow(false);
                break;
            case MessageType.NoRoom:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "NO_ROOM");
                break;
            case MessageType.FullRoom:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "FULL_ROOM");
                break;
            case MessageType.SendJoinOk:
                GameControlUnit.Instance.JoinConfirm.JoinCancelView();
                break;
            case MessageType.AlreadyPlay:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "ALREADY_PLAY");
                break;
            case MessageType.RoomCreator:
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "ROOM_CREATOR");
                break;
            default:
                break;
        }
    }
}

//SCNotiNews
public partial class SCNotiNews : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiNews RECEIVED......................................................WAIT ACTION ON CLIENT...");

        GameControlUnit.Instance.News.newsList = this.newsList.newsList;
        GameControlUnit.Instance.News.UpdateNews();
    }
}
#endif