﻿#define CLIENT

#if CLIENT 

using UnityEngine;
using UnityEngine.SceneManagement;

//SCNotiFriendList 
public partial class SCNotiFriendList : Packet
{
    public override void Excute()
    {
        Debug.Log("SCNotiFriendList RECEIVED......................................................WAIT ACTION ON CLIENT...");

        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }

        GameControlUnit.Instance.Friend.ClearFriends(this.friendList.tabType);
        foreach (Friend friend in this.friendList.friendList)
        {
            GameControlUnit.Instance.Friend.AddFriend(friend, this.friendList.tabType);
        }
        if(this.friendList.tabType == FriendTabType.MyFriends)
        {
            GameControlUnit.Instance.Friend.Title = this.friendList.friendList.Count.ToString() + "/20";
        }
    }
}

//SCRspFriendResut 
public partial class SCRspFriendResut : Packet
{
    public override void Excute()
    {
        Debug.Log(this.result.ToString());
    }
}

#endif