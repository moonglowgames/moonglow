﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MsgBoxLocalizationManager : Singleton<MsgBoxLocalizationManager>
{
    private static Dictionary<string, string[]> _dict = new Dictionary<string, string[]>();
    private const string _FILE = @"Data/MsgBoxLocalization";
    private const int DECODE_PARSE_COUNT = 5;

    void Awake()
    {
        LoadFromFile();
    }

    void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        string[] lines = file.text.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        char[] charsToTrim = { ' ', '\t' };

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);
            if (ln[0] == ';') { continue; }
            else
            {
                if (ln != string.Empty)
                {
                    string[] parsings = ln.Split(',');
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        _dict[parsings[0].Trim(charsToTrim)] = new string[DECODE_PARSE_COUNT - 1];
                        for (int i = 0; i < DECODE_PARSE_COUNT - 1; i++)
                        {
                            _dict[parsings[0].Trim(charsToTrim)][i] = parsings[i + 1].Trim(charsToTrim).Replace("@", System.Environment.NewLine);
                        }
                    }
                }
            }
        }
    }

    public string GetHeader(string key)
    {
        if (!_dict.ContainsKey(key)) { Debug.LogError("Wrong key: " + key); return string.Empty; }

        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                return _dict[key][0];
            case Language.Korean:
                return _dict[key][2];
            default:
                return _dict[key][0];
        }
    }

    public string GetMessage(string key)
    {
        if (!_dict.ContainsKey(key)) { Debug.LogError("Wrong key: " + key); return string.Empty; }

        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                return _dict[key][1];
            case Language.Korean:
                return _dict[key][3];
            default:
                return _dict[key][1];
        }
    }
}