﻿using UnityEngine;
using UnityEngine.UI;

public class LocalizationUnit : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    public string key;

    void Awake()
    {
        if (_text == null) { _text = GetComponent<Text>(); }
    }

    public void UpdateUI()
    {
        _text.text = LocalizationManager.Instance.Get(key);
    }

    public void UpdateUI(string key_)
    {
        key = key_;
        UpdateUI();
    }
}