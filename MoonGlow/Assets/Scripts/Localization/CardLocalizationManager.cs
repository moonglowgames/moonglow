﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class CardLocalizationManager : Singleton<CardLocalizationManager>
{
    private Dictionary<int, string[]> _dict = new Dictionary<int, string[]>();
    private const string _FILE = @"Data/CardLocalization";
    private const int DECODE_PARSE_COUNT = 7;

    private Language _language;

    void Awake()
    {
        _language = (Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0);
        LoadFromFile();
    }

    void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        char[] charsToTrim = { ' ', '\t' };

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);
            if (ln[0] == ';') { continue; }
            else
            {
                if (ln != string.Empty)
                {
                    string[] parsings = ln.Split(',');
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        int iDx = Convert.ToInt32(parsings[0].Trim(charsToTrim));
                        _dict[iDx] = new string[DECODE_PARSE_COUNT - 1];
                        for (int i = 0; i < DECODE_PARSE_COUNT - 1; i++)
                        {
                            _dict[iDx][i] = parsings[i + 1].Trim(charsToTrim);
                        }
                    }
                }
            }
        }
    }

    public string GetName(int key)
    {
        if (!_dict.ContainsKey(key)) { Debug.LogError("Wrong key: " + key); return string.Empty; }

        switch (_language)
        {
            case Language.English:
                return _dict[key][0];
            case Language.Korean:
                return _dict[key][2];
            default:
                return _dict[key][0];
        }
    }

    public string GetDescription(int key)
    {
        if (!_dict.ContainsKey(key)) { Debug.LogError("Wrong key: " + key); return string.Empty; }

        switch (_language)
        {
            case Language.English:
                return _dict[key][1];
            case Language.Korean:
                return _dict[key][3];
            default:
                return _dict[key][1];
        }
    }
}