﻿using System;

public enum UserItem
{
    etherealInk,
    saqqaraTablet,
    rosemaryOil,
    stimPack,
    eveningPrimroseElixir,
	suassureaObliviataElixir,
};

[Serializable]
public class UserInfo
{
	public int avatarIdx;
	public string userName;
	public int userLevel;
    public int badgeIdx;
    public int exp;
	public int nextLevelExp;
	public int stamina;
	public int maxStamina;
	public int moonRuby;
	public int gold;
	public bool extraHP;
	public bool extraSP;
	public int[] items = new int[6];
}