﻿using UnityEngine;
using System;
using System.Collections.Generic;


public class WinConditionManager : Singleton<WinConditionManager>
{
    private Dictionary<int, WinCondition> _items = new Dictionary<int, WinCondition>();
    private const string _FILE = @"Data/WinCondition";

    void Awake()
    {
        LoadFromFile();
    }

    void LoadFromFile()
    {

        TextAsset file = Resources.Load(_FILE) as TextAsset;

        char[] charsToTrim = { ' ', '\t' };

        Debug.Log("LOADED WIN CONDITION DATA FILE" + file.name);

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        WinCondition tempWinCondition = null;

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);
            if (ln[0] != ';')
            {
                tempWinCondition = new WinCondition();
                tempWinCondition.Decode(ln);
                _items[tempWinCondition.iDx] = tempWinCondition;
            }
        }
    }

    public WinCondition Get(GameType gameType)
    {
        if (_items.ContainsKey((int)gameType))
        {
            return _items[(int)gameType];
        }
        else
        {
            Debug.Log("Didn't find Win condition info: " + gameType.ToString());
            return null;
        }
    }
}