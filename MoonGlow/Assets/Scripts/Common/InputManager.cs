﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class InputManager : MonoBehaviour
{
    private bool _properExit = false;

    void Awake()
    {
        Input.multiTouchEnabled = false;
    }

    void OnApplicationQuit()
    {
        if (!_properExit) { PacketManager.Instance.SendCSReqLogout(); }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                PlaySettingsUnit.Instance.ToggleWindow(true);
            }
            else if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "ExitProcedure", null, "EXIT_CONFIRM");
                //Application.Quit();
            }
        }
    }

    void ExitProcedure()
    {
        StartCoroutine("Exit");
    }

    public IEnumerator Exit()
    {
        _properExit = true;
        PacketManager.Instance.SendCSReqLogout();
        yield return new WaitForSeconds(0.3f);
        Debug.Log("Closing the application.");
        Application.Quit();
    }

    public void QuickStartButtonClick()
    {
        SoundControlUnit.Instance.PlayUiClip(UiClip.QuickStart);
        PacketManager.Instance.SendCSReqQuickStart();
    }

    public void BackHomeButtonClick()
    {
        //		LoadLevel("Home");
        PacketManager.Instance.SendCSReqWantGame(GameAction.Exit);
    }
}