﻿using UnityEngine;

public class GameControlUnit : Singleton<GameControlUnit>
{
    private RoomInfo _roomInfo = new RoomInfo();
    private ChatManager _chatManager;
    private UserUnit _userUnit;
    private UserInfo _userInfo;
    private AlarmManager _alarmManager;
    private RoomListUnit _roomListUnit;
    private MailUnit _mailUnit;
    private QuestUnit _questUnit;
    private MyPageUnit _myPageUnit;
    private RankUnit _rankUnit;
    private FriendUnit _friendUnit;
    private JokerUnit _jokerUnit;
    private ShopUnit _shopUnit;
    private NewsUnit _newsUnit;
    private JoinConfirmUnit _joinConfirmUnit;
    private PlayerListUnit _playerListUnit;
    private GameNoticeUnit _gameNoticeUnit;
    private HandUnit _handUnit;
    private HandManagerUnit _handManagerUnit;
    private ChatBalloonUnit _chatBalloonUnit;
    private HistoryUnit _historyUnit;
    private TimerUnit _timerUnit;
    private InventoryUnit _inventoryUnit;
    private ResultUnit _resultUnit;

#if UNITY_ANDROID || UNITY_IOS
    private AdManager _adManager;
    private IAPManager _iapManager;

    public AdManager Ad
    {
        get { return _adManager; }
        set { _adManager = value; }
    }

    public IAPManager Iap
    {
        get { return _iapManager; }
        set { _iapManager = value; }
    }
#endif

    public UserUnit User
    {
        get { return _userUnit; }
        set { _userUnit = value; }
    }

    public UserInfo PlayerInfo
    {
        get { return _userInfo; }
        set { _userInfo = value; }
    }

    public AlarmManager Alarm
    {
        get { return _alarmManager; }
        set { _alarmManager = value; }
    }

    public RoomListUnit RoomList
    {
        get { return _roomListUnit; }
        set { _roomListUnit = value; }
    }

    public MailUnit Mail
    {
        get { return _mailUnit; }
        set { _mailUnit = value; }
    }

    public QuestUnit Quest
    {
        get { return _questUnit; }
        set { _questUnit = value; }
    }

    public MyPageUnit MyPage
    {
        get { return _myPageUnit; }
        set { _myPageUnit = value; }
    }

    public RankUnit Rank
    {
        get { return _rankUnit; }
        set { _rankUnit = value; }
    }

    public FriendUnit Friend
    {
        get { return _friendUnit; }
        set { _friendUnit = value; }
    }

    public JokerUnit Joker
    {
        get { return _jokerUnit; }
        set { _jokerUnit = value; }
    }

    public ShopUnit Shop
    {
        get { return _shopUnit; }
        set { _shopUnit = value; }
    }

    public NewsUnit News
    {
        get { return _newsUnit; }
        set { _newsUnit = value; }
    }

    public JoinConfirmUnit JoinConfirm
    {
        get { return _joinConfirmUnit; }
        set { _joinConfirmUnit = value; }
    }

    public PlayerListUnit PlayerList
    {
        get { return _playerListUnit; }
        set { _playerListUnit = value; }
    }

    public GameNoticeUnit GameNotice
    {
        get { return _gameNoticeUnit; }
        set { _gameNoticeUnit = value; }
    }

    public HandUnit Hand
    {
        get { return _handUnit; }
        set { _handUnit = value; }
    }

    public HandManagerUnit HandManager
    {
        get { return _handManagerUnit; }
        set { _handManagerUnit = value; }
    }

    public ChatBalloonUnit ChatBalloon
    {
        get { return _chatBalloonUnit; }
        set { _chatBalloonUnit = value; }
    }

    public HistoryUnit History
    {
        get { return _historyUnit; }
        set { _historyUnit = value; }
    }

    public InventoryUnit Inventory
    {
        get { return _inventoryUnit; }
        set { _inventoryUnit = value; }
    }

    public ResultUnit Result
    {
        get { return _resultUnit; }
        set { _resultUnit = value; }
    }

    public TimerUnit Timer
    {
        get { return _timerUnit; }
        set { _timerUnit = value; }
    }

    public UserType UserAccess { get; set; }

    public RoomInfo RoomInfo { get { return _roomInfo; } }

    public bool ForceLogin { get; set; }
    public int LastScene { get; set; }

    public string mailAlarm { get; set; }
    public string friendAlarm { get; set; }
    public string shopAlarm { get; set; }
    public string newsAlarm { get; set; }

    public ChatManager Chat
    {
        get
        {
            if (_chatManager == null) { ConnectChatManager(); }
            return _chatManager;
        }
    }

    private void ConnectChatManager()
    {
        _chatManager = GameObject.FindObjectOfType<ChatManager>();

        if (_chatManager == null) { Debug.LogError("ChatManager is null in GameControlUnit.cs"); }
    }
}