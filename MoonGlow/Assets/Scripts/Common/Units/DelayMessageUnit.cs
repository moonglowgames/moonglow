﻿using UnityEngine;
using UnityEngine.UI;

public class DelayMessageUnit : MonoBehaviour
{
    private Text _text;

    public string Message
    {
        get { return _text.text; }
        set { _text.text = value; }
    }

    void Awake()
    {
        _text = transform.FindChild("Canvas/Panel/Text").GetComponent<Text>();
    }

    public void SetMessage(string message, Color32 textColor)
    {
        Message = message;
        _text.color = textColor;
    }
}