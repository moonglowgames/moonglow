﻿using UnityEngine;

public enum UiClip {Cancel, ChatButton, Close, Join, MenuButton, MenuTab, QuickStart, Complete, GameTypeButton, NormalTap, SoftButton, SuccessfullPurchase, UseButton, UseButton1, UseButton2};
public enum PlayClip {CardDragCancel, CardDragStart, CardDrop, CardShare, FocusChange, ResultDraw, ResultLose, ResultWin, TimeAlert, TimeOut};

[RequireComponent(typeof(AudioSource))]
public class SoundControlUnit : Singleton<SoundControlUnit>
{
    private AudioSource _audio;
    private AudioClip[] _playClips;
    private AudioClip[] _uiClips;

    public bool isOn { get; set; }

    void Awake()
    {
        _audio = GetComponent<AudioSource>();
        _uiClips = Resources.LoadAll<AudioClip>("Sounds/Clip/Ui");
        _playClips = Resources.LoadAll<AudioClip>("Sounds/Clip/Play");
        isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.sound, 1) == 1;
    }

    public void PlayUiClip(UiClip clip)
    {
        if (!isOn) { return; }
        _audio.PlayOneShot(_uiClips[(int)clip]);
    }

    public void EffectClip(AudioClip clip)
    {
        if (!isOn) { return; }
        _audio.PlayOneShot(clip);
    }

    public void PlaySceneClip(PlayClip clip)
    {
        if (!isOn) { return; }
        _audio.PlayOneShot(_playClips[(int)clip]);
    }
}