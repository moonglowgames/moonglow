﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class PlayerProfileUnit : MonoBehaviour
{
    [SerializeField]
    private Image _avatar;
    [SerializeField]
    private Text _name, _level, _record;

    [SerializeField]
    private Image _badgeIcon;
    [SerializeField]
    private Text _badgeName;

    [SerializeField]
    private Image[] _petIcon;
    [SerializeField]
    private Text[] _petName;
    [SerializeField]
    private Text[] _petDescription;

    public void UpdateView(PlayerProfile profile)
    {
        //user
        _avatar.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatar, profile.faceIdx % 100);
        _name.text = profile.nickName;
        _level.text = profile.level.ToString();
        _record.text = profile.winCount.ToString();
        //badge
        if (profile.badgeIdx > 0)
        {
            _badgeName.text = ItemManager.Instance.Get(profile.badgeIdx).name;
            _badgeIcon.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, profile.badgeIdx);
            _badgeIcon.enabled = true;
        }
        else
        {
            _badgeName.text = string.Empty;
            _badgeIcon.enabled = false;
        }
        
        //pets
        foreach (Image pet in _petIcon) { pet.transform.parent.gameObject.SetActive(false); }

        if (profile.petList != null)
        {
            for (int i = 0; i < profile.petList.Count; i++)
            {
                ItemElementInfo itemInfo = ItemManager.Instance.Get(profile.petList[i]);
                _petName[i].text = itemInfo.name;
                _petDescription[i].text = itemInfo.description;
                _petIcon[i].sprite = ItemSpriteFactory.Instance.Get(SpriteType.Pet, profile.petList[i] % 100 - 1);
                _petIcon[i].transform.parent.gameObject.SetActive(true);
            }
        }
    }

    public void ToggleWindow(bool state)
    {
        this.gameObject.SetActive(state);
    }
}