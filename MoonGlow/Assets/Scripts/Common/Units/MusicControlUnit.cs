﻿using UnityEngine;

public enum BgMusic { Home, Play, Tutorial, Credits  }

[RequireComponent(typeof(AudioSource))]
public class MusicControlUnit : Singleton<MusicControlUnit>
{
    private AudioSource _audio;

    public bool isOn { get; set; }

    void Awake()
    {
        if (_audio == null) { _audio = GetComponent<AudioSource>(); }
        isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.music, 1) == 1;
    }

    void OnLevelWasLoaded(int level)
    {
        switch (level)
        {
            case 1:
                _audio.clip = Resources.Load<AudioClip>("Sounds/BgMusic/BgmHome");
                break;
            case 2:
                _audio.clip = Resources.Load<AudioClip>("Sounds/BgMusic/" + GameControlUnit.Instance.RoomInfo.gameType);
                break;
            case 3:
                _audio.clip = Resources.Load<AudioClip>("Sounds/BgMusic/Tutorial");
                break;
        }
        if (isOn) { Play(); }
    }

    public void Play()
    {
        if (!_audio.isPlaying) { _audio.Play(); }
    }

    public void Stop()
    {
        _audio.Stop();
    }
}