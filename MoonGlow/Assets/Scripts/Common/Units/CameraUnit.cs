﻿using UnityEngine;

public enum RatioType { Standart, Standart800, aPhone, aPad }

public class CameraUnit : Singleton<CameraUnit>
{
    public float Width;
    public float Height;
    public RatioType Ratio {get; set;}

    void Awake()
    {
        Width = Screen.width;
        Height = Screen.height;

        float ratio = Width / Height;
        if (ratio < 1.4) { Ratio = RatioType.aPad; }
        else if (ratio < 1.51) { Ratio = RatioType.aPhone; }
        else if (ratio < 1.7) { Ratio = RatioType.Standart800; }
        else { Ratio = RatioType.Standart; }

        //GetComponent<Camera>().orthographicSize = (Ratio < 1.5) ? 4.8f : 3.6f;
        GetComponent<Camera>().orthographicSize = 6.4f * Height / Width;
    }

    void Start()
    {
        ChangeLayout();
    }

    public void ChangeLayout()
    {
        foreach (var uiObject in GameObject.FindGameObjectsWithTag("LayoutElement"))
        {
            uiObject.SendMessage("UpdateUI", Ratio);
        }
    }
}