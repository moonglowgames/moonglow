﻿using UnityEngine;
using UnityEngine.UI;

public class PopUpUnit : MonoBehaviour
{
    private Text _header;
    private Text _text;

    public GameObject ReturnObject;
    public string FuncName;
    public object FuncValue;

    public string Header
    {
        get { return _header.text; }
        set { _header.text = value; }
    }

    public string Message
    {
        get { return _text.text; }
        set { _text.text = value; }
    }

    void Awake()
    {
        _header = transform.FindChild("Canvas/Frame/Header/Text").GetComponent<Text>();
        _text = transform.FindChild("Canvas/Frame/Text").GetComponent<Text>();
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    public void ToggleWindow(bool state)
    {
        this.gameObject.SetActive(state);
        if (!state) { SoundControlUnit.Instance.PlayUiClip(UiClip.Close); }
    }

    public void OkButton()
    {
        if (ReturnObject != null && !string.IsNullOrEmpty(FuncName))
        {
            ReturnObject.SendMessage(FuncName, FuncValue, SendMessageOptions.DontRequireReceiver);
            SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
        }
        else
        {
            Debug.Log("Function is Null");
        }

        ToggleWindow(false);
    }

    public void CancelButton()
    {
        ReturnObject = null;
        FuncName = string.Empty;
        FuncValue = null;

        ToggleWindow(false);
    }
}