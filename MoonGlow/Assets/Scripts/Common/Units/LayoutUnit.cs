﻿using UnityEngine;

public class LayoutUnit : MonoBehaviour
{
    private RectTransform _canvas;

    void Awake()
    {
        _canvas = GetComponent<RectTransform>();
    }

    public static Vector2 GetSize()
    {
        switch (CameraUnit.Instance.Ratio)
        {
            case RatioType.Standart:
                return new Vector2(1280f, 720f);
            case RatioType.Standart800:
                return new Vector2(1280f, 800f);
            case RatioType.aPhone:
                return new Vector2(1280f, 854f);
            case RatioType.aPad:
                return new Vector2(1280f, 960f);
            default:
                return new Vector2(1280f, 720f);
        }
    }

    public void UpdateUI()
    {
        _canvas.sizeDelta = GetSize();
    }
}