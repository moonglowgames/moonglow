﻿using UnityEngine;
using System.Collections;

public class GlobalMessageUnit : Singleton<GlobalMessageUnit>
{
    private string MessagePrefebName = "UI/MessageBox";
    private string ChatMessagePrefebName = "UI/ChatMessage";
    private string ServerMessagePrefebName = "UI/ServerMessage";
    private PopUpUnit _popUpUnit = null;

    private GameObject goPopup = null;
    private GameObject goChatMessage = null;
    private GameObject goServerMessage = null;

    void Awake()
    {
        // Make Popup instance
        goPopup = (GameObject)Instantiate(Resources.Load(MessagePrefebName));
        goPopup.transform.SetParent(this.transform);
        _popUpUnit = goPopup.GetComponent<PopUpUnit>();
        goPopup.SetActive(false);

        // Make Chat message instance
        goChatMessage = (GameObject)Instantiate(Resources.Load(ChatMessagePrefebName));
        goChatMessage.transform.SetParent(this.transform);
        goChatMessage.SetActive(false);

        // Make Server message instance
        goServerMessage = (GameObject)Instantiate(Resources.Load(ServerMessagePrefebName));
        goServerMessage.transform.SetParent(this.transform);
        goServerMessage.SetActive(false);

        //DontDestroyOnLoad(transform.gameObject);
    }

    public void ShowMessage(GameObject go, string FuncName, object arg, string key, string args = null)
    {
        _popUpUnit.ToggleWindow(true);

        _popUpUnit.Header = MsgBoxLocalizationManager.Instance.GetHeader(key);
        _popUpUnit.Message = args == null ? MsgBoxLocalizationManager.Instance.GetMessage(key) : MsgBoxLocalizationManager.Instance.GetMessage(key).Replace("*", args);
        _popUpUnit.ReturnObject = go;
        _popUpUnit.FuncName = FuncName;
        _popUpUnit.FuncValue = arg;
    }

    //public void ShowMessage(GameObject go, string FuncName, object arg, string Header, string Message)
    //{
    //    _popUpUnit.ToggleWindow(true);

    //    _popUpUnit.Header = Header;
    //    _popUpUnit.Message = Message;
    //    _popUpUnit.ReturnObject = go;
    //    _popUpUnit.FuncName = FuncName;
    //    _popUpUnit.FuncValue = arg;
    //}

    public void ShowChatMessage(string message)
    {
        StartCoroutine("ChatMessage", message);
    }

    private IEnumerator ChatMessage(string message)
    {
        goChatMessage.SetActive(true);
        goChatMessage.GetComponent<DelayMessageUnit>().SetMessage(message, Color.white);

        yield return new WaitForSeconds(3.0f);

        goChatMessage.SetActive(false);
    }

    public void ShowServerMessage(string message)
    {
        StartCoroutine("ServerMessage", message);
    }

    private IEnumerator ServerMessage(string message)
    {
        goServerMessage.SetActive(true);
        goServerMessage.GetComponent<DelayMessageUnit>().SetMessage(message, Color.yellow);

        yield return new WaitForSeconds(5.0f);

        goServerMessage.SetActive(false);
    }
}