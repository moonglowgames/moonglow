﻿using UnityEngine;
using UnityEngine.UI;

public class FocusFrameUnit : MonoBehaviour
{
    private Image _frame;
    private int _i;

    static private Sprite[] sheet;

    void Awake()
    {
        _frame = GetComponent<Image>();
        if(sheet == null)
        {
            sheet = Resources.LoadAll<Sprite>("Images/FocusFrame");
        }
    }

    void OnEnable()
    {
        _frame.enabled = true;
        InvokeRepeating("Refresh", 0f, 0.1f);
    }

    void OnDisable()
    {
        _frame.enabled = false;
        CancelInvoke("Refresh");
    }

    void Refresh()
    {
        if (_i==8) { _i = 0; }
        _frame.sprite = sheet[_i];
        _i++;
    }
}