﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ChatUnit : MonoBehaviour
{
    [SerializeField]
    private InputField _input;
    [SerializeField]
    private ScrollRect _logAreaPanel;
    [SerializeField]
    private Text _logArea;
    [SerializeField]
    private Toggle _devModeToggle;

    private int _currentProgress = 0;
    private int _count = 20;
    private bool _isDeveloper;
    private bool _animDelay;
    //private bool _isSpam;
    private string _msg;

    void Start()
    {
        transform.FindChild("Canvas").GetComponent<RectTransform>().sizeDelta = LayoutUnit.GetSize();
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
        if (GameControlUnit.Instance.UserAccess != UserType.User)
        {
            if (_devModeToggle != null) { _devModeToggle.gameObject.SetActive(true); }
        }
    }

    void OnEnable()
    {
        if (_logAreaPanel != null)
        {
            EventSystem.current.SetSelectedGameObject(_input.gameObject);
            _logAreaPanel.verticalNormalizedPosition = 0f;
        }
    }

    public void EndType(string msg)
    {
        _input.text = string.Empty;
        if (msg != string.Empty)
        {
            _msg = msg;
            //			_chatLog += "<color=#1e90ffff>" + myName + ": " + msg + "</color>" + System.Environment.NewLine;
            //			TrimLine();
            //			_logArea.text = _chatLog;
            //Debug.Log("SEND MESSAGE: " + msg);
            if (_msg.Length > 40) { _msg = _msg.Substring(0, 40); }
            if (_isDeveloper)
            {
                PacketManager.Instance.SendCSReqCMD(_msg);
            }
            else
            {
                PacketManager.Instance.SendCSNotiChat(_msg);
            }
            //if (_isSpam) { InvokeRepeating("SpamPackets", 0f, 0.05f); }
            //else { PacketManager.Instance.SendCSNotiChat(_msg); }
        }
    }

    void CancelAnimationDelay()
    {
        _animDelay = false;
    }

    public void SendEmoticons(int Idx)
    {
        if (_animDelay) { return; }
        Invoke("CancelAnimationDelay", 3f);
        _animDelay = true; 
        PacketManager.Instance.SendCSNotiChat(string.Empty, Idx);
    }

    public void ReceiveMessage(string msg)
    {
        if (SceneManager.GetActiveScene().buildIndex != 1) { return; }
        _logArea.text = msg;
    }

    public void CloseWindow()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1) { HomeUIManagerUnit.Instance.ToggleDefaultUI(true); }
        SoundControlUnit.Instance.PlayUiClip(UiClip.ChatButton);
        this.gameObject.SetActive(false);
    }

    public void TestButton(bool state)
    {
        //_isSpam = state;
    }

    public void DevModeButton(bool state)
    {
        _isDeveloper = state;
        _input.GetComponent<Image>().color = state ? new Color(1f, 0.2f, 0.2f) : Color.white;
    }

    void SpamPackets()
    {
        _currentProgress++;
        PacketManager.Instance.SendCSNotiChat(_msg);
        if (_currentProgress == _count)
        {
            _currentProgress = 0;
            CancelInvoke();
        }
    }
}