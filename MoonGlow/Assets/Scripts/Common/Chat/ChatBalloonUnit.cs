﻿#pragma warning disable 649

using UnityEngine;

public class ChatBalloonUnit : MonoBehaviour
{
    [SerializeField]
    private ChatBalloonItemUnit[] _balloons;

    void Awake()
    {
        GameControlUnit.Instance.ChatBalloon = this;
    }

    public void Clear()
    {
        foreach (var balloon in _balloons) { balloon.ShowText(false); }
    }

    public void Show(int pos, string msg, int iconIdx)
    {
        pos = RoomManagerUnit.Instance.Map.GetNumber(pos - 1);
        if (iconIdx == 0)
        {
            _balloons[pos].ShowText(true, msg);
        }
        else
        {
            _balloons[pos].ShowEmoticon(true, iconIdx);
        }
    }

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                transform.position += new Vector3(0f, 0.3f, 0f);
                break;
            case RatioType.aPhone:
                transform.position += new Vector3(0f, 0.5f, 0f);
                break;
            case RatioType.aPad:
                transform.position += new Vector3(0f, 1f, 0f);
                break;
        }
    }
}