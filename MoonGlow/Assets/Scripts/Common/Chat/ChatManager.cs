﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChatManager : Singleton<ChatManager>
{
    private ChatUnit _chatWindow;
	private string _chatLog;

    public ChatUnit ChatWindowPrefab;
    public int maxLines = 10;
	public string myName = "MoonGlow";
    public string chatColor; // Home - 9400D3; Play F9E5CF

    void TrimLine()
    {
        int n = 0;
        foreach (var c in _chatLog)
        {
            if (c == '\n') { n++; }
        }
        if (n >= maxLines)
        {
            int index = _chatLog.IndexOf(System.Environment.NewLine);
            _chatLog = _chatLog.Substring(index + System.Environment.NewLine.Length);
        }
    }

    public void ChatWindowToggle(bool state)
	{
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            HomeUIManagerUnit.Instance.ToggleDefaultUI(false, false);
            GlobalMessageUnit.Instance.ShowChatMessage(string.Empty);
        }

        if (_chatWindow == null)
        {
            _chatWindow = Instantiate(ChatWindowPrefab);
        }
        _chatWindow.ReceiveMessage(_chatLog);
        _chatWindow.gameObject.SetActive(state);


        SoundControlUnit.Instance.PlayUiClip(UiClip.ChatButton);
	}

    public void ReceiveMessage(string senderName, int playerNumber, string msg, int iconIdx)
    {
        if (SceneManager.GetActiveScene().buildIndex == 0) { return; }

        string[] msgArray = msg.Split('\n');

        for (int i = 0; i <= msgArray.GetUpperBound(0); i++)
        {
            _chatLog += "<color=#" + chatColor + ">" + senderName + ": " + msgArray[i] + "</color>" + System.Environment.NewLine;
            TrimLine();
            if (_chatWindow != null)
            {
                _chatWindow.ReceiveMessage(_chatLog);
            }

            switch (SceneManager.GetActiveScene().buildIndex)
            {
                case 1:
                    if (_chatWindow == null || !_chatWindow.gameObject.activeSelf)
                    {
                        GlobalMessageUnit.Instance.ShowChatMessage(senderName + ": " + msgArray[i]);
                    }
                    break;
                case 2:
                    //RoomManagerUnit.Instance.ChatEntry(senderName + ": " + msgArray[i]);
                    GameControlUnit.Instance.ChatBalloon.Show(playerNumber, msgArray[i], iconIdx);
                    break;
            }
        }
    }

    public void ReceiveCMD(string msg)
    {
        string[] msgArray = msg.Split('\n');
        for (int i = 0; i <= msgArray.GetUpperBound(0); i++)
        {
            _chatLog += "<color=red>" + msgArray[i] + "</color>" + System.Environment.NewLine;
            TrimLine();
            if (_chatWindow != null)
            {
                _chatWindow.ReceiveMessage(_chatLog);
            }
        }
    }
}