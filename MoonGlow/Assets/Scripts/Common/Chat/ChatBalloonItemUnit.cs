﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class ChatBalloonItemUnit : MonoBehaviour
{
    private Sequence _textSequence, _emoticonSequence;
    [SerializeField]
    private CanvasGroup _canvas;
    [SerializeField]
    private RectTransform _baseSize;
    [SerializeField]
    private Text _text;
    [SerializeField]
    private Image _originImage;
    [SerializeField]
    private Image _emoticon;
    private int _iDx, _i;

    public float padding = 0.1f;
    public float minwidth = 40f;
    public bool leftAlignment = true;

    void Awake()
    {
        _textSequence = DOTween.Sequence();
        _textSequence.Append(_canvas.DOFade(1f, 0f));
        _textSequence.AppendInterval(3f);
        _textSequence.Append(_canvas.DOFade(0f, 1f));
        _textSequence.SetAutoKill(false).Pause();

        _emoticonSequence = DOTween.Sequence();
        _emoticonSequence.Append(_emoticon.DOFade(1f, 0f));
        _emoticonSequence.AppendInterval(2f);
        _emoticonSequence.Append(_emoticon.DOFade(0f, 1f));
        _emoticonSequence.AppendCallback(CancelInvoke);
        _emoticonSequence.SetAutoKill(false).Pause();

        ChangeAlignment();
    }

    void ChangeAlignment()
    {
        _originImage.rectTransform.anchoredPosition = new Vector3(leftAlignment ? -42f : 42f, -36.5f, 0f);
        _originImage.rectTransform.localScale = new Vector3(leftAlignment ? -1f : 1f, 1f, 1f);
        _emoticon.rectTransform.anchoredPosition = new Vector3(leftAlignment ? 120f : -120f, 10f, 1f);
        _emoticon.rectTransform.localScale = new Vector3(leftAlignment ? -1f : 1f, 1f, 1f);
    }

    IEnumerator BalloonText(string msg)
    {
        _text.text = msg;
        yield return new WaitForEndOfFrame();
        float width = _text.rectTransform.rect.width + padding * 2;
        if (width < minwidth) { width = minwidth; }

        _baseSize.sizeDelta = new Vector2(width, _text.rectTransform.rect.height + padding * 2);
    }

    public void ShowText(bool state, string msg = null)
    {
        if (state)
        {
            StartCoroutine(BalloonText(msg));
            _textSequence.Restart();
        }
        else { _canvas.alpha = 0f; }
    }

    //JOY, SAD, SURPRISE, ANGER, FEAR, FRUSTRATE, PRAISE, BANTER
    public void ShowEmoticon(bool state, int iDx = 0)
    {
        if (state)
        {
            _iDx = iDx;
            _i = 0;
            CancelInvoke();
            //_emoticon.enabled = true;
            InvokeRepeating("StartEmoticon", 0f, 0.05f);
            _emoticonSequence.Restart();
        }
        else { _canvas.alpha = 0f; }
    }

    void StartEmoticon()
    {
        if (_i == 15)
        {
            _i = 0;
            //CancelInvoke();
            //_emoticon.enabled = false;
        }
        _emoticon.sprite = PlaySpriteFactory.Instance.Get(SpriteType.Emoticon, (_iDx % 100 - 1) * 16 + _i);
        _i++;
    }
}