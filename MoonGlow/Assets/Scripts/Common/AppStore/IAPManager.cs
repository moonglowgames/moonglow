﻿#if UNITY_ANDROID || UNITY_IOS

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public enum IAPItem { NONE = 0, MOONRUBY_KR_5 = 401, MOONRUBY_KR_10 = 402, MOONRUBY_KR_20 = 403, MOONRUBY_KR_30 = 404, MOONRUBY_KR_50 = 405, MOONRUBY_KR_100 = 406 };

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;                                                                  // Reference to the Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider;                                                         // Reference to store-specific Purchasing subsystems.
    //private string _iapGuid = string.Empty;
    // Product identifiers for all products capable of being purchased: "convenience" general identifiers for use with Purchasing, and their store-specific identifier counterparts 
    // for use with and outside of Unity Purchasing. Define store-specific identifiers also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    private static string kProductIDConsumable_moonruby_kr_5 = "moonruby_kr_5";                                                         // General handle for the consumable product.
    private static string kProductIDConsumable_moonruby_kr_10 = "moonruby_kr_10";
    private static string kProductIDConsumable_moonruby_kr_20 = "moonruby_kr_20";
    private static string kProductIDConsumable_moonruby_kr_30 = "moonruby_kr_30";
    private static string kProductIDConsumable_moonruby_kr_50 = "moonruby_kr_50";
    private static string kProductIDConsumable_moonruby_kr_100 = "moonruby_kr_100";

    // Apple App Store identifier for the consumable product.
    private static string kProductNameAppleConsumable_moonruby_kr_5 = "com.moonglow.moonruby_kr_5";
    private static string kProductNameAppleConsumable_moonruby_kr_10 = "com.moonglow.moonruby_kr_10";
    private static string kProductNameAppleConsumable_moonruby_kr_20 = "com.moonglow.moonruby_kr_20";
    private static string kProductNameAppleConsumable_moonruby_kr_30 = "com.moonglow.moonruby_kr_30";
    private static string kProductNameAppleConsumable_moonruby_kr_50 = "com.moonglow.moonruby_kr_50";
    private static string kProductNameAppleConsumable_moonruby_kr_100 = "com.moonglow.moonruby_kr_100";

    // Google Play Store identifier for the consumable product.
    private static string kProductNameGooglePlayConsumable_moonruby_kr_5 = "moonruby_kr_5";
    private static string kProductNameGooglePlayConsumable_moonruby_kr_10 = "moonruby_kr_10";
    private static string kProductNameGooglePlayConsumable_moonruby_kr_20 = "moonruby_kr_20";
    private static string kProductNameGooglePlayConsumable_moonruby_kr_30 = "moonruby_kr_30";
    private static string kProductNameGooglePlayConsumable_moonruby_kr_50 = "moonruby_kr_50";
    private static string kProductNameGooglePlayConsumable_moonruby_kr_100 = "moonruby_kr_100";

    void Awake()
    {
        GameControlUnit.Instance.Iap = this;
    }

    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
        
        // try to recover from failed purchase.
        if (EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.iapInfoResult))
        {
            string[] args = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.iapInfoResult).Split('\n');
            PacketManager.Instance.SendCSReqIAPResult(args[0], (IAPResult)Enum.Parse(typeof(IAPResult), args[1]));
        }
        if (EncryptedPlayerPrefs.HasKey(PlayerPrefsKey.iapInfoBuy))
        {
            string[] args = EncryptedPlayerPrefs.GetString(PlayerPrefsKey.iapInfoBuy).Split('\n');
            PacketManager.Instance.SendCSReqIAPResult(args[0], IAPResult.Fail);
        }
    }

    private void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiPzWb1D2awhZigzwF0ksHHsu7g1w0ZVEhngkUOlTY7qAdCiyBlezpb+Y7vHtr4YMHAV2Q7wAw8nFw9Xnh0tFNLdLkujIqDjpa8ex0ftAfpZV9rHP86XswbCXeeS/bCZFjB1bq6Z1VC6o00zzXcRrW3IDb+QYqE3TBkTQdOSp5EHEycgtF4h2FCIFLw2Rmj+R0YakbPQzTGNYi79l3c2FDSlSR/2vE/vcvppwN3qCe/+XKqFil1m0TDoAYjmkhAGPeHVRrX1BAPefUWihbVWW2VeMayISha9fo0WjV9e+REho7pCSnD7s/T1I/6M8po9iaG+Ps+s85b3dwZ6wN+SJ+wIDAQAB");
        // Add a product to sell / restore by way of its identifier, associating the general identifier with its store-specific identifiers.
        builder.AddProduct(kProductIDConsumable_moonruby_kr_5, ProductType.Consumable, new IDs() { { kProductNameAppleConsumable_moonruby_kr_5, AppleAppStore.Name }, { kProductNameGooglePlayConsumable_moonruby_kr_5, GooglePlay.Name }, });
        builder.AddProduct(kProductIDConsumable_moonruby_kr_10, ProductType.Consumable, new IDs() { { kProductNameAppleConsumable_moonruby_kr_10, AppleAppStore.Name }, { kProductNameGooglePlayConsumable_moonruby_kr_10, GooglePlay.Name }, });
        builder.AddProduct(kProductIDConsumable_moonruby_kr_20, ProductType.Consumable, new IDs() { { kProductNameAppleConsumable_moonruby_kr_20, AppleAppStore.Name }, { kProductNameGooglePlayConsumable_moonruby_kr_20, GooglePlay.Name }, });
        builder.AddProduct(kProductIDConsumable_moonruby_kr_30, ProductType.Consumable, new IDs() { { kProductNameAppleConsumable_moonruby_kr_30, AppleAppStore.Name }, { kProductNameGooglePlayConsumable_moonruby_kr_30, GooglePlay.Name }, });
        builder.AddProduct(kProductIDConsumable_moonruby_kr_50, ProductType.Consumable, new IDs() { { kProductNameAppleConsumable_moonruby_kr_50, AppleAppStore.Name }, { kProductNameGooglePlayConsumable_moonruby_kr_50, GooglePlay.Name }, });
        builder.AddProduct(kProductIDConsumable_moonruby_kr_100, ProductType.Consumable, new IDs() { { kProductNameAppleConsumable_moonruby_kr_100, AppleAppStore.Name }, { kProductNameGooglePlayConsumable_moonruby_kr_100, GooglePlay.Name }, });
        //builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){{ kProductNameAppleSubscription,       AppleAppStore.Name },{ kProductNameGooglePlaySubscription,  GooglePlay.Name },});// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyConsumable(string guid, int elementIdx)
    {
        // Buy the consumable product using its general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
        //_iapGuid 
         EncryptedPlayerPrefs.SetString("SHOP_GUID",guid);
        PlayerPrefs.Save();

        switch ((IAPItem)elementIdx)
        {
            case IAPItem.MOONRUBY_KR_5:
                BuyProductID(kProductIDConsumable_moonruby_kr_5);
                break;
            case IAPItem.MOONRUBY_KR_10:
                BuyProductID(kProductIDConsumable_moonruby_kr_10);
                break;
            case IAPItem.MOONRUBY_KR_20:
                BuyProductID(kProductIDConsumable_moonruby_kr_20);
                break;
            case IAPItem.MOONRUBY_KR_30:
                BuyProductID(kProductIDConsumable_moonruby_kr_30);
                break;
            case IAPItem.MOONRUBY_KR_50:
                BuyProductID(kProductIDConsumable_moonruby_kr_50);
                break;
            case IAPItem.MOONRUBY_KR_100:
                BuyProductID(kProductIDConsumable_moonruby_kr_100);
                break;
            default:
                Debug.LogError("Is Not Consumable type: " + elementIdx.ToString());
                break;
        }
    }

    //    public void BuyNonConsumable()
    //    {
    //        // Buy the non-consumable product using its general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
    //        BuyProductID(kProductIDNonConsumable);
    //    }
    //
    //
    //    public void BuySubscription()
    //    {
    //        // Buy the subscription product using its the general identifier. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
    //        BuyProductID(kProductIDSubscription);
    //    }
    //

    void BuyProductID(string productId)
    {
        // If the stores throw an unexpected exception, use try..catch to protect my logic here.
        try
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing system's products collection.
                Product product = m_StoreController.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }
        catch (Exception e)
        {
            // ... by reporting any unexpected exception for later diagnosis.
            Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
        }
    }

    // Restore purchases previously made by this customer. Some platforms automatically restore purchases. Apple currently requires explicit purchase restoration for IAP.
    private void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("IAP OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        string shopGuid = EncryptedPlayerPrefs.GetString("SHOP_GUID");

        // A consumable product has been purchased by this user.
        if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable_moonruby_kr_5, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.OK);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable_moonruby_kr_10, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.OK);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable_moonruby_kr_20, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.OK);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable_moonruby_kr_30, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.OK);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable_moonruby_kr_50, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.OK);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable_moonruby_kr_100, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));//If the consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.OK);
        }
        // Or ... a non-consumable product has been purchased by this user.
        //        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
        //        {
        //            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));}// Or ... a subscription product has been purchased by this user.
        //        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
        //        {
        //            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));}// Or ... an unknown product has been purchased by this user. Fill in additional products here.
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.Fail);
        }// Return a flag indicating wither this product has completely been received, or if the application needs to be reminded of this purchase at next app launch. Is useful when saving purchased products to the cloud, and when that save is delayed.
        
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing this reason with the user.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));

        string shopGuid = EncryptedPlayerPrefs.GetString("SHOP_GUID");

        switch (failureReason)
        {
            case PurchaseFailureReason.UserCancelled:
                PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.Cancel);
                break;
            default:
                PacketManager.Instance.SendCSReqIAPResult(shopGuid, IAPResult.Fail);
                break;
        }
    }
}

#endif