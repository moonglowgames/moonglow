﻿#if UNITY_ANDROID || UNITY_IOS

using UnityEngine;
using UnityEngine.Advertisements;
using System;
using System.Collections;

public class AdManager : MonoBehaviour
{
    [SerializeField]
    string gameID = "1066248";

    private string _guid;

    void Awake()
    {
        Advertisement.Initialize(gameID, true);
        GameControlUnit.Instance.Ad = this;
    }

    public void OnRequestShowAD()
    {
        // send to server requestShowAD or buyitem....
    }

    public void OnResponseShowAD(bool isOK, string guid, int todayRemainCount, string remainTime)
    {
        if (isOK)
        {
            ShowAd("");
            _guid = guid;
        }
        else
        {
            if (todayRemainCount == 0)
            {
                GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "AD_LIMIT");
                return;
            }
            GlobalMessageUnit.Instance.ShowMessage(null, string.Empty, null, "AD_LIMIT_TIME", remainTime.ToString());
        }
        // display todayRemainCount , remainTime , fail message
    }

    void OnFinishAD()
    {
        // send to serer requestFinishAD with response "guid"
        PacketManager.Instance.SendCSNotiFinishAD(_guid);
    }

    public void GetReward(int itemIdx)
    {
        switch (itemIdx)
        {
            case 2:
                var goldObject = (GameObject)Instantiate(Resources.Load("Effects/Acquisition/EffCoinReward"));
                goldObject.GetComponent<EffectUnit>().Animate();
                break;
            case 3:
                var rubyObject = (GameObject)Instantiate(Resources.Load("Effects/Acquisition/EffRubyReward"));
                rubyObject.GetComponent<EffectUnit>().Animate();
                break;
        }
    }

    public void ShowAd(string zone = "")
    {
#if UNITY_EDITOR
        StartCoroutine(WaitForAd());
#endif

        if (string.Equals(zone, "")) { zone = null; }

        ShowOptions options = new ShowOptions();
        options.resultCallback = AdCallbackhandler;


        if (Advertisement.IsReady(zone))
        {
            Advertisement.Show(zone, options);
        }
    }

    void AdCallbackhandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("Ad Finished. Rewarding player...");
                // Send To Server
                OnFinishAD();
                break;
            case ShowResult.Skipped:
                Debug.Log("Ad skipped. Son, I am dissapointed in you");
                break;
            case ShowResult.Failed:
                Debug.Log("I swear this has never happened to me before");
                break;
        }
    }

    IEnumerator WaitForAd()
    {
        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        yield return null;

        while (Advertisement.isShowing)
        {
            yield return null;
        }

        Time.timeScale = currentTimeScale;
    }
}

#endif