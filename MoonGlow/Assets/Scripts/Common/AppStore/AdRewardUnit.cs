﻿using UnityEngine;
using DG.Tweening;

public class AdRewardUnit : MonoBehaviour
{
    void Start()
    {
        Sequence  _moveSequence = DOTween.Sequence();
        //_emoticonSequence.Append(transform.DOPunchPosition(new Vector3(-1f, -1f), 2f));
        //_emoticonSequence.AppendInterval(0.2f);
        _moveSequence.Append(transform.DOMove(new Vector3(1.25f, -0.75f), 1f).SetRelative(true));
        _moveSequence.Append(transform.DOMove(new Vector3(3f, 8.75f), 3f).SetRelative(true));
        _moveSequence.AppendCallback(()=>Destroy(this.gameObject));
    }
}