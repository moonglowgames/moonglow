﻿public enum SpriteType
{
    Item,
    CharacterAvatar,
    CharacterAvatarMini,
    CharacterAvatarMax,
    JokerFace,
    ActiveFrame,
    CardArt,
    MiniCardBody,
    CardBody,
    CardSkin,
    CardSkinMini,
    PriceTag,
    PlayBg,
    GameType,
    MenuControl,
    Rank,
    RankChannel,
    Reward,
    ShopItem,
    ShopPriceType,
    ShopJoker,
    ShopGold,
    ShopRuby,
    Pet,
    PetMini,
    Badge,
    Emoticon,
    MyPageActiveItem
}