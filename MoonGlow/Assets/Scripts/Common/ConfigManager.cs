﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public enum configKey{LoginServerIP, LoginServerPort}

public class ConfigManager : Singleton<ConfigManager>
{
    private const string _FILE = @"Config/config";
    private Dictionary<string, string> _configData = new Dictionary<string, string>();
    private bool _isLoaded = false;

    void Awake()
    {
        if (_isLoaded == false) { LoadFromFile(); }
    }

    void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        char[] charsToTrim = { ' ', '\t' };

        Debug.Log("LOADED CONFIG FILE: " + file.name);
        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);

            if (ln == string.Empty)
            {
                // ignore blank line
            }
            else if (ln[0] == ';')
            {
                // ignore comment line
            }
            else
            {
                string[] configData = ln.Split('=');
                _configData.Add(configData[0].Trim(charsToTrim), configData[1].Trim(charsToTrim));
            }
        }
        _isLoaded = true;
    }


    public string Get(configKey key)
    {
        if (_isLoaded == false) { LoadFromFile(); }

        string retvalue = string.Empty;
        string tmpKey = key.ToString();

        if (_configData.ContainsKey(tmpKey))
        {
            retvalue = _configData[tmpKey];
            //Debug.Log(retvalue);
        }
        else { Debug.LogError("No key found :(" + key + ")"); }

        return retvalue;
    }
}