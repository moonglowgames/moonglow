﻿using System;

public class WinCondition
{
    public int iDx;
    public string gameType;
    public int goalHP;
    public int goalResource;
    public int moonDust;
    public int moonStone;
    public int mana;
    public int zenStone;
    public int rodEnergy;
    public int moonCrystal;
    public int startHP;
    public int startSP;

    const int DECODE_PARSE_COUNT = 12;
    public char[] charsToTrim = { ' ', '\t' };

    public bool Decode(string cvsLineData_)
    {
        bool retvalue = false;

        if (cvsLineData_ != string.Empty)
        {
            cvsLineData_.Trim(charsToTrim);

            switch (cvsLineData_[0])
            {
                case ';':
                    break;
                default:
                    string[] parsings = cvsLineData_.Split(',');
                    int parscount = 0;
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        try
                        {
                            this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.gameType = parsings[parscount++].Trim(charsToTrim);
                            this.goalHP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.goalResource = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonDust = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonStone = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.mana = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.zenStone = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.rodEnergy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonCrystal = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.startHP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.startSP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            retvalue = true;
                        }
                        catch (Exception e)
                        {
                            System.Console.Write(e.Message);
                            retvalue = false;
                        }
                    }
                    break;
            }
        }

        return retvalue;
    }

    public override string ToString()
    {
        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 1))
        {
            case Language.English:
                return "Reach " + goalHP.ToString() + " HP or" + System.Environment.NewLine + "accumulate " + goalResource + " of each resource first.";
            case Language.Korean:
                return "HP가 " + goalHP.ToString() + " 에 도달하거나" + System.Environment.NewLine + "모든리소스가 " + goalResource + " 이상이면 승리.";
            default:
                return string.Empty;
        }
        //returnString += "Or reduce enemy Hit Point down to 0.";
    }
}