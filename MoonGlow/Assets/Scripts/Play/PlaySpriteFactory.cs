﻿using UnityEngine;
using System;

public class PlaySpriteFactory : Singleton<PlaySpriteFactory>
{
    private Sprite _errorSprite;
    private Sprite[] _gameTypeArt;
    private Sprite[] _cardArt;
    private Sprite[] _cardArtJoker;
    private Sprite[] _cardBody;
    private Sprite[] _priceTag;
    private Sprite[] _petStickers;
    private Sprite[] _emoticons;
    private Sprite _goldItem;
    private Sprite _rubyItem;
    private const int CARD_COUNT = 96;

    void Awake()
    {
        _errorSprite = Resources.Load<Sprite>("Images/Error");
        _gameTypeArt = Resources.LoadAll<Sprite>("Images/playTimer");
        _cardArt = Resources.LoadAll<Sprite>("Images/Card/CardArtSum");
        //_cardArt = new Sprite[CARD_COUNT];
        _cardArtJoker = Resources.LoadAll<Sprite>("Images/Card/CardArtJoker");
        _cardBody = Resources.LoadAll<Sprite>("Images/Card/CardBody");
        _priceTag = Resources.LoadAll<Sprite>("Images/Card/PriceTag");
        _petStickers = Resources.LoadAll<Sprite>("Images/PetStickers");
        _emoticons = Resources.LoadAll<Sprite>("Images/Emoticons");
        _goldItem = Resources.Load<Sprite>("Images/Card/Item/playCoin");
        _rubyItem = Resources.Load<Sprite>("Images/Card/Item/playRuby");
    }

    public Sprite Get(SpriteType type, int iDx)
    {
        switch (type)
        {
            case SpriteType.PlayBg:
                return GetPlayBgSprite(iDx);
            case SpriteType.GameType:
                return GetGameTypeSprite(iDx);
            case SpriteType.CardArt:
                return GetCardArtSprite(iDx);
            case SpriteType.JokerFace:
                return GetCardArtJokerSprite(iDx);
            case SpriteType.CardBody:
                return GetCardBodySprite(iDx);
            case SpriteType.PriceTag:
                return GetPriceTagSprite(iDx);
            case SpriteType.PetMini:
                return GetPetStickerSprite(iDx);
            case SpriteType.Emoticon:
                return GetEmoticonSprite(iDx);
            case SpriteType.ShopGold:
                return _goldItem;
            case SpriteType.ShopRuby:
                return _rubyItem;
            default:
                Debug.LogError("Sprite type mismatch SpriteFactory.cs");
                return null;
        }
    }

    Sprite GetPlayBgSprite(int iDx)
    {
        //enum GameType { Unknown = 0, All = 1, Colmaret = 2, Joha = 3, Cheshire = 4, Zatoo = 5, Armdree = 6, Tarassen = 7, Krazan = 8, Yanan = 9 }
        return Resources.Load<Sprite>(string.Format("Images/PlayBg/{0:00}{1}", iDx, Enum.GetName(typeof(GameType), iDx)));
    }

    Sprite GetGameTypeSprite(int iDx)
    {
        //enum GameType {Unknown = 0, All = 1, Mercury = 2, Venus = 3, Earth = 4, Mars = 5, Jupiter = 6, Saturn = 7, Uranus = 8, Neptune = 9}
        iDx -= 2;
        if (iDx >= 0 && iDx < _gameTypeArt.Length)
        {
            return _gameTypeArt[iDx];
        }
        else
        {
            Debug.LogError("Sprite game type index outside boundaries " + iDx.ToString() + " PlaySpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCardArtSprite(int iDx)
    {
        iDx--;
        if (iDx >= 0 && iDx < _cardArt.Length)
        {
            return _cardArt[iDx];
        }
        else
        {
            Debug.LogError("Sprite art index outside boundaries " + iDx.ToString() + " PlaySpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCardArtJokerSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _cardArtJoker.Length)
        {
            return _cardArtJoker[iDx];
        }
        else
        {
            Debug.LogError("Joker avatar index outside boundaries " + iDx.ToString() + " ItemSpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetCardBodySprite(int iDx)
    {
        if (iDx >= 0 && iDx < _cardBody.Length)
        {
            return _cardBody[iDx];
        }
        else
        {
            Debug.LogError("Sprite body index outside boundaries " + iDx.ToString() + " SpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetPriceTagSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _priceTag.Length)
        {
            return _priceTag[iDx];
        }
        else
        {
            Debug.LogError("Sprite price tag index outside boundaries " + iDx.ToString() + " SpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetPetStickerSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _petStickers.Length)
        {
            return _petStickers[iDx];
        }
        else
        {
            Debug.LogError("Pet sticker index outside boundaries " + iDx.ToString() + " SpriteFactory.cs");
            return _errorSprite;
        }
    }

    Sprite GetEmoticonSprite(int iDx)
    {
        if (iDx >= 0 && iDx < _emoticons.Length)
        {
            return _emoticons[iDx];
        }
        else
        {
            Debug.LogError("Sprite price tag index outside boundaries " + iDx.ToString() + " SpriteFactory.cs");
            return _errorSprite;
        }
    }
}