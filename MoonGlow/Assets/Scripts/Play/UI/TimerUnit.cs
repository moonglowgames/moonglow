﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class TimerUnit : MonoBehaviour
{
    [SerializeField]
    private Text _time;

    private float _timeRemaining;

    public float timeInitial;

    void Awake()
    {
        GameControlUnit.Instance.Timer = this;
    }

    void Start()
    {
        _time.text = timeInitial.ToString("00");
    }

    void Tick()
    {
        //		Debug.Log ("Tick" +timeRemaining.ToString() );
        _timeRemaining--;
        _time.text = _timeRemaining.ToString("00");
        if (_timeRemaining < 0.1f)
        {
            CancelInvoke();
            TurnOver();
            return;
        }
        if (_timeRemaining < 5)
        {
            SoundControlUnit.Instance.PlaySceneClip(PlayClip.TimeAlert);
            return;
        }
        if (_timeRemaining == 5)
        {
            SoundControlUnit.Instance.PlaySceneClip(PlayClip.TimeAlert);
            _time.color = Color.red;
        }
    }

    void Reset()
    {
        _timeRemaining = timeInitial;
        _time.color = new Color(0.976f, 0.898f, 0.812f);
        StartTimer();
    }

    void TurnOver()
    {
        SoundControlUnit.Instance.PlaySceneClip(PlayClip.TimeOut);
        Debug.Log("TurnOver");
    }

    public void StartTimer()
    {
        Debug.Log("Start Timer");
        CancelInvoke();
        _timeRemaining = timeInitial;
        _time.color = new Color(0.976f, 0.898f, 0.812f);
        InvokeRepeating("Tick", 0f, 1f);
    }

    public void StopTimer()
    {
        CancelInvoke();
    }
}