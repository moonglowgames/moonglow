﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class PlaySettingsUnit : Singleton<PlaySettingsUnit>
{
    [SerializeField]
    private CanvasGroup _canvas;
    [SerializeField]
    private Toggle _sound, _music, _vibration;

    void Start()
    {
        _sound.isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.sound, 1) == 1;
        _music.isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.music, 1) == 1;
        _vibration.isOn = EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.vibration, 1) == 1;
    }

    public void ToggleWindow(bool state)
    {
        _canvas.alpha = state ? 1f : 0f;
        _canvas.blocksRaycasts = state;
        SoundControlUnit.Instance.PlayUiClip(UiClip.UseButton);
    }

    public void SoundOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.sound, isOn ? 1 : 0);
        SoundControlUnit.Instance.isOn = isOn;
    }

    public void MusicOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.music, isOn ? 1 : 0);
        MusicControlUnit.Instance.isOn = isOn;
        if (isOn)
        {
            MusicControlUnit.Instance.Play();
        }
        else
        {
            MusicControlUnit.Instance.Stop();
        }
    }

    public void VibrationOption(bool isOn)
    {
        EncryptedPlayerPrefs.SetInt(PlayerPrefsKey.vibration, isOn ? 1 : 0);
    }
}