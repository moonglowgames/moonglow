﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryItemUnit : MonoBehaviour
{
    private Text _name;
    private Image _image;
    private Text _price;
    private Image _priceType;
    private Text _description;

    public bool needUpdate = true;
    public int iDx;
    public int elementIDx;
    public double price;
    public PayType payType;

    void Awake()
    {
        _name = transform.FindChild("Description/Name").GetComponent<Text>();
        _image = transform.FindChild("Image").GetComponent<Image>();
        _price = transform.FindChild("Price/Amount").GetComponent<Text>();
        _priceType = transform.FindChild("Price/Image").GetComponent<Image>();
        _description = transform.FindChild("Description/Description").GetComponent<Text>();
    }

    void Update()
    {
        if (needUpdate)
        {
            needUpdate = false;
            UpdateView();
        }
    }

    void UpdateView()
    {
        ItemElementInfo itemInfo = ItemManager.Instance.Get(elementIDx);
        if (itemInfo == null)
        {
            Debug.Log("ItemInfo is null ShopUnit.cs");
            return;
        }

        //elementIDx = itemInfo.elementIDx;
        _name.text = itemInfo.name;
        _price.text = price.ToString();
        _description.text = itemInfo.description;

        if (ShopSpriteFactory.Instance != null)
        {

            _priceType.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopPriceType, (int)itemInfo.payType);
            _image.sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, elementIDx % 100 - 1);
            _image.enabled = true;
        }
        else { Debug.Log("ShopSpriteFactory is null MiniCardUnit.cs"); }
    }

    public void BuyAction()
    {
        PacketManager.Instance.SendCSReqBuyElement(elementIDx, 1);
    }
}