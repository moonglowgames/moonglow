﻿using UnityEngine;

public class InventoryUnit : MonoBehaviour
{
    private CanvasGroup _canvas;

    void Awake()
    {
        GameControlUnit.Instance.Inventory = this;
        _canvas = GetComponent<CanvasGroup>();
    }

    public void ToggleWindow(bool state)
    {
        _canvas.alpha = state ? 1f : 0f;
        _canvas.blocksRaycasts = state;
    }
}