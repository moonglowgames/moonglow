﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class RoomManagerUnit : Singleton<RoomManagerUnit>
{
    [SerializeField]
    private Image _gameType, _playBg, _helpEn, _helpKr;
    [SerializeField]
    private Text _goalHP, _goalRP, _stake, _chat;
    private PlayerProfileUnit _playerProfileUnit;

    public PlayerProfileUnit profilePrefab;

    public DropArea userPlayer;
    public DropArea deck;
    public DropArea[] enemies;

    public TargetMap Map { get; set; }

    public bool GameStart { get; set; }

    void OnLevelWasLoaded(int level)
    {
        if (level == 2)
        {
            Map = new TargetMap(GameControlUnit.Instance.RoomInfo.playMode);

            switch (GameControlUnit.Instance.RoomInfo.playTime)
            {
                case PlayTime.Sec_30:
                    GameControlUnit.Instance.Timer.timeInitial = 30;
                    break;
                case PlayTime.Sec_60:
                    GameControlUnit.Instance.Timer.timeInitial = 60;
                    break;
                default:
                    GameControlUnit.Instance.Timer.timeInitial = 30;
                    break;
            }

            LocalizationManager.Instance.ChangeLanguage();
            SoundControlUnit.Instance.PlayUiClip(UiClip.Complete);
        }
    }

    void Start()
    {
        GameStart = false;
        _goalHP.text = GameControlUnit.Instance.RoomInfo.goalHP.ToString();
        _goalRP.text = GameControlUnit.Instance.RoomInfo.goalRP.ToString();
        _stake.text = GameControlUnit.Instance.RoomInfo.stake.ToString();
        _gameType.sprite = PlaySpriteFactory.Instance.Get(SpriteType.GameType, (int)GameControlUnit.Instance.RoomInfo.gameType);
        _playBg.sprite = PlaySpriteFactory.Instance.Get(SpriteType.PlayBg, (int)GameControlUnit.Instance.RoomInfo.gameType);
    }

    void ClearChat()
    {
        _chat.text = string.Empty;
    }

    public void ChatEntry(string msg)
    {
        CancelInvoke("ClearChat");
        _chat.text = msg;
        Invoke("ClearChat", 10f);
    }

    public void HelpToggle(bool state)
    {
        switch ((Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0))
        {
            case Language.English:
                _helpEn.gameObject.SetActive(state);
                break;    
            case Language.Korean:
                _helpKr.gameObject.SetActive(state);
                break;
            default:
                _helpEn.gameObject.SetActive(state);
                break;
        }
        SoundControlUnit.Instance.PlayUiClip(UiClip.SoftButton);
    }

    public void ShowPlayerProfile(PlayerProfile profile)
    {
        if (_playerProfileUnit == null)
        {
            _playerProfileUnit = Instantiate(profilePrefab);
            _playerProfileUnit.transform.SetParent(_playBg.transform.parent, false);
        }
        _playerProfileUnit.UpdateView(profile);
        _playerProfileUnit.ToggleWindow(true);
    }
}