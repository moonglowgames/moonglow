﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;

public class HandManagerUnit : MonoBehaviour
{
    [SerializeField]
    private Transform[] _toggles;
    private Vector3 upsideDown = new Vector3(1f,-1f,1f);

    void Awake()
    {
        GameControlUnit.Instance.HandManager = this;
        ToggleState(false);
    }

    public void ToggleState(bool state)
    {
        this.gameObject.SetActive(state);
    }

    public void OnClick(bool isOn)
    {
        for (int i = 0; i <= _toggles.GetUpperBound(0); i++)
        {
            _toggles[i].localScale = isOn ? upsideDown : Vector3.one;
        }
        GameControlUnit.Instance.Hand.Slide(isOn);
        //_image.sprite = isOn ? buttonOn : buttonOff;
        //pairToggle.isOn = isOn;
    }
}