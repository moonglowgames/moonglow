﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class CardManager : Singleton<CardManager>
{
    private Dictionary<int, CardInfo> _deck = new Dictionary<int, CardInfo>();
    private const string _FILE = @"Data/CardData";

    void Awake()
    {
        LoadFromFile();
    }

    void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        char[] charsToTrim = { ' ', '\t' };

        Debug.Log("LOADED CARD DATA FILE" + file.name);

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        CardInfo tempCardDataInfo = null;

        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);
            if (ln[0] != ';')
            {
                tempCardDataInfo = new CardInfo();
                if (tempCardDataInfo.Decode(ln)) { _deck[tempCardDataInfo.iDx] = tempCardDataInfo; }
            }
        }
    }

    public CardInfo Get(int iDx)
    {
        if (_deck.ContainsKey(iDx))
        {
            return _deck[iDx];
        }
        else
        {
            Debug.LogError("Didn't find Card Info: " + iDx.ToString());
            return null;
        }
    }
}