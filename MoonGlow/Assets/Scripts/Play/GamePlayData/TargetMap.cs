﻿public class TargetMap
{
    private int[,] _map;

    public int Shift { get; set; }

    public TargetMap(PlayMode mode)
    {
        Shift = -1;
        switch (mode)
        {
            case PlayMode.Single2:
                _map = new int[2, 2] { { 0, 1 }, { 1, 0 } };
                break;
            case PlayMode.Single3:
                _map = new int[3, 3] { { 0, 1, 2 }, { 2, 0, 1 }, { 1, 2, 0 } };
                break;
            case PlayMode.Single4:
                _map = new int[4, 4] { { 0, 1, 2, 3 }, { 3, 0, 1, 2 }, { 2, 3, 0, 1 }, { 1, 2, 3, 0 } };
                break;
            case PlayMode.Team:
                _map = new int[4, 4] { { 0, 1, 2, 3 }, { 3, 0, 1, 2 }, { 2, 3, 0, 1 }, { 1, 2, 3, 0 } };
                break;
            default:
                _map = new int[2, 2] { { 0, 1 }, { 1, 0 } };
                break;
        }
    }

    public int GetNumber(int i)
    {
        if (Shift == -1 || i < 0) { return 0; }
        return _map[Shift, i];
    }

    public int ToInt(PlayerPosition pos)
    {
        if (Shift == -1) { return 0; }
        for (int i = 0; i <= _map.GetUpperBound(1); i++)
        {
            if (_map[Shift, i] == (int)pos) { return i; }
        }
        return 0;
        //		return _map[Shift, (int)pos];
    }
}