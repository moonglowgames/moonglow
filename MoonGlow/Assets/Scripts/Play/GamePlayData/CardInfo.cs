﻿using System;

[Flags]
public enum AbilityToolTip { NONE = 0, DAMAGE = 1,  HP = 2, SP = 4, MSPOWER = 8, MOONDUST = 16, ZSPOWER = 32, MANA = 64, MCPOWER = 128, RODENERGY = 256 };

[Flags]
public enum TargetOption { DECK = 0, ME = 1, ENEMY = 2}

public class CardInfo
{
    public int iDx ;
	public string name;
	public int priceMD;
	public int priceRE;
	public int priceM;
	public int priceColor;
	public int special;
	public int myHP;
	public int HPEnemy;
	public int SP;
	public int SPEnemy;
	public int damage;
	public int damageMe;
	public int moonDust;
	public int moonStone;
	public int mana;
	public int zenStone;
	public int rodEnergy;
	public int moonCrystal;
	public int moonDustEnemy;
	public int moonStoneEnemy;
	public int manaEnemy;
	public int zenStoneEnemy;
	public int rodEnergyEnemy;
	public int moonCrystalEnemy;
	public int playAgain;
	public TargetOption target;
    public int art;
    public int pet;
    public string specialDescription;

	const int DECODE_PARSE_COUNT = 29;
	public char[] charsToTrim = { ' ','\t'};

    public bool Decode(string cvsLineData_)
    {
        bool retvalue = false;

        if (cvsLineData_ != string.Empty)
        {
            cvsLineData_.Trim(charsToTrim);

            switch (cvsLineData_[0])
            {
                case ';':
                    break;
                default:
                    string[] parsings = cvsLineData_.Split(',');
                    int parscount = 0;
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        try
                        {
                            this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            parscount++;
                            //this.name = parsings [parscount++].Trim(charsToTrim);
                            this.priceMD = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.priceRE = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.priceM = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.priceColor = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.special = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.myHP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.HPEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.SP = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.SPEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.damage = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.damageMe = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonDust = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonStone = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.mana = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.zenStone = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.rodEnergy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonCrystal = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonDustEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonStoneEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.manaEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.zenStoneEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.rodEnergyEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.moonCrystalEnemy = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.playAgain = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.target = (TargetOption)Enum.Parse(typeof(TargetOption), parsings[parscount++].Trim(charsToTrim));
                            this.art = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.pet = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            //this.specialDescription = parsings [parscount++].Trim(charsToTrim);
                            
                            this.name = CardLocalizationManager.Instance.GetName(this.iDx);
                            this.specialDescription = CardLocalizationManager.Instance.GetDescription(this.iDx);
                            this.specialDescription = this.specialDescription.Replace("@", Environment.NewLine);
                            this.specialDescription = this.specialDescription.Replace('^', ',');
                            retvalue = true;
                        }
                        catch (Exception e)
                        {
                            Console.Write(e.Message);
                            retvalue = false;
                        }
                    }
                    break;
            }
        }
        return retvalue;
    }
	
	public override string ToString ()
	{
		return string.Format (iDx.ToString() + "," + name);
	}

    public int GetPrice()
    {
        switch (priceColor)
        {
            case 0:
                return priceMD;
            case 1:
                return priceM;
            case 2:
                return priceRE;
            default:
                //Debug.Log("GetPrice mismatch price color CardInfo.cs");
                return 0;
        }
    }

	string CreateDescription(int amount, string type, bool secondLine)//, string color)
    {
        string amountStr = (amount > 0) ? CardLocalizationManager.Instance.GetDescription(2101).Replace("*", amount.ToString()) : CardLocalizationManager.Instance.GetDescription(2102).Replace("*", Math.Abs(amount).ToString());
        if (secondLine)
        { return Environment.NewLine + type.Replace("*", amountStr); }
        else
        { return type.Replace("*", amountStr); }
	}

	public string GetDescription()
    {
		string returnValue =string.Empty; //"normal" + iDx.ToString() + mana.ToString();
        bool secondLine = false;
        if (secondLine) { returnValue += Environment.NewLine; }
        //normal card
        if (special == 0)
        {
            if (damage != 0)
            {
                returnValue += CardLocalizationManager.Instance.GetDescription(2001).Replace("*", damage.ToString());
                secondLine = true;
            }
            if (HPEnemy != 0)
            {
                returnValue += CreateDescription(HPEnemy, CardLocalizationManager.Instance.GetDescription(2002), secondLine);
                secondLine = true;
            }
            if (SPEnemy != 0)
            {
                returnValue += CreateDescription(SPEnemy, CardLocalizationManager.Instance.GetDescription(2003), secondLine);
                secondLine = true;
            }
            if (damageMe != 0)
            {
                if (secondLine) { returnValue += Environment.NewLine; }
                returnValue += CardLocalizationManager.Instance.GetDescription(2004).Replace("*", damageMe.ToString());
                secondLine = true;
            }
            if (myHP != 0)
            {
                returnValue += CreateDescription(myHP, CardLocalizationManager.Instance.GetDescription(2005), secondLine);
                secondLine = true;
            }
            if (SP != 0)
            {
                returnValue += CreateDescription(SP, CardLocalizationManager.Instance.GetDescription(2006), secondLine);
                secondLine = true;
            }
            if (moonDustEnemy != 0)
            {
                returnValue += CreateDescription(moonDustEnemy, CardLocalizationManager.Instance.GetDescription(2007), secondLine);
                secondLine = true;
            }
            if (moonStoneEnemy != 0)
            {
                returnValue += CreateDescription(moonStoneEnemy, CardLocalizationManager.Instance.GetDescription(2008), secondLine);
                secondLine = true;
            }
            if (manaEnemy != 0)
            {
                returnValue += CreateDescription(manaEnemy, CardLocalizationManager.Instance.GetDescription(2009), secondLine);
                secondLine = true;
            }
            if (zenStoneEnemy != 0)
            {
                returnValue += CreateDescription(zenStoneEnemy, CardLocalizationManager.Instance.GetDescription(2010), secondLine);
                secondLine = true;
            }
            if (rodEnergyEnemy != 0)
            {
                returnValue += CreateDescription(rodEnergyEnemy, CardLocalizationManager.Instance.GetDescription(2011), secondLine);
                secondLine = true;
            }
            if (moonCrystalEnemy != 0)
            {
                returnValue += CreateDescription(moonCrystalEnemy, CardLocalizationManager.Instance.GetDescription(2012), secondLine);
                secondLine = true;
            }
            if (moonDust != 0)
            {
                returnValue += CreateDescription(moonDust, CardLocalizationManager.Instance.GetDescription(2013), secondLine);
                secondLine = true;
            }
            if (moonStone != 0)
            {
                returnValue += CreateDescription(moonStone, CardLocalizationManager.Instance.GetDescription(2014), secondLine);
                secondLine = true;
            }
            if (mana != 0)
            {
                returnValue += CreateDescription(mana, CardLocalizationManager.Instance.GetDescription(2015), secondLine);
                secondLine = true;
            }
            if (zenStone != 0)
            {
                returnValue += CreateDescription(zenStone, CardLocalizationManager.Instance.GetDescription(2016), secondLine);
                secondLine = true;
            }
            if (rodEnergy != 0)
            {
                returnValue += CreateDescription(rodEnergy, CardLocalizationManager.Instance.GetDescription(2017), secondLine);
                secondLine = true;
            }
            if (moonCrystal != 0)
            {
                returnValue += CreateDescription(moonCrystal, CardLocalizationManager.Instance.GetDescription(2018), secondLine);
                secondLine = true;
            }
            if (playAgain == 1)
            {
                if (secondLine) { returnValue += Environment.NewLine; }
                returnValue += CardLocalizationManager.Instance.GetDescription(2019);
            }
        }
        else { returnValue = specialDescription; }
		return returnValue;
	}

	public AbilityToolTip ToolTipMe
	{ 
		get
        {
            AbilityToolTip tip = AbilityToolTip.NONE;
            if (damageMe != 0) { tip |= AbilityToolTip.DAMAGE; }
            if (myHP != 0) { tip |= AbilityToolTip.HP; }
            if (SP != 0) { tip |= AbilityToolTip.SP; }
            if (moonStone != 0) { tip |= AbilityToolTip.MSPOWER; }
            if (moonDust != 0) { tip |= AbilityToolTip.MOONDUST; }
            if (zenStone != 0) { tip |= AbilityToolTip.ZSPOWER; }
            if (mana != 0) { tip |= AbilityToolTip.MANA; }
            if (moonCrystal != 0) { tip |= AbilityToolTip.MCPOWER; }
            if (rodEnergy != 0) { tip |= AbilityToolTip.RODENERGY; }
            return tip;
        }
	}

    public AbilityToolTip ToolTipEnemy
    {
        get
        {
            AbilityToolTip tip = AbilityToolTip.NONE;
            if (damage != 0) { tip |= AbilityToolTip.DAMAGE; }
            if (HPEnemy != 0) { tip |= AbilityToolTip.HP; }
            if (SPEnemy != 0) { tip |= AbilityToolTip.SP; }
            if (moonStoneEnemy != 0) { tip |= AbilityToolTip.MSPOWER; }
            if (moonDustEnemy != 0) { tip |= AbilityToolTip.MOONDUST; }
            if (zenStoneEnemy != 0) { tip |= AbilityToolTip.ZSPOWER; }
            if (manaEnemy != 0) { tip |= AbilityToolTip.MANA; }
            if (moonCrystalEnemy != 0) { tip |= AbilityToolTip.MCPOWER; }
            if (rodEnergyEnemy != 0) { tip |= AbilityToolTip.RODENERGY; }
            return tip;
        }
    }
}