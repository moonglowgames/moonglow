﻿using UnityEngine;
using System.Collections.Generic;

public enum CardPosition {Hand1, Hand2, Hand3, Hand4, Hand5, Hand6, Deck, Player1, Player2, Player3, Player4};

public class CardPositionManager : Singleton<CardPositionManager>
{
    private Dictionary<int, Vector3> _positions;
    private float shiftValue = 1.025f;

    void Awake()
    {
        _positions = new Dictionary<int, Vector3>(10);
        _positions.Add((int)CardPosition.Hand1, new Vector3(-shiftValue * 5, 0f, 0f));
        _positions.Add((int)CardPosition.Hand2, new Vector3(-shiftValue * 3, 0f, 0f));
        _positions.Add((int)CardPosition.Hand3, new Vector3(-shiftValue, 0f, 0f));
        _positions.Add((int)CardPosition.Hand4, new Vector3(shiftValue, 0f, 0f));
        _positions.Add((int)CardPosition.Hand5, new Vector3(shiftValue * 3, 0f, 0f));
        _positions.Add((int)CardPosition.Hand6, new Vector3(shiftValue * 5, 0f, 0f));
        _positions.Add((int)CardPosition.Deck, new Vector3(0f, 0.3f, 0f));
        _positions.Add((int)CardPosition.Player1, new Vector3(-5.6f, 2.1f, 0f));
        _positions.Add((int)CardPosition.Player2, new Vector3(5.6f, 2.1f, 0f));
        _positions.Add((int)CardPosition.Player3, new Vector3(5.6f, -0.75f, 0f));
        _positions.Add((int)CardPosition.Player4, new Vector3(-5.6f, -0.75f, 0f));
    }

    void Start()
    {
        UpdateUI(CameraUnit.Instance.Ratio);
    }

    public Vector3 GetPlayerPosition(int playerNumber)
    {
        switch (playerNumber)
        {
            case 0:
                return _positions[(int)CardPosition.Player1];
            case 1:
                return _positions[(int)CardPosition.Player2];
            case 2:
                return _positions[(int)CardPosition.Player3];
            case 3:
                return _positions[(int)CardPosition.Player4];
            default:
                return GetPosition(CardPosition.Deck);
        }
    }

    public Vector3 GetPosition(CardPosition pos)
    {
        return _positions[(int)pos];
    }

    public void UpdateUI(RatioType ratio)
    {
        Vector3 shiftUp;
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                shiftUp = new Vector3(0f, 0.3f, 0f);
                _positions[(int)CardPosition.Deck] += shiftUp;
                _positions[(int)CardPosition.Player1] += shiftUp;
                _positions[(int)CardPosition.Player2] += shiftUp;
                _positions[(int)CardPosition.Player3] += shiftUp;
                _positions[(int)CardPosition.Player4] += shiftUp;
                break;
            case RatioType.aPhone:
                shiftUp = new Vector3(0f, 0.5f, 0f);
                _positions[(int)CardPosition.Deck] += shiftUp;
                _positions[(int)CardPosition.Player1] += shiftUp;
                _positions[(int)CardPosition.Player2] += shiftUp;
                _positions[(int)CardPosition.Player3] += shiftUp;
                _positions[(int)CardPosition.Player4] += shiftUp;
                break;
            case RatioType.aPad:
                shiftUp = new Vector3(0f, 1f, 0f);
                _positions[(int)CardPosition.Deck] += shiftUp;
                _positions[(int)CardPosition.Player1] += shiftUp;
                _positions[(int)CardPosition.Player2] += shiftUp;
                _positions[(int)CardPosition.Player3] += shiftUp;
                _positions[(int)CardPosition.Player4] += shiftUp;
                break;
        }
    }
}