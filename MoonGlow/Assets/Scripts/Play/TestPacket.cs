using UnityEngine;
using System.Collections;

public class TestPacket : MonoBehaviour
{

    private bool state = false;

    public void ToggleTestPanel()
    {
        state = !state;
        this.gameObject.SetActive(state);
    }

    public void TestSCNotiPlayerList()
    {
        Debug.Log("Create SCNotiPlayerList packet");

        SCNotiPlayerList pck = new SCNotiPlayerList();
        //		pck.playerList.playerList.Add(new PlayerInfo(1, Random.Range(0,6), "First", Random.Range(0, 40), true));
        //		pck.playerList.playerList.Add(new PlayerInfo(2, Random.Range(0,6), "Second", Random.Range(0, 40), true));
        //		pck.playerList.playerList.Add(new PlayerInfo(3, Random.Range(0,6), "Third", Random.Range(0, 40), true));
        //		pck.playerList.playerList.Add(new PlayerInfo(4, Random.Range(0,6), "Fourth", Random.Range(0, 40), true));

        PacketManager.Instance.SendMe(pck);

        SCNotiStartGame pck2 = new SCNotiStartGame();
        PacketManager.Instance.SendMe(pck2);
    }

    public void TestSCNotiPlayerStatus()
    {
        Debug.Log("Create SCNotiPlayerStatus packet");

        SCNotiPlayerStatus pck = new SCNotiPlayerStatus();
        pck.playerNumber = Random.Range(0, 4); // or random ( 1 ~ 4 )
        pck.HP = Random.Range(0, 50);
        pck.SP = Random.Range(0, 20);
        pck.MD = Random.Range(0, 20);
        pck.MA = Random.Range(0, 20);
        pck.RE = Random.Range(0, 20);
        pck.MS = Random.Range(0, 10);
        pck.ZS = Random.Range(0, 10);
        pck.MC = Random.Range(0, 10);

        PacketManager.Instance.SendMe(pck);
    }

    public void TestSCNotiPlayerFocus()
    {
        Debug.Log("Create SCNotiPlayerFocus packet");

        SCNotiPlayerFocus pck = new SCNotiPlayerFocus();
        pck.playerNumber = Random.Range(0, 4); // or random ( 1 ~ 4 )

        PacketManager.Instance.SendMe(pck);
    }

    public void TestHand()
    {
        Debug.Log("Create SCNotiDecToHand packet");

        SCNotiDeckToHand pck = new SCNotiDeckToHand();
        pck.cardList.cardList.Add(new CardListInfo(Random.Range(1, 97), 0, 1));
        pck.cardList.cardList.Add(new CardListInfo(Random.Range(1, 97), 0, 2));
        pck.cardList.cardList.Add(new CardListInfo(Random.Range(1, 97), 0, 3));
        pck.cardList.cardList.Add(new CardListInfo(Random.Range(1, 97), 0, 4));
        pck.cardList.cardList.Add(new CardListInfo(Random.Range(1, 97), 0, 5));
        pck.cardList.cardList.Add(new CardListInfo(Random.Range(1, 97), 0, 6));

        PacketManager.Instance.SendMe(pck);
    }

    public void TestDeckInfo()
    {
    }

    public void TestEffect(int type)
    {
    }
}