﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class DragItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private CardUnit _cardUnit;
    [SerializeField]
    private Canvas _canvas;
    
    private bool isDragStart = false;
    private Camera actioncamera = null;
    private Vector3 _startPosition;

    public bool SuccessDrop { get; set; }

    void Awake()
    {
        actioncamera = Camera.main;
    }

    void HighlightDropAreas(bool state)
    {
        switch (_cardUnit.Target)
        {
            case TargetOption.ME:
                RoomManagerUnit.Instance.userPlayer.Highlight(state);
                break;
            case TargetOption.ENEMY:
                foreach (DropArea area in RoomManagerUnit.Instance.enemies)
                {
                    area.Highlight(state);
                }
                break;
            case TargetOption.ME | TargetOption.ENEMY:
                RoomManagerUnit.Instance.userPlayer.Highlight(state);
                foreach (DropArea area in RoomManagerUnit.Instance.enemies)
                {
                    area.Highlight(state);
                }
                break;
        }
        RoomManagerUnit.Instance.deck.Highlight(state);
    }

    #region IBeginDragHandler implementation
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (eventData.pointerEnter == null) { return; }

        isDragStart = true;
        _startPosition = actioncamera.ScreenToWorldPoint(eventData.pressPosition);
        _startPosition -= eventData.pointerEnter.transform.position;
        _startPosition.z = 0;

        _cardUnit.ToggleTrailParticles(true);

        _cardUnit.ToggleRaycasts(false);
        _canvas.sortingOrder = 100;

        GameControlUnit.Instance.Hand.Hide(true);

        HighlightDropAreas(true);

        RoomManagerUnit.Instance.userPlayer.ToggleToolTips(true, _cardUnit.ToolTipMe);
        foreach (DropArea area in RoomManagerUnit.Instance.enemies)
        {
            area.ToggleToolTips(true, _cardUnit.ToolTipEnemy);
        }

        SoundControlUnit.Instance.PlaySceneClip(PlayClip.CardDragStart);
        SuccessDrop = false;
    }

    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {
        if (isDragStart)
        {
            if (actioncamera == null) { Debug.Log("Camera is Null"); return; }
            if (eventData.pointerDrag == null) { Debug.Log(" pointer is Numm"); return; }

            Vector3 position = actioncamera.ScreenToWorldPoint(Input.mousePosition);
            position.z = eventData.pointerDrag.transform.position.z;
            eventData.pointerDrag.transform.position = (position - _startPosition);
        }
    }

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
    {
        isDragStart = false;

        _cardUnit.ToggleTrailParticles(false);

        GameControlUnit.Instance.Hand.Hide(false);

        _cardUnit.ToggleRaycasts(true);
        _canvas.sortingOrder = 0;
        HighlightDropAreas(false);

        RoomManagerUnit.Instance.userPlayer.ToggleToolTips(false);
        foreach (DropArea area in RoomManagerUnit.Instance.enemies)
        {
            area.ToggleToolTips(false);
        }

        if (SuccessDrop)
        {
            _cardUnit.HideIntoDeck();
        }
        else
        {
            _cardUnit.UpdatePosition();
            SoundControlUnit.Instance.PlaySceneClip(PlayClip.CardDragCancel);
        }
    }

    #endregion
}