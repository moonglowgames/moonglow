﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DiscardArea : DropArea
{
    #region IDropHandler implementation

    public override void OnDrop(PointerEventData eventData)
    {
        Debug.Log("!--Discarding");
        if (GameControlUnit.Instance.GameNotice.MyTurn)
        {
            GameControlUnit.Instance.GameNotice.MyTurn = false;
            int cardIdx = (int)eventData.pointerDrag.GetComponent<CardUnit>().cardPosition;
            GameControlUnit.Instance.Hand.LastCardIndex = cardIdx;
            PacketManager.Instance.SendCSReqUseCard(cardIdx + 1, 0);
            eventData.pointerDrag.GetComponent<DragItem>().SuccessDrop = true;
            SoundControlUnit.Instance.PlaySceneClip(PlayClip.CardDrop);
        }
    }

    #endregion

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                transform.position += new Vector3(0f, 0.3f, 0f);
                break;
            case RatioType.aPhone:
                transform.position += new Vector3(0f, 0.85f, 0f);
                break;
            case RatioType.aPad:
                transform.position += new Vector3(0f, 1f, 0f);
                break;
        }
    }
}