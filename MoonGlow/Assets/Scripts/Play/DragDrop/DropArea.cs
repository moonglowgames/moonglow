﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.EventSystems;

public class DropArea : MonoBehaviour, IDropHandler
{
    [SerializeField]
    private ParticleSystem _focusFrame;
    [SerializeField]
    private ParticleSystem[] _cardToolTips;

    public PlayerPosition playerPosition;

    public void Highlight(bool state)
    {
        if (_focusFrame != null)
        {
            if (state) { _focusFrame.Play(true); }
            else { _focusFrame.Stop(true); }
        }
    }

    public void ToggleToolTips(bool state, AbilityToolTip ability = AbilityToolTip.NONE)
    {
        // HP, SP, MSPOWER, MOONDUST, ZSPOWER, MANA, MCPOWER, RODENERGY
        if (state)
        {
            if ((ability & AbilityToolTip.DAMAGE) == AbilityToolTip.DAMAGE)
            {
                if (GetComponent<PlayerUnit>().shieldPoints == 0) { _cardToolTips[0].Play(); }
                else { _cardToolTips[1].Play(); }
            }
            if ((ability & AbilityToolTip.HP) == AbilityToolTip.HP) { _cardToolTips[0].Play(); }
            if ((ability & AbilityToolTip.SP) == AbilityToolTip.SP) { _cardToolTips[1].Play(); }
            if ((ability & AbilityToolTip.MSPOWER) == AbilityToolTip.MSPOWER) { _cardToolTips[2].Play(); }
            if ((ability & AbilityToolTip.MOONDUST) == AbilityToolTip.MOONDUST) { _cardToolTips[3].Play(); }
            if ((ability & AbilityToolTip.ZSPOWER) == AbilityToolTip.ZSPOWER) { _cardToolTips[4].Play(); }
            if ((ability & AbilityToolTip.MANA) == AbilityToolTip.MANA) { _cardToolTips[5].Play(); }
            if ((ability & AbilityToolTip.MCPOWER) == AbilityToolTip.MCPOWER) { _cardToolTips[6].Play(); }
            if ((ability & AbilityToolTip.RODENERGY) == AbilityToolTip.RODENERGY) { _cardToolTips[7].Play(); }
        }
        else
        {
            foreach (ParticleSystem toolTip in _cardToolTips) { toolTip.Stop(); }
        }
    }

    #region IDropHandler implementation

    public virtual void OnDrop(PointerEventData eventData)
    {
        Debug.Log("Drop  " + this.name + ":" + eventData.pointerDrag.name);

        if (GameControlUnit.Instance.GameNotice.MyTurn)
        {
            GameControlUnit.Instance.GameNotice.MyTurn = false;
            if (eventData.pointerDrag.GetComponent<CardUnit>().itemIdx > 0) { GameControlUnit.Instance.History.ItemCard = true; }
            int cardIdx = (int)eventData.pointerDrag.GetComponent<CardUnit>().cardPosition;
            GameControlUnit.Instance.Hand.LastCardIndex = cardIdx;
            PacketManager.Instance.SendCSReqUseCard(cardIdx + 1, RoomManagerUnit.Instance.Map.ToInt(playerPosition) + 1);
            eventData.pointerDrag.GetComponent<DragItem>().SuccessDrop = true;
            SoundControlUnit.Instance.PlaySceneClip(PlayClip.CardDrop);
        }
    }

    #endregion
}