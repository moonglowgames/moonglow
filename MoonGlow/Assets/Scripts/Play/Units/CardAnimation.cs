﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CardAnimation : MonoBehaviour
{
    [SerializeField]
    private Image _backCover;
    private Vector3 _pos;

    void Start()
    {
        _pos = CardPositionManager.Instance.GetPosition(GetComponent<CardUnit>().cardPosition);
    }

    private void ToggleCoverImage(bool state)
    {
        _backCover.enabled = state;
    }

    public void RevealCard()
    {
        Sequence _reveal = DOTween.Sequence();
        _reveal.Append(transform.DOLocalRotate(new Vector3(0f, 90f, 0f), 0.5f, 0));
        _reveal.AppendCallback(() => ToggleCoverImage(false));
        _reveal.Append(transform.DOLocalRotate(Vector3.zero, 0.5f, 0));
    }

    public void StartToHand()
    {
        Vector3 _deck = transform.parent.InverseTransformPoint(CardPositionManager.Instance.GetPosition(CardPosition.Deck));
        Sequence _deckToHand = DOTween.Sequence();
        _deckToHand.AppendCallback(() => ToggleCoverImage(true));
        _deckToHand.Append(transform.DOLocalMove(_deck, 0.01f, false));
        _deckToHand.Append(transform.DOLocalMove(_pos, 0.75f, false));
        _deckToHand.AppendCallback(() => ToggleCoverImage(false));
    }

    public void DeckToHand()
    {
        Sequence _deckToHand = DOTween.Sequence();
        _deckToHand.AppendCallback(() => ToggleCoverImage(true));
        _deckToHand.Append(transform.DOLocalMove(_pos, 0.75f, false));
        _deckToHand.AppendCallback(() => ToggleCoverImage(false));
    }

    public void PlayerToPlayer(int start, int end)
    {
        //transform.position = CardPositionManager.Instance.GetPlayerPosition(start);
        //Sequence sequence = DOTween.Sequence();
        //sequence.AppendCallback(() => EnableCanvas(true));
        //sequence.Append(transform.DOMove(CardPositionManager.Instance.GetPlayerPosition(end), 1f, false));
        //sequence.AppendCallback(() => EnableCanvas(false));
    }
}