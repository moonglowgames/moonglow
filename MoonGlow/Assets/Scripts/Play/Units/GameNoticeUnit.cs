﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public enum Notice { Start, YourTurn, Discard, PlayAgain, End }

public class GameNoticeUnit : MonoBehaviour
{
    Sequence _sequence;
    private CanvasGroup _canvas;
    private Text _notice;
    private ParticleSystem _effect;

    private Language _language;

    public bool MyTurn { get; set; }
    public string noticeText;

    void Awake()
    {
        GameControlUnit.Instance.GameNotice = this;

        MyTurn = false;
        _canvas = GetComponent<CanvasGroup>();
        _notice = transform.FindChild("Canvas/Notice").GetComponent<Text>();
        _effect = transform.FindChild("Effect").GetComponent<ParticleSystem>();
        //		Animate();
    }

    void OnLevelWasLoaded(int level)
    {
        if (level == 2)
        {
            _language = (Language)EncryptedPlayerPrefs.GetInt(PlayerPrefsKey.language, 0);
        }
    }

    void Start()
    {
        _sequence = DOTween.Sequence();
        _sequence.AppendCallback(() => ToggleAlpha(true));
        _sequence.Append(transform.DOScale(0f, 1f).From());
        _sequence.AppendInterval(2.0f);
        _sequence.AppendCallback(() => ToggleAlpha(false));
        _sequence.SetAutoKill(false).Pause();
    }

    void ToggleAlpha(bool state)
    {
        _canvas.alpha = state ? 1f : 0f;
    }

    void Animate()
    {
        _sequence.Restart();
    }

    public void Show(Notice notice)
    {
        switch (notice)
        {
            case Notice.Start:
                noticeText = "START";
                break;
            case Notice.YourTurn:
                if (MyTurn) { Show(Notice.PlayAgain); break; }
                noticeText = "YOUR TURN";
                break;
            case Notice.Discard:
                noticeText = "DISCARD";
                break;
            case Notice.PlayAgain:
                noticeText = "PLAY AGAIN";
                break;
            case Notice.End:
                noticeText = "END";
                break;
        }
        _notice.text = noticeText;
        _effect.Play();
        Animate();
    }

    public void ShowEffect(Notice notice)
    {
        string prefab = "Effects/Notice/";
        switch (notice)
        {
            case Notice.Start:
                prefab += "EffStart";
                break;
            case Notice.YourTurn:
                prefab += _language + "/EffYourTurn";
                break;
            case Notice.Discard:
                prefab += _language + "/EffDiscard";
                break;
            case Notice.PlayAgain:
                prefab += _language + "/EffPlayAgain";
                break;
            case Notice.End:
                prefab += "EffEnd";
                break;
        }
        var effectObject = (GameObject)Instantiate(Resources.Load(prefab));
        effectObject.GetComponent<EffectUnit>().Animate();
    }
}