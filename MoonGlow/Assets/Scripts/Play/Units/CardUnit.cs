﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class CardUnit : MonoBehaviour
{
    [SerializeField]
    protected CanvasGroup _canvas;
    [SerializeField]
    private Text _header, _description, _price;
    [SerializeField]
    protected Image _art, _body, _skin, _priceTag;
    [SerializeField]
    private ParticleSystem _trail;
    protected TargetOption _target;

    protected int _cardIdx, _skinIdx;
    private bool _pet = false;
    private bool _item = false;

    public CardPosition cardPosition;
    public int cardIdx;
    public int cardSkinIdx;
    public bool Joker = false;
    public Resource resource = Resource.Red;
    public int price;
    public TargetOption Target { get; set; }
    public AbilityToolTip ToolTipMe { get; set; }
    public AbilityToolTip ToolTipEnemy { get; set; }
    public int jokerFaceIdx;
    public int priceM;
    public int priceRE;

    public int itemIdx;
    public List<int> spellIdxList = new List<int>(3);

    protected virtual void Update()
    {
        if (_cardIdx != cardIdx)
        {
            _cardIdx = cardIdx;
            if (_pet) { Destroy(_body.transform.FindChild("Pet").gameObject); _pet = false; }
            if (_item) { Destroy(_art.transform.FindChild("Item").gameObject); _item = false; }
            UpdateView();
        }
    }

    protected virtual void UpdateView()
    {
        if (_cardIdx == 0 || _cardIdx == 999) { return; }

        CardInfo cardInfo = CardManager.Instance.Get(_cardIdx);
        if (cardInfo == null)
        {
            Debug.Log("CardInfo is null MiniCardUnit.cs");
            return;
        }

        if (Joker) { RemoveJoker(); }

        resource = (Resource)cardInfo.priceColor;

        _header.text = cardInfo.name;
        _description.text = cardInfo.GetDescription();
        price = cardInfo.GetPrice();
        _price.text = price.ToString();
        _target = cardInfo.target;
        Target = _target;
        ToolTipMe = cardInfo.ToolTipMe;
        ToolTipEnemy = cardInfo.ToolTipEnemy;

        if (PlaySpriteFactory.Instance != null)
        {
            _priceTag.sprite = PlaySpriteFactory.Instance.Get(SpriteType.PriceTag, (int)resource);
            _body.sprite = PlaySpriteFactory.Instance.Get(SpriteType.CardBody, (int)resource);
            _art.sprite = PlaySpriteFactory.Instance.Get(SpriteType.CardArt, cardInfo.art);
        }
        else { Debug.Log("PlaySpriteFactory is null MiniCardUnit.cs"); }

        if (cardInfo.pet > 0) { PetIcon(cardInfo.pet); }
        if (itemIdx > 0) { ItemIcon(); }
    }

    private void RemoveJoker()
    {
        Joker = false;
        foreach (Transform tag in _art.transform) { if (tag.name != "PriceTag") { Destroy(tag.gameObject); } }
    }

    public void SetJoker()
    {
        if (Joker) { return; }
        Joker = true;
        string description = string.Empty;
        _target = TargetOption.ME;
        ToolTipMe = AbilityToolTip.NONE;
        ToolTipEnemy = AbilityToolTip.NONE;
        price = 0; priceM = 0; priceRE = 0;
        //spellIdxList = new List<int>(); spellIdxList.Add(301); spellIdxList.Add(1001);
        for (int i = 0; i < spellIdxList.Count; i++)
        {
            if (i > 2) { Debug.LogError("Only 3 spells Allowed! JokerProfileUnit.cs" + i.ToString()); }
            SpellInfo spellInfo = SpellManager.Instance.Get(spellIdxList[i]);
            if (spellInfo == null)
            {
                Debug.Log("ItemInfo is null CardUnit.cs");
                return;
            }
            switch (spellInfo.spellType)
            {
                case SpellType.MoonDust:
                    price += spellInfo.ability.priceMD;
                    break;
                case SpellType.Mana:
                    priceM += spellInfo.ability.priceM;
                    break;
                case SpellType.RodEnergy:
                    priceRE += spellInfo.ability.priceRE;
                    break;
            }
            if (i > 0) { description += Environment.NewLine; }
            description += spellInfo.Description;
            if (spellInfo.target == TargetOption.ENEMY) { _target = TargetOption.ENEMY; }
            ToolTipMe |= spellInfo.ToolTipMe;
            ToolTipEnemy |= spellInfo.ToolTipEnemy;
        }

        resource = Resource.Yellow;
        _header.text = LocalizationManager.Instance.Get("JOKER");//"Joker";
        _body.sprite = PlaySpriteFactory.Instance.Get(SpriteType.CardBody, (int)resource);
        _art.sprite = PlaySpriteFactory.Instance.Get(SpriteType.JokerFace, jokerFaceIdx % 100 - 1);
        _priceTag.sprite = PlaySpriteFactory.Instance.Get(SpriteType.PriceTag, 0);
        _price.text = price == 0 ? "0" : price.ToString();
        _description.text = description;
        Target = _target;

        var priceTagM = Instantiate(_priceTag.gameObject);
        priceTagM.transform.SetParent(_art.transform, false);
        priceTagM.GetComponent<RectTransform>().anchoredPosition = new Vector3(34f, -45f, 0f);
        priceTagM.GetComponent<Image>().sprite = PlaySpriteFactory.Instance.Get(SpriteType.PriceTag, 1);
        priceTagM.GetComponentInChildren<Text>().text = priceM == 0 ? "0" : priceM.ToString();

        var priceTagRE = Instantiate(_priceTag.gameObject);
        priceTagRE.transform.SetParent(_art.transform, false);
        priceTagRE.GetComponent<RectTransform>().anchoredPosition = new Vector3(34f, -90f, 0f);
        priceTagRE.GetComponent<Image>().sprite = PlaySpriteFactory.Instance.Get(SpriteType.PriceTag, 2);
        priceTagRE.GetComponentInChildren<Text>().text = priceRE == 0 ? "0" : priceRE.ToString();
    }

    public IEnumerator UpdateSkin()
    {
        if (_skinIdx != cardSkinIdx)
        {
            _skinIdx = cardSkinIdx;
            if (_skinIdx == 900) { _skin.enabled = false; StopCoroutine("UpdateSkin"); }
            //wait for Update() to finish
            yield return new WaitForEndOfFrame();
            _skin.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CardSkin, _skinIdx % 100 - 1);
            if (_trail != null)
            {
                Destroy(_trail.transform.FindChild("Trail").gameObject);
                var effectObject = (GameObject)Instantiate(Resources.Load("Effects/CardSkin/" + _skinIdx.ToString()));
                effectObject.name = "Trail";
                effectObject.transform.SetParent(_trail.transform, false);
            }
        }
        if (_skinIdx == 900) { StopCoroutine("UpdateSkin"); }
        yield return new WaitForEndOfFrame();
        _skin.color = CardSkinManager.Instance.Get(_skinIdx, resource);
        _skin.enabled = true;
    }

    public void ToggleTrailParticles(bool state)
    {
        if (state) { _trail.Play(true); }
        else { _trail.Stop(true); }
    }

    public void PetIcon(int petIdx)
    {
        _pet = true;
        var petFrame = new GameObject("Pet");
        var petFrameTransform = petFrame.AddComponent<RectTransform>();
        petFrameTransform.SetParent(_body.transform, false);
        petFrameTransform.sizeDelta = new Vector2(194f, 56f);
        petFrameTransform.pivot = new Vector2(0.5f, 0f);
        petFrameTransform.anchorMin = new Vector2(0.5f, 1f);
        petFrameTransform.anchorMax = new Vector2(0.5f, 1f);
        petFrameTransform.anchoredPosition = new Vector2(0f, -14f);
        petFrame.AddComponent<Image>().sprite = PlaySpriteFactory.Instance.Get(SpriteType.PetMini, petIdx % 100 - 1);
    }

    public void ItemIcon()
    {
        _item = true;
        var item = new GameObject("Item");
        var itemTransform = item.AddComponent<RectTransform>();
        itemTransform.SetParent(_art.transform, false);
        itemTransform.sizeDelta = new Vector2(80f, 80f);
        itemTransform.pivot = new Vector2(0.5f, 0.5f);
        itemTransform.anchorMin = new Vector2(1f, 0f);
        itemTransform.anchorMax = new Vector2(1f, 0f);
        itemTransform.anchoredPosition = new Vector2(-64f, 30f);
        item.AddComponent<Image>().preserveAspect = true;
        switch (itemIdx)
        {
            case 2:
                item.GetComponent<Image>().sprite = PlaySpriteFactory.Instance.Get(SpriteType.ShopGold, 0);
                break;
            case 3:
                item.GetComponent<Image>().sprite = PlaySpriteFactory.Instance.Get(SpriteType.ShopRuby, 0);
                break;
            default:
                break;
        }
    }

    public IEnumerator CheckPrice()
    {
        bool notEnoughResource;
        yield return new WaitForEndOfFrame();
        if (Joker)
        {
            notEnoughResource = price > GameControlUnit.Instance.PlayerList.UserPlayer.moonDust || priceM > GameControlUnit.Instance.PlayerList.UserPlayer.mana || priceRE > GameControlUnit.Instance.PlayerList.UserPlayer.rodEnergy;
        }
        else
        {
            int currentResource = 0;
            switch (resource)
            {
                case Resource.Red:
                    currentResource = GameControlUnit.Instance.PlayerList.UserPlayer.moonDust;
                    break;
                case Resource.Blue:
                    currentResource = GameControlUnit.Instance.PlayerList.UserPlayer.mana;
                    break;
                case Resource.Green:
                    currentResource = GameControlUnit.Instance.PlayerList.UserPlayer.rodEnergy;
                    break;
            }
            notEnoughResource = price > currentResource;
        }
        if (notEnoughResource) { DiscardState(); } else { NormalState(); }
    }

    public void DiscardState()
    {
        //_backCover.color = new Color(1f, 1f, 1f, 0.6f);
        //_backCover.transform.SetAsLastSibling();
        Target = TargetOption.DECK;
        //GetComponent<MiniCardAnimation>().TogglePriceMask(true);
        _body.material = ItemSpriteFactory.Instance.GrayScale;
        _skin.material = ItemSpriteFactory.Instance.GrayScale;
        _art.material = ItemSpriteFactory.Instance.GrayScale;
    }

    public void NormalState()
    {
        _body.material = null;
        _skin.material = null;
        _art.material = null;
        Target = _target;
    }

    public void HideIntoDeck()
    {
        ToggleView(false);
        transform.position = CardPositionManager.Instance.GetPosition(CardPosition.Deck);
        Invoke("UpdatePosition", 3f);
    }

    public void UpdatePosition()
    {
        transform.localPosition = CardPositionManager.Instance.GetPosition(cardPosition);
        ToggleView(true);
    }

    public void ToggleAlpha(bool state)
    {
        _canvas.alpha = state ? 1f : 0f;
    }

    public void ToggleRaycasts(bool state)
    {
        _canvas.blocksRaycasts = state;
    }

    public void ToggleView(bool state)
    {
        _canvas.alpha = state ? 1f : 0f;
        _canvas.blocksRaycasts = state;
    }
}