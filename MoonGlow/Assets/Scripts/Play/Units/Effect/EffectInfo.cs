﻿using System;

public enum EffectPosition { TargetCenter, TargetBottom, TargetTop, MeToTarget, TargetTome, SkyTotarget, PlayCenter, ActiveCard }

public class EffectInfo
{
    //;idx , type , position ,delaytime(msec),Vibration, prefab , decription ,tag
    public int iDx;
    public int type;
    public EffectPosition position;
    public int delay;
    public int vibration;
    public string prefab;
    public string description;
    public string tag;

    const int DECODE_PARSE_COUNT = 8;
    public char[] charsToTrim = { ' ', '\t' };

    public bool Decode(string cvsLineData_)
    {
        bool retvalue = false;

        if (cvsLineData_ != string.Empty)
        {
            cvsLineData_.Trim(charsToTrim);

            switch (cvsLineData_[0])
            {
                case ';':
                    break;
                default:
                    string[] parsings = cvsLineData_.Split(',');
                    int parscount = 0;
                    if (parsings.GetLength(0) >= DECODE_PARSE_COUNT)
                    {
                        try
                        {
                            this.iDx = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.type = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.position = (EffectPosition)Enum.Parse(typeof(EffectPosition), parsings[parscount++].Trim(charsToTrim));
                            this.delay = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.vibration = Convert.ToInt32(parsings[parscount++].Trim(charsToTrim));
                            this.prefab = parsings[parscount++].Trim(charsToTrim);
                            this.description = parsings[parscount++].Trim(charsToTrim);
                            this.tag = parsings[parscount++].Trim(charsToTrim);

                            retvalue = true;
                        }
                        catch (Exception /* e */ )
                        {
                            retvalue = false;
                        }
                    }
                    break;
            }
        }
        return retvalue;
    }
}