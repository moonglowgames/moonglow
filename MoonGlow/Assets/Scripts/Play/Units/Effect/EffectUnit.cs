﻿using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class EffectUnit : MonoBehaviour
{
    private ParticleSystem _particle;
    private Animator _anim;

    public AudioClip audioClip = null;
    public AudioClip audioClip2 = null;

    void Awake()
    {
        _particle = GetComponent<ParticleSystem>();
        _anim = GetComponent<Animator>();
    }

    public void Animate()
    {
        //transform.position = Vector3.zero;
        _particle.Play();
        if (_anim) { _anim.Play("Action"); }
        if (audioClip != null) { SoundControlUnit.Instance.EffectClip(audioClip); }
        if (audioClip2 != null) { SoundControlUnit.Instance.EffectClip(audioClip2); }

        Destroy(this.gameObject, 5f);
    }

    public void Animate(Vector3 initPosition)
    {
        transform.position = initPosition;
        _particle.Play();
        if (_anim) { _anim.Play("Action"); }
        if (audioClip != null) { SoundControlUnit.Instance.EffectClip(audioClip); }
        if (audioClip2 != null) { SoundControlUnit.Instance.EffectClip(audioClip2); }

        Destroy(this.gameObject, 5f);
    }

    public void Animate(EffectInfo effectInfo, int playerNumber)
    {
        Debug.Log(this.name + " Animating");
        switch (effectInfo.position)
        {
            case EffectPosition.TargetCenter:
                transform.position = CardPositionManager.Instance.GetPlayerPosition(playerNumber);
                break;
            case EffectPosition.TargetBottom:
                transform.position = CardPositionManager.Instance.GetPlayerPosition(playerNumber) + new Vector3(0f,-0.525f,0f);
                break;
            case EffectPosition.TargetTop:
                transform.position = CardPositionManager.Instance.GetPlayerPosition(playerNumber) + new Vector3(0f, 0.525f, 0f);
                break;
            case EffectPosition.PlayCenter:
                transform.position = Vector3.zero;
                break;
            default:
                transform.position = CardPositionManager.Instance.GetPlayerPosition(playerNumber);
                break;
        }
        
        _particle.Play();
        if (_anim) { _anim.Play("Action"); }
#if UNITY_IOS || UNITY_ANDROID
        if (EncryptedPlayerPrefs.GetInt("Vibration", 1) == 1)
        {
            if (effectInfo.vibration > 0) { Handheld.Vibrate(); }
        }
#endif
        if (audioClip != null) { SoundControlUnit.Instance.EffectClip(audioClip); }
        if (audioClip2 != null) { SoundControlUnit.Instance.EffectClip(audioClip2); }

        Destroy(this.gameObject, 8f);
    }
}