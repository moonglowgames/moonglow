﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class EffectManager : Singleton<EffectManager>
{
    private Dictionary<int, EffectInfo> _effects = new Dictionary<int, EffectInfo>();
    private const string _FILE = @"Data/EffectData";

    void Awake()
    {
        LoadFromFile();
    }

    public void LoadFromFile()
    {
        TextAsset file = Resources.Load(_FILE) as TextAsset;

        string content = file.text;
        string[] lines = content.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

        EffectInfo tempEffectInfo = null;

        char[] charsToTrim = { ' ', '\t' };
        foreach (string ln in lines)
        {
            ln.Trim(charsToTrim);

            if (ln.Length == 0) continue;

            if (ln[0] == ';')
            {
                // ignore comment line
            }
            else
            {
                tempEffectInfo = new EffectInfo();
                if (tempEffectInfo.Decode(ln)) { _effects[tempEffectInfo.iDx] = tempEffectInfo; }
            }
        }
    }

    public EffectInfo Get(int iDx)
    {
        if (_effects.ContainsKey(iDx))
        {
            return _effects[iDx];
        }
        else
        {
            Debug.LogError("Didn't find Effect Info: " + iDx.ToString());
            return null;
        }
    }

    //public void InstantiateEffect(int iDx, int playerNumber)
    //{
    //    EffectInfo effectInfo = Get(iDx);
    //    var effectObject = (GameObject)Instantiate(Resources.Load("Effects/" + effectInfo.prefab));
    //    effectObject.GetComponent<EffectUnit>().Animate(effectInfo, RoomManagerUnit.Instance.Map.GetNumber(playerNumber - 1));
    //}
    private Dictionary<int, UnityEngine.Object> _objPool = new Dictionary<int, UnityEngine.Object>();

    UnityEngine.Object GetObject(int idx, EffectInfo effectInfo)
    {
        if (!_objPool.ContainsKey(idx))
        {
            _objPool[idx] = Resources.Load("Effects/" + effectInfo.prefab);
        }

        return _objPool[idx];

    }
    public void InstantiateEffect(int iDx, int playerNumber)
    {
        EffectInfo effectInfo = Get(iDx);
        // var effectObject = (GameObject)Instantiate(Resources.Load("Effects/" + effectInfo.prefab));
        var effectObject = (GameObject)Instantiate(GetObject(iDx, effectInfo));
        effectObject.GetComponent<EffectUnit>().Animate(effectInfo, RoomManagerUnit.Instance.Map.GetNumber(playerNumber - 1));
    }
}