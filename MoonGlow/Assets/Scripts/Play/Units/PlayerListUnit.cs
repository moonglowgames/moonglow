﻿#pragma warning disable 649

using UnityEngine;

public enum PlayerPosition{LeftTop, RightTop, RightBottom, LeftBottom, Deck};

public class PlayerListUnit : MonoBehaviour
{
    [SerializeField]
    private PlayerUnit[] _playerList;

    void Awake()
    {
        GameControlUnit.Instance.PlayerList = this;
    }

    public PlayerUnit GetPlayer(int playerNumber)
    {
        return _playerList[RoomManagerUnit.Instance.Map.GetNumber(playerNumber - 1)];
    }

    public PlayerUnit UserPlayer
    {
        get { return _playerList[(int)PlayerPosition.LeftTop]; }
    }

    public void SetPlayerTurn(int playerNumber)
    {
        int turnNumber = RoomManagerUnit.Instance.Map.GetNumber(playerNumber - 1);
        for (int i = 0; i < _playerList.Length; i++)
        {
            _playerList[i].turn = i == turnNumber ? true : false;
            _playerList[i].needUpdate = true;
        }
        SoundControlUnit.Instance.PlaySceneClip(PlayClip.FocusChange);
    }

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                transform.position += new Vector3(0f, 0.3f, 0f);
                break;
            case RatioType.aPhone:
                transform.position += new Vector3(0f, 0.5f, 0f);
                break;
            case RatioType.aPad:
                transform.position += new Vector3(0f, 1f, 0f);
                break;
        }
    }
}