﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerUnit : MonoBehaviour
{
    [SerializeField]
    private Image _avatar, _badge, _SpImage;
    [SerializeField]
    private Image[] _pet;
    [SerializeField]
    private Toggle _hpPlus, _spPlus;
    [SerializeField]
    private Text _name, _lvl, _hitPoints, _shieldPoints, _moonStonePower, _moonDust, _zenStonePower, _mana, _moonCrystalPower, _rodEnergy;
    [SerializeField]
    private RectTransform _life, _resources, _HpProgress, _moonDustProgress, _manaProgress, _rodEnergyProgress;
    [SerializeField]
    private ParticleSystem _turnFrame;

    private float _resourceMaxSize;
    private float _HpMaxSize;
    private int _avatarIdx;
    private int _badgeIdx;
    private int _hp, _sp, _md, _mn, _rd;

    public bool needUpdate = true;
    public bool itemSetup = false;
    public bool leftAlignment = true;

    public int avatarIdx { get; set; }
    public string playerName { get; set; }
    public int level { get; set; }
    public int hitPoints { get; set; }
    public int maxHitPoints { get; set; }
    public int maxResource { get; set; }
    public int shieldPoints { get; set; }
    public int moonDust { get; set; }
    public int moonStonePower { get; set; }
    public int zenStonePower { get; set; }
    public int mana { get; set; }
    public int moonCrystalPower { get; set; }
    public int rodEnergy { get; set; }
    public bool hpPlus { get; set; }
    public bool spPlus { get; set; }
    public List<int> petList { get; set; }
    public int badgeIdx { get; set; }
    public bool turn { get; set; }
    public bool isUser { get; set; }
    public bool isMe { get; set; }

    void Start()
    {
        _resourceMaxSize = _resources.FindChild("Red/MoonDust").GetComponent<RectTransform>().sizeDelta.x + 5f; //5px for the Shadow offset
        _HpMaxSize = _life.FindChild("HitPointPanel").GetComponent<RectTransform>().sizeDelta.y;

        maxHitPoints = GameControlUnit.Instance.RoomInfo.goalHP;
        maxResource = GameControlUnit.Instance.RoomInfo.goalRP;

        ChangeAlignment();
    }

    void Update()
    {
        if (needUpdate == true)
        {
            needUpdate = false;
            UpdateView();
        }

        if (itemSetup == true)
        {
            itemSetup = false;
            ItemView();
        }
    }

    void UpdateView()
    {
        if (_avatarIdx != avatarIdx)
        {
            _avatarIdx = avatarIdx;
            _avatar.sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatarMini, avatarIdx % 100);
        }

        _lvl.text = level == 0 ? string.Empty : "Lv." + level.ToString();
        _name.text = playerName;
        _moonStonePower.text = "+" + moonStonePower.ToString();
        if (_md != moonDust)
        {
            _md = moonDust;
            _moonDust.text = _md.ToString();
            _moonDustProgress.sizeDelta = new Vector2(_md >= maxResource ? _resourceMaxSize : _md * _resourceMaxSize / maxResource, 0f);
        }

        _zenStonePower.text = "+" + zenStonePower.ToString();
        if (_mn != mana)
        {
            _mn = mana;
            _mana.text = _mn.ToString();
            _manaProgress.sizeDelta = new Vector2(_mn >= maxResource ? _resourceMaxSize : _mn * _resourceMaxSize / maxResource, 0f);
        }

        _moonCrystalPower.text = "+" + moonCrystalPower.ToString();
        if (_rd != rodEnergy)
        {
            _rd = rodEnergy;
            _rodEnergy.text = _rd.ToString();
            _rodEnergyProgress.sizeDelta = new Vector2(_rd >= maxResource ? _resourceMaxSize : _rd * _resourceMaxSize / maxResource, 0f);
        }

        if (_hp != hitPoints)
        {
            _hp = hitPoints;
            _hitPoints.text = _hp.ToString();
            _HpProgress.sizeDelta = new Vector2(0f, _hp >= maxHitPoints ? _HpMaxSize : _hp * _HpMaxSize / maxHitPoints);
        }

        if (_sp != shieldPoints)
        {
            _sp = shieldPoints;
            _shieldPoints.text = _sp.ToString();

            if (_sp < 11) { _SpImage.color = new Color(1f, 0.85f, 0.5f); }
            else if (_sp < 21) { _SpImage.color = new Color(0.95f, 0.8f, 0.25f); }
            else { _SpImage.color = new Color(1f, 0.72f, 0f); }
        }

        if (turn) { _turnFrame.Play(true); }
        else { _turnFrame.Stop(true); }
        //if (_turnFrame.isStopped) { _turnFrame.Play(); }
        //if (_turnFrame.isPlaying)
        //{
        //    _turnFrame.GetComponent<Renderer>().enabled = turn;
        //}
    }

    void ItemView()
    {
        for (int i = 0; i < _pet.Length; i++)
        {
            if (i < petList.Count)
            {
                _pet[i].sprite = ItemSpriteFactory.Instance.Get(SpriteType.PetMini, petList[i] % 100 - 1);
                _pet[i].transform.parent.gameObject.SetActive(true);
            }
            else
            {
                _pet[i].transform.parent.gameObject.SetActive(false);
            }
        }

        if(_badgeIdx != badgeIdx)
        {
            _badgeIdx = badgeIdx;
            if (_badgeIdx == 0) { _badge.enabled = false; }
            else
            {
                _badge.sprite = ItemSpriteFactory.Instance.Get(SpriteType.Badge, _badgeIdx);
                _badge.enabled = true;
            }
        }

        _hpPlus.isOn = hpPlus;
        _spPlus.isOn = spPlus;
    }

    void ChangeAlignment()
    {
        if (leftAlignment == true)
        {
            _life.SetAsLastSibling();
            _SpImage.transform.SetAsLastSibling();
            _turnFrame.transform.localPosition = new Vector3(-0.65f, 0.75f, 0f);
            _resources.transform.localScale = Vector3.one;
            //red
            _moonStonePower.transform.localScale = Vector3.one;
            _moonDust.transform.localScale = Vector3.one;
            //blue
            _zenStonePower.transform.localScale = Vector3.one;
            _mana.transform.localScale = Vector3.one;
            //green
            _moonCrystalPower.transform.localScale = Vector3.one;
            _rodEnergy.transform.localScale = Vector3.one;
            //Pets
            for (int i = 0; i < _pet.Length; i++) { _pet[i].transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(185f, i * 63f); }
        }
        else
        {
            Vector3 minusOne = new Vector3(-1f, 1f, 1f);
            _life.SetAsFirstSibling();
            _SpImage.transform.SetAsFirstSibling();
            _turnFrame.transform.localPosition = new Vector3(0.65f, 0.75f, 0f);
            _resources.transform.localScale = minusOne;
            //red
            _moonStonePower.transform.localScale = minusOne;
            _moonDust.transform.localScale = minusOne;
            //blue
            _zenStonePower.transform.localScale = minusOne;
            _mana.transform.localScale = minusOne;
            //green
            _moonCrystalPower.transform.localScale = minusOne;
            _rodEnergy.transform.localScale = minusOne;
            //Pets
            for (int i = 0; i < _pet.Length; i++) { _pet[i].transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(-185f, i * 63f); }
            //Card ability Tooltips
            transform.FindChild("ToolTips").transform.localScale = minusOne;
        }
    }

    public void Profile()
    {
        //if (playerPosition != PlayerPosition.LeftBottom) { PacketManager.Instance.SendCSReqPlayerProfile(playerName); }
        PacketManager.Instance.SendCSReqPlayerProfile(playerName);
    }
}