﻿#pragma warning disable 649

using UnityEngine;
using System.Collections.Generic;

public class HistoryUnit : MonoBehaviour
{
    [SerializeField]
    private List<CardUnit> _history;
    private byte[] _stackIdx = new byte[5];

    private static float _shift = 0.15f;
    private static Vector3[] _tokenShift = { new Vector3(0f, _shift), new Vector3(_shift, _shift), new Vector3(-_shift, _shift), new Vector3(-_shift, -_shift), new Vector3(_shift, -_shift) };

    public CardUnit historyToken;

    public bool ItemCard { get; set; }

    void Awake()
    {
        GameControlUnit.Instance.History = this;
        ItemCard = false;
    }

    public CardUnit GetCard(int iDx)
    {
        return _history[iDx];
    }

    public void Clear()
    {
        //remove extra tokens from Play Again.
        for (int i = _history.Count - 1; i > 4; i--)
        {
            Destroy(_history[i].gameObject);
            _history.RemoveAt(i);
        }
        foreach (var card in _history) { card.ToggleAlpha(false); }
        for (int i = 0; i < _stackIdx.Length; i++) { _stackIdx[i] = 0; }
    }

    public void ShowHistoryToken(int pos, int cardIndex, int itemIdx, int cardSkinIdx, int jokerFaceIdx, List<int> spellIdxList)
    {
        // 0 = Deck, otherwise get correct target map position.
        if (pos != 0) { pos = RoomManagerUnit.Instance.Map.GetNumber(pos - 1) + 1; }
        if (_stackIdx[pos] == 0)
        {
            _history[pos].cardIdx = cardIndex;
            if (cardIndex == 999)
            {
                _history[pos].jokerFaceIdx = jokerFaceIdx;
                _history[pos].spellIdxList = spellIdxList;
                _history[pos].SetJoker();
            }
            _history[pos].cardSkinIdx = cardSkinIdx;
            _history[pos].StartCoroutine("UpdateSkin");

            //_history[pos].itemIdx = itemIdx;
            if (ItemCard)
            {
                ItemCard = false;
                switch (itemIdx)
                {
                    case 2:
                        var goldObject = (GameObject)Instantiate(Resources.Load("Effects/Acquisition/EffCoinAcquisition"));
                        goldObject.GetComponent<EffectUnit>().Animate(_history[pos].transform.position + new Vector3(0.45f, 0f, 0f));
                        break;
                    case 3:
                        var rubyObject = (GameObject)Instantiate(Resources.Load("Effects/Acquisition/EffRubyAcquisition"));
                        rubyObject.GetComponent<EffectUnit>().Animate(_history[pos].transform.position + new Vector3(0.45f, 0f, 0f));
                        break;
                }
            }
            _history[pos].ToggleAlpha(true);
        }
        else
        {
            CardUnit token = Instantiate(historyToken);
            token.cardIdx = cardIndex;
            if (cardIndex == 999)
            {
                token.jokerFaceIdx = jokerFaceIdx;
                token.spellIdxList = spellIdxList;
                token.SetJoker();
            }
            token.cardSkinIdx = cardSkinIdx;
            token.StartCoroutine("UpdateSkin");

            //token.itemIdx = itemIdx;
            if (ItemCard)
            {
                ItemCard = false;
                switch (itemIdx)
                {
                    case 2:
                        var goldObject = (GameObject)Instantiate(Resources.Load("Effects/Acquisition/EffCoinAcquisition"));
                        goldObject.GetComponent<EffectUnit>().Animate(token.transform.position + new Vector3(0.45f, 0f, 0f));
                        break;
                    case 3:
                        var rubyObject = (GameObject)Instantiate(Resources.Load("Effects/Acquisition/EffRubyAcquisition"));
                        rubyObject.GetComponent<EffectUnit>().Animate(token.transform.position + new Vector3(0.45f, 0f, 0f));
                        break;
                }
            }
            token.GetComponentInChildren<Canvas>().sortingOrder++;
            token.transform.SetParent(this.transform);
            token.transform.localPosition = _history[pos].transform.localPosition + _tokenShift[pos] * _stackIdx[pos];
            _history.Add(token);
        }
        _stackIdx[pos]++;
    }

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                transform.position += new Vector3(0f, 0.3f, 0f);
                break;
            case RatioType.aPhone:
                transform.position += new Vector3(0f, 0.5f, 0f);
                _history[0].transform.position += new Vector3(0f, 0.4f, 0f);
                break;
            case RatioType.aPad:
                transform.position += new Vector3(0f, 1f, 0f);
                break;
        }
    }
}