﻿#pragma warning disable 649

using UnityEngine;
using UnityEngine.UI;
using System;

public class ResultUnit : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup _canvas;
    [SerializeField]
    private Image _decorationL, _decorationR;
    [SerializeField]
    private LocalizationUnit _header;
    [SerializeField]
    private Text _winReason, _currentLvl;
    [SerializeField]
    private Slider _sliderDown, _sliderUp;
    [SerializeField]
    private Transform[] _playerList;
    [SerializeField]
    private Transform[] _rewardList;
    [SerializeField]
    private Button _rubyAD, _goldAD;

    private Color _decorColor; //255,190,1;L0.5;Draw 157,126,98
    private int _friendIdx;

    public GameResult gameResult { get; set; }

    void Awake()
    {
        GameControlUnit.Instance.Result = this;
    }

    void Start()
    {
        foreach (LocalizationUnit textUnit in GetComponentsInChildren<LocalizationUnit>())
        {
            textUnit.UpdateUI();
        }
    }

    void UpdateReward()
    {
        for (int i = 0; i < gameResult.playFriendList.Count; i++)
        {
            _playerList[i].transform.FindChild("Avatar/CharacterAvatar").GetComponent<Image>().sprite = ItemSpriteFactory.Instance.Get(SpriteType.CharacterAvatarMini, gameResult.playFriendList[i].friendFaceIdx % 100);
            _playerList[i].transform.FindChild("Level").GetComponent<Text>().text = "Lv. " + gameResult.playFriendList[i].friendLevel.ToString();
            _playerList[i].transform.FindChild("Name").GetComponent<Text>().text = gameResult.playFriendList[i].friendNick;
            _playerList[i].GetComponentInChildren<LocalizationUnit>().UpdateUI("REQ_FRIEND");
            _playerList[i].gameObject.SetActive(true);
        }
        switch (gameResult.result)
        {
            case Result.Lose:
                _decorColor = new Color(0.5f, 0.5f, 0.5f);
                _header.UpdateUI("LOSE");
                SoundControlUnit.Instance.PlaySceneClip(PlayClip.ResultLose);
                break;
            case Result.Draw:
                _decorColor = new Color(1f, 0.455f, 0.07f);
                _header.UpdateUI("DRAW");
                SoundControlUnit.Instance.PlaySceneClip(PlayClip.ResultDraw);
                break;
            case Result.Win:
                _decorColor = new Color(1f, 0.745f, 0f);
                _header.UpdateUI("WIN");
                SoundControlUnit.Instance.PlaySceneClip(PlayClip.ResultWin);
                var fanfareObject1 = (GameObject)Instantiate(Resources.Load("Effects/EffFanfare"));
                fanfareObject1.GetComponent<EffectUnit>().Animate();
                var fanfareObject2 = (GameObject)Instantiate(Resources.Load("Effects/EffFanfare2"));
                fanfareObject2.GetComponent<EffectUnit>().Animate(new Vector3(-4f, 2f, 0f));
                var fanfareObject3 = (GameObject)Instantiate(Resources.Load("Effects/EffFanfare2"));
                fanfareObject3.GetComponent<EffectUnit>().Animate(new Vector3(4f, 2f, 0f));
                break;
        }

        _header.GetComponent<Text>().color = _decorColor;
        _decorationL.sprite = Resources.Load<Sprite>(string.Format("Images/Result/{0:00}Decoration", (int)gameResult.result));
        _decorationR.sprite = _decorationL.sprite;
        string winReason = LocalizationManager.Instance.Get(Enum.GetName(typeof(GameEndReason), gameResult.reason)).Replace('^', ',');
        winReason = winReason.Replace("@", Environment.NewLine);
        _winReason.text = winReason.Replace("*", gameResult.reasonValue.ToString());

        _currentLvl.text = gameResult.currentLevel.ToString();

        if (gameResult.rankPoint > 0)
        {
            _rewardList[0].FindChild("Amount").GetComponent<Text>().text = "+" + gameResult.rankPoint.ToString();
            _rewardList[0].gameObject.SetActive(true);
        }
        if (gameResult.coin > 0)
        {
            _rewardList[1].FindChild("Amount").GetComponent<Text>().text = "+" + gameResult.coin.ToString();
            _rewardList[1].gameObject.SetActive(true);
        }
        if (gameResult.ruby > 0)
        {
            _rewardList[2].FindChild("Amount").GetComponent<Text>().text = "+" + gameResult.ruby.ToString();
            _rewardList[2].gameObject.SetActive(true);
        }
        if (gameResult.itemInfo.itemIndex > 0)
        {
            _rewardList[3].FindChild("Icon").GetComponent<Image>().sprite = ShopSpriteFactory.Instance.Get(SpriteType.ShopItem, gameResult.itemInfo.itemIndex % 100 - 1); ;
            _rewardList[3].FindChild("Amount").GetComponent<Text>().text = "+" + gameResult.itemInfo.count.ToString();
            _rewardList[3].gameObject.SetActive(true);
        }
        if (gameResult.stake !=0 )
        {
            _rewardList[4].FindChild("Amount").GetComponent<Text>().text = gameResult.stake > 0 ? "+" + gameResult.stake.ToString() : gameResult.stake.ToString();
            _rewardList[4].gameObject.SetActive(true);
        }
        if (gameResult.levelUp)
        {
            _rewardList[5].gameObject.SetActive(true);
        }
        if (gameResult.levelDown)
        {
            _rewardList[6].gameObject.SetActive(true);
        }

        _rubyAD.gameObject.SetActive(gameResult.adOK);
        _goldAD.gameObject.SetActive(gameResult.adOK);

        if (gameResult.exp < 0)
        {
            _sliderDown.maxValue = Mathf.Abs(gameResult.downLevelExp);
            _sliderDown.value = Mathf.Abs(gameResult.exp);
            _sliderDown.transform.FindChild("FullTip").gameObject.SetActive(_sliderDown.normalizedValue == 1);
            _sliderDown.transform.FindChild("Handle Slide Area/Handle/Text").GetComponent<Text>().text = gameResult.exp.ToString();
            _sliderDown.transform.FindChild("Handle Slide Area/Handle").gameObject.SetActive(true);
        }
        else
        {
            _sliderUp.maxValue = gameResult.upLevelExp;
            _sliderUp.value = gameResult.exp;
            _sliderUp.transform.FindChild("FullTip").gameObject.SetActive(_sliderUp.normalizedValue == 1);
            _sliderUp.transform.FindChild("Handle Slide Area/Handle/Text").GetComponent<Text>().text = gameResult.exp.ToString();
            _sliderUp.transform.FindChild("Handle Slide Area/Handle").gameObject.SetActive(true);
        }
    }

    public void RubyADButton()
    {
        PacketManager.Instance.SendCSReqBuyElement(407, 1);
        _rubyAD.interactable = false;
        _goldAD.interactable = false;
    }

    public void GoldADButton()
    {
        PacketManager.Instance.SendCSReqBuyElement(304, 1);
        _rubyAD.interactable = false;
        _goldAD.interactable = false;
    }

    public void FriendRequestButton(int friendIdx)
    {
        GlobalMessageUnit.Instance.ShowMessage(this.gameObject, "ReqFriendRequest", gameResult.playFriendList[friendIdx].iDx, "REQ_FRIEND");
        _friendIdx = friendIdx;
    }

    void ReqFriendRequest(int friendIdx)
    {
        PacketManager.Instance.SendCSReqRequestFriend(FriendTabType.RecommendFriends, friendIdx);
        _playerList[_friendIdx].GetComponentInChildren<Button>().interactable = false;
    }

    public void Restart()
    {
        PacketManager.Instance.SendCSReqWantGame(GameAction.Restart);
    }

    public void EndGame()
    {
        PacketManager.Instance.SendCSReqWantGame(GameAction.EndGame);
    }

    public void ResultWindowToggle(bool state)
    {
        _canvas.alpha = state ? 1f : 0f;
        _canvas.blocksRaycasts = state;
        UpdateReward();
    }
}