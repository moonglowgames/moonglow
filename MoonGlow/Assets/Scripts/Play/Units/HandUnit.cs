﻿#pragma warning disable 649

using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;

public class HandUnit : MonoBehaviour
{
    [SerializeField]
    private CardUnit[] _hand;
    private Sequence _slide;
    //private Tween _hide;
    private bool _isUp, _discardState, _padDevice;

    public int LastCardIndex { get; set; }

    void Awake()
    {
        GameControlUnit.Instance.Hand = this;

        _discardState = false;
    }

    void Start()
    {
        //_hide = transform.DOMoveY(-2f, 0.15f).SetRelative(true).SetAutoKill(false).Pause();
        _slide = DOTween.Sequence();
        _slide.Append(transform.DOMoveY(2.5f, 0.75f));
        //if (GameControlUnit.Instance.HandManager != null) { _slide.Join(GameControlUnit.Instance.HandManager.transform.DOMoveY(2.4f, 0.75f)); }
        _slide.SetAutoKill(false).SetRelative(true).Pause();
    }

    public CardUnit GetCard(int pos)
    {
        return _hand[pos];
    }

    public void MakeJoker(int pos, int jokerFaceIdx, List<int> spellIdxList)
    {
        _hand[pos].cardIdx = 999;
        _hand[pos].jokerFaceIdx = jokerFaceIdx;
        _hand[pos].spellIdxList = spellIdxList;
        _hand[pos].SetJoker();
    }

    public void GetFromDeck(int pos)
    {
        if (RoomManagerUnit.Instance.GameStart)
        {
            _hand[LastCardIndex].CancelInvoke();
            _hand[pos].GetComponent<CardAnimation>().DeckToHand();
        }
        else
        {
            _hand[pos].GetComponent<CardAnimation>().StartToHand();
        }
        
        _hand[pos].ToggleView(true);
        _hand[pos].StartCoroutine("CheckPrice");
    }

    public void RevealCards()
    {
        foreach (var card in _hand)
        {
            card.ToggleView(true);
            card.GetComponent<CardAnimation>().RevealCard();
            card.GetComponent<DragItem>().enabled = true;
        }
    }

    public void ResetLastCard()
    {
        _hand[LastCardIndex].CancelInvoke();
        _hand[LastCardIndex].UpdatePosition();
    }

    public void ToggleDragOption(bool state)
    {
        foreach (var card in _hand)
        {
            card.GetComponent<DragItem>().enabled = state;
        }
    }

    public void ResetDragOption()
    {
        foreach (var card in _hand)
        {
            card.GetComponent<DragItem>().OnEndDrag(null);
            card.GetComponent<DragItem>().enabled = false;
        }
    }

    public void UpdatePrices()
    {
        if (_discardState) { return; }
        if (!RoomManagerUnit.Instance.GameStart) { return; }
        foreach (var card in _hand)
        {
            card.StartCoroutine("CheckPrice");
        }
    }

    public void SetDiscardState()
    {
        _discardState = true;
        foreach (var card in _hand)
        {
            card.DiscardState();
        }
    }

    public void SetNormalState()
    {
        _discardState = false;
        foreach (var card in _hand)
        {
            card.StartCoroutine("CheckPrice");
        }
    }

    public void Slide(bool up)
    {
        _isUp = up;
        //ToggleDragOption(false);
        //transform.DOMoveY(up ? -2f : -4.5f, 0.5f).OnComplete(() => ToggleDragOption(true));
        //GameControlUnit.Instance.HandManager.transform.position += new Vector3(0f, up ? 2.4f : -2.4f, 0f);
        ToggleDragOption(false);
        if (_isUp)
        {
            _slide.SetEase(Ease.OutBounce).OnComplete(() => ToggleDragOption(true)).PlayForward();
        }
        else
        {
            _slide.SetEase(Ease.InBounce).OnRewind(() => ToggleDragOption(true)).PlayBackwards();
        }
    }

    public void Hide(bool state)
    {
        //if (!_isUp) { return; }
        if (_padDevice) { return; }
        transform.position += new Vector3(0f, state ? -2f : 2f, 0f);
        //if (state)
        //{

        //    _hide.PlayForward();
        //}
        //else
        //{
        //    _hide.PlayBackwards();
        //}
    }

    public void UpdateUI(RatioType ratio)
    {
        switch (ratio)
        {
            case RatioType.Standart:
                break;
            case RatioType.Standart800:
                break;
            case RatioType.aPhone:
                transform.position += new Vector3(0f, -0.7f, 0f);
                break;
            case RatioType.aPad:
                _padDevice = true;
                transform.position += new Vector3(0f, -1.2f, 0f);
                break;
        }
    }
}