
Third Party Services
Our services may contain third party tracking tools from our service providers, examples of which include Google Analytics and MobileAppTracking by Tune. Such third parties may use cookies, APIs, and SDKs in our services to enable them to collect and analyze user information on our behalf. The third parties may have access to information such as your device identifier, MAC address, IMEI, locale (specific location where a given language is spoken), geo-location information, and IP address for the purpose of providing their services under their respective privacy policies. Our privacy policy does not cover the use of tracking tools from third parties.  We do not have access or control over these third parties.
Ad Networks
We may feature advertising within our Service.  The advertisers may collect and use information about you, such as your Service session activity, device identifier, MAC address, IMEI, geo-location information and IP address.  They may use this information to provide advertisements of interest to you.
In addition, you may see our games advertised in other services.  After clicking on one of these advertisements and installing our game, you will become a user of our Service.  In order to verify the installs, a device identifier may be shared with the advertiser.
How We Use Information
We use information collected through our Service for purposes described in this Policy or disclosed to you in connection with our Service. For example, we may use your information to:
•	create game accounts and allow users to play our games;
•	identify and suggest connections with other Moonglow users;
•	operate and improve our Service;
•	understand you and your preferences to enhance your experience and enjoyment using our Service;
•	respond to your comments and questions and provide customer service;
•	provide and deliver products and services you request;
•	send you related information, including confirmations, invoices, technical notices, updates, security alerts, and support and administrative messages;
•	communicate with you about promotions, rewards, upcoming events, and other news about products and services offered by Moonglow and our selected partners;
•	enable you to communicate with other users; and
•	link or combine it with other information we get from third parties, to help understand your preferences and provide you with better services.
Disclosure of Your information
Moonglow does not share your personal information except as approved by you or as described below:
•	With your consent, for example, when you agree to our sharing your information with other third parties for their own marketing purposes subject to their separate privacy policies.  If you no longer wish to allow us to share your information with third parties, please contact us at postmaster@moonglowgames.com.  If you no longer wish to receive such communications from third parties, please contact that third party directly.
•	Moonglow may engage other companies and individuals to perform services on our behalf.  Example of these services include analyzing data and providing customer support.  These agents and service providers may have access to your personal information in connection with the performance of services for Moonglow.
•	We may release your information as permitted by law, such as to comply with a subpoena, or when we believe that release is appropriate to comply with the law; investigate fraud, respond to a government request, enforce or apply our rights; or protect the rights, property, or safety of us or our users, or others.  This includes exchanging information with other companies and organizations for fraud protection.
•	Moonglow may share your information in connection with any merger, sale of our assets, or a financing or acquisition of all or a portion of our business to another company.  You will be notified via email and/or notice on our site of any change in ownership or users of your personal information.
•	We may share aggregate or anonymous information about you with advertisers, publishers, business partners, sponsors, and other third parties.
Changes to the Policy
We may update this privacy policy to reflect changes to our information practices. If we make any material changes we will notify you by email (sent to the e-mail address specified in your account) or by means of a notice on this Site prior to the change becoming effective. We encourage you to periodically review this page for the latest information on our privacy practices.
Security
Moonglow takes reasonable measures to protect your information from unauthorized access or against loss, misuse or alteration by third parties.
Although we make good faith efforts to store the information collected on the Service in a secure operating environment that is not available to the public, we cannot guarantee the absolute security of that information during its transmission or its storage on our systems.  Further, while we attempt to ensure the integrity and security of our network and systems, we cannot guarantee that our security measures will prevent third-party “hackers" from illegally obtaining access to this information.  We do not warrant or represent that your information will be protected against, loss, misuse, or alteration by third parties. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however.  Therefore, we cannot guarantee its absolute security.
Access to Personal Information
If your personal information changes, or if you no longer desire our service, you may correct, update, or delete inaccuracies by making the change within your account settings or by contacting us at postmaster@moonglowgaems.com.  We will respond to your access request within 30 days.
Data Retention
We will retain your information for as long as your account is active or as needed to provide you services.  If you wish to cancel your account or request that we no longer use your information to provide you services, contact us at postmaster@moonglowgames.com.  We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.
Our Policy Regarding Children
We do not knowingly collect or solicit personal information from anyone under the age of 13 or knowingly allow such persons to use our Service.  If you are under 13, please do not send any information about yourself to us, including your name, address, telephone number, or email address.  No one under the age of 13 may provide any personal information.  In the event that we learn that we have collected personal information from a child under age 13, we will delete that information as quickly as possible.  If you believe that we might have any information from or about a child under the age of 13, please contact us at postmaster@moonglowgames.com.
Opting Out of Marketing
You may opt-out of receiving promotional emails from us by following the instructions in those emails by emailing us at postmaster@moonglowgames.com.  If you opt-out, we may still send you non-promotional emails, such as emails about your accounts or our ongoing business relations.
You may also opt-out of receiving SMS notifications from us, either via SMS or by emailing us at postmaster@moonglowgames.com.
International Transfer
We may transfer information that we collect about you to affiliated entities, or to other third parties across borders and from your country or jurisdiction to other countries or jurisdictions around the world. Please note that these countries and jurisdictions may not have the same data protection laws as your own jurisdiction, and you consent to the transfer of information to the U.S. and the use and disclosure of information about you, including personal information, as described in this Policy.
Contact Information
Moonglow Co., Ltd.,
H213 Howard bld., PaiChai univ., 155-40, Baejae-ro, Seo-gu, Daejeon, Korea.  
If you have any questions about this Policy, please contact us at postmaster@moonglowgames.com.